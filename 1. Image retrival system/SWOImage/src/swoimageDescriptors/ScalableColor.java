/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */
public class ScalableColor implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = -7037579668241890201L;

    private int[] coef;
    
    private ScalableColor(){};
    
    public ScalableColor(int[] y){
        coef = y;
    }

    @Override
    public double compareTo(DescriptorInterface interf) {
        double result = Double.MAX_VALUE;
        if(interf instanceof ScalableColor){
            ScalableColor other = (ScalableColor)interf;
            if(other.coef.length!=coef.length)
                return result;
            double sum1 = 0;
            for(int i=0;i<coef.length;i++){
                sum1+=(coef[i]-other.coef[i])*(coef[i]-other.coef[i]);
            }
            result = Parameters.scalableColorWeight*Math.sqrt(sum1);
        }
        
        return result;
    }
    
}
