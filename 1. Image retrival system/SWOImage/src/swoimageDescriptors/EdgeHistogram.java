/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */
public class EdgeHistogram implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = -3468020095894058701L;
    private int[] coef;
    private double[] global;
    private double[] semiglobal;
    
    private EdgeHistogram(){}
    
    public EdgeHistogram(int[] newCoef){
        coef = newCoef;
        global = new double[5];
        semiglobal = new double[65];
        
        if(coef.length==80){
            //global edge histogram
            for(int i=0;i<80;i+=5){
                for(int j=0;j<5;j++){
                    global[j]+=coef[i+j];
                }
            }
            
            for(int j=0;j<5;j++){
                global[j]/=16.0; //bo 16 bloków z podzialu 4x4
            }
            
            
            //vertical
            for(int j=0;j<20;j++){
                for(int i=0;i<4;i++){
                    semiglobal[j]+=coef[i*20+j];
                }
            }
            for(int j=0;j<20;j++){
                semiglobal[j]/=4.0;
            }
            
            //horizontal
            for(int j=0, k=20 ;k<40;j++, k++){
                if(j==5||j==25||j==45)
                    j+=15;
                for(int i=0;i<4;i++){
                    semiglobal[k]=coef[j+i*5];
                }
                //semiglobal[j]+=coef[j*20+j];
            }
            for(int j=20;j<40;j++){
                semiglobal[j]/=4.0;
            }
            
            //neighbour
            int[] ptrs= new int[]{0,10,40,50,25};
            for(int i=0;i<ptrs.length;i++){
                for(int j=0;j<5;j++){
                    semiglobal[40+i*5+j]=coef[ptrs[i]+j]+coef[ptrs[i]+5+j]+coef[ptrs[i]+20+j]+coef[ptrs[i]+25+j];
                }
            }
            for(int j=40;j<65;j++){
                semiglobal[j]/=4.0;
            }
        }
    }

    @Override
    public double compareTo(DescriptorInterface interf) {
        double result = Double.MAX_VALUE;
        if(interf instanceof EdgeHistogram){
            EdgeHistogram other = (EdgeHistogram)interf;
            result = 0;
            int sum1 = 0, sum2 = 0, sum3 = 0;
            
            for(int i=0;i<coef.length;i++){
                sum1+=Math.abs(coef[i]-other.coef[i]);
            }
            for(int i=0;i<global.length;i++){
                sum2+=Math.abs(global[i]-other.global[i]);
            }
            for(int i=0;i<semiglobal.length;i++){
                sum3+=Math.abs(semiglobal[i]-other.semiglobal[i]);
            }
            result = sum1 + 5*sum2 + sum3;
            result *= Parameters.edgeHistogramWeight/Parameters.EHD_MAX_DIST;
        }
        
        return result;
    }
}
