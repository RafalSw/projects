/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */
public class ColorLayout implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = 7028237321099784088L;
    private int[] yCoef;
    private int[] cbCoef;
    private int[] crCoef;
    
    private ColorLayout(){};
    
    public ColorLayout(int[] y, int []cr, int[] cb){
        yCoef = y;
        cbCoef = cb;
        crCoef = cr;
    }
    
    @Override
    public double compareTo(DescriptorInterface interf) {
        double result = Double.MAX_VALUE;
        if(interf instanceof ColorLayout){
            ColorLayout other = (ColorLayout)interf;
            if(other.yCoef.length!=yCoef.length || other.cbCoef.length!=cbCoef.length || other.crCoef.length!=crCoef.length)
                return result;
            int sum1 = 0, sum2 = 0, sum3 = 0;
            for(int i=0;i<yCoef.length;i++){
                int value = (yCoef[i]-other.yCoef[i])*(yCoef[i]-other.yCoef[i]);
                if(i<3) //first coefficients has higher weight
                    value*=2;
                sum1+=value;
            }
            for(int i=0;i<cbCoef.length;i++){
                int value =(cbCoef[i]-other.cbCoef[i])*(cbCoef[i]-other.cbCoef[i]);
                if(i == 0)
                    value*=2;
                sum2+=value;
            }
            for(int i=0;i<crCoef.length;i++){
                int value = (crCoef[i]-other.crCoef[i])*(crCoef[i]-other.crCoef[i]);
                if(i==0)
                    value*=4;
                else if(i<3)
                    value*=2;
                
                sum3+=value;
            }
            result = Parameters.colorLayoutWeight*Math.sqrt(Parameters.colorLayoutYWeight*sum1)+Math.sqrt(Parameters.colorLayoutCBWeight*sum2)+Math.sqrt(Parameters.colorLayoutCRWeight*sum3);
            result/=Parameters.CLD_MAX_DIST;
        }
        
        return result;
    }
    
}
