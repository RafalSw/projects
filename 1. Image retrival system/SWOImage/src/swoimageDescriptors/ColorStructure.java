/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */
public class ColorStructure implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = 2191232245877006841L;
    private int[] coef;
    
    private ColorStructure(){};
    
    public ColorStructure(int[] y){
        coef = y;
    }

    @Override
    public double compareTo(DescriptorInterface interf) {
        double result = Double.MAX_VALUE;
        if(interf instanceof ColorStructure){
            ColorStructure other = (ColorStructure)interf;
            if(other.coef.length!=coef.length)
                return result;
            double sum1 = 0;
            for(int i=0;i<coef.length;i++){
                sum1+=Math.abs(coef[i]-other.coef[i]);
            }
            result = Parameters.colorStructureWeight*sum1/Parameters.CSD_MAX_DIST;
        }
        
        return result;
    }
}
