/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */
public class HomogeneusTexture implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = -8564608639182395291L;
    private final int mean;
    private final int stdDev; //standard deviation
    private final int[] energy; //base layer energy
    private final int[] energyDeviation;

    public HomogeneusTexture(int mean1, int std1, int[] baseLayerEnergy, int[] energyDev){
        mean = mean1;
        stdDev = std1;
        energy = baseLayerEnergy;
        energyDeviation = energyDev;
    }
    @Override
    public double compareTo(DescriptorInterface interf) {
       
        double result = Double.MAX_VALUE;
        if(interf instanceof HomogeneusTexture){
            HomogeneusTexture other = (HomogeneusTexture)interf;
            result = 0;
            double stdDevValue;
            if(other.stdDev!=0)
                stdDevValue = other.stdDev;
            else
                stdDevValue = 1;
            
            result += Math.abs((mean-other.mean)/stdDevValue);
            result += Math.abs((stdDev - other.stdDev)/stdDevValue);
            
            for(int i = 0; i<energy.length; i++){
                result += Math.abs((energy[i]-other.energy[i])/stdDevValue);
            }
            
            for(int i = 0; i<energyDeviation.length; i++){
                result += Math.abs((energyDeviation[i]-other.energyDeviation[i])/stdDevValue);
            }
            result *= Parameters.homogeneusTextureWeight;
        }
        
        return result;
    }
    
}
