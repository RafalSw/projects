/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;

/**
 *
 * @author Rafal
 */
public class SingleDominantColorHelper implements Serializable{
    private static final long serialVersionUID = -7451076166435386711L;
    
    public int[] colorValues;
    public int[] colorVariance;
    public double percentage;
    
    private SingleDominantColorHelper(){}
    public SingleDominantColorHelper(int p, int[] values, int[] variance){
        percentage = p/100.0;
        colorValues = values;
        colorVariance = variance;
    }
}
