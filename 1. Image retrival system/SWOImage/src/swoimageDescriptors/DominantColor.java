/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimageDescriptors;

import java.io.Serializable;
import swoimage.Parameters;

/**
 *
 * @author Rafal
 */

public class DominantColor implements DescriptorInterface, Serializable{
    private static final long serialVersionUID = -8064430306989807595L;
    int spatialCoherency = Integer.MIN_VALUE;
    private SingleDominantColorHelper[] colors;
    
    private DominantColor(){}
    
    public DominantColor(int size, int[] data1){
        colors = new SingleDominantColorHelper[size];
        for(int i=0, j=0; j<size; i+=4, j++){
            colors[j] = new SingleDominantColorHelper(data1[i], new int[]{data1[i+1], data1[i+2], data1[i+3]}, null);
        }
    }
    
    public DominantColor(int size, int[] data1, int[] data2){
        colors = new SingleDominantColorHelper[size];
        for(int i=0, j=0, k = 0; j<size; i+=4, j++, k+=3){
            colors[j] = new SingleDominantColorHelper(data1[i], new int[]{data1[i+1], data1[i+2], data1[i+3]}, new int[]{data2[k], data2[k+1], data2[k+2]});
        }
    }
    
    public DominantColor(int size, int[] data1, int spatial){
        colors = new SingleDominantColorHelper[size];
        spatialCoherency = spatial;
        for(int i=0, j=0; j<size; i+=4, j++){
            colors[j] = new SingleDominantColorHelper(data1[i], new int[]{data1[i+1], data1[i+2], data1[i+3]}, null);
        }
        
    }
    
    public DominantColor(int size, int[] data1, int[] data2, int spatial){
        colors = new SingleDominantColorHelper[size];
        spatialCoherency = spatial;
        for(int i=0, j=0, k = 0; j<size; i+=4, j++, k+=3){
            colors[j] = new SingleDominantColorHelper(data1[i], new int[]{data1[i+1], data1[i+2], data1[i+3]}, new int[]{data2[k], data2[k+1], data2[k+2]});
        }
    }
    
    public double getColorDistance(int[] color1, int[] color2){
        if(color1.length != color2.length)
            return Double.MAX_VALUE;
        
        double result = 0;
        for(int i=0; i<color1.length;i++){
            result+=(color2[i]-color1[i])*(color2[i]-color1[i]);
        }
        return Math.sqrt(result);
    }
    
    public double getSimilarityCoefficient(int[] color1, int[] color2){
        double colorDistance = getColorDistance(color1, color2);
        if(colorDistance>Parameters.dominantColorTargetDistance)
            return 0;
        else
            return 1-colorDistance/Parameters.dominantColorDistanceMax;
    }
    
    public double getNewSimilarityCoefficient(double percentageColor1, double percentageColor2){
        return (1-Math.abs(percentageColor1 - percentageColor2))*Math.min(percentageColor1, percentageColor2);
    }

    @Override
    public double compareTo(DescriptorInterface interf) {
        if(interf instanceof DominantColor){
            DominantColor other = (DominantColor)interf;
            double distance = 0;
            
            //MPEG-7 Standard
            
            for (SingleDominantColorHelper color : colors) {
                distance += color.percentage * color.percentage;
            }

            for (SingleDominantColorHelper color : other.colors) {
                distance += color.percentage * color.percentage;
            }

            for (SingleDominantColorHelper color : colors) {
                for (SingleDominantColorHelper color1 : other.colors) {
                    distance -= 2*getSimilarityCoefficient(color.colorValues, color1.colorValues) * color.percentage * color1.percentage;
                }
            }
            distance = Math.sqrt(distance);
            
            
            /*
            //New: A fast MPEG-7 dominant color extraction with new similarity measure for image retrieval by Nai-Chung Yang, Wei-Han Chang, Chung-Ming Kuo, Tsia-Hsing Li
            double similarity = 0;
            for (SingleDominantColorHelper color : colors) {
                for (SingleDominantColorHelper color1 : other.colors) {
                    double sim2 = similarity;
                    similarity += getSimilarityCoefficient(color.colorValues, color1.colorValues)*getNewSimilarityCoefficient(color.percentage,color1.percentage);
                    if(sim2!=similarity){
                        System.out.println(color.percentage+" "+color1.percentage);
                    }
                }
            }
            distance = 1 - similarity;
            distance = Math.sqrt(distance);*/
            
            
            if((spatialCoherency !=Integer.MIN_VALUE) && (other.spatialCoherency != Integer.MIN_VALUE)){
                distance = Parameters.dominantColorSpatialWeight*Math.abs(spatialCoherency - other.spatialCoherency)*distance + Parameters.dominantColorNonSpatialWeight*distance;
            }
            
            return Parameters.dominantColorWeight*distance;
        }
        return Double.MAX_VALUE;
    }
    
}
