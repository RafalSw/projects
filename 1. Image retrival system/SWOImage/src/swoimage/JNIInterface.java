/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimage;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */
public class JNIInterface {
    public static final int DIMENSIONS = 1000;
    public static final int COLOR_LAYOUT = 1001;
    public static final int COLOR_STRUCTURE = 1002;
    public static final int SCALABLE_COLOR = 1003;
    public static final int HOMOGENEUS_TEXTURE = 1004;
    public static final int EDGE_HISTOGRAM = 1005;
    public static final int DOMINANT_COLOR = 1006;
    public static final int DOMINANT_COLOR_WITH_SPATIAL = 1007;
    public static final int DOMINANT_COLOR_WITH_VARIANCE = 1008;
    public static final int DOMINANT_COLOR_WITH_SPATIAL_AND_VARIANCE = 1009;

    JNIInterface() throws Exception{
        try{
            System.loadLibrary("opencv_core");
            System.loadLibrary("opencv_imgproc");
            System.loadLibrary("opencv_highgui");
            System.loadLibrary("MPEG7FexWrapper");            
        }
        catch(UnsatisfiedLinkError e){
		boolean result=false;
		try{
		    System.loadLibrary("opencv_core249");
		    System.loadLibrary("opencv_imgproc249");
		    System.loadLibrary("opencv_highgui249");    
			result=true;       
		}
		catch(UnsatisfiedLinkError ex){
		    Logger.getLogger(JNIInterface.class.getName()).log(Level.SEVERE, "Nie można odnaleźć poprawnej biblioteki!"+ex.toString());
		    throw new Exception();
		}
		if(result!=true){
		    Logger.getLogger(JNIInterface.class.getName()).log(Level.SEVERE, "Nie można odnaleźć poprawnej biblioteki!"+e.toString());
		    throw new Exception();
		}
        }
	try{
            System.loadLibrary("MPEG7FexWrapper");            
        }
        catch(UnsatisfiedLinkError e){
            Logger.getLogger(JNIInterface.class.getName()).log(Level.SEVERE, "Nie można odnaleźć poprawnej biblioteki!"+e.toString());
	    throw new Exception();
        }
        
    }
    
    public int[] calculate(String path){
        return calculate(path, null, null);
    }
    
    public int[] calculate(String path, DescriptorParameters params){
        return calculate(path, null, params);
    }
    
    public int[] calculate(String path, String thumbnailPath){
        return calculate(path, thumbnailPath, null);
    }
    
    public int[] calculate(String path, String thumbnailPath, DescriptorParameters params){
        if(params == null)
            return calculateDescriptor(path, thumbnailPath, null);
        else
            return calculateDescriptor(path, thumbnailPath, params.toArray());
    }

    public native int[] calculateDescriptor(String path, String thumbnailPath, int[] params);
 
}
