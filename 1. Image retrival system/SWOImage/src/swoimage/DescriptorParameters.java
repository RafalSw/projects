/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimage;

/**
 *
 * @author Rafal
 */
public class DescriptorParameters {
    int thumbnailSizeX = 0; //0 - obraz jest skalowany proporcjonalnie względem thumbnailSizeY
    int thumbnailSizeY = 300;
    
    int colorLayoutDescriptorNumYCoef = 64;
    int colorLayoutDescriptorNumCCoef = 64;
    int colorStructureDescriptorSize=64; // descSize: (32, 64, 128, 256)
    

    // Dominant Color Descriptor (CLD)
    // normalize: normalize values to MPEG-7 range (color:0-32, variance:0,1, weight:0-32)?
    //            if false, (color:RGB values[0-255], variance:as computed, weight:0-100 percent)
    // variance: compute the variance?
    // spatial: compute the spatial coherency
    // bin1, bin2, bin3: bin numbers to quantize the dominat color values
    int normalize = 0; //1 - true, 0 - false; false required for this implementation 
    int variance = 0; //1 - true, 0 - false
    int spatial = 1; //1 - true, 0 - false
    int bin1 = 32;
    int bin2 = 32;
    int bin3 = 32;
    
    int homogeneousTextureDescriptorlayerFlag = 1; // layerFlag: base-layer 32-components,  true:full-layer 62 components (both energy and deviation)
    
    
    int scalableColorDescriptorSize = 128; //desc size: (16, 32, 64, 128, 256)
    
    public int[] toArray(){
        int[] result = new int[13];
        result[0] = thumbnailSizeX;
        result[1] = thumbnailSizeY;
        result[2] = colorLayoutDescriptorNumYCoef;
        result[3] = colorLayoutDescriptorNumCCoef;
        result[4] = colorStructureDescriptorSize;
        result[5] = normalize;
        result[6] = variance;
        result[7] = spatial;
        result[8] = bin1;
        result[9] = bin2;
        result[10] = bin3;
        result[11] = homogeneousTextureDescriptorlayerFlag;
        result[12] = scalableColorDescriptorSize;
        return result;
    }
}
