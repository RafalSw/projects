/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimage;

import SWOInterface.SWOData;
import swoimageDescriptors.ColorLayout;
import swoimageDescriptors.ColorStructure;
import swoimageDescriptors.DominantColor;
import swoimageDescriptors.EdgeHistogram;
import swoimageDescriptors.HomogeneusTexture;
import swoimageDescriptors.ScalableColor;

/**
 *
 * @author Rafal
 */
public class SWOImageData extends SWOData{
        
    private static final long serialVersionUID = -5427925749608939160L;
    private int heightInPixels;
    private int widthInPixels;
    private ColorLayout colorLayout;
    private ColorStructure colorStructure;
    private ScalableColor scalableColor;
    private HomogeneusTexture homogeneusTexture;
    private EdgeHistogram edgeHistogram;
    private DominantColor dominantColor;
    
    public SWOImageData(int[] descriptorData){
        if(descriptorData==null)
            return;
        
        int currentPosition = 0;
        while(currentPosition<descriptorData.length){
            if(descriptorData[currentPosition] == JNIInterface.DIMENSIONS && descriptorData.length>currentPosition+2){
                widthInPixels = descriptorData[currentPosition+1];
                heightInPixels = descriptorData[currentPosition+2];
                currentPosition+=3;
            }
            else if(descriptorData[currentPosition] == JNIInterface.COLOR_LAYOUT && descriptorData.length>currentPosition+2){
                int colorLayoutDescriptorNumYCoef = descriptorData[currentPosition+1];
                int colorLayoutDescriptorNumCCoef = descriptorData[currentPosition+2];
                currentPosition+=3;
                if(descriptorData.length>=currentPosition+colorLayoutDescriptorNumYCoef+2*colorLayoutDescriptorNumYCoef){
                    
                    int[] colorLayoutDescriptorYCoef = new int[colorLayoutDescriptorNumYCoef];
                    for(int i=0; i<colorLayoutDescriptorNumYCoef; i++){
                        colorLayoutDescriptorYCoef[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=colorLayoutDescriptorNumYCoef;
                    
                    int[] colorLayoutDescriptorCBCoef = new int[colorLayoutDescriptorNumCCoef];
                    int[] colorLayoutDescriptorCRCoef = new int[colorLayoutDescriptorNumCCoef];
                    
                    for(int i=0; i<colorLayoutDescriptorNumYCoef; i++){
                        colorLayoutDescriptorCBCoef[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=colorLayoutDescriptorNumCCoef;
                    
                    for(int i=0; i<colorLayoutDescriptorNumYCoef; i++){
                        colorLayoutDescriptorCRCoef[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=colorLayoutDescriptorNumCCoef;
                    colorLayout = new ColorLayout(colorLayoutDescriptorYCoef, colorLayoutDescriptorCRCoef, colorLayoutDescriptorCBCoef);
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.COLOR_STRUCTURE && descriptorData.length>currentPosition+1){
                int colorStructureDescriptorSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+colorStructureDescriptorSize){
                    int[] colorStructureDescriptor = new int[colorStructureDescriptorSize];
                    for(int i=0; i<colorStructureDescriptorSize; i++){
                        colorStructureDescriptor[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=colorStructureDescriptorSize;
                    colorStructure = new ColorStructure(colorStructureDescriptor);
                }
                
            }
            else if(descriptorData[currentPosition] == JNIInterface.SCALABLE_COLOR && descriptorData.length>currentPosition+1){
                int scalableColorDescriptorSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+scalableColorDescriptorSize){
                    int[] scalableColorDescriptor = new int[scalableColorDescriptorSize];
                    for(int i=0; i<scalableColorDescriptorSize; i++){
                        scalableColorDescriptor[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=scalableColorDescriptorSize;
                    scalableColor = new ScalableColor(scalableColorDescriptor);
                }
            }
            
            else if(descriptorData[currentPosition] == JNIInterface.HOMOGENEUS_TEXTURE && descriptorData.length>currentPosition+1){
                
                int homogeneusTextureDescriptorSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+homogeneusTextureDescriptorSize && (homogeneusTextureDescriptorSize == 32 || homogeneusTextureDescriptorSize==62)){
                    int mean = currentPosition;
                    currentPosition++;
                    int std = currentPosition;
                    currentPosition++;
                    
                    int[] homogeneusEnergy = new int[30];
                    for(int i=0; i<homogeneusEnergy.length; i++){
                        homogeneusEnergy[i] = descriptorData[currentPosition+i];
                    }
                    currentPosition+=homogeneusEnergy.length;
                    
                    if(homogeneusTextureDescriptorSize == 32){
                        homogeneusTexture = new HomogeneusTexture(mean, std, homogeneusEnergy, null);
                    }
                    else{
                        int[] homogeneusEnergyDeviation = new int[30];
                        for(int i=0; i<homogeneusEnergyDeviation.length; i++){
                            homogeneusEnergyDeviation[i] = descriptorData[currentPosition+i];
                        }
                        currentPosition+=homogeneusEnergyDeviation.length;
                        homogeneusTexture = new HomogeneusTexture(mean, std, homogeneusEnergy, homogeneusEnergyDeviation);
                    }
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.EDGE_HISTOGRAM && descriptorData.length>currentPosition+1){
                int edgeHistogramSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+edgeHistogramSize){
                    int[] edgeHistogramDescriptor = new int[edgeHistogramSize];
                    for(int i=0; i<edgeHistogramSize; i++){
                        edgeHistogramDescriptor[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=edgeHistogramSize;
                    edgeHistogram = new EdgeHistogram(edgeHistogramDescriptor);
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.DOMINANT_COLOR && descriptorData.length>currentPosition+1){
                int dominantColorSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+dominantColorSize*4){
                    int[] dscrData = new int[dominantColorSize*4];
                    
                    for(int i=0; i<dominantColorSize*4; i++){
                        dscrData[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*4;
                    dominantColor = new DominantColor(dominantColorSize, dscrData);
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.DOMINANT_COLOR_WITH_VARIANCE && descriptorData.length>currentPosition+1){
                int dominantColorSize = descriptorData[currentPosition+1];
                currentPosition+=2;
                if(descriptorData.length>=currentPosition+dominantColorSize*7){
                    int[] dscrData = new int[dominantColorSize*4];
                    int[] variance = new int[dominantColorSize*3];
                    
                    for(int i=0; i<dominantColorSize*4; i++){
                        dscrData[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*4;
                    
                    for(int i=0; i<dominantColorSize*3; i++){
                        variance[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*3;
                    dominantColor = new DominantColor(dominantColorSize, dscrData, variance);
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.DOMINANT_COLOR_WITH_SPATIAL && descriptorData.length>currentPosition+2){
                int dominantColorSize = descriptorData[currentPosition+1];
                int spatial = descriptorData[currentPosition+2];
                currentPosition+=3;
                if(descriptorData.length>=currentPosition+dominantColorSize*4){
                    int[] dscrData = new int[dominantColorSize*4];
                    
                    for(int i=0; i<dominantColorSize*4; i++){
                        dscrData[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*4;
                    dominantColor = new DominantColor(dominantColorSize, dscrData, spatial);
                }
            }
            else if(descriptorData[currentPosition] == JNIInterface.DOMINANT_COLOR_WITH_SPATIAL_AND_VARIANCE && descriptorData.length>currentPosition+2){
                int dominantColorSize = descriptorData[currentPosition+1];
                int spatial = descriptorData[currentPosition+2];
                currentPosition+=3;
                if(descriptorData.length>=currentPosition+dominantColorSize*7){
                    int[] dscrData = new int[dominantColorSize*4];
                    int[] variance = new int[dominantColorSize*3];
                    
                    for(int i=0; i<dominantColorSize*4; i++){
                        dscrData[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*4;
                    
                    for(int i=0; i<dominantColorSize*3; i++){
                        variance[i]=descriptorData[currentPosition+i];
                    }
                    currentPosition+=dominantColorSize*3;
                    dominantColor = new DominantColor(dominantColorSize, dscrData, variance, spatial);
                }
            }
            else
                currentPosition++;
        }
    }
    
    public int getWidth(){
        return widthInPixels;
    }
    
    public int getHeight(){
        return heightInPixels;
    }
    
    @Override
    public double compareTo(SWOData data) {
        if(data instanceof SWOImageData){
            double result = 0;
            double cl = 0, cs = 0, sc = 0, ht = 0, eh = 0, dc = 0;
            SWOImageData other = (SWOImageData)data;
            double tagsValue = super.compareTo(data)*Parameters.tagsWeight;
            result += tagsValue;
            
            if(colorLayout!=null && other.colorLayout!=null)
                cl += colorLayout.compareTo(other.colorLayout);
            result += cl;

            if(colorStructure!=null && other.colorStructure!=null)
                cs += colorStructure.compareTo(other.colorStructure);
            result += cs;
            
            if(scalableColor!=null && other.scalableColor!=null)
                sc += scalableColor.compareTo(other.scalableColor);
            result += sc;
            
            if(homogeneusTexture!=null && other.homogeneusTexture!=null)
                ht += homogeneusTexture.compareTo(other.homogeneusTexture);
            result += ht;
            
            if(edgeHistogram!=null && other.edgeHistogram!=null)
                eh += edgeHistogram.compareTo(other.edgeHistogram);
            result += eh;
            
            if(dominantColor!=null && other.dominantColor!=null)
                dc += dominantColor.compareTo(other.dominantColor);
            result += dc;
            
            if(Parameters.verbose>0){
                System.out.print("Tags:\t"+ tagsValue + "\t");
                System.out.print("Color layout:\t"+ cl + "\t");
                System.out.print("Color structure:\t"+ cs + "\t");
                System.out.print("Scalable color:\t"+ sc + "\t");
                System.out.print("Homogeneus texture:\t"+ ht + "\t");
                System.out.print("Edge histogram:\t"+ eh + "\t");
                System.out.print("Dominant color:\t"+ dc + "\t\n");
            }
            
            return result;
        }
        else
            return super.compareTo(data)+Parameters.unknownTypeOffset; //przesuwa inne pliki na koniec listy wyszukiwania
    }
}
