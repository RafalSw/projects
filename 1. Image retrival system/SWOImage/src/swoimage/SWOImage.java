/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimage;

import SWOInterface.SWOData;
import SWOInterface.SWOInterface;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */
public class SWOImage implements SWOInterface{

    private JNIInterface jni;
    private final String[] supportedFiles;
    private final DescriptorParameters descriptorParameters;
    private final Parameters parameters;
    
    public SWOImage(){
        parameters = new Parameters();
        supportedFiles = new String[]{"jpeg","jpg","bmp","png","jp2","pbm","dib","jpe","pgm","ppm","sr","ras","tiff","tif","tmp"}; //http://docs.opencv.org/modules/highgui/doc/reading_and_writing_images_and_video.html
        descriptorParameters = new DescriptorParameters();
    }

    @Override
    public boolean initialize() {
        parameters.readProperties();
	try{
        	jni = new JNIInterface();
		return true;
	}
	catch(Exception e){
		return false;
	}
    }


    @Override
    public boolean close() {
        return true;
    }

    @Override
    public String[] getFileTypes() {
        return supportedFiles;
    }

    @Override
    public SWOData calculate(Path path) {
        return calculate(path, null);
    }

    @Override
    public SWOData calculate(Path path, String thumbnailPath) {
        String filePath = path.toString();

        int[] descriptors = jni.calculate(filePath, thumbnailPath, descriptorParameters);
        
        if(descriptors == null){
            Logger.getLogger(SWOImage.class.getName()).log(Level.SEVERE,"Nie udało się policzyć desktyptorów dla "+ filePath);
        }
        
        return new SWOImageData(descriptors);
    }
  
    
    public static void main(String[] args) {
        SWOImage newOne = new SWOImage();
        newOne.initialize();
        SWOData newData = newOne.calculate(Paths.get("C:\\Users\\Rafal\\Desktop\\Na FB\\test\\01.jpg"));
        SWOData newData2 = newOne.calculate(Paths.get("C:\\Users\\Rafal\\Desktop\\Na FB\\02.jpg"));
        double result = newData.compareTo(newData2);
        System.out.println(result);
    }

    @Override
    public String getAdditionalInfo(SWOData data) {
        if(data instanceof SWOImageData){
            SWOImageData ref = (SWOImageData)data;
            return "Rozmiar: "+ref.getWidth()+"x"+ref.getHeight()+"px";
        }
        else
            return "Brak informacji";
    }
}
