/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoimage;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */
public class Parameters {
    public static double colorLayoutWeight = 0.96;
    public static double colorLayoutYWeight = 1;
    public static double colorLayoutCRWeight = 1;
    public static double colorLayoutCBWeight = 1;
    public static double colorStructureWeight = 1.61;
    public static double dominantColorAlphaValue = 1; //1.0 to 1.5
    public static double dominantColorTargetDistance = 15; //10 to 20
    public static double dominantColorDistanceMax = dominantColorAlphaValue*dominantColorTargetDistance;
    public static double dominantColorSpatialWeight = 0.3; //0.3
    public static double dominantColorNonSpatialWeight = 1 - dominantColorSpatialWeight; // 0.7
    public static double dominantColorWeight = 0.1857;
    public static double edgeHistogramWeight = 1.49;
    public static double homogeneusTextureWeight = 0.0945;
    public static double scalableColorWeight = 1;
    public static double tagsWeight = 1;
    public static double unknownTypeOffset = Double.MAX_VALUE/2;
    public static int verbose = 1; //0 - off, 1 - on
    
    private final Properties properties = new Properties();
    
    
    /* for particular implementation - based on constant from C++ lib */
    public static double CSD_MAX_DIST = 6701;
    public static double CLD_MAX_DIST = 130; //approx
    public static double EHD_MAX_DIST = 554; //approx
    
    
    public void readProperties(){
        InputStream input = null;
        boolean fileFound = true;
        boolean needForRewrite = false;
        
        try {
            input = new  FileInputStream("SWOImageConfig.properties");
            properties.load(input);
        } catch (FileNotFoundException ex) {
        } catch (IOException ex) {
        }
        
        try{
            colorLayoutWeight = Integer.parseInt(properties.getProperty("verbose"));
        } catch (NumberFormatException | NullPointerException e){
            colorLayoutWeight = 0.96;
            properties.setProperty("verbose", Integer.toString(verbose));
            needForRewrite = true;
        }
        
        try{
            colorLayoutWeight = Double.parseDouble(properties.getProperty("colorLayoutWeight"));
        } catch (NumberFormatException | NullPointerException e){
            colorLayoutWeight = 1;
            properties.setProperty("colorLayoutWeight", Double.toString(colorLayoutWeight));
            needForRewrite = true;
        }

        try{
            colorLayoutYWeight = Double.parseDouble(properties.getProperty("colorLayoutYWeight"));
        } catch (NumberFormatException | NullPointerException e){
            colorLayoutYWeight = 1;
            properties.setProperty("colorLayoutYWeight", Double.toString(colorLayoutYWeight));
            needForRewrite = true;
        }

        try{
            colorLayoutCRWeight = Double.parseDouble(properties.getProperty("colorLayoutCRWeight"));
        } catch (NumberFormatException | NullPointerException e){
            colorLayoutCRWeight = 1;
            properties.setProperty("colorLayoutCRWeight", Double.toString(colorLayoutCRWeight));
            needForRewrite = true;
        }

        try{
            colorLayoutCBWeight = Double.parseDouble(properties.getProperty("colorLayoutCBWeight"));
        } catch (NumberFormatException | NullPointerException e){
            colorLayoutCBWeight = 1;
            properties.setProperty("colorLayoutCBWeight", Double.toString(colorLayoutCBWeight));
            needForRewrite = true;
        }

        try{
            colorStructureWeight = Double.parseDouble(properties.getProperty("colorStructureWeight"));
        } catch (NumberFormatException | NullPointerException e){
            colorStructureWeight = 1.61;
            properties.setProperty("colorStructureWeight", Double.toString(colorStructureWeight));
            needForRewrite = true;
        }

        try{
            dominantColorAlphaValue = Double.parseDouble(properties.getProperty("dominantColorAlphaValue"));
        } catch (NumberFormatException | NullPointerException e){
            dominantColorAlphaValue = 1;
            properties.setProperty("dominantColorAlphaValue", Double.toString(dominantColorAlphaValue));
            needForRewrite = true;
        }

        try{
            dominantColorTargetDistance = Double.parseDouble(properties.getProperty("dominantColorTargetDistance"));
        } catch (NumberFormatException | NullPointerException e){
            dominantColorTargetDistance = 15;
            properties.setProperty("dominantColorTargetDistance", Double.toString(dominantColorTargetDistance));
            needForRewrite = true;
        }

        dominantColorDistanceMax = dominantColorAlphaValue * dominantColorTargetDistance;

        try{
            dominantColorSpatialWeight = Double.parseDouble(properties.getProperty("dominantColorSpatialWeight"));
        } catch (NumberFormatException | NullPointerException e){
            dominantColorSpatialWeight = 0.3;
            properties.setProperty("dominantColorSpatialWeight", Double.toString(dominantColorSpatialWeight));
            needForRewrite = true;
        }

        dominantColorNonSpatialWeight = 1 - dominantColorSpatialWeight;

        try{
            dominantColorWeight = Double.parseDouble(properties.getProperty("dominantColorWeight"));
        } catch (NumberFormatException | NullPointerException e){
            dominantColorWeight = 0.1857;
            properties.setProperty("dominantColorWeight", Double.toString(dominantColorWeight));
            needForRewrite = true;
        }

        try{
            edgeHistogramWeight = Double.parseDouble(properties.getProperty("edgeHistogramWeight"));
        } catch (NumberFormatException | NullPointerException e){
            edgeHistogramWeight = 1.49;
            properties.setProperty("edgeHistogramWeight", Double.toString(edgeHistogramWeight));
            needForRewrite = true;
        }

        try{
            homogeneusTextureWeight = Double.parseDouble(properties.getProperty("homogeneusTextureWeight"));
        } catch (NumberFormatException | NullPointerException e){
            homogeneusTextureWeight = 0.0945;
            properties.setProperty("homogeneusTextureWeight", Double.toString(homogeneusTextureWeight));
            needForRewrite = true;
        }

        try{
            scalableColorWeight = Double.parseDouble(properties.getProperty("scalableColorWeight"));
        } catch (NumberFormatException | NullPointerException e){
            scalableColorWeight = 1;
            properties.setProperty("scalableColorWeight", Double.toString(scalableColorWeight));
            needForRewrite = true;
        }

        try{
            tagsWeight = Double.parseDouble(properties.getProperty("tagsWeight"));
        } catch (NumberFormatException | NullPointerException e){
            tagsWeight = 1;
            properties.setProperty("tagsWeight", Double.toString(tagsWeight));
            needForRewrite = true;
        }

        try{
            unknownTypeOffset = Double.parseDouble(properties.getProperty("unknownTypeOffset"));
        } catch (NumberFormatException | NullPointerException e){
            unknownTypeOffset = Double.MAX_VALUE/2;
            properties.setProperty("unknownTypeOffset", Double.toString(unknownTypeOffset));
            needForRewrite = true;
        }
        
        if(needForRewrite == true || fileFound == false){
            try {
                OutputStream output = new FileOutputStream("SWOImageConfig.properties");
                properties.store(output, null);
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SWOImage.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(SWOImage.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
