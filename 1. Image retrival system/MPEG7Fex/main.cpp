#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <ctime>
#include "DescriptorExporter.h"
#include "swoimage_JNIInterface.h"

#if defined(_WIN32) || defined(_WIN64)
#include <Windows.h>
#endif

JNIEXPORT jintArray JNICALL Java_swoimage_JNIInterface_calculateDescriptor(JNIEnv * env, jobject obj, jstring src, jstring thumbnail, jintArray params)
{
	if(src == NULL){
		std::cout<<"MPEG7FexWrapped - Wrong src"<<std::endl;
		return NULL;
	}

#if defined(_WIN32) || defined(_WIN64)
	const jchar *fileSrc1 = env->GetStringChars(src, JNI_FALSE);
	int fileNameLen = env->GetStringLength(src)+1;
	wchar_t *fileName = new wchar_t[fileNameLen];
	wcsncpy(fileName,(wchar_t*)fileSrc1, fileNameLen-1);
	env->ReleaseStringChars(src, fileSrc1);
	fileName[fileNameLen-1]='\0';
	char* fileSrc = new char[fileNameLen];
	WideCharToMultiByte(CP_ACP,0,fileName,-1, fileSrc,fileNameLen,NULL, NULL);
	std::string absPath(fileSrc, strlen(fileSrc));
	delete fileName;
	delete fileSrc;
#else
	const char* fileSrc = env->GetStringUTFChars(src, JNI_FALSE);
	std::string absPath(fileSrc, strlen(fileSrc));
	env->ReleaseStringUTFChars(src, fileSrc);
	//delete fileSrc;
#endif

	std::string thumbnailPath;
	
	if(thumbnail == NULL)
	{
		thumbnailPath="";
		
	}
	else
	{
#if defined(_WIN32) || defined(_WIN64)
		const jchar *fileThumbnail1 = env->GetStringChars(thumbnail, JNI_FALSE);
		int fileThumbnailLen = env->GetStringLength(thumbnail)+1;
		wchar_t *thumbnailName = new wchar_t[fileThumbnailLen];
		wcsncpy(thumbnailName,(wchar_t*)fileThumbnail1, fileThumbnailLen-1);
		env->ReleaseStringChars(thumbnail, fileThumbnail1);
		thumbnailName[fileThumbnailLen-1]='\0';
		char* thumbnailSrc = new char[fileThumbnailLen];
		WideCharToMultiByte(CP_ACP,0,thumbnailName,-1, thumbnailSrc,fileThumbnailLen,NULL, NULL);
		std::string temp(thumbnailSrc,strlen(thumbnailSrc));
		delete thumbnailName;
		delete thumbnailSrc;
		
		thumbnailPath = temp;
#else
		const char* fileThumbnail = NULL;
		fileThumbnail = env->GetStringUTFChars(thumbnail, JNI_FALSE);
		std::string temp(fileThumbnail,strlen(fileThumbnail));
		thumbnailPath = temp;
		env->ReleaseStringUTFChars(thumbnail, fileThumbnail);
#endif

	}
	jint* parameters = NULL;
	bool paramsInitialized = false;
	int param[13];
	if(params != NULL)
	{
		jsize len = env->GetArrayLength(params);
		if(len == 13)
		{
			parameters = env->GetIntArrayElements(params, JNI_FALSE);
			for(int i = 0; i<len; i++)
			{
				param[i]=(int)parameters[i];
			}
			env->ReleaseIntArrayElements(params, parameters, JNI_ABORT);
			paramsInitialized = true;
		}
	}

	if(!paramsInitialized)
	{
		param[0] = 0; //imgSizeX
		param[1] = 300; //imgSizeY
		param[2] = 64; //numYCoef
		param[3] = 64; //numCCoef
		param[4] = 64; //descSize
		param[5] = 1; //normalize
		param[6] = 0; //variance
		param[7] = 0; //spatial
		param[8] = 32; //bin1
		param[9] = 32; //bin2
		param[10] = 32; //bin3
		param[11] = 1; //layerFlag
		param[12] = 128; //scalableColorDescriptorSize
	}

	DescriptorExporter* descriptorExporter = new DescriptorExporter(param);
	try{
		descriptorExporter->open(absPath, thumbnailPath);
		descriptorExporter->calculate();
		descriptorExporter->print();

		
		int resultSize = descriptorExporter->getLen();
		jintArray result = env->NewIntArray(resultSize);
		int *resultIntArray;
		descriptorExporter->fill(&resultIntArray);
		env->SetIntArrayRegion(result, 0, resultSize, (jint*)resultIntArray);
		delete descriptorExporter;
		return result;
	}
	catch (...)
	{
		std::cout<<"MPEG7FexWrapped - Error"<<std::endl;
		delete descriptorExporter;
		return NULL;
	}

}
/*
int main()
{
	std::string absPath = "C:\\Users\\Rafal\\Desktop\\LargeSWOtest\\Jastarnia\\20130823_164633.jpg";
	std::string thumbnailPath;

	thumbnailPath="";
	
	int param[13];

	param[0] = 0; //imgSizeX
	param[1] = 300; //imgSizeY
	param[2] = 64; //numYCoef
	param[3] = 64; //numCCoef
	param[4] = 64; //descSize
	param[5] = 1; //normalize
	param[6] = 0; //variance
	param[7] = 0; //spatial
	param[8] = 32; //bin1
	param[9] = 32; //bin2
	param[10] = 32; //bin3
	param[11] = 1; //layerFlag
	param[12] = 128; //scalableColorDescriptorSize

	DescriptorExporter* descriptorExporter = new DescriptorExporter(param);
	try{
		descriptorExporter->open(absPath, thumbnailPath);
		descriptorExporter->calculate();
		descriptorExporter->print();

		
		int resultSize = descriptorExporter->getLen();
		int *resultIntArray;
		descriptorExporter->fill(&resultIntArray);
	}
	catch (...)
	{
		std::cout<<"MPEG7FexWrapped - Error"<<std::endl;
		delete descriptorExporter;
		return NULL;
	}
}*/
