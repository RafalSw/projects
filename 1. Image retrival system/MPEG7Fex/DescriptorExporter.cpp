#include "DescriptorExporter.h"
#include <iostream>
#include <ctime>

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

DescriptorExporter::DescriptorExporter(int params[13])
{
	scd=NULL;
	rsd=NULL;
	htd=NULL;
	ehd=NULL;
	dcd=NULL;
	cspd=NULL;
	cshd=NULL;
	csd=NULL;
	cqd=NULL;
	cld=NULL;
	frame=NULL;
	
	imgSizeX = params[0];
	imgSizeY = params[1];
    numYCoef = params[2];;
    numCCoef = params[3];
    descSize = params[4];

	if(params[5] == 1)
		normalize = true;
	else
		normalize = false;

	if(params[6] == 1)
		variance = true;
	else
		variance = false;
	if(params[7] == 1)
		spatial = true;
	else
		spatial = false;

    bin1 = params[8];
    bin2 = params[9];
    bin3 = params[10];

	if(params[11] == 1)
		layerFlag = true;
	else
		layerFlag = false;

    scalableColorDescriptorSize = params[12];
}

DescriptorExporter::~DescriptorExporter()
{
	if(frame)
		delete frame;
	if(cld)
		delete cld;
	if(cqd)
		delete cqd;
	if(csd)
		delete csd;
	if(cshd)
		delete cshd;
	if(cspd)
		delete cspd;
	if(dcd)
		delete dcd;
	if(ehd)
		delete ehd;
	if(htd)
		delete htd;
	if(rsd)
		delete rsd;
	if(scd)
		delete scd;
	
}

void DescriptorExporter::open(std::string absPath, std::string thumbnailPath)
{
	double ratio;
	Mat gray;

	Mat image = imread(absPath);
	if(image.data == NULL)
		std::cout<<"MPEG7Wrapper - nie udalo sie wczytac zdjecia "<<absPath<<std::endl;
	ratio = (double)image.cols/image.rows;
	origImgSizeX = image.cols;
	origImgSizeY = image.rows;

	if(imgSizeX == 0)
		imgSizeX = (int)(ratio*imgSizeY);

	cv::Size size;
	size.width = imgSizeX;
	size.height = imgSizeY;
	cv::resize(image, image, size);

	if(thumbnailPath.compare("") != 0){
		imwrite(thumbnailPath,image);
	}
	
	cvtColor( image, gray, CV_BGR2GRAY );
	
	if(frame != NULL)
	{
		delete frame;
	}
	frame = new Frame(image.cols, image.rows, true, true, true);

	frame->setImage(image);
	frame->setGray(gray);
}

void DescriptorExporter::calculate()
{
	cld = Feature::getColorLayoutD( frame, numYCoef, numCCoef ); //0.598
	cqd = NULL; //todo
	csd = Feature::getColorStructureD(frame, descSize); //1.057
	//cshd = Feature::getContourShapeD(frame); 
	cspd = NULL; //todo
	dcd = Feature::getDominantColorD( frame, normalize, variance, spatial, bin1, bin2, bin3 ); //27.238
	ehd = Feature::getEdgeHistogramD( frame ); //0.105
	htd = Feature::getHomogeneousTextureD( frame, layerFlag ); //0.115
	//rsd = Feature::getRegionShapeD( frame ); //wymaga: frame->setMask(mask, rid, 255, 0);
	scd = Feature::getScalableColorD( frame, true, scalableColorDescriptorSize ); //3.616
}

int DescriptorExporter::getLen()
{
	int result = 0;
	result+=(1+2); //size x, y
	result+=(3+cld->GetNumberOfYCoeff()+cld->GetNumberOfCCoeff()*2);
	result+=(2+csd->GetSize());
	result+=(2+scd->GetNumberOfCoefficients());

	//HomogeneusTexture
	if(layerFlag)
		result+=(2+62); 
	else
		result+=(2+32);

	result+=(2+ehd->GetSize());
	
	//DominantColor
	int domColSize = dcd->GetDominantColorsNumber();
	if(spatial)
	{
		if(variance)
		{
			result+=(2+1+domColSize*4+domColSize*3);
		}
		else
		{
			result+=(2+1+domColSize*4);
		}
	}
	else
	{
		if(variance)
		{
			result+=(2+domColSize*4+domColSize*3);
		}
		else
		{
			result+=(2+domColSize*4);
		}
	}
	return result;
}


void DescriptorExporter::fill(int** data)
{
	int len = getLen();
	/*
	if(*data != NULL)
		delete[] *data;*/
	*data = new int[len];


	(*data)[0]=DIMENSIONS;
	(*data)[1]=origImgSizeX;
	(*data)[2]=origImgSizeY;
	int currentPosition = 3;

	//ColorLayout
	int *y_coeff, *cb_coeff, *cr_coeff;
	y_coeff = cld->GetYCoeff();
	cb_coeff = cld->GetCbCoeff();
	cr_coeff = cld->GetCrCoeff();
	(*data)[currentPosition] = COLOR_LAYOUT;
	currentPosition++;
	(*data)[currentPosition]=cld->GetNumberOfYCoeff();
	currentPosition++;
	(*data)[currentPosition]=cld->GetNumberOfCCoeff();
	currentPosition++;

	for(int i=currentPosition; i<cld->GetNumberOfYCoeff()+currentPosition; i++)
	{
		(*data)[i]=y_coeff[i-currentPosition];
	}
	currentPosition+=cld->GetNumberOfYCoeff();

	for(int i=currentPosition; i<cld->GetNumberOfCCoeff()+currentPosition; i++)
	{
		(*data)[i]=cb_coeff[i-currentPosition];
	}
	currentPosition+=cld->GetNumberOfCCoeff();

	for(int i=currentPosition; i<cld->GetNumberOfCCoeff()+currentPosition; i++)
	{
		(*data)[i]=cr_coeff[i-currentPosition];
	}
	currentPosition+=cld->GetNumberOfCCoeff();


	//ColorStructure
	(*data)[currentPosition]=COLOR_STRUCTURE;
	currentPosition++;
	(*data)[currentPosition]=csd->GetSize();
	currentPosition++;

	for(int i=currentPosition; i<(int)csd->GetSize()+currentPosition; i++)
	{
		(*data)[i]=(int)csd->GetElement(i-currentPosition);
	}
	currentPosition+=csd->GetSize();


	//ScalableColor
	(*data)[currentPosition]=SCALABLE_COLOR;
	currentPosition++;
	(*data)[currentPosition]=scd->GetNumberOfCoefficients();
	currentPosition++;

	for(int i=currentPosition; i<(int)scd->GetNumberOfCoefficients()+currentPosition; i++)
	{
		(*data)[i]=(int)scd->GetCoefficient(i-currentPosition);
	}
	currentPosition+=scd->GetNumberOfCoefficients();

	//HomogeneusTexture
	(*data)[currentPosition]=HOMOGENEUS_TEXTURE;
	currentPosition++;
	if(layerFlag)
		(*data)[currentPosition]=62;
	else
		(*data)[currentPosition]=32;
	currentPosition++;

	int* values = htd->GetHomogeneousTextureFeature();
	for(int i=currentPosition; i<32+currentPosition; i++)
	{
		(*data)[i]=(int)values[i-currentPosition];
	}
	currentPosition+=32;

	if(layerFlag)
	{
		for(int i=currentPosition; i<30+currentPosition; i++)
		{
			(*data)[i]=(int)values[32+i-currentPosition];
		}
		currentPosition+=30;
	}

	
	//EdgeHistogram
	(*data)[currentPosition]=EDGE_HISTOGRAM;
	currentPosition++;
	(*data)[currentPosition]=ehd->GetSize();
	currentPosition++;

	char* de = ehd->GetEdgeHistogramElement();
	for(int i=currentPosition; i<(int)ehd->GetSize()+currentPosition; i++)
	{
		(*data)[i]=(int)de[i-currentPosition];
	}
	currentPosition+=ehd->GetSize();


	
	//DominantColor
	if(spatial && variance)
		(*data)[currentPosition]=DOMINANT_COLOR_WITH_SPATIAL_AND_VARIANCE;
	else if(spatial && (!variance))
		(*data)[currentPosition]=DOMINANT_COLOR_WITH_SPATIAL;
	else if((!spatial) && variance)
		(*data)[currentPosition]=DOMINANT_COLOR_WITH_VARIANCE;
	else
		(*data)[currentPosition]=DOMINANT_COLOR;
	currentPosition++;

	(*data)[currentPosition] = dcd->GetDominantColorsNumber();
	currentPosition++;

	if(spatial)
	{
		(*data)[currentPosition]=dcd->GetSpatialCoherency();
		currentPosition++;
	}

	XM::DOMCOL* domcol = dcd->GetDominantColors();
	for(int i=currentPosition, j = 0; j<dcd->GetDominantColorsNumber(); i+=4, j++)
	{
		(*data)[i]=(int)domcol[j].m_Percentage;
		(*data)[i+1]=(int)domcol[j].m_ColorValue[0];
		(*data)[i+2]=(int)domcol[j].m_ColorValue[1];
		(*data)[i+3]=(int)domcol[j].m_ColorValue[2];
	}
	currentPosition+=dcd->GetDominantColorsNumber()*4;

	if(variance)
	{
		for(int i=currentPosition, j = 0; j<dcd->GetDominantColorsNumber(); i+=3,j++)
		{
			(*data)[i]=(int)domcol[j].m_ColorVariance[0];
			(*data)[i+1]=(int)domcol[j].m_ColorVariance[1];
			(*data)[i+2]=(int)domcol[j].m_ColorVariance[2];
		}
		currentPosition+=dcd->GetDominantColorsNumber()*3;
	}
}

void DescriptorExporter::print()
{
	std::cout<<"ColorLayout"<<std::endl;
	// number of coefficients
	unsigned int numberOfYCoeff = cld->GetNumberOfYCoeff();
	unsigned int numberOfCCoeff = cld->GetNumberOfCCoeff();


	// values
	int *y_coeff, *cb_coeff, *cr_coeff;
	y_coeff = cld->GetYCoeff();
	cb_coeff = cld->GetCbCoeff();
	cr_coeff = cld->GetCrCoeff();


	unsigned int i = 0;
	// Y coeff (DC and AC)
	for ( i = 0; i < numberOfYCoeff; i++ )
		std::cout << y_coeff[i] << " " ;
	std::cout<<std::endl;
	//Cb coeff  (DC and AC)
	for ( i = 0; i < numberOfCCoeff; i++ )
		std::cout << cb_coeff[i] << " ";
	std::cout<<std::endl;
	//Cr coeff  (DC and AC)
	for ( i = 0; i < numberOfCCoeff; i++ )
		std::cout << cr_coeff[i] << " ";
	std::cout << std::endl;


	std::cout<<"ColorStructure"<<std::endl;
	for(i = 0; i < csd->GetSize(); i++)
		std::cout << (int)csd->GetElement(i) << " " ;
	std::cout << std::endl;

	/*std::cout<<"ContourShape"<<std::endl;
		  printf("Contour Shape Descriptor\n");

	  unsigned long lgcv[2];
	  cshd->GetGlobalCurvature(lgcv[0], lgcv[1]);
	  printf("Global Curvature: %d  %d\n", (int)lgcv[0], (int)lgcv[1]);

	  unsigned int num = cshd->GetNoOfPeaks();
	  if (num > 0)
	  {
		unsigned long lpcv[2];
		cshd->GetPrototypeCurvature(lpcv[0], lpcv[1]);
		printf("Prototype Curvature: %d  %d\n", (int)lpcv[0], (int)lpcv[1]);

	  }

	  printf("Highest Peak Y: %d\n", (int)(cshd->GetHighestPeakY)());

	  if (num > 0)
		printf("Peaks:\n");

	  for (unsigned int i = 1; i < num; i++)
	  {
		unsigned short xp, yp;

		cshd->GetPeak(i, xp, yp);
		printf("Peak X: %d\n", xp );
		printf("Peak Y: %d\n", yp );
	  }*/
	std::cout<<"\n\nDominantColor"<<std::endl;
	int ndc = dcd->GetDominantColorsNumber();
	std::cout << "Liczba kolorow dominujacych: " << ndc <<std::endl;

	// spatial coherency
	if(spatial)
		std::cout << dcd->GetSpatialCoherency()<<std::endl;

	// dominant colors: percentage(1) centroid value (3) color variance (3)
	XM::DOMCOL* domcol = dcd->GetDominantColors();
	for( int i = 0; i < ndc; i++ )
	{
		std::cout << " " << domcol[i].m_Percentage
					<< " " << domcol[i].m_ColorValue[0]
					<< " " << domcol[i].m_ColorValue[1]
					<< " " << domcol[i].m_ColorValue[2]<<std::endl;
		if(variance)
		std::cout << " " << domcol[i].m_ColorVariance[0]
					<< " " << domcol[i].m_ColorVariance[1]
					<< " " << domcol[i].m_ColorVariance[2]<<std::endl;
	}

	std::cout << std::endl;

	  std::cout<<"\n\nEdgeHistogram"<<std::endl;
	  char* de = ehd->GetEdgeHistogramElement();
	  for( unsigned int i = 0; i < ehd->GetSize(); i++)
		  std::cout << (int)de[i] << " ";
	  std::cout  << std::endl;

	std::cout<<"\n\nHomogeneis Texture"<<std::endl;

	int* values = htd->GetHomogeneousTextureFeature();

	// write to screen

	// values[0]: mean, values[1]: std, values[2-31] base layer (energy)
	for(i = 0; i < 32; i++)
	std::cout << values[i] << " " ;

	// if full layer, print values[32-61] (energy deviation)
	for(i = 32; i < 62; i++)
		std::cout << values[i] << " " ;
	
	/*
	std::cout<<"\n\nRegionShape"<<std::endl;
	int j;
	for(i=0; i<ART_ANGULAR; i++)
		for(j=0; j<ART_RADIAL; j++)
			if(i != 0 || j != 0)
				std::cout << (int)rsd->GetElement(i, j) << " ";
	*/

	std::cout<<"\n\nScalableColor"<<std::endl;
	for(unsigned int i = 0; i < scd->GetNumberOfCoefficients(); i++)
		std::cout << (int)  scd->GetCoefficient(i) << " ";
	std::cout << std::endl;
}
