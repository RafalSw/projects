#include "Feature.h"

class DescriptorExporter
{
private:
	Frame* frame;
	
	XM::ColorLayoutDescriptor* cld;
	XM::ColorQuantizerDescriptor* cqd;
	XM::ColorStructureDescriptor* csd;
	XM::ContourShapeDescriptor* cshd;
	XM::ColorSpaceDescriptor* cspd;
	XM::DominantColorDescriptor* dcd;
	XM::EdgeHistogramDescriptor* ehd;
	XM::HomogeneousTextureDescriptor* htd;
	XM::RegionShapeDescriptor* rsd;
	XM::ScalableColorDescriptor* scd;

	int origImgSizeX, origImgSizeY;
	int imgSizeX, imgSizeY;
	int descSize;
	int numYCoef;
	int numCCoef;
	bool normalize;
	bool variance;
	bool spatial;
	int bin1;
	int bin2;
	int bin3;
	bool layerFlag;
	int scalableColorDescriptorSize;

	static const int DIMENSIONS = 1000;
	static const int COLOR_LAYOUT = 1001;
	static const int COLOR_STRUCTURE = 1002;
	static const int SCALABLE_COLOR = 1003;
	static const int HOMOGENEUS_TEXTURE = 1004;
	static const int EDGE_HISTOGRAM = 1005;
	static const int DOMINANT_COLOR = 1006;
	static const int DOMINANT_COLOR_WITH_SPATIAL = 1007;
	static const int DOMINANT_COLOR_WITH_VARIANCE = 1008;
	static const int DOMINANT_COLOR_WITH_SPATIAL_AND_VARIANCE = 1009;

public:
	DescriptorExporter(int[13]);
	~DescriptorExporter();
	void open(std::string absPath, std::string thumbnailPath);
	void calculate();
	int getLen();
	void fill(int** data);
	void print();
};