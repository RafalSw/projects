/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoindexer;

import SWOInterface.SWOData;
import hash.Hash;
import hash.HashException;
import java.io.File;
import java.nio.file.Paths;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.prevayler.Prevayler;

/**
 *
 * @author Rafal
 */
public class DataAnalyzer implements Runnable{
    private int cores;
    private Prevayler prevayler;
    private PluginsManager plugins;
    private String thumbnailPath;
    private AtomicBoolean shouldRun;
    private AtomicInteger delay;
    private Hash hash;
    private ExecutorService fixedPool;
    
    
    private DataAnalyzer(){};
    
    public DataAnalyzer(Prevayler prev, PluginsManager pluginManager, String thumbnails, int delayOfProcess){
        prevayler = prev;
        plugins = pluginManager;
        thumbnailPath = thumbnails;
        hash = new Hash();
        cores = Runtime.getRuntime().availableProcessors();
        if(cores<1)
            cores = 1;
        shouldRun = new AtomicBoolean(false);
        fixedPool = Executors.newFixedThreadPool(cores);
        if(delayOfProcess>=0)
            delay = new AtomicInteger(delayOfProcess);
        else
            delay = new AtomicInteger(0);
    }
    
    private class Analyze implements Runnable{
        String analyzedFile;
        public Analyze(String p){
            analyzedFile = p;
        }
        @Override
        public void run() {
            System.out.println("Analizowanie "+ analyzedFile);
            File f = new File(analyzedFile);
            if(f.exists()){
                String newHash;
                try {
                    newHash = hash.calculateToStr(f);
                } catch (HashException ex) {
                    Logger.getLogger(DataAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                    return;
                }
                if(!(Boolean)prevayler.execute(new SWODataDatabaseTransactionDoesExist(newHash))){
                    if(!(Boolean)prevayler.execute(new SWODataDatabaseTransactionRevertDelete(newHash, analyzedFile))){
                        String extension = f.getName().substring(f.getName().lastIndexOf(".")+1);
                        SWOData result = plugins.getPluginByType(extension).calculate(Paths.get(analyzedFile), thumbnailPath+newHash+".jpg");
                        result.setHash(newHash);
                        result.addFile(analyzedFile);
                        result.setThumbnail(thumbnailPath+newHash+".jpg");
                        prevayler.execute(new SWODataDatabaseTransactionPut(result, analyzedFile));
                    }
                }
                else{
                    prevayler.execute(new SWODataDatabaseTransactionAddFileToData(newHash, analyzedFile));
                }
            }
            else{
                Logger.getLogger(DataAnalyzer.class.getName()).log(Level.SEVERE, "Nie można przanalizować pliku "+analyzedFile);
            }
            System.out.println("Przeanalizowano "+ analyzedFile);
            
        }
    }
    
    public void stop(){
        shouldRun.set(false);
        fixedPool.shutdown();
        fixedPool = Executors.newFixedThreadPool(cores);
        
    }
    
    public boolean isRunning(){
        return shouldRun.get();
    }
    
    public void setDelay(int d){
        if(d>=0)
            delay.set(d);
    }

    @Override
    public void run() {
        shouldRun.set(true);
        int counter = 0;
        while(shouldRun.get()){
            String s = (String)prevayler.execute(new SWODataDatabaseTransactionGetCreated());
            if(s != null){
                fixedPool.submit(new Analyze(s));
                
                //-----------
                if(delay.get()!=0)
                {
                    try {

                        Thread.yield();
                        Thread.sleep(delay.get());
                    } catch (InterruptedException ex) {
                        Logger.getLogger(DataAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
                //------------
                counter++;
                if(counter>=30){
                    try {
                        prevayler.takeSnapshot();
                        counter = 0;
                    } catch (Exception ex) {
                        Logger.getLogger(DataAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            else{
                try {
                    Thread.yield();
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(DataAnalyzer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
