/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoindexer;

import SWOInterface.SWOData;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.prevayler.SureTransactionWithQuery;
import org.prevayler.Transaction;

/**
 *
 * @author M
 */

class SWODataDatabaseTransactionCreatedFile implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 2644445384172703814L;

    String str;
    
    private SWODataDatabaseTransactionCreatedFile(){};
    
    public SWODataDatabaseTransactionCreatedFile(String path){
        str = path;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        p.created.add(str);
    }
}

class SWODataDatabaseTransactionModifiedFile implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 3482366565121493848L;

    String path;
    
    private SWODataDatabaseTransactionModifiedFile(){};
    
    public SWODataDatabaseTransactionModifiedFile(String p){
        path = p;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        String hash = p.indexes.get(path);
        SWOData copy = p.data.get(hash);
        
        if(copy!=null){
            copy.deleteFile(Paths.get(path));
            if(copy.getAllPaths().length == 0){
                p.removed.put(p.indexes.get(hash), copy);
                p.data.remove(hash);
            }
        }
        
        p.indexes.remove(path);
        p.created.add(path);
    }
}

class SWODataDatabaseTransactionGetCreated implements SureTransactionWithQuery<SWODataDatabase, String>{
    private static final long serialVersionUID = 7041028570504074112L;

    @Override
    public String executeAndQuery(SWODataDatabase p, Date date) {
        if(p.created.size()>0){
            String result = p.created.get(0);
            p.processedCreated.add(result);
            p.created.remove(0);
            return result;
        }
        else
            return null;
    }
}

class SWODataDatabaseTransactionDeleted implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 7952729550854663737L;

    String path;
    
    private SWODataDatabaseTransactionDeleted(){};
    
    public SWODataDatabaseTransactionDeleted(String p){
        path = p;
    }
    
    private void action(SWODataDatabase p, List<String> paths){
        
        for(String s:paths){
            String hash = p.indexes.get(s);
            if(hash !=null){
                SWOData copy = p.data.get(hash);
                if(copy!=null){
                    copy.deleteFile(Paths.get(s));
                    if(copy.getAllPaths().length == 0){
                        p.removed.put(hash, copy);
                        p.data.remove(hash);
                    }
                }
                p.indexes.remove(s);
            }
        }
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date){
        List<String> toBeRemoved = new ArrayList<>();
        if(p.indexes.containsKey(path)){
            toBeRemoved.add(path);
            
        }
        else{
            for(String s: p.indexes.keySet()){
                if(s.startsWith(path)){
                    toBeRemoved.add(s);
                }
            }
        }
        action(p,toBeRemoved);
    }
}

class SWODataDatabaseTransactionMovedFile implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 6232152609469380974L;

    String[] path;
    
    private SWODataDatabaseTransactionMovedFile(){};
    
    public SWODataDatabaseTransactionMovedFile(String[] paths){
        path = paths;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        String hash = p.indexes.get(path[0]);
        SWOData copy = p.data.get(hash);
        
        if(copy!=null){
            copy.deleteFile(Paths.get(path[0]));
            copy.addFile(Paths.get(path[1]));
        }
        
        p.indexes.remove(path[0]);
        p.indexes.put(path[1], hash);
    }
}

class SWODataDatabaseTransactionMovedDirectory implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = -5820994195352641142L;

    String[] path;
    
    private SWODataDatabaseTransactionMovedDirectory(){};
    
    public SWODataDatabaseTransactionMovedDirectory(String[] paths){
        path = paths;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        ArrayList<String> toBeRemoved = new ArrayList<>();
        for(String s: p.indexes.keySet()){
            if(s.startsWith(path[0])){
                String newPath = s.replace(path[0], path[1]);
                String hash = p.indexes.get(s);
                SWOData copy = p.data.get(hash);
                if(copy!=null){
                    copy.deleteFile(Paths.get(s));
                    copy.addFile(Paths.get(newPath));
                    p.indexes.put(newPath, hash);
                }
            }
            toBeRemoved.add(s);
            
        }
        
        for(String s: toBeRemoved){
            p.indexes.remove(s);
        }
    }
}

class SWODataDatabaseTransactionPut implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = -5375474565129038666L;

    SWOData data;
    String path;
    
    private SWODataDatabaseTransactionPut(){};
    
    public SWODataDatabaseTransactionPut(SWOData newData, String p){
        data = newData;
        path = p;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date){
        SWOData temp = p.data.get(data.getHash());      
        if(temp!=null)
            temp.addFile(Paths.get(path));
        else
            p.data.put(data.getHash(), data);
        p.indexes.put(path, data.getHash());
        p.processedCreated.remove(path);
    }
}

class SWODataDatabaseTransactionUpdateTag implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = -6085516307055501584L;

    String hash;
    String[] tags;
    
    private SWODataDatabaseTransactionUpdateTag(){};
    
    public SWODataDatabaseTransactionUpdateTag(String h, String[] p){
        hash = h;
        tags = p;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date){
        p.data.get(hash).updateTags(tags);
    }
}

class SWODataDatabaseTransactionAddFileToData implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = -5046856486581004136L;

    String path;
    String hash;
    
    private SWODataDatabaseTransactionAddFileToData(){};
    
    public SWODataDatabaseTransactionAddFileToData(String h, String p){
        hash = h;
        path = p;
    }

    @Override
    public void executeOn(SWODataDatabase p, Date date){
        SWOData d = p.data.get(hash);
        if(d!=null)
        {
            d.addFile(Paths.get(path));
            p.indexes.put(path, hash);
            p.processedCreated.remove(path);
        }
        else{
            Logger.getLogger(SWODataDatabaseTransactionAddFileToData.class.getName()).log(Level.SEVERE,"Dodać pliku do wpisu - wpis nie istnieje");
        }
    }
}

class SWODataDatabaseTransactionClear implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 5147510567856097097L;

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        for(Entry<String, SWOData> e:p.removed.entrySet()){
            try{
                Files.delete(Paths.get(e.getValue().getThumbnail()));
            }
            catch(InvalidPathException | NullPointerException | IOException ex){}
        }
        p.removed.clear();
    }
}

class SWODataDatabaseTransactionClearAll implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = -9141489987449012091L;

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        p.data.clear();
        p.created.clear();
        p.indexes.clear();
        p.processedCreated.clear();
        p.removed.clear();
    }
}

class SWODataDatabaseTransactionRestartHappened implements Transaction<SWODataDatabase>{
    private static final long serialVersionUID = 1892416293040117151L;

    @Override
    public void executeOn(SWODataDatabase p, Date date) {
        p.created.addAll(p.processedCreated);
        p.processedCreated.clear();
    }
}

class SWODataDatabaseTransactionGetDatabase implements SureTransactionWithQuery<SWODataDatabase, List<SWOData>>{
    private static final long serialVersionUID = -9003888677403668481L;

    @Override
    public List<SWOData> executeAndQuery(SWODataDatabase p, Date date) {
        return new ArrayList<>(p.data.values());
    }
}

class SWODataDatabaseTransactionDoesExist implements SureTransactionWithQuery<SWODataDatabase, Boolean>{
    private static final long serialVersionUID = -1488270036811838382L;

    String hash;
    private SWODataDatabaseTransactionDoesExist(){}
    
    public SWODataDatabaseTransactionDoesExist(String s){
        hash = s;
    }
    
    @Override
    public Boolean executeAndQuery(SWODataDatabase p, Date date) {
        return p.data.containsKey(hash);
    }
}

class SWODataDatabaseTransactionRevertDelete implements SureTransactionWithQuery<SWODataDatabase, Boolean>{
    private static final long serialVersionUID = -5628385521363113038L;
    String hash;
    String path;
    
    private SWODataDatabaseTransactionRevertDelete(){};
    
    public SWODataDatabaseTransactionRevertDelete(String h, String p){
        hash = h;
        path = p;
    }
    
    @Override
    public Boolean executeAndQuery(SWODataDatabase p, Date date) {
        SWOData reverted = p.removed.get(hash);
        if(reverted != null){
            p.data.put(hash, reverted);
            p.removed.remove(hash);
            p.indexes.put(path, hash);
            p.processedCreated.remove(path);
            return true;
        }
        return false;
    }
}

public class SWODataDatabase implements Serializable{
    private static final long serialVersionUID = 4237634573088822800L;
    protected final Map<String, SWOData> data; //hash, swoData
    protected final List<String> created;
    protected final List<String> processedCreated;
    protected final Map<String, SWOData> removed; //hash, swoData
    protected final Map<String, String> indexes; //path, hash
    
    
    public SWODataDatabase(){
        data = new HashMap<>();
        removed = new HashMap<>();
        indexes = new HashMap<>();
        created = new ArrayList<>();
        processedCreated = new ArrayList<>();
    }
}
