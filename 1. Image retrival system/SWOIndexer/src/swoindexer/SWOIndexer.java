/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package swoindexer;

import dirwatcher.DirWatcher;
import dirwatcher.DirWatcherListener;
import dirwatcher.EventType;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import org.prevayler.Prevayler;
import org.prevayler.PrevaylerFactory;
import org.prevayler.foundation.serialization.JavaSerializer;

/**
 *
 * @author Rafal
 */
class NewOutputStream extends OutputStream{
    private final JTextArea area;
    private final PrintStream old;
    private final ByteArrayOutputStream baos;
    
    public NewOutputStream(PrintStream str){
        area = new JTextArea();
        area.setEditable(false);
        old = str;
        baos = new ByteArrayOutputStream();
    }
    
    public JTextArea getArea(){
        return area;
    }
    
    public void update(){
        try {
            area.append(baos.toString(Charset.defaultCharset().name()));
            area.setCaretPosition(area.getDocument().getLength());
            baos.reset();
        } catch (UnsupportedEncodingException ex) {
        }
    }

    @Override
    public void write(int i) throws IOException {
        old.write(i);
        baos.write(i);
        if(i==(int)'\n')
            update();
    }
    
}


public class SWOIndexer implements DirWatcherListener{
    private static SWOIndexer instance;
    private String dir;
    private String thumbnailPath;
    private PluginsManager plugins; 
    private Prevayler prevayler;
    private DirWatcher dirWatcher;
    private ArrayList<Path> ignoredPaths;
    private ArrayList<String> ignoredFiles;
    private DataAnalyzer analyzer;
    private PrintStream printStream;
    private NewOutputStream newOutputStream;
    private Properties properties;
    private MainWindow window;
    private int delayOfAnalyzer;
    
    private final String[] ignoredFilesArray;
    private final String[] ignoredPathsArray;
    
    public static SWOIndexer getInstance(){
        if(instance == null){
            instance = new SWOIndexer();
        }
        return instance;
    }
    
    private SWOIndexer(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        newOutputStream = new NewOutputStream(System.out);
        try {
            printStream = new PrintStream(newOutputStream, true, Charset.defaultCharset().name());
            System.setOut(printStream);
            System.setErr(printStream);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        ignoredPaths = new ArrayList<>();
        
        
        properties = new Properties();
        if(!readProperties()){
            writeProperties();
        }
        dir = properties.getProperty("directory");
        if(dir == null || (!new File(dir).exists()))
            dir = "";
        
        String delay = properties.getProperty("delayOfAnalyzer");
        if(delay != null && !delay.isEmpty()){
            try{
                delayOfAnalyzer = Integer.parseInt(delay);
                if(delayOfAnalyzer<0)
                    delayOfAnalyzer = 0;
            }catch(NumberFormatException e){
                delayOfAnalyzer = 0;
            }
        }
        else
            delayOfAnalyzer = 0;
        
        String ignoredFilesProperties = properties.getProperty("ignoredFiles");
        
        if(ignoredFilesProperties!=null){
            ignoredFilesArray = ignoredFilesProperties.split(";");
            ignoredFiles = new ArrayList<>(Arrays.asList(ignoredFilesArray));
        }
        else{
            ignoredFilesArray = null;
            ignoredFiles = new ArrayList<>();
        }
        
        ignoredPaths = new ArrayList<>();
        
        String ignoredPathsProperties = properties.getProperty("ignoredPaths");
        if(ignoredPathsProperties!=null){
            ignoredPathsArray = ignoredPathsProperties.split(";");
            for(String s:ignoredPathsArray){
                s = Paths.get(s).toAbsolutePath().toString();
                ignoredPaths.add(Paths.get(s));
            }
        }
        else{
            ignoredPathsArray = null;
        }
            
        
        thumbnailPath = new File(new File("").getAbsolutePath())+FileSystems.getDefault().getSeparator()+"SWOThumbnails"+FileSystems.getDefault().getSeparator();
        new File(thumbnailPath).mkdirs();
        String pluginsPath = new File(new File("").getAbsolutePath())+FileSystems.getDefault().getSeparator()+"plugins"+FileSystems.getDefault().getSeparator();
        new File(pluginsPath).mkdirs();

        plugins = new PluginsManager();
        plugins.init();
        
        try {
            PrevaylerFactory<SWODataDatabase> prevaylerFactory = new PrevaylerFactory<>();
            prevaylerFactory.configurePrevalentSystem(new SWODataDatabase());
            prevaylerFactory.configurePrevalenceDirectory("Database");
            JavaSerializer serializer = new JavaSerializer(plugins.getClassLoader());
            prevaylerFactory.configureJournalSerializer(serializer);
            prevaylerFactory.configureSnapshotSerializer(serializer);
            prevayler = prevaylerFactory.create();
            prevayler.execute(new SWODataDatabaseTransactionRestartHappened());
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.INFO, "Utworzono bazę danych");
        } catch (Exception ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, "Nie udało się zainicjować bazy danych", ex);
            JOptionPane.showMessageDialog(null, ex.getMessage());
            return;
        }
        
        analyzer = new DataAnalyzer(prevayler, plugins, thumbnailPath, delayOfAnalyzer);
        
        try{
            Registry registry = LocateRegistry.createRegistry(1099);
            registry.rebind("SWO", new RMIServer(prevayler));
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.INFO, "Udało się zainicjować serwer danych");
        }
        catch(RemoteException ex){
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, "Nie udało się zainicjować serwera danych");
        }
        
        
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                window = new MainWindow(newOutputStream.getArea());
                window.setPath(dir);
                if(ignoredFilesArray != null)
                    window.setIgnoredFiles(ignoredFilesArray);
                if(ignoredPathsArray != null)
                    window.setIgnoredPaths(ignoredPathsArray);
                window.getApplyButton().setEnabled(false);
                synchronized(SWOIndexer.getInstance()){
                    SWOIndexer.getInstance().notify();
                }
            }
        });
    }
    
    public String getDataTypes(){
        String result = "";
        for(String s:plugins.getDataTypes()){
            result+=s+" ";
        }
        return result;
    }
    
    public void show(){
        window.setVisible(true);
    }
    
    public String getPluginNames(){
        return plugins.getPluginsNames();
    }
    
    public boolean closePlugins(){
        return plugins.closeAllPlugins();
    }
    
    private boolean readProperties(){
        InputStream input = null;
        try {
            input = new  FileInputStream("config.properties");
        } catch (FileNotFoundException ex) {
        }
        
        if(input == null)
            input = SWOIndexer.class.getClassLoader().getResourceAsStream("config.properties");

        if(input!=null){
            try {
                properties.load(input);
                return true;
            } catch (IOException ex) {
            }
        }
        return false;
    }
    
    private void writeProperties(){
            properties.setProperty("directory", "");
            if(System.getProperty("os.name").toLowerCase().contains("win")){ //Tylko Windows
                 properties.setProperty("ignoredFiles", "Thumbs.db");
            }
            properties.setProperty("delayOfAnalyzer", "0");
            updateProperties();
    }
    
    private void updateProperties(){
        try {
            OutputStream output = new FileOutputStream("config.properties");
            
            properties.store(output, dir);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setDelay(int d){
        properties.setProperty("delayOfAnalyzer", Integer.toString(d));
        updateProperties();
        analyzer.setDelay(d);
    }
    
    public void clearDbHistory(){
        prevayler.execute(new SWODataDatabaseTransactionClear());
    }
    
    public void clearDbAll(){
        prevayler.execute(new SWODataDatabaseTransactionClearAll());
        File thumbDir = new File(thumbnailPath);
        for(File f: thumbDir.listFiles()){
            f.delete();
        }
        if(dirWatcher!=null)
            dirWatcher.clearAll();
    }
    
    public synchronized void updatePath(String s, String[] ignoredFilesArray, String[] ignoredPathsArray){
        properties.setProperty("directory", s);
        if(ignoredFilesArray != null){
            StringBuilder sb1 = new StringBuilder();
            for(String ignoredFile:ignoredFilesArray){
                sb1.append(ignoredFile+";");
            }
            if(sb1.length()!=0){
                sb1.deleteCharAt(sb1.length()-1);
                properties.setProperty("ignoredFiles",sb1.toString());
            }
        }
        else{
            properties.setProperty("ignoredFiles","");
        }
        
        
        ignoredPaths.clear();
        if(ignoredPathsArray!=null){
            StringBuilder sb2 = new StringBuilder();
            for(String ignoredPath:ignoredPathsArray){
                sb2.append(ignoredPath+";");
                ignoredPaths.add(Paths.get(ignoredPath));
            }
            if(sb2.length()!=0){
                sb2.deleteCharAt(sb2.length()-1);
                properties.setProperty("ignoredPaths",sb2.toString());
            }
        }
        else{
            properties.setProperty("ignoredPaths","");
        }
        updateProperties();
        dir = s;
	if(ignoredFilesArray!=null)
        	ignoredFiles = new ArrayList<>(Arrays.asList(ignoredFilesArray));
	else
		ignoredFiles = new ArrayList<>();
        clearDbAll();
        updateDirWatcher();
    }
    
    private synchronized void updateDirWatcher(){
        if(dirWatcher != null){
            dirWatcher.stop();
            dirWatcher.clearAll();
        }
        else{
            dirWatcher = new DirWatcher();
        }
        dirWatcher.initIgnored(ignoredPaths, ignoredFiles);
        dirWatcher.init((DirWatcherListener)this, Paths.get(dir), true, 3000);
        dirWatcher.start();
        
    }
    
    public boolean isPathIgnored(String s){
        return ignoredPaths.contains(Paths.get(s));
    }
    
    public synchronized boolean start(){
        if(!dir.isEmpty()){
            
            if(window == null){
                try {
                    this.wait();
                } catch (InterruptedException ex) {
                    Logger.getLogger(SWOIndexer.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
            }
            window.getStartButton().setEnabled(false);
            if(dirWatcher == null){
                dirWatcher = new DirWatcher();
                dirWatcher.initIgnored(ignoredPaths, ignoredFiles);
                dirWatcher.init((DirWatcherListener)this, Paths.get(dir), true, 3000);
                dirWatcher.start();
            }
            
            
            if(analyzer != null){
                analyzer.stop();
            }
            Thread runAnalyzer = new Thread(analyzer);
            runAnalyzer.start();
            window.getAnalyzeButton().setEnabled(true);
            window.getAnalyzeButton().setSelected(false);
            
            return true;
        }
        return false;
    }
    
    public synchronized boolean suspendAnalyzer(){
        if(analyzer.isRunning()){
            analyzer.stop();
            return false;
        }
        else{
            Thread runAnalyzer = new Thread(analyzer);
            runAnalyzer.start();
            return true;
        }
            
    }

    boolean isDataTypeConsidered(Path path){
        try {
            String content = Files.probeContentType(path);
            for(String s:plugins.getDataTypes()){
                if(content!=null && content.endsWith(s))
                    return true;
                else if(path.toString().endsWith(s)){
                    Logger.getLogger(SWOIndexer.class.getName()).log(Level.INFO, "Plik "+path.toString()+" został rozpoznany po rozszerzeniu a nie treści");
                    return true;
                }
            }
        } catch (IOException ex) {
        }
        return false;
    }
 
    @Override
    public void dirWatcherEventOccured(Map<String, dirwatcher.EventType> changes) {
        for(Map.Entry<String, EventType> e:changes.entrySet()){
            System.out.println(e.getKey() + " " + e.getValue()+" "+isDataTypeConsidered(Paths.get(e.getKey())));
            if(e.getValue().equals(EventType.CREATED)){
                if(isDataTypeConsidered(Paths.get(e.getKey())))
                    prevayler.execute(new SWODataDatabaseTransactionCreatedFile(e.getKey()));
            }
            else if(e.getValue().equals(EventType.DELETED))
                prevayler.execute(new SWODataDatabaseTransactionDeleted(e.getKey()));
            else if(e.getValue().equals(EventType.MODIFIED)){
                if(isDataTypeConsidered(Paths.get(e.getKey())))
                    prevayler.execute(new SWODataDatabaseTransactionModifiedFile(e.getKey()));
            }
            else{
                Logger.getLogger(PluginsManager.class.getName()).log(Level.SEVERE, "Otrzymano informację o przepełnieniu podczas analizy zdarzeń.");
            }
            Thread.yield();
        }
    }

    @Override
    public void dirWatcherEventOccured(List<String[]> renamed) {  
        for(String[] s: renamed){
            System.out.println(s[0] + " zmieniono na " + s[1]);
            
            File f = new File(s[1]);
            if(f.isDirectory())
                prevayler.execute(new SWODataDatabaseTransactionMovedDirectory(s));
            else if(isDataTypeConsidered(Paths.get(s[1])))
                prevayler.execute(new SWODataDatabaseTransactionMovedFile(s));
            else if(isDataTypeConsidered(Paths.get(s[0])))
                prevayler.execute(new SWODataDatabaseTransactionDeleted(s[0]));
            
            Thread.yield();
        }
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int port = 9999;
        try {
            ServerSocket socket = new ServerSocket(port,0,InetAddress.getByAddress(new byte[] {127,0,0,1}));
            socket.setReuseAddress(false);
        }
        catch (BindException | NullPointerException e) {
            JOptionPane.showMessageDialog(null, "Nie można uruchomić aplikacji SWOindexer.\nJest już uruchomiona jedna instancja.");
            System.err.println("Already running.");
            System.exit(1);
        }
        catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Nie można uruchomić aplikacji SWOindexer.");
            System.exit(2);
        }
        
        SWOIndexer indexer = SWOIndexer.getInstance();
        indexer.start();
    }
}
