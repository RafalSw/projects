/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swoindexer;

import SWOInterface.SWOData;
import SWOInterface.SWORemote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.List;
import java.util.Map;
import org.prevayler.Prevayler;

/**
 *
 * @author Rafal
 */
public class RMIServer extends UnicastRemoteObject implements SWORemote{
    private static final long serialVersionUID = -5591379753159759374L;
    private Prevayler prevayler;
    
    private RMIServer() throws RemoteException{}
    
    public RMIServer(Prevayler prev) throws RemoteException{
        prevayler = prev;
    }
        
    @Override
    public List<SWOData> getData() throws RemoteException{
        return (List<SWOData>)prevayler.execute(new SWODataDatabaseTransactionGetDatabase());
    }

    @Override
    public void updateTag(String hash, String[] tags) throws RemoteException {
        prevayler.execute(new SWODataDatabaseTransactionUpdateTag(hash, tags));
    }
    
    
}
