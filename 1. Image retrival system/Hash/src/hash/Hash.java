/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Rafal
 */
public class Hash {
    public Hash(){
        try{
            System.loadLibrary("SHA3");
        } catch(UnsatisfiedLinkError ex){
            JOptionPane.showMessageDialog(null, "Brak biblioteki SHA3\n"+ex);
	    Logger.getLogger(Hash.class.getName()).log(Level.SEVERE,null, ex);
            System.exit(-1);
        }
    }
    
    public native byte[] calculate(String path);
    public native byte[] calculate(byte[] data);
    
    public String calculateToStr(File file) throws HashException{
        if(file.exists() && !file.isDirectory()){
            String fileName = file.getAbsolutePath();
            byte[] bytes = calculate(fileName);
            
            if(bytes == null)
                throw new HashException("Nie można policzyć SHA3 dla pliku: "+file.getAbsolutePath());
            
            StringBuilder sb = new StringBuilder();
            for(byte b : bytes){
                sb.append(String.format("%02x", b&0xff));
            }
            return sb.toString();
        }
        else{
            throw new HashException("Plik nie istnieje: "+file.getAbsolutePath());
        }
    }
    
    public String calculateToStr(byte[] data){
        byte[] bytes = calculate(data);
        StringBuilder sb = new StringBuilder();
        for(byte b : bytes){
            sb.append(String.format("%02x", b&0xff));
        }
        return sb.toString();
    }
}
