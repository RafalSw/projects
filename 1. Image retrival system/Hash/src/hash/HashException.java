/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

/**
 *
 * @author Rafal
 */
public class HashException extends Exception{
        private static final long serialVersionUID = 8225671184515106978L;
        String message = null;
        public HashException(String s){
            message = s;
        }
        
        @Override
        public String toString(){
            return "Hash exception: " + message;
        }
    }