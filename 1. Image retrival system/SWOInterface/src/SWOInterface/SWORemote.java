/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SWOInterface;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

/**
 *
 * @author Rafal
 */
public interface SWORemote extends Remote{
    public List<SWOData> getData() throws RemoteException;
    public void updateTag(String hash, String[] tags) throws RemoteException;
}
