/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SWOInterface;
//import com.aliasi.spell.EditDistance;
//import com.aliasi.util.Distance;
import com.aliasi.spell.EditDistance;
import com.aliasi.util.Distance;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Rafal
 */


class SWOFile implements Serializable{
    private static final long serialVersionUID = -2814922757463837314L;
    public String path;
    public long lastModified;
    public long created;
    public long accessed;
    public long size;
    
    public SWOFile(Path p){
        update(p);
    }
    
    public final void update(Path p){
        path = p.toString();
        try {
            BasicFileAttributes attr = Files.readAttributes(p, BasicFileAttributes.class);
            lastModified = attr.lastModifiedTime().toMillis();
            created = attr.creationTime().toMillis();
            accessed = attr.lastAccessTime().toMillis();
            size = attr.size();
        } catch (IOException ex) {
            lastModified = Long.MIN_VALUE;
            created = Long.MIN_VALUE;
            accessed = Long.MIN_VALUE;
            size = Long.MIN_VALUE;
            Logger.getLogger(SWOFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isPathSame(String p){
        return path.equals(p);
    }
    
    public boolean isPathSame(Path p){
        return isPathSame(p.toString());
    }
    
    public String getName(){
        String result = Paths.get(path).getFileName().toString();
        int i = result.lastIndexOf(".");
        if(i>0)
            result = result.substring(0, i);
        return result;
    }
    
    public String getExtansion(){
        int i = path.lastIndexOf(".");
        String result;
        if(i>0)
            result = path.substring(i+1).toLowerCase();
        else
            result = null;
        return result;
    }
}

public class SWOData implements Serializable{
    private static final long serialVersionUID = -6372681104994971896L;
    private String[] tags;
    private final List<SWOFile> files;
    private String thumbnail;
    private String hash;
    
    public SWOData(){
        files = new ArrayList<>();
    }
    
    public synchronized boolean setHash(String s){
        if(hash == null){
            hash = s;
            return true;
        }
        return false;
    }
    
    public synchronized String getHash(){
        return hash;
    }
    
    public synchronized void setThumbnail(String s){
        thumbnail = s;
    }
    
    public synchronized String getThumbnail(){
        return thumbnail;
    }
    
    public synchronized void addFile(String p){
        addFile(Paths.get(p));
    }
    
    public synchronized void addFile(Path p){
        if(containsPath(p)){
            updatePath(p,p);
        }
        else{
            SWOFile newFile = new SWOFile(p);
            files.add(newFile);
        }
    }
    
    public synchronized void deleteFile(Path old){
        List<SWOFile> toBeRemoved = new ArrayList<>();
        if(containsPath(old)){
            for(SWOFile f:files){
                if(Paths.get(f.path).equals(old))
                    toBeRemoved.add(f);
            }
        }
        
        for(SWOFile f:toBeRemoved){
            files.remove(f);
        }
    }
    
    public synchronized Path[] getAllPaths(){
        Path[] result = new Path[files.size()];
        for(int i=0; i<files.size(); i++){
            result[i] = Paths.get(files.get(i).path);
        }
        return result;
    }
    
    public boolean containsPath(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return true;
        }
        return false;
    }
    
    public synchronized void updatePath(Path old, Path newPath){
        for(SWOFile f:files){
            if(f.isPathSame(old)){
                f.update(newPath);
            }
        }
    }
    
    public synchronized String[] getTags(){
        return tags;
    } 
    
    public synchronized void updateTags(String[] tag){
        tags = tag;
    }
    
    public synchronized Path[] containsFileName(String name){
        List<Path> temp = new ArrayList<>();
        for(SWOFile f:files){
            if(f.getName().equals(name)){
                temp.add(Paths.get(f.path));
            }
        }
        Path[] result = new Path[temp.size()];
        temp.toArray(result);
        return result;
    }
    
    public synchronized Path[] containsExtension(String ext){
        List<Path> temp = new ArrayList<>();
        for(SWOFile f:files){
            if(f.getExtansion().equals(ext.toLowerCase())){
                temp.add(Paths.get(f.path));
            }
        }
        Path[] result = new Path[temp.size()];
        temp.toArray(result);
        return result;
    }
    
    public synchronized Path[] getFilesByValues(long minCreated, long maxCreated, long minModified, long maxModified, long minAccessed, long maxAccessed, long minSize, long maxSize){
        List<Path> temp = new ArrayList<>();
        
        for(SWOFile f:files){
            if(minCreated != Long.MIN_VALUE && f.created<minCreated)
                continue;
            
            if(maxCreated != Long.MIN_VALUE && f.created>maxCreated)
                continue;
            
            if(minModified != Long.MIN_VALUE && f.lastModified<minModified)
                continue;
            
            if(maxModified != Long.MIN_VALUE && f.lastModified>maxModified)
                continue;
            
            if(minAccessed != Long.MIN_VALUE && f.accessed<minAccessed)
                continue;
            
            if(maxAccessed != Long.MIN_VALUE && f.accessed>maxAccessed)
                continue;
            
            if(minSize != Long.MIN_VALUE && f.size<minSize)
                continue;
            
            if(maxSize != Long.MIN_VALUE && f.size>maxSize)
                continue;
            
            temp.add(Paths.get(f.path));
        }
        
        Path[] result = new Path[temp.size()];
        temp.toArray(result);
        return result;
    }
    
    public synchronized String getFileName(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return f.getName();
        }
        return null;
    }
    
    public synchronized String getFileExtension(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return f.getExtansion();
        }
        return null;
    }
    public synchronized long getFileCreated(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return f.created;
        }
        return Long.MIN_VALUE;
    }
    public synchronized long getFileModified(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return f.lastModified;
        }
        return Long.MIN_VALUE;
    }
    public synchronized long getFileSize(Path p){
        for(SWOFile f:files){
            if(f.isPathSame(p))
                return f.size;
        }
        return Long.MIN_VALUE;
    }
    
    
    public double compareTo(SWOData data){
        if(tags==null || tags.length ==0){
            return 0;
        }
        if(data.tags == null)
            return 1;
        else
        {
            Distance<CharSequence> D1 = new EditDistance(true);
            double result1=2, result2=3;
            {
                ArrayList<String> ss1 = new ArrayList<>();
                for(String s:tags){
                    String[] values = s.split("\\s");
                    for(String ss:values)
                        if(ss.length()>3)
                            ss1.add(ss);
                }
                ArrayList<String> ss2 = new ArrayList<>();
                for(String s:data.tags){
                    String[] values = s.split("\\s");
                    for(String ss:values)
                        if(ss.length()>3)
                            ss2.add(ss);
                }

                double sum = 0;
                int matchedSize = 0;
                int counter = 0;
                int s1Sum = 0;
                
                for(String s:ss1){
                    double tempSum = 0;
                    for(String ss:ss2){
                        double dist = D1.distance(s, ss);
                        dist=dist/(s.length()+ss.length());
                        //System.out.print("<"+dist+">");
                        tempSum+=dist;
                        if(dist == 0){
                            matchedSize = s.length();
                            counter++;
                            tempSum = 0;
                            break;
                        }
                    }
                    sum+=tempSum;
                    s1Sum+=s.length();
                }
                sum/=ss1.size()+ss2.size();

                result1 = sum*((double)(s1Sum-matchedSize)/s1Sum)*((double)(ss1.size()-counter)/ss1.size());
            }
            {
                double sum = 0;
                int matchedSize = 0;
                int counter = 0;
                int s1Sum = 0;
                for(String s:tags){
                    double tempSum = 0;
                    for(String ss:data.tags){
                        double dist = D1.distance(s, ss);
                        dist=dist/(s.length()+ss.length());
                        tempSum+=dist;
                        if(dist == 0){
                            matchedSize = s.length();
                            counter++;
                            tempSum = 0;
                            break;
                        }
                    }
                    sum+=tempSum;
                    s1Sum+=s.length();
                }
                sum/=tags.length+data.tags.length;

                result2 = sum*((double)(s1Sum-matchedSize)/s1Sum)*((double)(tags.length-counter)/tags.length);
            }
            
            return Math.pow(result2*result1,(double)1/2);
        }
    }
}
