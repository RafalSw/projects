package SWOInterface;

import java.nio.file.Path;

/**
 *
 * @author Rafal
 */
public interface SWOInterface {
    public String[] getFileTypes();
    public boolean initialize();
    public SWOData calculate(Path path);
    public SWOData calculate(Path path, String hash);
    public boolean close();
    public String getAdditionalInfo(SWOData data);
}
