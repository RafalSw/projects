#define _CRT_SECURE_NO_WARNINGS
#include <cstdlib>


#include <string>
#include <cstdio>


#include "keccak.h"
#include "hash.h"

namespace{
	const int HASH_SIZE=32; // daje SHA-256
}

JNIEXPORT jbyteArray JNICALL Java_hash_Hash_calculate__Ljava_lang_String_2(JNIEnv * env, jobject obj, jstring path)
{
	FILE* file;
	uint8_t* buffer;
	long len;
	uint8_t calculatedHash[HASH_SIZE];
	jbyteArray result;

#if defined(_WIN32) || defined(_WIN64)
	const jchar *nativeString = env->GetStringChars(path, JNI_FALSE);
	int fileNameLen = env->GetStringLength(path)+1;
	wchar_t *fileName=new wchar_t[fileNameLen];
	wcsncpy(fileName,(wchar_t*)nativeString, fileNameLen-1);
	env->ReleaseStringChars(path, nativeString);
	fileName[fileNameLen-1]='\0';
	file = _wfopen(fileName, L"rb");
	if(file == NULL)
	{
		printf("SHA3: Biblioteka SHA3 nie mo�e odczyta� pliku %s\n", fileName);
		delete fileName;
		return NULL;
	}
	delete fileName;
#else
	const char *nativeString = env->GetStringUTFChars(path, JNI_FALSE);
	int fileNameLen = env->GetStringLength(path)+1;
	char *fileName=new char[fileNameLen];
	strcpy(fileName, nativeString);
	file = fopen(nativeString, "rb");
	env->ReleaseStringUTFChars(path, nativeString);
	if(file == NULL)
	{
		printf("SHA3: Biblioteka SHA3 nie mo�e odczyta� pliku %s\n", fileName);
		delete fileName;
		return NULL;
	}
	delete fileName;
#endif

	fseek(file, 0, SEEK_END);
	len = ftell(file);
	rewind(file);

	buffer = (uint8_t*)malloc((len+1)*sizeof(uint8_t));
	fread(buffer, len, 1, file);
	fclose(file);
	
	
	if(keccak(buffer, len, calculatedHash, HASH_SIZE) != 0)
	{
		printf("SHA3: Biblioteka SHA3 nie moze policzy� wartosci hash.\n");
		return NULL;
	}
	free(buffer);
	result = env->NewByteArray(HASH_SIZE);
	env->SetByteArrayRegion(result, 0, HASH_SIZE, (jbyte*)calculatedHash);
	return result;
}

JNIEXPORT jbyteArray JNICALL Java_hash_Hash_calculate___3B(JNIEnv * env, jobject obj, jbyteArray data)
{	
	uint8_t calculatedHash[HASH_SIZE];
	jbyte* buffer;
	long len;
	jbyteArray result;

	len = env->GetArrayLength(data);
	buffer = env->GetByteArrayElements(data, JNI_FALSE);

	if(keccak((uint8_t*)buffer, len, calculatedHash, HASH_SIZE) != 0)
	{
		printf("SHA3: Nie mo�na wykona�. %x %l\n",buffer,len);
		return NULL;
	}
	
	env->ReleaseByteArrayElements(data, buffer, JNI_ABORT);

	result = env->NewByteArray(HASH_SIZE);
	env->SetByteArrayRegion(result, 0, HASH_SIZE, (jbyte*)calculatedHash);
	return result;
}
