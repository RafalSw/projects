/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

import SWOInterface.SWOData;
import SWOInterface.SWOInterface;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import simplifieddirwatcher.EventType;
import simplifieddirwatcher.SimplifiedDirWatcher;
import simplifieddirwatcher.SimplifiedDirWatcherListener;

/**
 *
 * @author Rafal
 */

class PluginsClassLoader extends ClassLoader{
    private final List<ClassLoader> classLoaders;
    
    public PluginsClassLoader(){
        classLoaders = new ArrayList<>();
        classLoaders.add(getClass().getClassLoader());
    }
    
    public void add(ClassLoader clsldr){
        if(!classLoaders.contains(clsldr)){
            classLoaders.add(clsldr);
        }
    }
    
    @Override
    public Class<?> loadClass(String name) throws ClassNotFoundException{
        for(ClassLoader clsldr: classLoaders){
            try{
                return clsldr.loadClass(name);
            }
            catch(ClassNotFoundException ex){}
        }
        throw new ClassNotFoundException();
    }
    
}

public class PluginsManager implements SimplifiedDirWatcherListener {
    private Path pluginDir;
    private final List<String> dataTypes;
    private final List<SWOInterface> plugins;
    private final Map<SWOInterface, String> pluginsNames;
    private final Map<String, SWOInterface> pluginByFile;
    private final Map<String, SWOInterface> pluginByType;
    private final SimplifiedDirWatcher simplifiedDirWatcher;
    private final PluginsClassLoader classLoader;
    
    public PluginsManager(){
        dataTypes = new ArrayList<>();
        pluginByFile = new ConcurrentHashMap<>();
        pluginByType = new ConcurrentHashMap<>();
        pluginsNames = new ConcurrentHashMap<>();
        List<SWOInterface> tempPlugins = new ArrayList<>();
        plugins = Collections.synchronizedList(tempPlugins);
        simplifiedDirWatcher = new SimplifiedDirWatcher();
        classLoader = new PluginsClassLoader();
    }
    
    public ClassLoader getClassLoader(){
        return classLoader;
    }
    
    public void init(){
        pluginDir=FileSystems.getDefault().getPath("plugins").toAbsolutePath();
        simplifiedDirWatcher.init(this, pluginDir);
        simplifiedDirWatcher.start();
    }
    
    public List<String> getDataTypes(){
        return dataTypes;
    }
    
    public boolean closeAllPlugins(){
        boolean result = true;
        for(SWOInterface p:plugins){
            if(p.close()==false){
                result = false;
            }
        }
        return result;
    }
    
    public String getPluginsNames(){
        String result="";
        for(String s:pluginsNames.values()){
            result+=s+"\n";
        }
        return result;
    }
    
    public String getAdditionalInfo(SWOData d){
        String result = "";
        for(SWOInterface interf: plugins){
            result += pluginsNames.get(interf)+":\n------------------\n"+interf.getAdditionalInfo(d)+"\n------------------\n\n";
        }
        return result;
    }
    
    public SWOInterface getPluginByType(String type){
        return pluginByType.get(type.toLowerCase());
    }
    
    private synchronized void refreshDateTypes(){
        pluginByType.clear();
        dataTypes.clear();
        for(SWOInterface s:plugins){
            for(String str:s.getFileTypes()){
                str = str.toLowerCase();
                pluginByType.put(str, s);
                if(!dataTypes.contains(str)){
                    dataTypes.add(str);
                }
            }
        }
    }
    
    
    private boolean addPlugin(Path pluginFile){
        String fileName = pluginFile.toString();
        
        if(!fileName.endsWith(".jar")){
            Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Plugin \""+fileName+"\" nie jest obsługiwany: jedynie pliki *.jar");
            return false;
        }
        
        try{
            System.out.println(fileName);
            JarFile jarFile = new JarFile(fileName);
            Manifest manifest = jarFile.getManifest();
            String className = manifest.getMainAttributes().getValue("Main-Class");
            String className2;
            if(className.lastIndexOf(".")!= -1){
                className2 = className.substring(className.lastIndexOf(".")+1);
            }
            else{
                className2 = className;
            }
            
            ClassLoader tempClassLoader = new URLClassLoader(new URL[]{pluginFile.toUri().toURL()}, getClass().getClassLoader());
            
            Class myClass = null;
            try{
                myClass = Class.forName(className, true, tempClassLoader);
            }catch (ClassNotFoundException ex) {
                try {
                    myClass = Class.forName(className2, true, tempClassLoader);
                } catch (ClassNotFoundException ex1) {
                    Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Nie można załadować klasy "+className2+". Sprawdź czy nazywa się tak samo jak plik jar");
                }
            } 
            if(myClass!=null){
                Object obj = myClass.newInstance();
                if(!(obj instanceof SWOInterface)){
                    Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Klasa "+className2+" nie implementuje właściwego interfejsu");
                }
                else{
                    SWOInterface swoObject = (SWOInterface)obj;
                    if(swoObject.initialize()){
                        classLoader.add(tempClassLoader);
                        pluginsNames.put(swoObject, className2);
                        pluginByFile.put(pluginFile.toString(), swoObject);
                        plugins.add(swoObject);
                        refreshDateTypes();
                        Logger.getLogger(PluginsManager.class.getName()).log(Level.INFO, "Załadowano plugin "+className2);
                        return true;
                    }
                    else{
                        Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Nie udało się zainicjować pluginu "+ className2);
                    }
                }
            }
        }
        catch (MalformedURLException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(PluginsManager.class.getName()).log(Level.SEVERE, "Nie można załadować pliku "+fileName, ex);
        } catch (IOException ex) {
            Logger.getLogger(PluginsManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    
    private synchronized boolean removePlugin(Path pluginFile){
        boolean result = true;
        SWOInterface swoObj = pluginByFile.get(pluginFile.toString());
        
        pluginsNames.remove(swoObj);
        if(swoObj == null){
            Logger.getLogger(PluginsManager.class.getName()).log(Level.SEVERE, "Do tego pliku nie był przypisany żaden plugin: "+pluginFile.getFileName().toString());
            return false;
        }
        swoObj.close();
        if(plugins.remove(swoObj) == false){
            
            Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Nie można usunąć pluginu z listy plugins dla "+pluginFile.getFileName().toString());
            result = false;
        }
        if(pluginByFile.remove(pluginFile.toString()) == null){
            Logger.getLogger(PluginsManager.class.getName()).log(Level.WARNING, "Nie można usunąć pluginu z listy pluginByFile dla "+pluginFile.getFileName().toString());
            result = false;
        }
        refreshDateTypes();
        return result;
    }
    
    private synchronized boolean validatePlugin(Path pluginFile, Path newPluginFile){
        if(newPluginFile == null){
            removePlugin(pluginFile);
            return addPlugin(pluginFile);
        }
        else{
            removePlugin(pluginFile);
            return addPlugin(newPluginFile);
        }
    }


    @Override
    public void simplifiedDirWatcherEventOccured(Map<String, EventType> changes) {
        for(Entry<String, EventType> e:changes.entrySet()){
            Path path = Paths.get(e.getKey());
            if(e.getValue().equals(EventType.CREATED))
                addPlugin(path);
            else if(e.getValue().equals(EventType.DELETED))
                removePlugin(path);
            else if(e.getValue().equals(EventType.MODIFIED)){
                validatePlugin(path, null);
            }
            else{
                Logger.getLogger(PluginsManager.class.getName()).log(Level.SEVERE, "Otrzymano informację o przepełnieniu podczas analizy zdarzeń.");
            }
        }
    }

    @Override
    public void simplifiedDirWatcherEventOccured(List<String[]> renamed) {
        for(String[] s: renamed){
            validatePlugin(Paths.get(s[0]), Paths.get(s[1]));
        }
    }
}
