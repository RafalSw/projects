/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

import SWOInterface.SWOData;
import java.awt.Cursor;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;

/**
 *
 * @author Rafal
 */
public class MainWindow extends javax.swing.JFrame {
    private List<SWOData> results;
    private SearchEngine searchEngine;
    private final DefaultListModel listModel;
    private static final String NO_IMAGE_INFO = "Brak miniatury";
    private final AdvancedSearch advancedSearch;
    private BufferedImage currentImage;
    /**
     * Creates new form MainWindow
     */
    public MainWindow() {
        setTitle("System Wyszukiwania Obrazów");
        initComponents();
        ImageIcon imic = new ImageIcon(getClass().getResource("/resources/blackLogo.png"));
        setIconImage(imic.getImage());
        
        jMenu2.addMenuListener(new MenuListener() {

            @Override
            public void menuSelected(MenuEvent e) {
                java.awt.EventQueue.invokeLater(new Runnable() {
                    public void run() {
                        new Info().setVisible(true);
                    }
                });
            }

            @Override
            public void menuDeselected(MenuEvent e) {
            }
            @Override
            public void menuCanceled(MenuEvent e) {
            }
        });
        
        //jLabel3.setText(NO_IMAGE_INFO);
        results = new ArrayList<>();
        listModel = new DefaultListModel();
        jList1.setModel(listModel);
        jList1.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if(!lse.getValueIsAdjusting()){
                    cleanResultView();
                    int index = ((JList)lse.getSource()).getSelectedIndex();
                    if(index == -1){
                        return;
                    }
                    Object o = results.get(((JList)lse.getSource()).getSelectedIndex());
                    SWOData data = (SWOData)o;
                    ImageIcon icon = null;
                    File f = new File(data.getThumbnail());
                    try {
                        BufferedImage img = ImageIO.read(f);
                        currentImage = img;
                        int imgWidth = img.getWidth();
                        int imgHeight = img.getHeight();
                        double ratio = (double)imgWidth/imgHeight;
                        if(imgWidth-jLabel3.getWidth()>imgHeight-jLabel3.getHeight())
                            icon = new ImageIcon(img.getScaledInstance(jLabel3.getWidth(), (int)(jLabel3.getWidth()/ratio), Image.SCALE_SMOOTH));
                        else
                            icon = new ImageIcon(img.getScaledInstance((int)(jLabel3.getHeight()*ratio), jLabel3.getHeight(), Image.SCALE_SMOOTH));
                        jLabel3.setIcon(icon);
                    } catch (IOException ex) {
                        System.out.println("Brak miniatury");
                    }
                    String tags = "";
                    if(data.getTags()!=null){
                        for(String s: data.getTags()){
                            tags+=s+", ";
                        }
                        tags=tags.substring(0,tags.length()-2);
                    }
                    jTextArea1.setText(tags);
                    
                    for(Path p:data.getAllPaths()){
                        jTabbedPane1.addTab(data.getFileName(p),new FileProperties(data,p));
                    }
                }
            }
        });
        setLocationRelativeTo(null);
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                 if(SWO.getInstance().getPluginsManager().closeAllPlugins()==false){
                        if(JOptionPane.showConfirmDialog(null, "Nie wszystkie wtyczki udało się zamknąć poprawnie. Czy na pewno chcesz wyjść?", "Problem z zamknięciem wtyczek", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION){
                            System.exit(0);
                        }
                    }
                    else{
                        System.exit(0);
                    }
            }
        });
        advancedSearch = new AdvancedSearch();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jButton4 = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Wyszukiwanie"));

        jLabel1.setText("Wzorzec:");

        jTextField1.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        jButton1.setText("...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setText("Słowa kluczowe:");

        jButton2.setText("Szukaj");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Opcje zaawansowane");
        jButton3.setToolTipText("");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton1))
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2, javax.swing.GroupLayout.DEFAULT_SIZE, 367, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jLabel2)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(jButton3)))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Wyniki"));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(300);

        jList1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jList1);

        jSplitPane1.setLeftComponent(jScrollPane1);

        jLabel3.setBackground(new java.awt.Color(255, 255, 255));
        jLabel3.setFont(new java.awt.Font("Calibri", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        jLabel3.setOpaque(true);

        jLabel5.setText("Podglad:");

        jLabel6.setText("Słowa kluczowe:");

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        jButton4.setText("Zapisz");
        jButton4.setEnabled(false);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jTabbedPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Pliki"));
        jTabbedPane1.setOpaque(true);

        jButton5.setText("Dodatkowe informacje");
        jButton5.setEnabled(false);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("+90");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("-90");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 388, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(138, 138, 138)
                                .addComponent(jButton7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton6)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addContainerGap(135, Short.MAX_VALUE))
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane2)
                            .addComponent(jButton5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 265, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addComponent(jScrollPane2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)))
                .addGap(9, 9, 9)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton5)
                    .addComponent(jButton6)
                    .addComponent(jButton7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 178, Short.MAX_VALUE))
        );

        jSplitPane1.setRightComponent(jPanel3);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        jMenu2.setText("Informacje");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jMenu2MouseReleased(evt);
            }
        });
        jMenu2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu2ActionPerformed(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenu2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu2ActionPerformed
        
    }//GEN-LAST:event_jMenu2ActionPerformed

    private void cleanResultView(){
        jLabel3.setIcon(null);
        jTextArea1.setText("");
        jTabbedPane1.removeAll();
    }
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        jList1.removeAll();
        results.clear();
        cleanResultView();
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        
        AdvancedSearchCriteria advanced = advancedSearch.getCriteria();
        if(advanced == null){
            JOptionPane.showMessageDialog(this, "Zaawansowane kryteria są nieprawidłowe.");
            return;
        }
        
        SearchCriteria sc = new SearchCriteria();
        sc.setAdvancedCriteria(advanced);
        
        if(!jTextField1.getText().isEmpty()){
            File f = new File(jTextField1.getText());
            if(f.exists() && f.isFile()){
                sc.setFile(f);
            }
        }
        if(!jTextField2.getText().isEmpty()){
            sc.setTags(jTextField2.getText().toLowerCase());
        }
        
        if(searchEngine != null){
            searchEngine.stop();
        }
        searchEngine = new SearchEngine(this, sc);
        new Thread(searchEngine).start();
        
    }//GEN-LAST:event_jButton2ActionPerformed

    private class TagsUpdater implements Runnable{
        String[] myTags;
        String hash;
        public TagsUpdater(String h, String tags){
            hash = h;
            myTags = tags.split(";|,");
        }
        @Override
        public void run() {
            try {
                Thread.currentThread().setContextClassLoader(SWO.getInstance().getClassLoader());
                SWO.getInstance().getRmiService().updateTag(hash, myTags);
            } catch (RemoteException ex) {
                Logger.getLogger(MainWindow.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Nie udało się zapisać nowych słów kluczowych.");
            }
        }
    }
    
    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        SWOData data = (SWOData)results.get(jList1.getSelectedIndex());
        String tags = jTextArea1.getText().toLowerCase();
        new Thread(new TagsUpdater(data.getHash(),tags)).start();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        advancedSearch.setLocationRelativeTo(this);
        advancedSearch.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Wybierz lokalizację pliku");
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setAcceptAllFileFilterUsed(false);
        if( fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION){
            jTextField1.setText(fileChooser.getSelectedFile().getAbsolutePath());
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        String info = SWO.getInstance().getPluginsManager().getAdditionalInfo(results.get(jList1.getSelectedIndex()));
        if(info == null || info.isEmpty())
            info = "Brak dodatkowych informacji.";
        JOptionPane.showMessageDialog(this, info , "Dodatkowe informacje:",JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if(currentImage!=null){
            BufferedImage rotated = new BufferedImage(currentImage.getHeight(), currentImage.getWidth(), currentImage.getType());
            Graphics2D graphics = (Graphics2D) rotated.getGraphics();
            graphics.rotate(Math.toRadians(90), rotated.getWidth() / 2, rotated.getHeight() / 2);
            graphics.translate((rotated.getWidth() - currentImage.getWidth()) / 2, (rotated.getHeight() - currentImage.getHeight()) / 2);
            graphics.drawImage(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), null);
            currentImage = rotated;

            ImageIcon icon = null;
            int imgWidth = rotated.getWidth();
            int imgHeight = rotated.getHeight();
            double ratio = (double)imgWidth/imgHeight;
            if(imgWidth-jLabel3.getWidth()>imgHeight-jLabel3.getHeight())
                icon = new ImageIcon(rotated.getScaledInstance(jLabel3.getWidth(), (int)(jLabel3.getWidth()/ratio), Image.SCALE_SMOOTH));
            else
                icon = new ImageIcon(rotated.getScaledInstance((int)(jLabel3.getHeight()*ratio), jLabel3.getHeight(), Image.SCALE_SMOOTH));
            jLabel3.setIcon(icon);
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        if(currentImage!=null){
            BufferedImage rotated = new BufferedImage(currentImage.getHeight(), currentImage.getWidth(), currentImage.getType());
            Graphics2D graphics = (Graphics2D) rotated.getGraphics();
            graphics.rotate(Math.toRadians(270), rotated.getWidth() / 2, rotated.getHeight() / 2);
            graphics.translate((rotated.getWidth() - currentImage.getWidth()) / 2, (rotated.getHeight() - currentImage.getHeight()) / 2);
            graphics.drawImage(currentImage, 0, 0, currentImage.getWidth(), currentImage.getHeight(), null);
            currentImage = rotated;

            ImageIcon icon = null;
            int imgWidth = rotated.getWidth();
            int imgHeight = rotated.getHeight();
            double ratio = (double)imgWidth/imgHeight;
            if(imgWidth-jLabel3.getWidth()>imgHeight-jLabel3.getHeight())
                icon = new ImageIcon(rotated.getScaledInstance(jLabel3.getWidth(), (int)(jLabel3.getWidth()/ratio), Image.SCALE_SMOOTH));
            else
                icon = new ImageIcon(rotated.getScaledInstance((int)(jLabel3.getHeight()*ratio), jLabel3.getHeight(), Image.SCALE_SMOOTH));
            jLabel3.setIcon(icon);
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jMenu2MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseReleased
        
    }//GEN-LAST:event_jMenu2MouseReleased
    
    public synchronized void updateResults(List<SWOData> data, boolean isDatabaseEmpty){
        results = data;
        listModel.removeAllElements();
        for(SWOData d: results){
            if(d.getAllPaths().length==1)
                listModel.addElement(d.getAllPaths()[0].toString());
            else if(d.getAllPaths().length>1 && d.getAllPaths().length<5 )
                listModel.addElement(d.getAllPaths()[0].toString() +" +"+(d.getAllPaths().length-1)+" inne");
            else
                listModel.addElement(d.getAllPaths()[0].toString() +" +"+(d.getAllPaths().length-1)+" innych");
        }
        if(!listModel.isEmpty()){
            jList1.setSelectedIndex(0);
            jButton4.setEnabled(true);
            jButton5.setEnabled(true);
        }
        else{
            if(isDatabaseEmpty == true )
                JOptionPane.showMessageDialog(this, "Baza danych jest pusta!");
            else
                JOptionPane.showMessageDialog(this, "Brak wyników. Spróbuj zmienić kryteria.");
        }
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JList jList1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
