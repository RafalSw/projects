/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

import SWOInterface.SWORemote;
import hash.Hash;
import java.io.File;
import java.nio.file.FileSystems;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Rafal
 */

public class SWO {
    private static SWO instance;
    private PluginsManager plugins; 
    private SWORemote service;
    private Hash hash;
    
    private SWO(){
        hash = new Hash();
        String pluginsPath = new File(new File("").getAbsolutePath())+FileSystems.getDefault().getSeparator()+"plugins"+FileSystems.getDefault().getSeparator();
        new File(pluginsPath).mkdirs();
        plugins = new PluginsManager();
        plugins.init();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SWO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SWO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SWO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SWO.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        try {
            service = (SWORemote)LocateRegistry.getRegistry("localhost", 1099).lookup("SWO");
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    new MainWindow().setVisible(true);
                }
            });
        } catch (RemoteException | NotBoundException ex) {
            JOptionPane.showMessageDialog(null, "Nie można połączyć z usługą SWOIndexer\n\n"+ex.getMessage());
            System.exit(-1);
            //Logger.getLogger(SWO.class.getName()).log(Level.SEVERE, "Nie można połączyć z usługą SWOIndexer\n\n"+ex.getMessage());
        }
    }
    
    public ClassLoader getClassLoader(){
        return plugins.getClassLoader();
    }
    
    public SWORemote getRmiService(){
        return service;
    }
    
    public Hash getHash(){
        return hash;
    }
    
    public PluginsManager getPluginsManager(){
        return plugins;
    }
        
    public static SWO getInstance(){
        if(instance == null)
            instance = new SWO();
        return instance;
    }
    
    public String getDataTypes(){
        String result = "";
        for(String s:plugins.getDataTypes()){
            result+=s+" ";
        }
        return result;
    }
    
    
    public String getPluginNames(){
        return plugins.getPluginsNames();
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SWO.getInstance();
    }
}

/*        String version = System.getProperty("java.version");
        String bitnessStr = System.getProperty("sun.arch.data.model");

        System.out.println("Java version: "+version +" "+bitnessStr+"-bit");
        
        int position = version.indexOf('.');
        position = version.indexOf('.', position+1);
        double ver = Double.parseDouble (version.substring (0, position));
        
        if(ver<1.7){
            JOptionPane.showMessageDialog(null, "Aplikacja wymaga środowiska JAVA w wersji 7 lub wyższej");
            return;
        }
        
        try{
            //System.setProperty("java.security.policy", "file:."+FileSystems.getDefault().getSeparator()+"src"+FileSystems.getDefault().getSeparator()+"Policy.policy");

            //if(ver == 1.7)
                //System.setSecurityManager(new RMISecurityManager());
            //else
                //System.setSecurityManager(new SecurityManager());
            
            
        }
        catch(SecurityException e){
            JOptionPane.showMessageDialog(null, "Nie można uruchomić aplikacji SWO"+e);
            return;
        }
*/
