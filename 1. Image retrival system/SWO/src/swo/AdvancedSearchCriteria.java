/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

/**
 *
 * @author Rafal
 */
public class AdvancedSearchCriteria {
    private String[] extensions = null;
    private String lookupDir = null;
    private long createdMin = Long.MIN_VALUE;
    private long createdMax = Long.MIN_VALUE;
    private long modifiedMin = Long.MIN_VALUE;
    private long modifiedMax = Long.MIN_VALUE;
    private long sizeMin = Long.MIN_VALUE;
    private long sizeMax = Long.MIN_VALUE;
    private double precision = Double.MAX_VALUE;
    private int resultSize = Integer.MAX_VALUE;
    
    public void setPrecision(Double d){
        precision = d;
    }
    
    public Double getPrecision(){
        return precision;
    }
    
    public void setResultSize(int i){
        resultSize = i;
    }
    
    public int getResultSize(){
        return resultSize;
    }
    
    public void setExtensions(String[] e){
        extensions = e;
    }
    public String[] getExtensions(){
        return extensions;
    }
    public void setLookupDir(String s){
        lookupDir = s;
    }
    public String getLookupDir(){
        return lookupDir;
    }
    public void setCreatedMin(long l){
        createdMin = l;
    }
    public long getCreatedMin(){
        return createdMin;
    }
    public void setCreatedMax(long l){
        createdMax = l;
    }
    public long getCreatedMax(){
        return createdMax;
    }
    public void setModifiedMin(long l){
        modifiedMin = l;
    }
    public long getModifiedMin(){
        return modifiedMin;
    }
    public void setModifiedMax(long l){
        modifiedMax = l;
    }
    public long getModifiedMax(){
        return modifiedMax;
    }
    public void setSizeMin(long l){
        sizeMin = l;
    }
    public long getSizeMin(){
        return sizeMin;
    }
    public void setSizeMax(long l){
        sizeMax = l;
    }
    public long getSizeMax(){
        return sizeMax;
    }
}
