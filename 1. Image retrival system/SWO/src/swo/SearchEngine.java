/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

import SWOInterface.SWOData;
import hash.HashException;
import java.io.File;
import java.nio.file.Path;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Rafal
 */
class DataSet implements Comparable<DataSet>{
    private final double value;
    private final SWOData data;
    
    public DataSet(double v, SWOData d){
        value = v;
        data = d;
    }
    
    public SWOData getData(){
        return data;
    }
    
    public double getValue(){
        return value;
    }

    @Override
    public int compareTo(DataSet t) {
        if(t.value<value)
            return 1;
        else if(t.value==value)
            return 0;
        return -1;
    }
}

public class SearchEngine implements Runnable{
    private final List<DataSet> results;
    private final AtomicBoolean shouldRun;
    private final MainWindow mainWindow;
    private final SearchCriteria searchCriteria;
    private final AtomicBoolean updaterReady;
    private final AtomicBoolean shouldStopUpdating;
    private Thread updater;
    private final AtomicBoolean isDatabaseEmpty;
    
    SearchEngine(MainWindow mw, SearchCriteria c){
        results = Collections.synchronizedList(new ArrayList<DataSet>());
        shouldRun = new AtomicBoolean(true);
        updaterReady = new AtomicBoolean(true);
        isDatabaseEmpty = new AtomicBoolean(true);
        shouldStopUpdating = new AtomicBoolean(false);
        mainWindow = mw;
        searchCriteria = c;
    }
    
    private class ResultsAnalyzer implements Runnable{
        @Override
        public void run() {
            updaterReady.set(false);
            List<DataSet> copy;
            synchronized(results){
                copy = new ArrayList<>(results);
            }
            Collections.sort(copy);
            if(copy.size()>0 && copy.get(0).getValue() == copy.get(copy.size()-1).getValue())
                searchCriteria.getAdvancedCriteria().setResultSize(Integer.MAX_VALUE);
            List<SWOData> result = new ArrayList<>();
            for(DataSet d:copy){
                if(result.size()>=searchCriteria.getAdvancedCriteria().getResultSize() || d.getValue()>=searchCriteria.getAdvancedCriteria().getPrecision())
                    break;
                result.add(d.getData());
            }
            if(shouldStopUpdating.get() == false){
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    @Override
                    public void run() {
                        mainWindow.updateResults(result, isDatabaseEmpty.get());
                        updaterReady.set(true);
                    }
                });
            }
            
        }
    }
    /*
    private void analyzeResults(){
        if(updaterReady.get()==true){
            updater=new Thread(new ResultsAnalyzer());
            synchronized(updaterReady){
                updater.start();
            }
        }
    }*/
    private void analyzeFinalResults(){
        //updater.interrupt();
        updater=new Thread(new ResultsAnalyzer());
        updater.start();
    }
    
    public synchronized void stop(){
        shouldRun.set(false);
    }
    
    @Override
    public void run() {
        int numberOfRetrys = 3;
        while(numberOfRetrys>0 && shouldRun.get()==true){
            try {
                Thread.currentThread().setContextClassLoader(SWO.getInstance().getClassLoader());
                
                //pobieranie bazy
                List<SWOData> data = SWO.getInstance().getRmiService().getData();
                if(data.size()==0)
                    isDatabaseEmpty.set(true);
                else
                    isDatabaseEmpty.set(false);
                Iterator<SWOData> iterator = data.iterator();
                while(iterator.hasNext()){
                    SWOData d = iterator.next();
                    
                    //-------------LookupDir check
                    if(searchCriteria.getAdvancedCriteria().getLookupDir()!=null){
                        for(Path p:d.getAllPaths()){
                            if(!p.toString().startsWith(searchCriteria.getAdvancedCriteria().getLookupDir())){
                                d.deleteFile(p);
                            }
                        }
                    }
                    
                    //-------------Extension check
                    if(searchCriteria.getAdvancedCriteria().getExtensions()!=null){
                        List<Path[]> listOfPaths = new ArrayList<>();
                        for(String s:searchCriteria.getAdvancedCriteria().getExtensions()){
                            Path[] paths = d.containsExtension(s);
                            if(paths!=null &&paths.length>0){
                                listOfPaths.add(paths);
                            }
                        }
                        
                        for(Path p:d.getAllPaths()){
                            boolean result = false;
                            for(Path[] paths:listOfPaths){
                                for(Path path:paths){
                                    if(path.equals(p)){
                                        result = true;
                                        break;
                                    }
                                }
                                if(result == true)
                                    break;
                            }
                            
                            if(result ==false){
                                d.deleteFile(p);
                            }
                        }
                        
                    }
                    
                    //-----------values check
                    Path[] validDataPaths = d.getFilesByValues(
                            searchCriteria.getAdvancedCriteria().getCreatedMin(),
                            searchCriteria.getAdvancedCriteria().getCreatedMax(),
                            searchCriteria.getAdvancedCriteria().getModifiedMin(),
                            searchCriteria.getAdvancedCriteria().getModifiedMax(),
                            Long.MIN_VALUE,
                            Long.MIN_VALUE,
                            searchCriteria.getAdvancedCriteria().getSizeMin(),
                            searchCriteria.getAdvancedCriteria().getSizeMax());
                    
                    for(Path p:d.getAllPaths()){
                        boolean result = false;
                        for(Path path:validDataPaths){
                            if(path.equals(p)){
                                result = true;
                                continue;
                            }
                        }
                        if(result == false){
                            d.deleteFile(p);
                        }
                    }
                    if(d.getAllPaths().length == 0){
                        iterator.remove();
                    }
                }
                
                if(data.size()>0){
                    //analiza wzorca
                    SWOData pattern = null;
                    File f = searchCriteria.getFile();
                    if(f != null){
                        if(f.exists()){
                            String newHash;

                            try {
                                newHash = SWO.getInstance().getHash().calculateToStr(f);
                            } catch (HashException ex) {
                                Logger.getLogger(SearchEngine.class.getName()).log(Level.SEVERE, null, ex);
                                return;
                            }

                            String extension = f.getName().substring(f.getName().lastIndexOf(".")+1);
                            pattern = SWO.getInstance().getPluginsManager().getPluginByType(extension).calculate(f.toPath());
                            pattern.setHash(newHash);
                        }
                        else{
                            Logger.getLogger(SearchEngine.class.getName()).log(Level.SEVERE, "Nie można przanalizować pliku "+f.getAbsolutePath());
                        }
                    }

                    if(pattern==null){
                        pattern = new SWOData();
                    }

                    String[] tags = searchCriteria.getTags();
                    if(tags != null){
                        pattern.updateTags(tags);
                    }

                    for(SWOData d:data){
                        DataSet newSet = new DataSet(pattern.compareTo(d), (SWOData)d);
                        results.add(newSet);
                        //analyzeResults();
                    }
                }
                numberOfRetrys=0;
                analyzeFinalResults();
            } catch (RemoteException ex) {
                numberOfRetrys--;
                JOptionPane.showMessageDialog(mainWindow, "Nie można pobrać danych z bazy.\n\n"+ex);
                Logger.getLogger(SearchEngine.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
}
