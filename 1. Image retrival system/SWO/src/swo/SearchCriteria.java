/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package swo;

import java.io.File;

/**
 *
 * @author Rafal
 */
public class SearchCriteria {
    private File file = null;
    private String[] tags = null;
    private AdvancedSearchCriteria advancedCriteria;
        
    public void setFile(File f){
        file = f;
    }
    
    public File getFile(){
        return file;
    }
    
    public void setTags(String s){
        tags = s.split("[,;]");
    }
    
    public String[] getTags(){
        return tags;
    }
    
    public void setAdvancedCriteria(AdvancedSearchCriteria a){
        advancedCriteria = a;
    }
    
    public AdvancedSearchCriteria getAdvancedCriteria(){
        return advancedCriteria;
    }
}
