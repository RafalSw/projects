/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplifieddirwatcher;

import hash.Hash;
import hash.HashException;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */

class TestClass implements SimplifiedDirWatcherListener{
    @Override
    public void simplifiedDirWatcherEventOccured(Map<String, EventType> changes) {
        for(Map.Entry<String, EventType> e: changes.entrySet()){
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    @Override
    public void simplifiedDirWatcherEventOccured(List<String[]> renamed) {
        for(String[] s: renamed){
            System.out.println(s[0] + " zmieniono na " + s[1]);
        }
    }
}

public class SimplifiedDirWatcher {

    private static final int DEFAULT_TIMEOUT = 3000;
    private int time;
    private Path dir;
    private List<Path> ignored;
    private SimplifiedDirWatcherListener listener;
    private boolean isInitialized;
    private WatchService watchService;
    private final Map<String, EventType> changedSets; //<path, event>
    private Timer timer;
    private final List<String[]> renamed;
    private Hash hash;
    private final Map<String,String> fileHash; //<hash, path>
    private final AtomicBoolean shouldRun;
    private Thread watchThread;
    
    
    public SimplifiedDirWatcher(){
        isInitialized = false;
        changedSets = new HashMap<>();
        renamed = new ArrayList<>();
        fileHash = new HashMap<>();
        shouldRun = new AtomicBoolean(true);
    }
        
    public boolean init(SimplifiedDirWatcherListener obj, Path path){       
        return init(obj, path, new ArrayList<>());
    }

    public boolean init(SimplifiedDirWatcherListener obj, Path path, int timeout){
        return init(obj, path, timeout, new ArrayList<>());
    }
    
    public boolean init(SimplifiedDirWatcherListener obj, Path path, List<Path> ignoredPaths){
        return init(obj, path,DEFAULT_TIMEOUT, ignoredPaths);
    }
    
    public boolean init(SimplifiedDirWatcherListener obj, Path path, int timeout, List<Path> ignoredPaths){
        try{
            hash = new Hash();
        }
        catch(UnsatisfiedLinkError ex){
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE,"Brak biblioteki \"SHA3\"");
            return false;
        }
        
        if(obj == null){
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - przekazany obiekt obserwatora to null");
            return false;
        }
        
        if(!path.isAbsolute()){
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - ścieżka główna nie jest bezwzględna");
            return false;
        }
        
        try{
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException ex) {
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        for(Path p:ignoredPaths){
            if(!p.isAbsolute()){
                Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - ścieżki w liście ignorowanych nie są bezwzględne");
                return false;
            }
        }

        if(ignoredPaths == null){
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.WARNING,"Lista ścieżek ignorowanych to null");
            ignored = new ArrayList<>();
        }
        else{
            ignored = ignoredPaths;
        }
        
        if(timeout<0){
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.WARNING,"Podany czas zwłoki jest ujemny, przyjmuję 0");
            time = 0;
        }
        else{
            time = timeout;
        }

        dir = path;
        this.time = timeout;
        listener = obj;
        registerDir(dir);
        isInitialized = true;
        return true;
    }
    
    private synchronized void registerDir(Path dir){
        if(!ignored.contains(dir)){
            try {
                System.out.println("Zarejestrowano: "+dir.toString());
                for(File f:dir.toFile().listFiles()){
                    if(!f.isDirectory()){
                        String currentHash = hash.calculateToStr(f);
                        if(currentHash != null){
                            fileHash.put(currentHash, f.getAbsolutePath());
                        }
                        changedSets.put(f.getAbsolutePath(), EventType.CREATED);
                    }
                }
                dir.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE, OVERFLOW);
                if(!changedSets.isEmpty()){
                    listener.simplifiedDirWatcherEventOccured(changedSets);
                    changedSets.clear();
                }
            } catch (IOException ex) {
                Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
            } catch (HashException ex) {
                Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private void resetTimer(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                for(Entry<String, EventType> e:changedSets.entrySet()){
                    if(e.getValue().equals(EventType.CREATED)){
                        try {
                            String currentHash = hash.calculateToStr(new File(e.getKey()));
                            if(fileHash.containsKey(currentHash)){
                                String original = fileHash.get(currentHash);
                                renamed.add(new String[]{original, e.getKey()});
                            }
                            fileHash.put(currentHash, e.getKey());
                        } catch (HashException ex) {
                            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    else if(e.getValue().equals(EventType.DELETED)){
                        for(Entry<String, String> entryOfFileHash: fileHash.entrySet()){
                            if(entryOfFileHash.getValue().equals(e.getKey())){
                                fileHash.remove(entryOfFileHash.getKey());
                                break;
                            }
                        }
                    }
                    else if(e.getValue().equals(EventType.MODIFIED)){
                        for(Entry<String, String> entryOfFileHash: fileHash.entrySet()){
                            if(entryOfFileHash.getValue().equals(e.getKey())){
                                fileHash.remove(entryOfFileHash.getKey());
                                break;
                            }
                        }
                        try {
                            String currentHash = hash.calculateToStr(new File(e.getKey()));
                            fileHash.put(currentHash, e.getKey());
                        } catch (HashException ex) {
                            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
                
                if(!renamed.isEmpty()){
                    listener.simplifiedDirWatcherEventOccured(renamed);
                    renamed.clear();
                }
                if(!changedSets.isEmpty()){
                    listener.simplifiedDirWatcherEventOccured(changedSets);
                    changedSets.clear();
                }
            }
        }, time);
    }
    
    public void start(){
        watchThread = new Thread(new Runnable(){
                @Override
                public void run() {
                    if(!isInitialized){
                        Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE,"Nie można wywołać run() - obiekt nie został zainicjowany metodą init()");
                        return;
                    }

                    if(!changedSets.isEmpty()){
                        listener.simplifiedDirWatcherEventOccured(changedSets);
                        changedSets.clear();
                    }
                    
                    shouldRun.set(true);
                    

                    try {
                        while(shouldRun.get()){
                            WatchKey key=watchService.take();
                            for(WatchEvent<?> event: key.pollEvents()){
                                WatchEvent<Path> ev = (WatchEvent<Path>)event;

                                String pathStr = dir.toString()+FileSystems.getDefault().getSeparator()+ev.context().toString();
                                File f = new File(pathStr);
                                
                                if(!f.isDirectory()){
                                    if(event.kind()==ENTRY_CREATE){
                                        changedSets.put(pathStr, EventType.CREATED);
                                    }
                                    else if(event.kind()==ENTRY_MODIFY){
                                        if(changedSets.get(pathStr) != EventType.CREATED){
                                            changedSets.put(pathStr, EventType.MODIFIED);
                                        }
                                    }
                                    else if(event.kind()==ENTRY_DELETE){
                                        if(changedSets.get(pathStr) != EventType.CREATED){
                                            changedSets.put(pathStr, EventType.DELETED);
                                        }
                                    }
                                    else{
                                        Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.WARNING,"Przeciążenie!");
                                    }
                                }
                            }

                            boolean valid = key.reset();
                            if (!valid) {
                                break;
                            }

                            resetTimer();
                        }
                    } catch (InterruptedException ex) {
                    }
                }
    
        });
        watchThread.start();
    }
    
    public synchronized void pause(){
        if(watchThread != null){
            shouldRun.set(false);
            //watchThread.interrupt();
        }
    }
    
    public synchronized void stop(){
        pause();
        try {
            watchService.close();
        } catch (IOException ex) {
            Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
        Logger.getLogger(SimplifiedDirWatcher.class.getName()).log(Level.INFO, "SimplifiedDirWatch zakończył działanie dla "+dir.toString());
    }    
}
