/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package simplifieddirwatcher;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Rafal
 */
public interface SimplifiedDirWatcherListener {
    public void simplifiedDirWatcherEventOccured(Map<String, EventType> changes);
    public void simplifiedDirWatcherEventOccured(List<String[]> renamed);
}
