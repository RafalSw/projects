/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dirwatcher;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.DosFileAttributeView;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */

public class UniqueFileManager {
    public static String FILENAME = ".dirWatcherUQ";
    
    public static UniqueFile read(Path dir){
        UniqueFile file = null;
        try(
            InputStream inputFile = new FileInputStream(dir.toString()+FileSystems.getDefault().getSeparator()+FILENAME);
            InputStream inputBuffer = new BufferedInputStream(inputFile);
            ObjectInput input = new ObjectInputStream(inputBuffer);
        ){
            file = (UniqueFile)input.readObject();
        } catch (FileNotFoundException ex) {
            //Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            //Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        return file;
    }
    
    public static boolean write(Path dir, UniqueFile file){
        try (
            OutputStream outputFile = new FileOutputStream(dir.toString()+FileSystems.getDefault().getSeparator()+FILENAME);
            OutputStream outputBuffer = new BufferedOutputStream(outputFile);
            ObjectOutput output = new ObjectOutputStream(outputBuffer);
        ){
            file.files.clear();
            for(File f: dir.toFile().listFiles()){
                if(!f.isDirectory()){
                    try {
                        file.files.put(f.getName(), new FileAttributes(f.getAbsolutePath()));
                    } catch (IOException ex) {
                        Logger.getLogger(UniqueFileManager.class.getName()).log(Level.WARNING, null, ex);
                    }
                }
            }
            output.writeObject(file);
            /*File f = new File(dir.toString()+FileSystems.getDefault().getSeparator()+FILENAME);
            if(f.exists()){
                Path path = Paths.get(dir.toString()+FileSystems.getDefault().getSeparator()+FILENAME);
                DosFileAttributeView view = Files.getFileAttributeView(path,DosFileAttributeView.class);
                //view.setSystem(true);
                //view.setHidden(true);
            }*/
            return true;
            
        }catch (FileNotFoundException ex) {
            Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return false;
    }
    
    public static UUID write(Path dir){
        UniqueFile file = new UniqueFile();
        file.uuid = UUID.randomUUID();

        
        if(write(dir,file))
            return file.uuid;
        else
            return null;
    }
    
    public static List<String> getDeleted(Path path, UniqueFile file){
        if(file != null){
            List<String> deleted = new ArrayList<>();
            
            for(String s: file.files.keySet()){
                String filePath = path.toString()+FileSystems.getDefault().getSeparator()+s;
                File f = new File(filePath);
                if(!f.exists())
                    deleted.add(filePath);
            }
            return deleted;
        }
        return null;
    }
    
    public static List<String> getDeleted(Path path){
        UniqueFile file = read(path);
        return getDeleted(path, file);
    }
    
    public static List<String> getModified(Path path, UniqueFile file){
        if(file != null){
            List<String> modified = new ArrayList<>();
            
            for(String s: file.files.keySet()){
                String filePath = path.toString()+FileSystems.getDefault().getSeparator()+s;
                File f = new File(filePath);
                if(f.exists()){
                    try {
                        FileAttributes attr = new FileAttributes(f.toPath());
                        FileAttributes attr2 = file.files.get(f.getName());
                        if(!attr.equals(attr2))
                             modified.add(filePath);
                    } catch (IOException ex) {
                        Logger.getLogger(UniqueFileManager.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            return modified;
        }
        return null;
    }
    
    public static List<String> getModified(Path path){
        UniqueFile file = read(path);
        return getModified(path, file);
    }
    
    public static List<String> getNew(Path path, UniqueFile file){
        
        List<String> newFiles = new ArrayList<>();
        if(file != null){
            File dir = path.toFile();
            for(File f:dir.listFiles()){
                if(!file.files.containsKey(f.getName()) && !f.isDirectory()){
                    newFiles.add(f.getAbsolutePath());
                }
            }
        }
        return newFiles;
    }
    
    public static List<String> getNew(Path path){
        UniqueFile file = read(path);
        return getNew(path, file);
    }
}
