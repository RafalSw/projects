/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dirwatcher;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author Rafal
 */
public class UniqueFile implements Serializable{
    private static final long serialVersionUID = 8860378249522890634L;
    public UUID uuid;
    public Map<String, FileAttributes> files;
    
    public UniqueFile(){
        files = new HashMap<>();
    }
}
