/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dirwatcher;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rafal
 */
class FileAttributesReader{
    public static Long[] read(Path p){
        Long[] result = new Long[2];
        BasicFileAttributes attr;
        try {
            attr = Files.readAttributes(p, BasicFileAttributes.class);
            result[0] = attr.lastAccessTime().toMillis();
            result[1] = attr.size();
        } catch (IOException ex) {
            Logger.getLogger(FileAttributesReader.class.getName()).log(Level.SEVERE, null, ex);
            result[0] = -1L;
            result[1] = -1L;
        }
        return result;
    }
}
public class FileAttributes implements Serializable{
    private static final long serialVersionUID = -593609376916690999L;
    private final long lastModified;
    private final long size;
    
    public FileAttributes(String path) throws IOException{
        Path p = Paths.get(path);
        Long[] results = FileAttributesReader.read(p);
        lastModified = results[0];
        size = results[1];
    }
    
    public FileAttributes(Path p) throws IOException{
        Long[] results = FileAttributesReader.read(p);
        lastModified = results[0];
        size = results[1];
    }
    
    public long lastModifiedTime(){
        return lastModified;
    }
    public long size(){
        return size;
    }
    
    public boolean equals(FileAttributes f){
        if(f.lastModified != lastModified)
            return false;
        return f.size == size;
    }
}
