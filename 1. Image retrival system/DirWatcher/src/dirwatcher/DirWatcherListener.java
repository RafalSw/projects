/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dirwatcher;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Rafal
 */
public interface DirWatcherListener {
    public void dirWatcherEventOccured(Map<String, EventType> changes);
    public void dirWatcherEventOccured(List<String[]> renamed);
}
