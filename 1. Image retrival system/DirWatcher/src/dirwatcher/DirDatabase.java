/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dirwatcher;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import org.prevayler.SureTransactionWithQuery;
import org.prevayler.Transaction;

/**
 *
 * @author Rafal
 */
class DirDatabaseTransactionGet implements SureTransactionWithQuery<DirDatabase,String>{
    private static final long serialVersionUID = 4136597041114331158L;

    private UUID uuid;
    private String path = null;
    
    private DirDatabaseTransactionGet(){};
    
    public DirDatabaseTransactionGet(UUID u){
        uuid = u;
    }
    
    public String get(){
        return path;
    }
    
    @Override
    public String executeAndQuery(DirDatabase p, Date date){
        return p.getPath(uuid); 
    }
}

class DirDatabaseTransactionPut implements Transaction<DirDatabase>{
    private static final long serialVersionUID = -3581535884196451324L;

    private UUID uuid;
    private String path;
    
    private DirDatabaseTransactionPut(){};
    
    public DirDatabaseTransactionPut(UUID u, String p){
        uuid = u;
        path = p;
    }
    
    @Override
    public void executeOn(DirDatabase p, Date date) {
        p.put(uuid, path);
    }   
}

class DirDatabaseTransactionClear implements Transaction<DirDatabase>{
    private static final long serialVersionUID = -7240468222065452610L;

    @Override
    public void executeOn(DirDatabase p, Date date) {
        p.clear();
    }   
}

class DirDatabaseTransactionGetAll implements SureTransactionWithQuery<DirDatabase,Map<UUID,String>>{
    private static final long serialVersionUID = -2213317137060338904L;



    @Override
    public Map<UUID, String> executeAndQuery(DirDatabase prevalentSystem, Date executionTime) {
        return prevalentSystem.getAll();
    }
}


class DirDatabaseTransactionRemove implements Transaction<DirDatabase>{
    private static final long serialVersionUID = -198920035748359437L;

    private String path;
    
    private DirDatabaseTransactionRemove(){};
    
    public DirDatabaseTransactionRemove(String p){
        path = p;
    }
    
    @Override
    public void executeOn(DirDatabase p, Date date) {
        p.remove(path);
    }   
}
class DirDatabaseTransactionRemoveByUUID implements Transaction<DirDatabase>{
    private static final long serialVersionUID = -72025018876121183L;

    private UUID id;
    
    private DirDatabaseTransactionRemoveByUUID(){};
    
    public DirDatabaseTransactionRemoveByUUID(UUID u){
        id = u;
    }
    
    @Override
    public void executeOn(DirDatabase p, Date date) {
        p.remove(id);
    }   
}

public class DirDatabase implements Serializable{
    private static final long serialVersionUID = -2255882062375592455L;
    private final Map<UUID, String> pathsByUUID = new HashMap<>();
    
    public String getPath(UUID u){
        return pathsByUUID.get(u);
    }
    public Map<UUID, String> getAll(){
        return new HashMap<>(pathsByUUID);
    }
    
    public void put(UUID u, String p){
        pathsByUUID.put(u, p);
    }
    
    public void remove(String p){
        for(Entry<UUID, String> entry:pathsByUUID.entrySet()){
            if(entry.getValue().equals(p)){
                pathsByUUID.remove(entry.getKey());
                break;
            }
        }
    }
    
    public void remove(UUID u){
        pathsByUUID.remove(u);        
    }
    
    public void clear(){
        pathsByUUID.clear();
    }
}
