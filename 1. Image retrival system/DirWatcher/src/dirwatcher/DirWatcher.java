/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package dirwatcher;

import java.io.File;
import java.io.IOException;
import java.nio.file.ClosedWatchServiceException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_MODIFY;
import static java.nio.file.StandardWatchEventKinds.OVERFLOW;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.prevayler.Prevayler;
import org.prevayler.PrevaylerFactory;

/**
 *
 * @author Rafal
 */

class TestClass implements DirWatcherListener{

    @Override
    public void dirWatcherEventOccured(Map<String, EventType> changes) {
        for(Entry<String, EventType> e: changes.entrySet()){
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    @Override
    public void dirWatcherEventOccured(List<String[]> moved) {
        for(String[] s: moved){
            System.out.println(s[0] + " zmieniono na " + s[1]);
        }
    }
}

public class DirWatcher{
    private static final int DEFAULT_TIMEOUT = 3000;
    private int time;
    private Path dir;
    private List<Path> ignored;
    private List<String> ignoredFileNames;
    private boolean rec;
    private DirWatcherListener listener;
    private boolean isInitialized;
    private final Map<Path, WatchKey> keysByPath;
    private WatchService watchService;
    private final Map<String, EventType> changedSets;
    private Timer timer;
    private Prevayler prevayler;
    private final List<String[]> moved;
    private final AtomicBoolean shouldRun;
    private Thread watchThread;
    private boolean ignoredInitiated;
    private final int DEFAULT_SNAPSHOT_COUNTDOWN = 30;
    private int snapshotCountdown = DEFAULT_SNAPSHOT_COUNTDOWN;
    private static final Object sharedLock = new Object();
    
    
    public DirWatcher(){
        isInitialized = false;
        keysByPath = Collections.synchronizedMap(new HashMap<Path, WatchKey>());
        changedSets = Collections.synchronizedMap(new HashMap<String, EventType>());
        moved = Collections.synchronizedList(new ArrayList<String[]>());
        shouldRun = new AtomicBoolean(true);
        ignoredInitiated = false;
    }
    
    public synchronized void initIgnored(ArrayList<Path> ignoredPaths, ArrayList<String> ignoredFiles){
        if(ignoredPaths == null){
            Logger.getLogger(DirWatcher.class.getName()).log(Level.WARNING,"Lista ścieżek ignorowanych to null");
            ignored = new ArrayList<>();
        }
        else{
            ignored = ignoredPaths;
        }
        
        if(ignoredFiles == null){
            Logger.getLogger(DirWatcher.class.getName()).log(Level.WARNING,"Lista plików ignorowanych to null");
            ignoredFileNames = new ArrayList<>();
        }
        else{
            ignoredFileNames = ignoredFiles;
        }
        
        ignoredFileNames.add(UniqueFileManager.FILENAME);

        
        ignoredInitiated = true;
    }
    
    public synchronized boolean init(DirWatcherListener obj, Path path){       
        return init(obj, path, true, DEFAULT_TIMEOUT);
    }
    
    public synchronized boolean init(DirWatcherListener obj, Path path, boolean recursive){       
        return init(obj, path, recursive, DEFAULT_TIMEOUT);
    }
    
    public synchronized boolean init(DirWatcherListener obj, Path path, boolean recursive, int timeout){
        if(obj == null){
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - przekazany obiekt obserwatora to null");
            return false;
        }
        if(!path.isAbsolute()){
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - ścieżka główna nie jest bezwzględna");
            return false;
        }
        
        if(!ignoredInitiated){
            ignored = new ArrayList<>();
            ignoredFileNames = new ArrayList<>();
            ignoredFileNames.add(UniqueFileManager.FILENAME);
        }
        else{
            for(Path p:ignored){
                if(!p.isAbsolute()){
                    Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE,"Nie można zainicjować klasy - ścieżki w liście ignorowanych nie są bezwzględne");
                    return false;
                }
            }
        }
        try{
            watchService = FileSystems.getDefault().newWatchService();
        } catch (IOException ex) {
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
        if(timeout<0){
            Logger.getLogger(DirWatcher.class.getName()).log(Level.WARNING,"Podany czas zwłoki jest ujemny, przyjmuję 0");
            time = 0;
        }
        else{
            time = timeout;
        }
        
        try {
            prevayler = PrevaylerFactory.createPrevayler(new DirDatabase(), "DirWatcher");
        } catch (Exception ex) {
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, "Nie udało się zainicjować bazy danych");
        }
        
        
        if(!ignoredInitiated){
            ignored = new ArrayList<>();
            ignoredFileNames = new ArrayList<>();
            ignoredFileNames.add(UniqueFileManager.FILENAME);
        }
        
        dir = path;
        this.time = timeout;
        rec = recursive;
        listener = obj;
        registerDir(dir);
        updateAtStart();
        isInitialized = true;
        return true;
    }
    
    private void updateAtStart(){
        validatePath(dir);
        Map<UUID,String> allPaths = (Map<UUID,String>)prevayler.execute(new DirDatabaseTransactionGetAll());
        for(Entry<UUID,String> e:allPaths.entrySet()){
            File f = new File(e.getValue());
            if(!f.exists()){
                changedSets.put(e.getValue(),EventType.DELETED);
                prevayler.execute(new DirDatabaseTransactionRemoveByUUID(e.getKey()));
            }
                    
        }
        
        synchronized(moved){
            listener.dirWatcherEventOccured(new ArrayList<>(moved));
            moved.clear();
        }
        synchronized(changedSets){
            listener.dirWatcherEventOccured(new HashMap<>(changedSets));
            changedSets.clear();
        }
        try {
            prevayler.takeSnapshot();
        } catch (Exception ex) {
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private synchronized void validatePath(Path dir){
        if(ignored.contains(dir))
            return;
        
        String pathOfFile = dir.toString() + FileSystems.getDefault().getSeparator();
        
        UniqueFile file = UniqueFileManager.read(dir);
        if(file != null){
            //Sprawdzenie pod katem przeniesienia
            String originalPath = (String)prevayler.execute( new DirDatabaseTransactionGet(file.uuid));
            if(originalPath ==null){
                prevayler.execute(new DirDatabaseTransactionPut(file.uuid, dir.toString()));
            }
            else if(!originalPath.equals(dir.toString())){
                moved.add(new String[]{originalPath, dir.toString()});
                
                prevayler.execute(new DirDatabaseTransactionPut(file.uuid, dir.toString()));
            }
            
            for(String s:UniqueFileManager.getDeleted(dir, file)){
                if(!ignoredFileNames.contains(s.substring(pathOfFile.length()))){
                    changedSets.put(s,EventType.DELETED);
                }
            }
            for(String s:UniqueFileManager.getModified(dir, file)){
                if(!ignoredFileNames.contains(s.substring(pathOfFile.length()))){
                    changedSets.put(s,EventType.MODIFIED);
                }
            }
            for(String s:UniqueFileManager.getNew(dir, file)){
                if(!ignoredFileNames.contains(s.substring(pathOfFile.length()))){
                    changedSets.put(s,EventType.CREATED);
                }
            }
            UniqueFileManager.write(dir, file);
        }
        else{
            for(File f:dir.toFile().listFiles()){
                if(!f.isDirectory() && !ignoredFileNames.contains(f.getAbsolutePath().substring(pathOfFile.length())))
                    changedSets.put(f.getAbsolutePath(), EventType.CREATED);
            }
            
            UUID uuid = UniqueFileManager.write(dir);
            if(uuid != null){
                
                prevayler.execute(new DirDatabaseTransactionPut(uuid, dir.toString()));
            }
            else
                Logger.getLogger(DirWatcher.class.getName()).log(Level.WARNING, ("Nie udało się dodać ścieżki "+ dir.toString()+" do obserwowanych na stale"));
        }
        
        if(rec){
            for(File f:dir.toFile().listFiles()){
                if(f.isDirectory())
                    validatePath(f.toPath());
            }
        }
    }
    
    private synchronized Path pathByKey(WatchKey key){
        if(!keysByPath.containsValue(key))
            return null;
        for(Entry<Path,WatchKey> entry:keysByPath.entrySet()){
            if(key.equals(entry.getValue()))
                return entry.getKey();
        }
        
        return null; 
    }
    
    private void registerDir(Path dir){
        synchronized(sharedLock){
            if(!keysByPath.containsKey(dir) && !ignored.contains(dir)){
                try {
                    Logger.getLogger(DirWatcher.class.getName()).log(Level.INFO, "Zarejestrowano: "+dir.toString());
                    WatchKey newKey = dir.register(watchService, ENTRY_CREATE, ENTRY_MODIFY, ENTRY_DELETE, OVERFLOW);
                    keysByPath.put(dir, newKey);
                    if(rec && dir.toFile().isDirectory()){
                        for(File f: dir.toFile().listFiles()){
                            if(f.isDirectory())
                                registerDir(f.toPath());
                        }
                    }
                } catch (IOException ex) {
                    Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    
    private synchronized void resetTimer(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                HashSet<String> pathsToBeValidated = new HashSet<>();
                
                for(Entry<String, EventType> e:changedSets.entrySet()){
                    File f = new File(e.getKey());
                    if(f.isDirectory()){
                        if(e.getValue().equals(EventType.DELETED)){
                            synchronized(sharedLock){
                                WatchKey key = keysByPath.get(Paths.get(e.getKey()));
                                key.cancel();
                                keysByPath.remove(Paths.get(e.getKey()));
                                prevayler.execute(new DirDatabaseTransactionRemove(e.getKey()));
                            }
                        }
                        else if(e.getValue().equals(EventType.CREATED) || e.getValue().equals(EventType.MODIFIED)){
                            Path p = Paths.get(e.getKey());
                            registerDir(p);
                            pathsToBeValidated.add(p.toString());
                        }
                    }
                    else
                        pathsToBeValidated.add(dir.toString());
                    Thread.yield();
                }
                
                Iterator<String> iter = pathsToBeValidated.iterator();
                while(iter.hasNext()){
                    validatePath(Paths.get(iter.next()));
                    iter.remove();
                    Thread.yield();
                }
                
                
                
                try {
                    prevayler.takeSnapshot();
                } catch (Exception ex) {
                    Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
                }
                synchronized(moved){
                    if(!moved.isEmpty()){
                        listener.dirWatcherEventOccured(new ArrayList<>(moved));
                        moved.clear();
                    }
                }
                synchronized(changedSets){
                    if(!changedSets.isEmpty()){
                        listener.dirWatcherEventOccured(new HashMap<>(changedSets));
                        changedSets.clear();
                    }
                }
                snapshotCountdown--;
                if(snapshotCountdown==0){
                    try {
                        prevayler.takeSnapshot();
                    } catch (Exception ex) {
                        Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    snapshotCountdown = DEFAULT_SNAPSHOT_COUNTDOWN;
                }
            }
        }, time);
    }
    
    public void start(){
        watchThread = new Thread(new Runnable(){
            @Override
            public void run() {

                if(!isInitialized){
                    Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE,"Nie można wywołać run() - obiekt nie został zainicjowany metodą init()");
                    return;
                }
                
                shouldRun.set(true);

                try {
                    while(shouldRun.get()){
                        WatchKey key=watchService.take();
                        for(WatchEvent<?> event: key.pollEvents()){
                            @SuppressWarnings("unchecked")
                            WatchEvent<Path> ev = (WatchEvent<Path>)event;
                            if(ev.context().toString().equals(UniqueFileManager.FILENAME) && event.kind() == ENTRY_DELETE){
                                Path occurencePath = pathByKey(key);
                                validatePath(occurencePath);
                            }
                            
                            if(!ignoredFileNames.contains(ev.context().toString())){
                                Path occurencePath = pathByKey(key);

                                String pathStr = occurencePath.toString()+FileSystems.getDefault().getSeparator()+ev.context().toString();
                                if(occurencePath.equals(dir)|| new File(pathStr).isDirectory())
                                {
                                    synchronized(changedSets){
                                        if(event.kind()==ENTRY_CREATE){
                                            changedSets.put(pathStr, EventType.CREATED);
                                        }
                                        else if(event.kind()==ENTRY_MODIFY){
                                            if(changedSets.get(pathStr) != EventType.CREATED){
                                                changedSets.put(pathStr, EventType.MODIFIED);
                                            }
                                        }
                                        else if(event.kind()==ENTRY_DELETE){
                                            changedSets.put(pathStr, EventType.DELETED);
                                        }
                                        else{
                                            Logger.getLogger(DirWatcher.class.getName()).log(Level.WARNING,"Przeciążenie!");
                                        }
                                    }
                                }
                            }
                        }

                        boolean valid = key.reset();
                        if (!valid) {
                            break;
                        }
                        Thread.yield();
                        resetTimer();
                    }
                } catch (InterruptedException | ClosedWatchServiceException ex) {
                }
            }
        });
        watchThread.start();
    }
    
    public synchronized void pause(){
        if(watchThread != null){
            shouldRun.set(false);
            //watchThread.interrupt();
        }
    }
    
    private synchronized void clear(Path dir){
        File file = new File(dir.toString()+FileSystems.getDefault().getSeparator()+UniqueFileManager.FILENAME);
        if(file.exists())
            file.delete();
        
        if(rec){
            for(File f:dir.toFile().listFiles()){
                if(f.isDirectory())
                    clear(f.toPath());
            }
        }
    }
    
    public synchronized void clearAll(){
        clear(dir);
        prevayler.execute(new DirDatabaseTransactionClear());
        try {
            prevayler.takeSnapshot();
        } catch (Exception ex) {
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public synchronized void stop(){
        pause();
        Logger.getLogger(DirWatcher.class.getName()).log(Level.INFO, "DirWatch zakończył działanie dla "+dir.toString());
        try {
            watchService.close();
        } catch (IOException ex) {
            Logger.getLogger(DirWatcher.class.getName()).log(Level.SEVERE, null, ex);
        }
    }   
}
