/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filebackup;

import com.healthmarketscience.rmiio.RemoteInputStream;
import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author Rafal
 */
public interface BackUpInterface extends Remote{
    public int sendFile(RemoteInputStream inFile, String name) throws RemoteException;    
    public int acceptFile(String name) throws RemoteException;
    public RemoteInputStream requestFile(String name) throws RemoteException;
    public void changeDir(String dir) throws RemoteException;
    public int removeFile(String name) throws RemoteException;
    public String[] getFileList() throws RemoteException;
    public String[] getDirectoryList() throws RemoteException;
    public String getDir() throws RemoteException;
    public String checkFile(String name) throws RemoteException;
    public int registerUser(String name) throws RemoteException;
    public int unregisterUser(String name) throws RemoteException;
}
