/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filebackup;

import com.healthmarketscience.rmiio.GZIPRemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamClient;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;


import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.rmi.AlreadyBoundException;
import java.rmi.RMISecurityManager;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
/**
 *
 * @author Rafal
 */
public class FileBackupClient{

    /**
     * @param args the command line arguments
     */
    static BackUpInterface stub, stubc;
    String path;
    String name,ip;
    int port;
    Registry registry;
    boolean isReady=false;
    FileManagement fm;
    MainWindow mw;
    String user="Uzytkownik";
    boolean logged=false;
    
    int startClient(int ports, String names, String ips) throws RemoteException, AlreadyBoundException, NotBoundException{
        port=ports;
        name=names;
        ip=ips;
        registry= LocateRegistry.getRegistry(ip,port);
        stub = (BackUpInterface)registry.lookup(name);
        isReady=true;
        new MainWindow(this);
        return 0;
    }
    
    public static String sha1(final File file) {
        final MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA1");
            InputStream is = new BufferedInputStream(new FileInputStream(file));
          final byte[] buffer = new byte[1024];
          for (int read = 0; (read = is.read(buffer)) != -1;) {
            messageDigest.update(buffer, 0, read);
          }


        // Convert the byte to hex format
        try (Formatter formatter = new Formatter()) {
          for (final byte b : messageDigest.digest()) {
            formatter.format("%02x", b);
          }
          return formatter.toString();
        }
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(FileBackupClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FileBackupClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void removeF(String name) throws RemoteException{
        int i,j=0;
        do{
            i=stub.removeFile(name);
            j++;
        }while(i!=0&&j<5);
        if(j==5)
            JOptionPane.showMessageDialog(null, "Nie udało się skasować pliku!");
    }    
    
    private FileBackupClient() throws UnknownHostException{
        String temp=System.getProperty("java.io.tmpdir");
        File tempFile = new File(temp+"\\FBCconfig.dat");
        if(tempFile.exists()){
            try(Scanner in = new Scanner(tempFile)){
                ip=in.nextLine();
                port=Integer.parseInt(in.nextLine());
                name=in.nextLine();
                try{
                    path=in.nextLine();
                }
                catch(Exception e){
                    path=System.getProperty("user.dir");
                }
                
                in.close();
                registry= LocateRegistry.getRegistry(ip,port);
                stub = (BackUpInterface)registry.lookup(name);
                isReady=true;
                mw=new MainWindow(this);
                System.out.println("test1");
            }
            catch(Exception e){
            JOptionPane.showMessageDialog(null, "Nie udało się uruchomić klienta\n"+e);
            new Settings(this);
            }
        }
        else
            new Settings(this);
    }
    public int login() throws RemoteException{
        String s=JOptionPane.showInputDialog("Podaj nazwe uzytkonwnika");
        System.out.println("s:"+s);
        
        if(s!=null){
            user=s;
            stub.registerUser(s);
            //fm.setEnabled(true);
        }
        //logged=true;
        return 0;
    }
    
    public int logout() throws RemoteException{
        if(logged){
        stub.unregisterUser(user);
        fm.setEnabled(false);
        }
        return 0;
    }
    public void sendF(File f) throws IOException{        
        RemoteInputStreamServer istream;
        istream = new GZIPRemoteInputStream(new BufferedInputStream(new FileInputStream(f)));
        String checksum=stub.checkFile(f.getName());
        if(checksum!=null){
            if(sha1(f).equals(checksum))
                JOptionPane.showMessageDialog(null, "Plik jest aktualny");
            else{
                int i,j=0;
                do{
                    i=stub.sendFile(istream.export(), f.getName());
                    j++;
                }while(i!=0&&j<5);
                if(j==5)
                    JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
                if(i==0){
                    int success=stub.acceptFile(f.getName());
                    if(success!=0)
                        JOptionPane.showMessageDialog(null, "Bład serwera");
                }
                istream.close();
                fm.loadRemoteFiles();
            }
        }
        else{
            int i,j=0;
            do{
                i=stub.sendFile(istream.export(), f.getName());
                j++;
            }while(i!=0&&j<5);
            if(j==5)
                JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
            if(i==0){
                int success=stub.acceptFile(f.getName());
                if(success!=0)
                    JOptionPane.showMessageDialog(null, "Bład serwera");
            }
            istream.close();
            fm.loadRemoteFiles();
        }
        
    }
    public void sendFfull(File f) throws IOException{        
        RemoteInputStreamServer istream;
        istream = new GZIPRemoteInputStream(new BufferedInputStream(new FileInputStream(f)));
        String checksum=stub.checkFile(f.getAbsolutePath());
        if(checksum!=null){
            if(sha1(f).equals(checksum))
                JOptionPane.showMessageDialog(null, "Plik jest aktualny");
            else{
                int i,j=0;
                do{
                    i=stub.sendFile(istream.export(), f.getAbsolutePath());
                    j++;
                }while(i!=0&&j<5);
                if(j==5)
                    JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
                if(i==0){
                    int success=stub.acceptFile(f.getAbsolutePath());
                    if(success!=0)
                        JOptionPane.showMessageDialog(null, "Bład serwera");
                }
                istream.close(); 
                fm.loadRemoteFiles();
            }
        }
        else{
            int i,j=0;
            do{
                i=stub.sendFile(istream.export(), f.getAbsolutePath());
                j++;
            }while(i!=0&&j<5);
            if(j==5)
                JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
            if(i==0){
                int success=stub.acceptFile(f.getAbsolutePath());
                if(success!=0)
                    JOptionPane.showMessageDialog(null, "Bład serwera");
            }
            istream.close();
            fm.loadRemoteFiles();
        }
        
    }

    public void requestF(String name) throws RemoteException {
        
        try {
            System.out.println("nazwa: "+name);
            String checksum=stub.checkFile(name);
            File f=new File(path+File.separator+name);
            if(f.exists())
                if(sha1(f).equals(checksum)){
                    JOptionPane.showMessageDialog(null, "Plik jest aktualny");
                    return;
                }
                    
            FileOutputStream ostream;
            InputStream istream;
            istream=RemoteInputStreamClient.wrap(stub.requestFile(name));
            
            File file = new File(path);
            file.mkdirs();
            File tempFile=new File(path+File.separator+name);
            System.out.println(path+name);
            tempFile.setWritable(true);
            ostream = new FileOutputStream(tempFile);
            byte[] buf = new byte[1048576];
            int bytesRead;
            while((bytesRead = istream.read(buf)) >= 0){
                ostream.write(buf, 0, bytesRead);
            }
            ostream.flush();
            ostream.close();
            if(!sha1(tempFile).equals(checksum)){
                JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
                tempFile.delete();
            }
            fm.loadLocalFiles();
        }
        catch(IOException ex){
            Logger.getLogger(FileBackupClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void requestFfull(String name) throws RemoteException {
        
        try {
            String checksum=stub.checkFile(name);
            String p=stub.getDir();
            String dir=p.substring(0,1)+":"+p.substring(1,p.length());
            System.out.println("dir+name:"+dir+name);
            File f=new File(dir+name);
            if(f.exists())
                if(sha1(f).equals(checksum)){
                    JOptionPane.showMessageDialog(null, "Plik jest aktualny");
                    return;
                }
            FileOutputStream ostream;
            InputStream istream;
            istream=RemoteInputStreamClient.wrap(stub.requestFile(name));
            File file = new File(dir);
            file.mkdirs();
            File tempFile=new File(dir+File.separator+name);
            System.out.println(dir+File.separator+name);
            tempFile.setWritable(true);
            ostream = new FileOutputStream(tempFile);
            byte[] buf = new byte[1048576];
            int bytesRead;
            while((bytesRead = istream.read(buf)) >= 0){
                ostream.write(buf, 0, bytesRead);
            }
            ostream.flush();
            ostream.close();
            if(!sha1(tempFile).equals(checksum)){
                JOptionPane.showMessageDialog(null, "Nie udało się przesłać pliku!");
                tempFile.delete();
            }
            fm.loadLocalFiles();
            
        }
        catch(IOException ex){
            Logger.getLogger(FileBackupClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void changeDir(String s) throws RemoteException{
        stub.changeDir(s);
    }
    public void getFileList() throws RemoteException{
        String[] files=stub.getFileList();
        for(int i=0;i<files.length;i++)
            System.out.println(files[i]);
    }
    
    public static void main(String[] args) throws RemoteException, NotBoundException, IOException, AlreadyBoundException{
        try{
            System.setProperty("java.security.policy", "file:./src/Uprawnienia.policy");
            System.setSecurityManager(new RMISecurityManager());
        }
        catch(SecurityException e){
            System.err.println("Security violation " + e);
        }
        FileBackupClient klient=new FileBackupClient();
        //File f=new File("C:/setup.exe");
        //klient.changeDir("hello");
        //klient.getFileList();
        //klient.sendF(f);
        //klient.requestF("test");
    }   
}
