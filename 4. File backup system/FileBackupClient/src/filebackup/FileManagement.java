/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filebackup;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JList;

/**
 *
 * @author Rafal
 */
public class FileManagement extends javax.swing.JPanel {

    /**
     * Creates new form FileManagement
     */
    FileBackupClient parrentClient;
    public FileManagement(FileBackupClient p) throws RemoteException {
        parrentClient=p;
        initComponents();
        System.out.println(parrentClient.path);
        jTextField1.setText(parrentClient.path);
        jTextField2.setText(parrentClient.stub.getDir());
        loadLocalFiles();
        loadRemoteFiles();
        parrentClient.login();
        setVisible(true);
        jList2.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent evt) {
                jList2 = (JList)evt.getSource();
                if (evt.getClickCount() == 2&& !evt.isConsumed()) {
                    try {
                        evt.consume();
                        parrentClient.stub.changeDir(jList2.getSelectedValue().toString());
                        jTextField2.setText(parrentClient.stub.getDir());
                        loadRemoteFiles();
                    } catch (RemoteException ex) {
                        Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } 
            }
        });
        jList1.addMouseListener(new MouseAdapter(){
            public void mouseClicked(MouseEvent evt) {
                jList2 = (JList)evt.getSource();
                if (evt.getClickCount() == 2) {
                    if(jList2.getSelectedValue().toString().equals("..")&&parrentClient.path.length()>3){
                        int i=parrentClient.path.lastIndexOf("\\");
                        parrentClient.path=parrentClient.path.substring(0,i);
                        if(parrentClient.path.length()<3)
                            parrentClient.path+="\\";
                        System.out.println("Path: "+parrentClient.path);
                    }
                    else if(parrentClient.path.length()>3){
                        String newPath=parrentClient.path+File.separator+jList2.getSelectedValue().toString();
                        System.out.println("newPath: "+newPath);
                        File f=new File(parrentClient.path+File.separator+(jList2.getSelectedValue().toString()).substring(0,(jList2.getSelectedValue().toString()).length()-1));
                        if(f.isDirectory()){
                            //int i=parrentClient.path.lastIndexOf("\\");
                            //parrentClient.path=parrentClient.path.substring(0,i);
                            parrentClient.path=parrentClient.path+File.separator+(jList2.getSelectedValue().toString()).substring(0,(jList2.getSelectedValue().toString()).length()-1);
                        }
                    }
                    else if(parrentClient.path.length()==3){
                        int i=parrentClient.path.lastIndexOf("\\");
                        parrentClient.path=parrentClient.path.substring(0,i);
                        String newPath=parrentClient.path+File.separator+jList2.getSelectedValue().toString();
                        System.out.println("newPath: "+newPath);
                        File f=new File(parrentClient.path+File.separator+(jList2.getSelectedValue().toString()).substring(0,(jList2.getSelectedValue().toString()).length()-1));
                        if(f.isDirectory()){
                            parrentClient.path=parrentClient.path+File.separator+(jList2.getSelectedValue().toString()).substring(0,(jList2.getSelectedValue().toString()).length()-1);
                        }
                    }
                    loadLocalFiles();
                    jTextField1.setText(parrentClient.path);
                }
            }
        });
        //jList2.addListSelectionListener(new SelectionHandler());
    }
    public void loadRemoteFiles() throws RemoteException{
        String[] files=parrentClient.stub.getFileList();
        String[] dirs=parrentClient.stub.getDirectoryList();
        if(dirs==null)
            dirs=new String[0];
        if(files==null)
            files=new String[0];
        String[] arr=new String[dirs.length+files.length+1];
        arr[0]="..";
        for(int i=1;i<=dirs.length;i++)
            arr[i]=dirs[i-1]+"\\";
        for(int i=dirs.length+1;i<=dirs.length+files.length;i++)
            arr[i]=files[i-dirs.length-1];
        for(int i=0;i<arr.length;i++)
            System.out.println(arr[i]);
        jList2.setListData(arr);
        //jList2.invalidate();
        jList2.repaint();
    }
    
    public void loadLocalFiles(){
        File file;
        try{
            file = new File(parrentClient.path);
        }
        catch(Exception e){
            parrentClient.path=System.getProperty("user.dir");
            file = new File(parrentClient.path);
        }
        
        
        String[] dirs=file.list(new FilenameFilter() {public boolean accept(File dir, String name) {return new File(dir, name).isDirectory();}});
        String[] files=file.list(new FilenameFilter() {public boolean accept(File dir, String name) {return new File(dir, name).isFile();}});
        if(dirs==null)
            dirs=new String[0];
        if(files==null)
            files=new String[0];
        String[] arr=new String[dirs.length+files.length+1];
        arr[0]="..";
        for(int i=1;i<=dirs.length;i++)
            arr[i]=dirs[i-1]+"\\";
        for(int i=dirs.length+1;i<=dirs.length+files.length;i++)
            arr[i]=files[i-dirs.length-1];
        jList1.setListData(arr);
        jList1.repaint();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList();
        jLabel2 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setEnabled(false);
        setPreferredSize(new java.awt.Dimension(780, 400));

        jLabel1.setText("Ścieżka lokalna:");

        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jScrollPane1.setViewportView(jList1);

        jScrollPane2.setViewportView(jList2);

        jLabel2.setText("Ścieżka zdalna:");

        jButton1.setText("...");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("<==");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("===>");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Usuń");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("--->");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("<--");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 294, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(jLabel1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 351, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButton4))
                    .addComponent(jTextField2))
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton6, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 282, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton4)
                        .addContainerGap(22, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        File f=new File(jTextField1.getText());
        if(f.isDirectory()){
            parrentClient.path=f.getAbsolutePath();
            loadLocalFiles();
            jTextField1.setText(f.getAbsolutePath());
            String temp=System.getProperty("java.io.tmpdir");
            File tempFile = new File(temp+"\\FBCconfig.dat");
            PrintWriter zapis;
            try {
                zapis = new PrintWriter(tempFile);
                zapis.println(parrentClient.ip);
                zapis.println(parrentClient.port);
                zapis.println(parrentClient.name);
                zapis.println(parrentClient.path);
                zapis.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        }
            
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        JFileChooser fc=new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        if((fc.showDialog(this, "Zatwierdź lokalizację")) == JFileChooser.APPROVE_OPTION){
            parrentClient.path=fc.getSelectedFile().getAbsolutePath();
            loadLocalFiles();
            jTextField1.setText(fc.getSelectedFile().getAbsolutePath());
            String temp=System.getProperty("java.io.tmpdir");
            File tempFile = new File(temp+"\\FBCconfig.dat");
            PrintWriter zapis;
            try {
                zapis = new PrintWriter(tempFile);
                System.out.println(parrentClient.ip);
                zapis.println(parrentClient.ip);
                zapis.println(parrentClient.port);
                zapis.println(parrentClient.name);
                zapis.println(parrentClient.path);
                zapis.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        if(jList1.getSelectedValue()!=null){
            File f=new File(parrentClient.path+"\\"+jList1.getSelectedValue());
            if(f.isFile())
                try {
                    parrentClient.sendFfull(f);
                    loadRemoteFiles();
                    loadLocalFiles();
                } catch (IOException ex) {
                    Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        if(jList2.getSelectedValue()!=null)
            try {
                parrentClient.requestFfull(jList2.getSelectedValue().toString());
            } catch (IOException ex) {
                Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);

        }
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if(jList2.getSelectedValue()!=null)
            try {
                parrentClient.removeF(jList2.getSelectedValue().toString());
                loadRemoteFiles();
            } catch (IOException ex) {
                Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        if(jList1.getSelectedValue()!=null){
            File f=new File(parrentClient.path+"\\"+jList1.getSelectedValue());
            if(f.isFile())
                try {
                    parrentClient.sendF(f);
                    loadRemoteFiles();
                    loadLocalFiles();
                } catch (IOException ex) {
                    Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
                }
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        if(jList2.getSelectedValue()!=null)
            try {
                parrentClient.requestF(jList2.getSelectedValue().toString());
                loadRemoteFiles();
                loadLocalFiles();
            } catch (IOException ex) {
                Logger.getLogger(FileManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
    }//GEN-LAST:event_jButton6ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JList jList1;
    private javax.swing.JList jList2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    // End of variables declaration//GEN-END:variables
}
