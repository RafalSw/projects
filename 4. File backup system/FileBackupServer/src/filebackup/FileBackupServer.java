/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package filebackup;

import com.healthmarketscience.rmiio.GZIPRemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStream;
import com.healthmarketscience.rmiio.RemoteInputStreamClient;
import com.healthmarketscience.rmiio.RemoteInputStreamServer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Formatter;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Rafal
 */
public class FileBackupServer{

    /**
     * @param args the command line arguments
     */
    private static final long serialVersionUID = 7L;
    public static final int REGISTRY_PORT = Registry.REGISTRY_PORT;
    Server server;
    static Registry registry;
    static BackUpInterface stub, stubc;
    
    int startServer(int port, String name, String path) throws RemoteException, AlreadyBoundException{
        server=new Server();
        server.port=port;
        server.name=name;
        server.path=path;
        server.dir="";
        System.out.println("Starting server");
        stub = (BackUpInterface)UnicastRemoteObject.exportObject(server, 0);
        registry = LocateRegistry.createRegistry(server.port);
        registry.bind(server.name, stub);
        System.out.println("Server started");
        return 0;
    }
    int stopServer() throws RemoteException, NotBoundException{
        registry.unbind(server.name);
        return 0;
    }
    public static class Server implements BackUpInterface
    {
        String dir;
        String path;
        int port;
        String name;
        private Object ex;
        
        public static String sha1(final File file) {
            final MessageDigest messageDigest;
            System.out.println("SHA gets "+file.getAbsolutePath());
            try {
                messageDigest = MessageDigest.getInstance("SHA1");
                InputStream is = new BufferedInputStream(new FileInputStream(file));
              final byte[] buffer = new byte[1024];
              for (int read = 0; (read = is.read(buffer)) != -1;) {
                messageDigest.update(buffer, 0, read);
              }

            try (Formatter formatter = new Formatter()) {
              for (final byte b : messageDigest.digest()) {
                formatter.format("%02x", b);
              }
              return formatter.toString();
            }
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(FileBackupServer.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(FileBackupServer.class.getName()).log(Level.SEVERE, null, ex);
            }
            return null;
        }
        
        File stringToFile(String name){
            File file, tempFile;
            if(name.charAt(1)==':'){
                        name=name.substring(0,1)+name.substring(2,name.length());
                        System.out.print(path+File.separator+name.substring(0,name.lastIndexOf("\\")));
                        file = new File(path+File.separator+name.substring(0,name.lastIndexOf("\\")));
                        file.mkdirs();
                        tempFile=new File(path+File.separator+name);
                        tempFile.setWritable(true); 
                    }
                    else{
                        tempFile=new File(path+File.separator+dir+File.separator+name);
                        tempFile.setWritable(true);
                    }
            return tempFile;
        }
        File stringToTempFile(String name){
            File file, tempFile;
            if(name.charAt(1)==':'){
                        name=name.substring(0,1)+name.substring(2,name.length());
                        System.out.print(path+File.separator+name.substring(0,name.lastIndexOf("\\")));
                        file = new File(path+File.separator+name.substring(0,name.lastIndexOf("\\")));
                        file.mkdirs();
                        tempFile=new File(path+File.separator+name+"NA");
                        tempFile.setWritable(true); 
                    }
                    else{
                        tempFile=new File(path+File.separator+dir+File.separator+name+"NA");
                        tempFile.setWritable(true);
                    }
            return tempFile;
        }
        @Override
        public String checkFile(String name) throws RemoteException{
            File f=stringToFile(name);
            if(f.exists())
                return sha1(f);
            else
                return null;
        }
        @Override
        public int removeFile(String name){
            File file = new File(path+dir+File.separator+name); 
            file.delete();
            return 0;
        }
        
        public int registerUser(String s){
            path+="\\"+s+"\\";
            File f=new File(path);
            f.mkdirs();
            return 0;
        }
        public int unregisterUser(String s){
            int index1=path.lastIndexOf(s+"\\");
            System.out.println(path.substring(0,index1));
            //path+=s+"\\";
            return 0;
        }
        @Override
        public String[] getFileList(){
            File file = new File(path+File.separator+dir);
            String[] files=file.list(new FilenameFilter() {public boolean accept(File dir, String name) {return new File(dir, name).isFile();}});
            return files;
        }
        
        @Override
        public int acceptFile(String name){
            File file, tempFile;
            System.out.println("to be accepted: "+name);
            if(name.charAt(1)==':'){
                System.out.println("to be accepted1: "+path+File.separator+name+"NA");
                name=name.substring(0,1)+name.substring(2,name.length());
                file=new File(path+File.separator+name+"NA");
                tempFile=new File(path+File.separator+name);
                tempFile.setWritable(true);
                file.renameTo(tempFile);
            }
            else{
                System.out.println("to be accepted1: "+path+File.separator+name+"NA");
                file=new File(path+File.separator+name+"NA");
                tempFile=new File(path+File.separator+dir+File.separator+name);
                tempFile.setWritable(true);
                file.renameTo(tempFile);
            }
            return 0;
              
        }
        @Override
        public String[] getDirectoryList(){
            File file = new File(path+File.separator+dir);
            String[] files=file.list(new FilenameFilter() {public boolean accept(File dir, String name) {return new File(dir, name).isDirectory();}});
            return files;
        }
        
        @Override
        public void changeDir(String s){
            if(s.equals("..")&&dir.length()>0){
                int i=dir.lastIndexOf("\\");
                dir=dir.substring(0,i);
                i=dir.lastIndexOf("\\");
                if(i==-1)
                    dir="";
                else
                    dir=dir.substring(0,i+1);
                if((path+File.separator+dir).length()<1)
                    dir+="\\";
                System.out.println("dir "+dir);
                System.out.println("path "+path);
            }
            else if(s.equals("..")&&dir.length()<=path.length()){
               System.out.println("dir "+dir);
               System.out.println("path "+path);
            }
            else{
                File file = new File(path+File.separator+dir+s);
                if(file.isDirectory())
                    dir+=s;
                    file = new File(path+File.separator+dir);
                    file.mkdirs();
                    System.out.println("dir "+dir);
               System.out.println("path "+path);
            }      
        }
        
        @Override
        public String getDir(){
            if(dir.length()==0)
                return ".\\";
            else
                return dir;
        }
        
        @Override
        public RemoteInputStream requestFile(String name) throws RemoteException, AccessException{
            System.out.println(path+File.separator+name);
            RemoteInputStreamServer istream;
            File f=new File(path+File.separator+name);
            if(f.isFile()){
                try {
                    System.out.println("Wysylanie do klienta");
                    istream = new GZIPRemoteInputStream(new BufferedInputStream(new FileInputStream(f)));
                    return istream;
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(FileBackupServer.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(FileBackupServer.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return null;   
        }
        
        @Override
        public int sendFile(RemoteInputStream inFile, String name){
            try {
                FileOutputStream ostream;
                try (InputStream istream = RemoteInputStreamClient.wrap(inFile)) {
                    File tempFile=stringToTempFile(name);
                    ostream = new FileOutputStream(tempFile);
                        byte[] buf = new byte[1048576];
                        int bytesRead;
                        while((bytesRead = istream.read(buf)) >= 0){
                            ostream.write(buf, 0, bytesRead);
                        }
                        ostream.flush();
                }
                ostream.close();
                return 0;
            }
            catch(IOException ex){
                Logger.getLogger(FileBackupServer.class.getName()).log(Level.SEVERE, null, ex);
                return -1;
            }
        }
    }
  
    public static void main(String[] args) throws Exception{
        FileBackupServer fbserver = new FileBackupServer();
        MainWindow okno=new MainWindow(fbserver);
    }
}
