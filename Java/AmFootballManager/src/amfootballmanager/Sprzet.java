package amfootballmanager;

import java.io.Serializable;


public class Sprzet implements Serializable{
    private static int id = 0;
    private static final long serialVersionUID = -449319487782434265L;
    private boolean dostepnosc;
    
    public Sprzet(){
        dostepnosc = true;
        id = id++;
    }
    
    public void ustawDostepnosc(boolean b){
        dostepnosc = b;
    }
    
    public boolean pobirzDostepnosc(){
        return dostepnosc;
    }
}
