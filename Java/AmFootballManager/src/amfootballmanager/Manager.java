package amfootballmanager;

import java.util.ArrayList;

public class Manager extends Osoba{
    private final String tytul;
    private double pensja;
    private final ArrayList<Zawodnik> zawodnicy;
    private final ArrayList<Trener> trenerzy;
    private final ArrayList<Sprzet> sprzet;
    
    
    public Manager(String newTytul, double newPensja){
        tytul = newTytul;
        pensja = newPensja;
        zawodnicy = new ArrayList<>();
        trenerzy = new ArrayList<>();
        sprzet = new ArrayList<>();
    }
    
    public void ustawPensja(double d){
        pensja = d;
    }
    
    public String pobierzTytul(){
        return tytul;
    }
    
    public double pobierzPensja(){
        return pensja;
    }
    
    public void dodajZawodnik(Zawodnik z){
        zawodnicy.add(z);
    }
    
    public void dodajTrener(Trener t){
        trenerzy.add(t);
    }
    
    public void dodajSprzet(Sprzet s){
        sprzet.add(s);
    }
    
    public Trener[] pobierzTrenerow(){
        Trener[] obiekty = new Trener[trenerzy.size()];
        trenerzy.toArray(obiekty);
        return obiekty;
    }
}
