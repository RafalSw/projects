package amfootballmanager;


public class OchraniaczBarkow extends Sprzet{
    private static final long serialVersionUID = 2757520922181127958L;
    private final String model;
    private final int rozmiar;
    private final String typ;
    
    public OchraniaczBarkow(String newModel, int newRozmiar, String newTyp){
        super();//wywolanie konstruktora Sprzet()
        model = newModel;
        rozmiar = newRozmiar;
        typ = newTyp;
    }
    public String pobierzModel(){
        return model;
    }
    
    public int pobierzRozmiar(){
        return rozmiar;
    }
    
    public String pobierzTyp(){
        return typ;
    }
}
