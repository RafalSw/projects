package amfootballmanager;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Trening implements Serializable{
    private Rozgrzewka rozgrzewka;
    private int id;
    private double dlugoscTrwania;
    private long data;
    private String planTreningu;
    private ArrayList<Zawodnik> zawodnicy;
    private ObiektSportowy obiekt;

    Trening(int size) {
        this.id = size;
        zawodnicy = new ArrayList<>();
    }
    
    public void ustawRozgrzewka(String typ, double dlugosc){
        rozgrzewka = new Rozgrzewka();
        rozgrzewka.ustawTyp(typ);
        rozgrzewka.ustawDlugosc(dlugosc);
    }
    
    public Rozgrzewka pobierzRozgrzewka(){
        return rozgrzewka;
    }
    
    public int pobierzId(){
        return id;
    }
    
    public void ustawDlugoscTrwania(double d){
        dlugoscTrwania = d;
    }
    
    public double pobierzDlugoscTrwania(){
        return dlugoscTrwania;
    }
    
    public void ustawPlanTreningu(String s){
        planTreningu = s;
    }
    
    public String pobierzPlanTreningu(){
        return planTreningu;
    }
    
    public void dodajZawodnika(Zawodnik z){
        zawodnicy.add(z);
    }
    
    public void ustawObiekt(ObiektSportowy o){
        obiekt = o;
    }
    
    public ObiektSportowy pobierzObiektSportowy(){
        return obiekt;
    }
    
    public void ustawData(long l){
        data = l;
    }

    public double getDlugoscTrwania() {
        return dlugoscTrwania;
    }
    
//    public String zwrocInfo(){
//        StringBuilder sb = new StringBuilder();
//        sb.append("Trening nr. ");
//        sb.append(Integer.toString(id));
//        
//        sb.append(System.lineSeparator());
//        
//        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
//        sb.append("Dnia: ");
//        sb.append(df.format(new Date(data)));
//        
//
//        
//        
//        return sb.toString();
//        
//    }
    
     public String zwrocInfo(){
        String result = "";
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
        result+="Trening nr. "+Integer.toString(id + 1)+ "\n" +
                "Data: " + (df.format(new Date(data)))+"\n";
        
        result+="Czas trwania: "+Double.toString(dlugoscTrwania)+"\n";
        result+="Na obiekcie: "+obiekt.pobierzNazwa()+"\n";
        result+="Plan:"+pobierzPlanTreningu()+"\n";
        result+="Rozgrzewka: "+rozgrzewka.pobierzTyp()+"\n Czas trwania:"+Double.toString(rozgrzewka.pobierzDlugosc())+"\n";
        result+="Zawodnicy:\n";
        for(Zawodnik z:zawodnicy){
            result+="\t"+z.toString()+"\n";
        }
        return result;
    }
    
    public String zwrocKrótkieInfo(){
        String result = "";
        result+="Trening nr: "+Integer.toString(id + 1) + "    ";
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        result+=df.format(this.data);
        return result;
    }
    
}
