
package amfootballmanager;

import java.util.ArrayList;

public class Trener extends Osoba{
    private String specjalizacja;
    private final ArrayList<String> poprzednieDruzyny;
    private double pensjaPodstawowa;
    private final ArrayList<Trening> treningi;
    private final ArrayList<ObiektSportowy> bazaObiektow;
    
    
    public Trener(){
        poprzednieDruzyny = new ArrayList<>();
        treningi = new ArrayList<>();
        pensjaPodstawowa = 0;
        specjalizacja = null;
        bazaObiektow = new ArrayList<>();
    }
    
    public void ustawSpecjalizacja(String s){
        specjalizacja = s;
    }
    
    public String pobierzSpecjalizacja(){
        return specjalizacja;
    }
    
    public void ustawPensjaPodstawowa(double i){
        pensjaPodstawowa = i;
    }
    
    public double pobierzPensjaPodstawowa(){
        return pensjaPodstawowa;
    }
    
    public double obliczBonus(){
        double czasTreningu = 0;
        for(Trening t: treningi){
            czasTreningu+=t.pobierzDlugoscTrwania();
        }
        return 10*czasTreningu;
    }
    
    public double obliczZarobek(){
        return obliczBonus()+pensjaPodstawowa;
    }
    
    public void odwolajTrening(){
        
    }
    
    public void dodajObiekt(ObiektSportowy ob){
        bazaObiektow.add(ob);
    }
    
    public ObiektSportowy[] pobierzBazeObiektow(){
        ObiektSportowy[] obiekty = new ObiektSportowy[bazaObiektow.size()];
        bazaObiektow.toArray(obiekty);
        return obiekty;
    }
    
    public void dodajTrening(Trening t){
        treningi.add(t);
    }
    
    public String wyswietlZawodnikow(){
        return null;
    }
    
    public String wyswietlSprzetWypozyczony(){
        return null;
    }
    
    public String toString(){
        return this.getImie()+" "+this.getNazwisko();
    }

    public ArrayList<Trening> getTreningi() {
        return treningi;
    }
    
}
