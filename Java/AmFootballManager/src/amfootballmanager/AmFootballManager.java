
package amfootballmanager;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class AmFootballManager implements Serializable{
    private static transient AmFootballManager instance;
    private static final long serialVersionUID = 8309827090090703177L;
    private final Manager manager;
    private static final String NAZWA_PLIKU="AM.amdata"; //dowolna nazwa i rozszerzenie
    
    private static List<Trening> wszystkieTreningi = new ArrayList<Trening>();
    private static Trening wybranyTrening = null;
    
    private AmFootballManager(){
        manager = new Manager("tytul", 1000);
        Trener trener1 = new Trener();
        trener1.setImie("Jan");
        trener1.setNazwisko("Kowalski");
        manager.dodajTrener(trener1);
        
        Trener trener2 = new Trener();
        trener2.setImie("Adam");
        trener2.setNazwisko("Nowak");
        manager.dodajTrener(trener2);
        
        ObiektSportowy obiekt1 = new ObiektSportowy();
        obiekt1.ustawNazwa("Obiekt 1");
        
        ObiektSportowy obiekt2 = new ObiektSportowy();
        obiekt2.ustawNazwa("Obiekt 2");
        
        ObiektSportowy obiekt3 = new ObiektSportowy();
        obiekt3.ustawNazwa("Obiekt 3");
        
        ObiektSportowy obiekt4 = new ObiektSportowy();
        obiekt4.ustawNazwa("Obiekt 4");
        
        trener1.dodajObiekt(obiekt1);
        trener1.dodajObiekt(obiekt2);
        trener2.dodajObiekt(obiekt3);
        trener2.dodajObiekt(obiekt4);
        
    }
    
    public Manager pobierzManagera(){
        return manager;
    }
    
    public static AmFootballManager getInstance(){
        if(instance == null){
            instance = AmFootballManager.odczyt(); //proba odczytu;
            if(instance==null)
                instance = new AmFootballManager();
        }
        return instance;
    }

    public boolean zapis(){
        try (
        OutputStream plik = new FileOutputStream(NAZWA_PLIKU);
        OutputStream bufor = new BufferedOutputStream(plik);
        ObjectOutput wyjscie = new ObjectOutputStream(bufor);
        ){
            wyjscie.writeObject(this);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(AmFootballManager.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public static AmFootballManager odczyt(){
        try(
        InputStream plik = new FileInputStream(NAZWA_PLIKU);
        InputStream bufor = new BufferedInputStream(plik);
        ObjectInput wejscie = new ObjectInputStream (bufor);
        ){
            AmFootballManager result = (AmFootballManager)wejscie.readObject();
            return result;
        }
        catch(ClassNotFoundException | IOException ex){
            Logger.getLogger(AmFootballManager.class.getName()).log(Level.SEVERE, "Brak pliku do odczytu lub plik niepoprawny.");
        }
        return null;
    }

    
    public void start(){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new OknoGlowne().setVisible(true);
            }
        });
    }
    
    public void przygotujTreningi(){
        
    }
    
    public static void main(String[] args) {
        AmFootballManager amFootballManager = AmFootballManager.getInstance();
        amFootballManager.start();
    }

    public Manager getManager() {
        return manager;
    }
    
}
