
package amfootballmanager;

import java.io.Serializable;

public class Osoba implements Serializable{
    private static final long serialVersionUID = -6507294302791300761L;
    private String imie;
    private String nazwisko;
    private int pesel;
    private int numerTelefonu;
        
    public void setImie(String newImie){
        imie = newImie;
    }
    
    public void setNazwisko(String newNazwisko){
        nazwisko = newNazwisko;
    }
    
    public void setPesel(int newPesel){
        pesel = newPesel;
    }
    
    public void setNumerTelefonu(int newNumerTelefonu){
         numerTelefonu = newNumerTelefonu;
    }
    
    public String getImie(){
        return imie;
    }
    
    public String getNazwisko(){
        return nazwisko;
    }
    
    public int getPesel(){
        return pesel;
    }
    
    public int getNumerTelefonu(){
        return numerTelefonu;
    }
}
