package amfootballmanager;

public class Kask extends Sprzet{
    private static final long serialVersionUID = -4061988798147140422L;
    private final String model;
    private final int rozmiar;
    
    public Kask(String newModel, int newRozmiar){
        super();//wywolanie konstruktora Sprzet()
        model = newModel;
        rozmiar = newRozmiar;
    }
    public String pobierzModel(){
        return model;
    }
    
    public int pobierzRozmiar(){
        return rozmiar;
    }
}
