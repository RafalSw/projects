package amfootballmanager;

import java.util.ArrayList;

public class Zawodnik extends Osoba{
    private int numer;
    private String pozycja;
    private double wysokoscSkladki;
    private final ArrayList<Trening> historiaTreningow;
    private final ArrayList<Wypozyczenie> historiaWypozyczen;
    
    public Zawodnik(){
        historiaTreningow = new ArrayList<>();
        historiaWypozyczen = new ArrayList<>();
    }
    
    public void dolaczDoTreningu(Trening t){
        t.dodajZawodnika(this);
        historiaTreningow.add(t);
    }
    
    public String wyswietlTreningInfo(){
        String result = "";
        
        for(Trening t: historiaTreningow){
            result+=t.zwrocInfo()+"\n\n";
        }
        
        return result;
    }
    
    public void wypozycz(Wypozyczenie w){
        historiaWypozyczen.add(w);
    }
}
