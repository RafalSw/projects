package amfootballmanager;

import java.io.Serializable;

public class Rozgrzewka implements Serializable{
    private static final long serialVersionUID = 634798369679027400L;
    private String typ;
    private double dlugoscTrwania;
    
    public void ustawTyp(String s){
        typ = s;
    }
    
    public String pobierzTyp(){
        return typ;
    }
    
    public void ustawDlugosc(double d){
        dlugoscTrwania = d;
    }
    
    public double pobierzDlugosc(){
        return dlugoscTrwania;
    }
}
