package amfootballmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Wypozyczenie implements Serializable{
    private static final long serialVersionUID = 274327049306385927L;
    private long dataWypozyczenia;
    private long dataZwrotu;
    private final ArrayList<Sprzet> sprzet;
    
    public Wypozyczenie(){
        sprzet = new ArrayList<>();
        dataWypozyczenia = new Date().getTime();
    }
    
    public void dodajSprzet(Sprzet s){
        sprzet.add(s);
    }
    
    public void zwroc(){
        dataZwrotu = new Date().getTime();
    }
}
