package amfootballmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

class Rezerwacja{
    private final long start;
    private final long stop;
    
    public Rezerwacja(Date s1, Date s2){
        start = s1.getTime();
        stop = s2.getTime();
    }
    
    boolean konflikt(Date d1, Date d2){
        if(!d1.before(new Date(start)) && d1.before(new Date(stop)))
            return true;
        if(d2.after(new Date(start)) && !d2.after(new Date(stop)))
            return true;
        return d2.after(new Date(stop)) && !d1.after(new Date(start));
    }
}

public class ObiektSportowy implements Serializable{
    private static final long serialVersionUID = 7056047503802317427L;
    private String adres;
    private String typ;
    private String nazwa;
    private final ArrayList<Rezerwacja> rezerwacje;
    
    public ObiektSportowy(){
        rezerwacje = new ArrayList<>();
    }
    
    public void ustawNazwa(String s){
        nazwa = s;
    }
    
    public String pobierzNazwa(){
        return nazwa;
    }
    
    public void ustawAdres(String s){
        adres = s;
    }
    
    public String pobierzAdres(){
        return adres;
    }
    
    public void ustawTyp(String s){
        typ = s;
    }
    
    public String pobierzTyp(){
        return typ;
    }
    
    public boolean zarezerwuj(Date start, Date stop){
        if(pobierzDostepnosc(start,stop)==false)
            return false;
        
        Rezerwacja nowa = new Rezerwacja(start,stop);
        rezerwacje.add(nowa);
        return true;
    }
    
    public boolean pobierzDostepnosc(Date start, Date stop){
        for(Rezerwacja r:rezerwacje){
            if(r.konflikt(start, stop))
                return false;
        }
        return true;
    }
    
    public String toString(){
        return nazwa;
    }
}
