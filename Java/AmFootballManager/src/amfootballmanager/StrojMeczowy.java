package amfootballmanager;

public class StrojMeczowy extends Sprzet{
    private static final long serialVersionUID = -5668553113279548954L;
    private final int numer;
    private final int rozmiar;
    private final boolean domowy;
    
    public StrojMeczowy(int newNumer, int newRozmiar, boolean newDomowy){
        super();//wywolanie konstruktora Sprzet()
        numer = newNumer;
        rozmiar = newRozmiar;
        domowy = newDomowy;
    }
    public int pobierzNumer(){
        return numer;
    }
    
    public int pobierzRozmiar(){
        return rozmiar;
    }
    
    public boolean pobierzDomowy(){
        return domowy;
    }
}
