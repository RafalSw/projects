
public class Start {

	public static void main(String[] args) {

// Tworze obiekt mojej klasy Curiosum implementujacej interfejs CuriosumI
		CuriosumI ci = new Curiosum();
		
// Tworze obiekt Panstwa klasy implemntujacej metode CallInterface
		CallInterface call = new Call();
		
// Przekazuje Panstwu dostep do mojej klasy
		call.setCuriosum( ci );
		
// Wywoluje kilkakrotnie metode nextCall z nadzieja na poprawny efekt jej pracy
		call.nextCall();
		call.nextCall();
		call.nextCall();
	}

}
