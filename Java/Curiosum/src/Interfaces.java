interface OnOffInterface {
	public void on();
	public void off();
	public void set( String s );
}

interface ControllInterface {
	public void turnLeft();
	public void turnRight();
	public void setName( String s );
}
