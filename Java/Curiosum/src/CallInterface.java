
/**
 * Interfejs przeznaczony do implementacji przez klase call.
 * @author 
 *
 */
public interface CallInterface {
	/**
	 * Przekazuje do klasy referencje do obiektu zgodnego z klasa CuriosumI. Dzieki
	 * tej metodzie klasa implementujaca interfejs CallInterface nie musi
	 * znac i samodzielnie tworzyc obiektow klasy, ktora implementuje interfejs CuriosumI.
	 * 
	 * @param ci - referencja do obiektu klasy implementujacej interfejs <code>CuriosumI</code>
	 */
	public void setCuriosum( CuriosumI ci );
	
	/**
	 * Zleca wykonanie calego jednego cyklu wywolan metody CuriosumI.get() interejsu CuriosumI.
	 * Od zdobycia wiedzy o typie obiektu, ktory zostanie zwrocony, az do wykonania zadanej metody
	 * na tym obiekcie.
	 */
	public void nextCall();
}
