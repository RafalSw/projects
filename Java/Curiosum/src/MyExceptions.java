class ExceptionInterfaceName extends Exception {
	private static final long serialVersionUID = 3803205720245606169L;
/**
 * Publiczne pole zawierajace informacje o nazwie interfejsu, ktorego
 * nalezy uzyc w celu wykonania metody na zwroconym obiekcie.
 */
	public String interfaceName;
 }
 
 
 class ExceptionMethodName extends Exception {
	private static final long serialVersionUID = -2080300339551169113L;
	private String om;
	 
     public ExceptionMethodName( String objectMethod ) {
    	 om = objectMethod;
     }
     
     public ExceptionMethodName() {
     }
     
     /**
      * Zwraca nazwe metody, ktora ma zostac wykonana.
      * 
      * @return - nazwa metody do wykonania.
      */
     public String getMethodName() {
    	 return om;
     }
 }
 
 
 class ExceptionMethodArgument extends ExceptionMethodName {
	private static final long serialVersionUID = 1593054739410866916L;
	private String ma;
	 
	 public ExceptionMethodArgument( String methodArgument ) {
		 ma = methodArgument;
	 }
	 
	 /**
	  * Zwraca argument typu String do przekazania do 
	  * wywolywanej metody.
	  * 
	  * @return Argument do przekazania do wywolywanej metody
	  */
	 public String getMethodArgument() {
		 return ma;
	 }
 }
