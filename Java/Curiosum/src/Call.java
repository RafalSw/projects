class Call implements CallInterface{
    CuriosumI curiosum; //przechowywane kuriozum
    
    public Call(){ //konstruktor bezparametrowy
        curiosum = null;
    }

    @Override
    public void setCuriosum(CuriosumI ci) {
        curiosum = ci; //przypisanie zadanego kuriozum
    }

    @Override
    public void nextCall() {
        
        if(curiosum == null){ //jeśli kuriozum nie jest przypisane, to nie ma jak wykonać metody get!
            System.out.println("Error! Curiosum not set");
            return;
        }
        
        //zmienne przechowujące nazwy interfejsu, metod i argumentu zadeklarowane i zainicjowane na null
        String interfaceName = null, objectMethod = null, methodArgument = null;
        Object object = null;
        //do puki wszystkie niezbędne dane nie są zainicjowane
        while(object == null || interfaceName == null || objectMethod == null || (methodArgument == null && (objectMethod.equalsIgnoreCase("set") || objectMethod.equalsIgnoreCase("setName")))){
            try{
               object = curiosum.get(); //próba wywołania get
            }
            catch(ExceptionInterfaceName e){ //wyłapywanie kolejnych wyjątków
                interfaceName = e.interfaceName;
            }
            catch(ExceptionMethodArgument e){
                methodArgument = e.getMethodArgument();
            }
            catch (ExceptionMethodName e) {
                objectMethod = e.getMethodName();
            }
        }
        
        //wykonywanie funkcji zależnie od zawartości zmiennych interfaceName i objectMethod
        if(interfaceName.equalsIgnoreCase("OnOffInterface")){
            if(objectMethod.equalsIgnoreCase("on")){
                ((OnOffInterface)object).on(); //rzutowanie obiektu na dany typ i wywołanie na nim metody on
            }
            else if(objectMethod.equalsIgnoreCase("off")){
                ((OnOffInterface)object).off();
            }
            else if(objectMethod.equalsIgnoreCase("set")){
                ((OnOffInterface)object).set(methodArgument);
            }
        }
        else if(interfaceName.equalsIgnoreCase("ControllInterface")){
            if(objectMethod.equalsIgnoreCase("turnLeft")){
                ((ControllInterface)object).turnLeft();
            }
            else if(objectMethod.equalsIgnoreCase("turnRight")){
                ((ControllInterface)object).turnRight();
            }
            else if(objectMethod.equalsIgnoreCase("setName")){
                ((ControllInterface)object).setName(methodArgument);
            }
        }
    }
}
