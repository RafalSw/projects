public interface CuriosumI {
/**
 * Interfejs zwraca referencje do obiektu zgodnego z interfejsem OnOffInterface i/lub z
 * interfejsem ControllInterface. Typ zwracanego obiektu przekazywany jest poprzez
 * wyjatki pojawiajace sie jako wynik wywolania metody get. Wyjatki poprzedzaja
 * zwrocenie referencji do obiektu. Kolejnosc w jakiej zwracane sa wyjatki z informacjami
 * poprzedzajacymi zwrocenie samego obiektu moze byc dowolna. Moze sie rowniez zdarzyc, ze
 * przed zwroceniem obiektu kilkukrotnie zwrocony zostanie ten sam wyjatek - w takiej
 * sytuacji uzytkownik powinien zastosowac sie do nowych (najnowszych) informacji.
 * 
 * @return referencja do obiektu zgodnego z wymienionymi w opisie metody dwoma interfejsami
 * 
 * @throws ExceptionInterfaceName - wyjatek zawiera informacje o nazwie interfejsu, z ktorym jest zgodny
 * kolejny obiekt, ktory zostanie zwrocony przez metode
 * @throws ExceptionMethodName - wyjatek zawiera informacje o nazwie metody, ktora nalezy
 * wywolac za pomoca zwroconej referencji
 * @throws ExceptionMethodArgument - wyjatek zawiera argument, ktory nalezy przeslac do metody
 * wywolywanej za pomoca zwroconej referencji
 */
	Object get() throws ExceptionInterfaceName, ExceptionMethodName, ExceptionMethodArgument;
}
