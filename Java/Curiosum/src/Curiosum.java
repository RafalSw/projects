
import java.util.Random;

class OnOffInterfaceObj implements OnOffInterface{

    @Override
    public void on() {
        System.out.println("on");
    }

    @Override
    public void off() {
        System.out.println("off");
    }

    @Override
    public void set(String s) {
        System.out.println("set: " + s);
    }
    
}

class ControllInterfaceObj implements ControllInterface{

    @Override
    public void turnLeft() {
        System.out.println("left");
    }

    @Override
    public void turnRight() {
        System.out.println("right");
    }

    @Override
    public void setName(String s) {
        System.out.println("setName: " + s);
    }
    
}

public class Curiosum implements CuriosumI{

    int objType;
    boolean inReturned;
    boolean mnReturned;
    boolean maReturned;
    boolean objectReturned;
    
    Curiosum(){
        calculate();
    }
    
    final void calculate(){
        if(inReturned&&mnReturned&&maReturned&&objectReturned)
        {
            System.out.println("calculate");
            Random rand = new Random();
            objType = rand.nextInt(2);
            inReturned=mnReturned=maReturned=objectReturned=false;
        }
    }
    
    @Override
    public Object get() throws ExceptionInterfaceName, ExceptionMethodName, ExceptionMethodArgument {
        calculate();
        if(objType==0)
        {
            OnOffInterfaceObj obj = new OnOffInterfaceObj();
        
            ExceptionInterfaceName in = new ExceptionInterfaceName();
            in.interfaceName="OnOffInterface";
            ExceptionMethodName mn = new ExceptionMethodName("on");
            ExceptionMethodArgument ma = new ExceptionMethodArgument(null);
            
            Random rand = new Random();
            int i = rand.nextInt(4);
            
            if(i==0){
                inReturned=true;
                System.out.println("Throw in a");
                throw in;
            }
            else if(i==1){
                mnReturned=true;
                System.out.println("Throw mn a");
                throw mn;
            }
            else if(i==2){
                System.out.println("Throw ma a");
                maReturned=true;
                throw ma;
            }
            else
            {
                objectReturned=true;
                return obj;
            }
            
        }
        else
        {
            ControllInterfaceObj obj = new ControllInterfaceObj();
        
            ExceptionInterfaceName in = new ExceptionInterfaceName();
            in.interfaceName="ControllInterface";
            ExceptionMethodName mn = new ExceptionMethodName("turnLeft");
            ExceptionMethodArgument ma = new ExceptionMethodArgument(null);
            
            Random rand = new Random();
            int i = rand.nextInt(4);
            
            if(i==0){
                inReturned=true;
                System.out.println("Throw in b");
                throw in;
            }
            else if(i==1){
                mnReturned=true;
                System.out.println("Throw mn b");
                throw mn;
            }
            else if(i==2){
                maReturned=true;
                System.out.println("Throw ma b");
                throw ma;
            }
            else
            {
                objectReturned=true;
                return obj;
            }
        }
    }
    
}
