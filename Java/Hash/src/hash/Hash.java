/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hash;

import java.io.File;


/**
 *
 * @author Rafal
 */
public class Hash {
    Hash(){
        System.loadLibrary("SHA3");
    }
    
    public native byte[] calculate(String path);
    public native byte[] calculate(byte[] data);
    
    public String calculateToStr(File file){
        if(file.exists() && !file.isDirectory()){
            byte[] bytes = calculate(file.getAbsolutePath());
            StringBuilder sb = new StringBuilder();
            for(byte b : bytes){
                sb.append(String.format("%02x", b&0xff));
            }
            return sb.toString();
        }
        else{
            System.err.println("Plik nie istnieje: "+file.getAbsolutePath());
            return null;
        }
    }
    
    public String calculateToStr(byte[] data){
        byte[] bytes = calculate(data);
        StringBuilder sb = new StringBuilder();
        for(byte b : bytes){
            sb.append(String.format("%02x", b&0xff));
        }
        return sb.toString();
    }
    

    public static void main(String[] args){
        Hash hash = new Hash();
        System.out.println(hash.calculateToStr(new File("C:\\Users\\Rafal\\Desktop\\Na FB\\testtt2\\01.jpg")));
    }
}
