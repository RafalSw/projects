package miniprojekt;

import java.io.Serializable;


//implementuje interfejs CeramikaI i rozszerza klase Dzielo - wielodziedziczenie
public class CeramikaGliniana extends Dzielo implements Serializable, CeramikaI{

    //implementacja metody abstrakcyjnej - niezbedna do polimorfizum
    public String zwrocTyp() {
        return "ceramika gliniana";
    }

    public String zwrocSklad() {
        return "glina";
    }
}
