package miniprojekt;

import java.io.Serializable;
import java.util.EnumSet;

//typy technik, niezbedne do overlappingu
enum Technika { Akwarela, Olejna, Witraz, Ogolna };

public class Obraz extends Dzielo implements Serializable{
    
    //dyskryminator overlappingu
    private EnumSet<Technika> technika = EnumSet.<Technika>of(Technika.Ogolna);
    
    //konstruktor przypisuje wartosc do dyskryminatora zaleznie od parametru
    public Obraz(int[] typ){ //0 - akwarela, 1 - olejna, 2 - Witraz, inna - Ogolna
        super();
        for(int i=0;i<typ.length;i++){
            if(typ[i]==0)
                technika.add(Technika.Akwarela);
            else if(typ[i]==1)
                technika.add(Technika.Olejna);
            else if(typ[i]==2)
                technika.add(Technika.Witraz);
        }
    }

    //implementacja metody abstrakcyjnej - niezbedna do polimorfizum
    public String zwrocTyp() {     
        //zamiana EnumSet na tablica
        Technika[] typy = new Technika[technika.size()];
        technika.toArray(typy);
        
        String techniki = "";
        
        if(typy.length==1)
            techniki+="ogolna ";
        else{
            for(int i=0;i<typy.length;i++){
                if(typy[i]==Technika.Akwarela)
                    techniki+="akwarela ";
                else if(typy[i]==Technika.Olejna)
                    techniki+="olejna ";
                else if(typy[i]==Technika.Witraz)
                    techniki+="witraz ";
            }
        }
        
        return "obraz, technika: " + techniki;
    }
}
