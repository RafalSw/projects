package miniprojekt;

import java.util.Vector;

public class Opiekun {
    private Pracownik pracownik;
    private Wystawa wystawa;
    private Vector<String> kompetencje = new Vector<>();
    
    private static Vector<Pracownik> pracownicyJakoOpiekunowie = new Vector<>();
    
    private Opiekun(){ 
    }
    
    public Opiekun(Pracownik p, Wystawa w){
        if(!pracownicyJakoOpiekunowie.contains(p)){
            pracownik = p;
            wystawa = w;
            w.dodajOpiekuna(this);
            pracownicyJakoOpiekunowie.add(p);
        }
        else{
            System.out.println("Ten pracownik juz jest opiekunem");
        }
    }
    
    public static void usun(Opiekun o){
        if(pracownicyJakoOpiekunowie.contains(o.pracownik)){
            pracownicyJakoOpiekunowie.remove(o.pracownik);
            
            Wystawa.usunOpiekuna(o);
        }
    }
    
    public void dodajKompetencje(String s){
        kompetencje.add(s);
    }
    
    public void wypiszKompetencje(){
        System.out.println("Do kompetencji opiekuna "+pracownik+" na wystawie "+wystawa.zwrocNazwe()+" naleza:");
        for(int i=0;i<kompetencje.size();i++){
            System.out.println((i+1)+" "+kompetencje.elementAt(i));
        }
    }
    
    public Wystawa zwrocWystawe(){
        return wystawa;
    }
   
    
    public static void wypiszPracownikowJakoOpiekunow(){
        System.out.println("Wszyscy pracownicy pracujacy jako opiekunowie:");
        for(int i=0;i<pracownicyJakoOpiekunowie.size();i++){
            System.out.println((i+1)+". "+pracownicyJakoOpiekunowie.get(i));
        }
    }
}
