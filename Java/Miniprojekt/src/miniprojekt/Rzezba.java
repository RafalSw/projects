package miniprojekt;

import java.io.Serializable;

public class Rzezba extends Dzielo implements Serializable{
    private String tworzywo;
    
    private Rzezba(){};
    
    //dziedziczenie dynamiczne
    public Rzezba(Dzielo poprzednieDzielo, String tworzywoN){
        super(poprzednieDzielo.galeria, poprzednieDzielo.nazwa, poprzednieDzielo.wymiary, poprzednieDzielo.autor, poprzednieDzielo.aktualnaWystawa, poprzednieDzielo.opis, poprzednieDzielo.uwagi, poprzednieDzielo.dataWykonania);
        tworzywo = tworzywoN;
        
        Dzielo.usunDzielo(poprzednieDzielo);
        if(poprzednieDzielo.zwrocWystawe()!=null)
        {
            poprzednieDzielo.zwrocWystawe().usunEksponat(poprzednieDzielo);
            poprzednieDzielo.zwrocWystawe().dodajEksponat(this);
        }   
        Dzielo.dodajDzielo(this);
        
    }
    
    //implementacja metody abstrakcyjnej - niezbedna do polimorfizum
    public String zwrocTyp() {
        return "rzezba wykonana z tworzywa "+ tworzywo;
    }
}
