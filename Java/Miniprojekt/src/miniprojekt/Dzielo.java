package miniprojekt;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;
import java.util.concurrent.TimeUnit;

//klasa abstrakcyjna
public abstract class Dzielo implements Serializable{ //Serializowanie niezbędne do zapisu na dysk
    protected static final float wartoscZaWiek = 1.0f; //wartosc dziela za jeden dzien, atrybut klasowy
    protected static final int MAXYMALNA_LICZBA_UWAG = 5; //atrybut klasowy
    protected String nazwa;
    protected String opis = null; //atrybut opcjonalny, domyślnie pusty (null), zmieniany w metodzie "dodajOpis"
    protected Wymiar wymiary;
    protected ArrayList<String> autor; //atrybut powtarzalny
    protected String[] uwagi; //atrybut powtarzalny
    protected Date dataWykonania; //atrybut zlozony
   
    //asocjacja binarna Dzielo - Galeria (jeden do wielu)
    protected GaleriaSztuki galeria;
    
    //asocjacja kwalifikowana
    protected Wystawa aktualnaWystawa = null;
    
    
    //asocjacja binarna
    public void przypiszGalerie(GaleriaSztuki g){
        galeria = g;
        g.dodajDzielo(this);
    }
    
    private static Vector<Dzielo> dzielaSztuki = new Vector<>(); //ekstensja, atrybut klasowy
    
    public Dzielo(){
        galeria = null;
        nazwa = "nazwa testowa";
        wymiary = new Wymiar(10,20,30);
        autor = new ArrayList<>();
        //autor.add("Jan Kowalski");
        dataWykonania = new Date(new Date().getTime()-259200000L); //3 dni temu
        uwagi = new String[MAXYMALNA_LICZBA_UWAG];
    }
    
    //konstruktor potrzebny do dziedziczenia dynamicznego
    public Dzielo(GaleriaSztuki galeriaN, String nazwaN, Wymiar wymiaryN, ArrayList<String> autorN, Wystawa wystawaN, String opisN, String[] uwagiN, Date dataWykonaniaN){
        galeria = galeriaN;
        nazwa = nazwaN;
        wymiary = wymiaryN;
        autor = autorN;
        aktualnaWystawa = wystawaN;
        opis = opisN;
        uwagi = uwagiN;
        dataWykonania = dataWykonaniaN;
    }
    
    public void przypiszNazwe(String n){
        nazwa = n;
    }
    
    public void przypiszAutora(String a){
        autor.add(a);
    }
    
    public void dodajOpis(String s){
        opis = s; //przypisanie wartosci do atrybutu opcjonalnego "opis"
    }
        
    public static void dodajDzielo(Dzielo noweDzielo){ //metoda klasowa
        dzielaSztuki.add(noweDzielo); //dodawanie dziela do ekstensji
    }
    
    public static void usunDzielo(Dzielo usuwaneDzielo){ //metoda klasowa
        dzielaSztuki.remove(usuwaneDzielo); //usuwanie dzieła z ekstensji
    }
    
    public Vector<Dzielo> zwrocDziela(){
        return dzielaSztuki;
    }
    
    public static void wypiszDziela(){ //metoda klasowa
        for(int i = 0; i<dzielaSztuki.size();i++)
            System.out.println(dzielaSztuki.get(i)); //wypisywanie kolejno dzieł w galerii
    }
    
    public String toString(){ //przesłonięcie
        String autorzy = "";
        for(int i=0;i<autor.size();i++){
            if(i==autor.size()-1) //po ostatnim autorze nie dodajemy przecinka
                autorzy+=autor.get(i);
            else
                autorzy+=autor.get(i)+", ";
        }
        return "Dzieło sztuki \""+nazwa+"\" autorstwa "+autorzy+" znajduje sie w galerii: "+galeria+" "+opis;
    }
    
    //atrybut pochodny:
    private int wiek(){ //funkcja liczaca wiek dziela wyrazony w dniach od obecnej daty
        long roznica = new Date().getTime() - dataWykonania.getTime();
        return (int)TimeUnit.DAYS.convert(roznica, TimeUnit.MILLISECONDS);
    }
    
    public float obliczWartosc(){ //metoda przeciazana
        return wiek()*wartoscZaWiek;
    }
    
    public float obliczWartosc(float kurs){ //metoda przeciazajaca
        return wiek()*wartoscZaWiek * kurs;
    }
    
    public String zwrocNazwe(){
        return nazwa;
    }
    
    public Wystawa zwrocWystawe(){
        return aktualnaWystawa;
    }
    
    public void przypiszWystawe(Wystawa w){
        aktualnaWystawa = w;
        w.dodajEksponat(this);
    }
    
    public void usunZWystawy(){
        if(aktualnaWystawa!=null)
            aktualnaWystawa.usunEksponat(this);
    }
    
    
    //funkcja polimorficzna
    public abstract String zwrocTyp();
}
