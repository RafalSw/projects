
package miniprojekt;

import java.util.ArrayList;

public class Pracownik {
    private String imie;
    private String nazwisko;
    
    // asocjacja z argumentem
    public ArrayList<GaleriaPracownik> galerie;
    
    public Pracownik(String i, String n){
        galerie = new ArrayList<>();
        imie = i;
        nazwisko = n;
    }
    
    public void zatrudnijSie(GaleriaSztuki g, int noweWynagrodzenie){
        GaleriaPracownik galeriaPracownik = new GaleriaPracownik();
        galeriaPracownik.utworzPolaczenie(g, this, noweWynagrodzenie);
    }
    
    public String toString(){
        return imie+" "+nazwisko;
    }
}
