package miniprojekt;

import java.util.HashSet;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

public class Wystawa {
    private String nazwa;
    private GaleriaSztuki galeria;
    //asocjacja kwalifikowana
    private TreeMap<String,Dzielo> eksponaty = new TreeMap<>();
    
    //kompozycja
    private Vector<Opiekun> opiekunowie = new Vector<>();
    private static HashSet<Opiekun> wszyscyOpiekunowie = new HashSet<>();
    
    public void dodajOpiekuna(Opiekun o){
        if(!opiekunowie.contains(o)){
            if(wszyscyOpiekunowie.contains(o)){
                System.out.println("Taki opiekun już istnieje na jakiejs wystawie");
            }
            else{
                opiekunowie.add(o);
                wszyscyOpiekunowie.add(o);
            }
        }
    }
    
    private void usunOpiekunaZWystawy(Opiekun o){
        if(opiekunowie.contains(o)){
            opiekunowie.remove(o);
        }
        else{
            System.out.println("Taki opiekun nie istnial na tej wystawie");
        }
    }
    
    public static void usunOpiekuna(Opiekun o){
        Opiekun.usun(o);
        if(wszyscyOpiekunowie.contains(o)){
            wszyscyOpiekunowie.remove(o);
            o.zwrocWystawe().usunOpiekunaZWystawy(o);
        }
        else{
            System.out.println("Taki opiekun nigdy nie zostal przypisany do zadnej wystawy");
        }
    }
    
    public Wystawa(String n){
        nazwa = n;
    }
    
    public void dodajEksponat(Dzielo d){
        if(!eksponaty.containsKey(d.zwrocNazwe())){
            eksponaty.put(d.zwrocNazwe(), d);
            d.przypiszWystawe(this);
        }
    }
    
    public void usunEksponat(Dzielo d){
        if(eksponaty.containsKey(d.zwrocNazwe())){
            eksponaty.remove(d.zwrocNazwe());
        }
    }
    
    public void wypiszEksponaty(){
        int i=0;
        System.out.println("Eksponaty z wystawy "+nazwa+":");
        for(Map.Entry<String, Dzielo> wpis : eksponaty.entrySet()){
            System.out.println((i+1)+". "+eksponaty.get(wpis.getKey()));
            i++;
        }
    }
    
    public void przypiszGalerie(GaleriaSztuki g){
        galeria = g;
        g.dodajWystawe(this);
    }
    
    public String zwrocNazwe(){
        return nazwa;
    }
}
