
package miniprojekt;

import java.util.Scanner;

public class Projekt {
    private Scanner scanner = new Scanner(System.in);
    GaleriaSztuki galeria = new GaleriaSztuki();
    
    private void menuKlienta(){
        while(true)
        {
            System.out.println("Menu klienta\n1. Wypisz pracownikow\n2. Wypisz dziela\n3. Powrot");
            int wybor = scanner.nextInt();
            if(wybor==1){
                galeria.wypiszPracownikow();
            }
            else if(wybor==2){
                galeria.wypiszDziela();
            }
            else if(wybor==3){
                break;
            }
            else
                System.out.println("Wybor nie poprawny.");
        }
    }
    
    private void menuPracownika(){
        while(true)
        {
            System.out.println("Menu pracownika\n1. Wypisz pracownikow\n2. Wypisz dziela\n3. Zarzadzanie dzielami\n4. Zarzadzanie wystawami\n5. Powrot");
            int wybor = scanner.nextInt();
            scanner.nextLine();
            if(wybor==1){
                galeria.wypiszPracownikow();
            }
            else if(wybor==2){
                galeria.wypiszDziela();
            }
            else if(wybor==3){
                System.out.println("Zarzadzanie dzielami\n1. Dodaj dzielo\n2. Usun dzielo\n3. Powrot");
                wybor = scanner.nextInt();
                scanner.nextLine();
                if(wybor==1){
                    Dzielo nowe=null;
                    System.out.println("Podaj typ dziela\n1. Obraz\n2. Ceramika gliniana\n3. Ceramika weglikowa");
                    wybor=scanner.nextInt();
                    scanner.nextLine();
                    
                    if(wybor == 1){
                        System.out.println("1. Akwarela\n2. Olejny\n3. Witraz\n4. Olejny witraz");
                        wybor = scanner.nextInt();
                        scanner.nextLine();
                        
                        if(wybor==1)
                            nowe = new Obraz(new int[]{0});
                        else if (wybor==2)
                            nowe = new Obraz(new int[]{1});
                        else if (wybor==3)
                            nowe = new Obraz(new int[]{2});
                        else if (wybor==4)
                            nowe = new Obraz(new int[]{1,2});
                    }
                    else if(wybor==2){
                        nowe = new CeramikaGliniana();
                    }
                    else if(wybor==3){
                        nowe = new CeramikaWeglikowa();
                    }
                    
                    if(nowe!=null){
                        System.out.println("Podaj nazwe");
                        String nazwa = scanner.nextLine();
                        nowe.przypiszNazwe(nazwa);
                        
                        System.out.println("Podaj autora");
                        String autor = scanner.nextLine();
                        nowe.przypiszAutora(autor);
                        
                        System.out.println("Dodaj opis dziela");
                        String opis = scanner.nextLine();
                        nowe.dodajOpis(opis);
                        
                        galeria.dodajDzielo(nowe);
                        System.out.println("Dzielo zostalo dodane do galerii: "+nowe);
                    }
                    else{
                        System.out.println("Nie udalo sie dodac dziela");
                    }
                    
                }
                else if(wybor==2){
                    System.out.println("Podaj numer dziela");
                    wybor = scanner.nextInt();
                    scanner.nextLine();
                    wybor = wybor-1;
                    if(wybor<0 || wybor>=galeria.iloscDziel()){
                        System.out.println("Numer dziela nie jest prawidlowy");
                    }
                    else{
                        System.out.println("Usuwanie dziela");
                        galeria.usunDzielo(wybor);
                    }
                }
                else
                    System.out.println("Nieprawidlowy wybor, powrot do menu.");
            }
            else if(wybor==4){
                System.out.println("Zarzadzanie wystawami\n1. Dodawanie wystawy\n2. Dodawanie opiekuna\n3. Dodawanie dziel do wystawy");
                wybor = scanner.nextInt();
                scanner.nextLine();
                if(wybor==1){
                    System.out.println("Podaj nazwe nowej wystawy");
                    String nazwa = scanner.nextLine();
                    Wystawa nowaWystawa = new Wystawa(nazwa);
                    galeria.dodajWystawe(nowaWystawa);
                }
                else if(wybor==2){
                    if(galeria.iloscPracownikow()==0 || galeria.iloscWystaw()==0){
                        System.out.println("Nie ma mozliwosci dodania opiekuna wystawy - brak pracownikow lub opiekunow");
                    }
                    else{
                        System.out.println("Podaj numer pracownika");
                        int pracownik = scanner.nextInt();
                        scanner.nextLine();
                        pracownik = pracownik-1;
                        if(pracownik<0 || pracownik >=galeria.iloscPracownikow()){
                            System.out.println("Nie ma takiego pracownika");
                        }
                        else{
                            System.out.println("Podaj numer wystawy");
                            int numerWystawy = scanner.nextInt();
                            scanner.nextLine();
                            numerWystawy = numerWystawy-1;
                            if(numerWystawy<0 || numerWystawy>=galeria.iloscWystaw()){
                                System.out.println("Nie ma takiej wystawy");
                            }
                            else{
                                Opiekun nowyOpiekun = new Opiekun(galeria.zwrocPracownika(pracownik),galeria.zwrocWystawe(numerWystawy));
                                System.out.println("Podaj kompetencje. Enter dla zatwierdzenia. Aby przerwac, pusta linia");
                                String kompetencje = null;
                                while(true){
                                    kompetencje = scanner.nextLine();
                                    if(kompetencje.length()==0)
                                        break;
                                    else
                                        nowyOpiekun.dodajKompetencje(kompetencje);
                                }
                                System.out.println("Wprowadzanie zmian zakonczone, opiekun dodany do wystawy");
                            }
                        }
                    }
                }
                else if(wybor==3){
                    System.out.println("Podaj numer wystawy");
                    int numerWystawy = scanner.nextInt();
                    scanner.nextLine();
                    numerWystawy--;
                    if(numerWystawy<0 || numerWystawy>=galeria.iloscWystaw()){
                        System.out.println("Nie ma takiej wystawy");
                    }
                    else{
                    
                        System.out.println("Podaj numer dziela, ktore chcesz dodac do wystawy");
                        int numerDziela = scanner.nextInt();
                        scanner.nextLine();
                        numerDziela--;
                        if(numerDziela<0 || numerDziela>=galeria.iloscDziel()){
                            System.out.println("Nie ma takieg o dziela");
                        }
                        else{
                            galeria.zwrocWystawe(numerWystawy).dodajEksponat(galeria.zwrocDzielo(numerDziela));
                        }
                    }
                }
                
                else{
                    System.out.println("Wybor nie jest prawidlowy");
                }
            }
            else if(wybor==5){
                break;
            }
            else
                System.out.println("Wybor nie poprawny.");
        }
    }
    
    private void menu(){
       
        int wyborTypu;
         
        while(true){
            System.out.println("Wybierz typ użytkownika:\n1. Klient\n2. Pracownik\n3. Dodawanie pracownika\n4. Wczytaj\n5. Zapisz\n6. Wyjscie");
            wyborTypu= scanner.nextInt();
            scanner.nextLine();

            if(wyborTypu==1){
                menuKlienta();
            }
            else if (wyborTypu==2){
                System.out.println("Podaj haslo");
                scanner.reset();
                String haslo = scanner.nextLine();
                if(haslo.equals("admin")){
                    System.out.println("Haslo poprawne");
                    menuPracownika();
                }
                else{
                    System.out.println("Haslo nie poprawne!");
                }
            }
            else if(wyborTypu==3){
                System.out.println("Dodawanie pracownika.\nPodaj imie");
                String imie = scanner.nextLine();
                System.out.println("Podaj nazwisko");
                String nazwisko = scanner.nextLine();
                System.out.println("Podaj wynagrodzenie");
                int wynagrodzenie = scanner.nextInt();
                scanner.nextLine();
                Pracownik nowyPracownik = new Pracownik(imie, nazwisko);
                nowyPracownik.zatrudnijSie(galeria, wynagrodzenie);
                System.out.println("Pracownik "+nowyPracownik+" zostal zatrudniony w galerii");
                
            }
            else if(wyborTypu==4){
                galeria.wczytaj();
            }
            else if(wyborTypu==5){
                galeria.zapisz();
            }
            else if(wyborTypu==6){
                System.exit(0);
            }
            else
                System.out.println("Wybor nie poprawny.");
        }
    }
    
    public static void main(String[] args) {
         Projekt projekt = new Projekt();
         projekt.menu();
         
    }
}
