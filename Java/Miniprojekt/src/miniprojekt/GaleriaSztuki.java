package miniprojekt;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class GaleriaSztuki{
    
    public String nazwa;
    //asocjacja binarna Dzielo - Galeria (jeden do wielu)
    private ArrayList<Dzielo> dziela;
    
    //asocjacja z atrybutem
    public ArrayList<GaleriaPracownik> pracownicy;
    
    //asocjacja kwalifikowana
    private TreeMap<String,Wystawa> wystawy = new TreeMap<>();
    
    
    //asocjacja binarna
    public void dodajDzielo(Dzielo d){
        if(!dziela.contains(d)){
            dziela.add(d);
            d.przypiszGalerie(this);
        }
    }
    
    public void usunDzielo(int idDziela){
        Dzielo d = dziela.get(idDziela);
        if(dziela.contains(d)){
            dziela.remove(d);
            d.usunZWystawy();
        }
    }
    
    public int iloscDziel(){
        return dziela.size();
    }
    
    public GaleriaSztuki(){ //konstruktor Galerii
        dziela = new ArrayList<>();
        pracownicy = new ArrayList<>();
        nazwa = "Mona Lisa";
    }
    public String toString(){
        return nazwa;
    }
    
    //asocjacja z atrybutem
    public void zatrudnijPracownika(Pracownik p, int noweWynagrodzenie){
        GaleriaPracownik galeriaPracownik = new GaleriaPracownik();
        galeriaPracownik.utworzPolaczenie(this, p, noweWynagrodzenie);
    }
    
    public void wypiszDziela(){
        System.out.println("Dziela w galerii:");
        for(int i=0;i<dziela.size();i++)
            System.out.println((i+1)+". "+dziela.get(i));
    }
    
    public Dzielo zwrocDzielo(int i){
        return dziela.get(i);
    }
    
    public void wypiszPracownikow(){
        System.out.println("Lista pracownikow: ");
        for(int i=0; i<pracownicy.size(); i++){
            System.out.println((i+1)+". "+pracownicy.get(i).zwrocPracownika()+" zarabia "+pracownicy.get(i).zwrocWynagrodzenie());
        }
    }
    
    public int iloscPracownikow(){
        return pracownicy.size();
    }
    
    public Pracownik zwrocPracownika(int i){
        return pracownicy.get(i).zwrocPracownika();
    }
    
    
    //asocjacja kwalifikowana
    public void dodajWystawe(Wystawa d){
        if(!wystawy.containsKey(d.zwrocNazwe())){
            wystawy.put(d.zwrocNazwe(), d);
            d.przypiszGalerie(this);
        }
    }
    
    public Wystawa zwrocWystawe(int i){
        int j=0;
        
        for(Map.Entry<String, Wystawa> wpis : wystawy.entrySet()){
            if(j==i)
                return wpis.getValue();
            i++;
        }
        return null;
    }
    
    //asocjacja kwalifikowana
    public void wypiszWystawy(){
        int i=0;
        System.out.println("Eksponaty z galerii "+nazwa+":");
        for(Map.Entry<String, Wystawa> wpis : wystawy.entrySet()){
            System.out.println((i+1)+". "+wpis.getKey());
            i++;
        }
    }
    
    public int iloscWystaw(){
        return wystawy.size();
    }
    
    //trwałość ekstensji - zapis i odczyt z pliku
    public void zapisz()
    {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("dane.dat"));
            out.writeObject(dziela);
            out.close();
        } catch (IOException ex) {
            System.out.println("Nie można utworzyć pliku do zapisu!" + ex);
        }
    }
    
    public final void wczytaj(){ //final bo metoda jest wykorzystywana w konstruktorze
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("dane.dat"));
            dziela = (ArrayList<Dzielo>)in.readObject();
            in.close();
        } catch (IOException ex) {
            System.out.println("Nie można otworzyć pliku do odczytu!");
        } catch (ClassNotFoundException ex) {
            System.out.println("Nie istnieje klasa, ktorej obiekt jest wczytywany");
        }
    }
}
