package miniprojekt;

import java.io.Serializable;


//implementuje interfejs CeramikaI i rozszerza klase Dzielo - wielodziedziczenie
public class CeramikaWeglikowa extends Dzielo implements Serializable, CeramikaI{

    //implementacja metody abstrakcyjnej - niezbedna do polimorfizum
    public String zwrocTyp() {
        return "ceramika weglikowa";
    }

    @Override
    public String zwrocSklad() {
        return "wolfram, tytan";
    }
}