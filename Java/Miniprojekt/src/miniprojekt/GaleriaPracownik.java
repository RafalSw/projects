package miniprojekt;

public class GaleriaPracownik {
    private int wynagrodzenie;
    
    //asocjacja z argumentem
    private GaleriaSztuki galeria;
    private Pracownik pracownik;
    
    public GaleriaPracownik(){
        
    }
    
    public GaleriaPracownik(GaleriaSztuki g, Pracownik p, int noweWynagrodzenie){
        galeria = g;
        pracownik = p;
        wynagrodzenie  = noweWynagrodzenie;
    }
    
    public void utworzPolaczenie(GaleriaSztuki g, Pracownik p, int noweWynagrodzenie){
        GaleriaPracownik nowa = new GaleriaPracownik(g,p,noweWynagrodzenie);
        p.galerie.add(nowa);
        g.pracownicy.add(nowa);
    }
    
    public GaleriaSztuki zwrocGalerie(){
        return galeria;
    }
    
    public Pracownik zwrocPracownika(){
        return pracownik;
    }
    
    public int zwrocWynagrodzenie(){
        return wynagrodzenie;
    }
}
