import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;
import java.io.Serializable;

public class Model implements Serializable{
    private int field1 = Integer.MIN_VALUE;
    private int field2 = Integer.MIN_VALUE;
    private int field3 = Integer.MIN_VALUE;

    public int getField1() {
        return field1;
    }
    
    public int getField2() {
        return field2;
    }
    
    public int getField3() {
        return field3;
    }
    
    public void setField1(int i){
        field1=i;
    }
    
    public void setField2(int i){
        field2=i;
    }
    
    public void setField3(int i){
        field3=i;
    }
}
