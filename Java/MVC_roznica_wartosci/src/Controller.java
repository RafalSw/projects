
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;
import java.beans.VetoableChangeSupport;

public class Controller {
    private final Model model;
    private final PropertyChangeSupport propertyChange = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetos = new VetoableChangeSupport(this);
    
    public Controller(Model m, ModelLimiter limiter){
        model = m;       
        addVetoableChangeListener(limiter);
    }
    
    public synchronized void addPropertyChangeListener(PropertyChangeListener l) {
        propertyChange.addPropertyChangeListener(l);
    }

    public synchronized void removePropertyChangeListener(PropertyChangeListener l) {
        propertyChange.removePropertyChangeListener(l);
    }
    
    public final synchronized void addVetoableChangeListener(VetoableChangeListener l) {
	vetos.addVetoableChangeListener(l);
    }

    public synchronized void removeVetoableChangeListener(VetoableChangeListener l) {
	vetos.removeVetoableChangeListener(l);
    }
    
        public final synchronized void setField1(String field1New) {
        if(field1New.equals("")){
            propertyChange.firePropertyChange("field1", null, "");
            propertyChange.firePropertyChange("field2", null, "hide");
            propertyChange.firePropertyChange("field3", null, "hide");
        }
        else{
           int oldValue = model.getField1();
            try{
                model.setField1(Integer.parseInt(field1New));
                if(model.getField1()!=oldValue){
                    propertyChange.firePropertyChange("field1", oldValue, field1New);
                    propertyChange.firePropertyChange("field2", oldValue, "");
                    propertyChange.firePropertyChange("field3", oldValue, "");
                    model.setField3(Integer.MIN_VALUE);
                    model.setField2(Integer.MIN_VALUE);
                }
            }
            catch(NumberFormatException e){
                if(oldValue==Integer.MIN_VALUE){
                    propertyChange.firePropertyChange("field1", null, "");
                }
                else
                    propertyChange.firePropertyChange("field1", Integer.toString(oldValue), Integer.toString(oldValue));
            }
        }
    }
    
    public final synchronized void setField2(String field2New) {
        int oldValue = model.getField2();
        try{
            int temp = Integer.parseInt(field2New);
            if(temp!=oldValue){
                if(oldValue!=Integer.MIN_VALUE){
                    vetos.fireVetoableChange(Integer.toString(model.getField1()), temp, model.getField3());
                }
                model.setField2(temp);
                propertyChange.firePropertyChange("field2", oldValue, model.getField2());
            }
        }
        catch(NumberFormatException |PropertyVetoException e){
            System.out.println(e.toString());
            if(oldValue==Integer.MIN_VALUE){
                propertyChange.firePropertyChange("field2", null, "");
            }
            else
                propertyChange.firePropertyChange("field2", null, oldValue);
        }
    }
    
    public final synchronized void setField3(String field3New) {
        int oldValue = model.getField3();
        try{
            int temp =  Integer.parseInt(field3New);
            if(temp!=oldValue){
                vetos.fireVetoableChange(Integer.toString(model.getField1()), model.getField2(), temp);
                model.setField3(temp);
                propertyChange.firePropertyChange("field3", oldValue, model.getField3());
            }
        }
        catch(NumberFormatException | PropertyVetoException e){
            System.out.println(e.toString());
            if(oldValue==Integer.MIN_VALUE){
                propertyChange.firePropertyChange("field3", null, "");
            }
            else
                propertyChange.firePropertyChange("field3", null, oldValue);
        }
    }
}
