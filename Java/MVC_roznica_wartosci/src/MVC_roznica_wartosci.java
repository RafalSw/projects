public class MVC_roznica_wartosci {

    public static void main(String[] args) {
        Test.test1();
        Test.test2();
        Test.test3();
        
        Controller controller = new Controller(new Model(), new ModelLimiter());
        
        FieldView field1View = new FieldView(controller, "field1");
        FieldView field2View = new FieldView(controller, "field2");
        FieldView field3View = new FieldView(controller, "field3");
        
        ControllerGUI gui = new ControllerGUI(controller, field1View, field2View, field3View);
    }
    
}
