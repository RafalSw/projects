
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

public class Test {
    public static void test1(){
        Model model = new Model();
        Controller controller = new Controller(model, new ModelLimiter());
        
        FieldView field1View = new FieldView(controller, "field1");
        FieldView field2View = new FieldView(controller, "field2");
        FieldView field3View = new FieldView(controller, "field3");
        
        ControllerGUI gui = new ControllerGUI(controller, field1View, field2View, field3View);
        gui.setVisible(false);
        
        field1View.setText("10");
        for(FocusListener listener: field1View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field1View,0));
        }
        field2View.setText("50");
        for(FocusListener listener: field2View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field2View,0));
        }
        field3View.setText("40");
        for(FocusListener listener: field3View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field3View,0));
        }
        
        if(model.getField1()==10 && model.getField2()==50 && model.getField3()==40)
            System.out.println("Test 1 - 1/3: SUCCESS");
        else
            System.out.println("Test 1 - 1/3: FAILURE");
        
        field3View.setText("39");
        for(FocusListener listener: field3View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field3View,0));
        }
        
        if(model.getField1()==10 && model.getField2()==50 && model.getField3()==40)
            System.out.println("Test 1 - 2/3: SUCCESS");
        else
            System.out.println("Test 1 - 2/3: FAILURE");
        
        field2View.setText("51");
        for(FocusListener listener: field2View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field2View,0));
        }
        
        if(model.getField1()==10 && model.getField2()==50 && model.getField3()==40)
            System.out.println("Test 1 - 3/3: SUCCESS");
        else
            System.out.println("Test 1 - 3/3: FAILURE");
    }
    
    public static void test2(){
        Model model = new Model();
        Controller controller = new Controller(model, new ModelLimiter());
        
        FieldView field1View = new FieldView(controller, "field1");
        FieldView field2View = new FieldView(controller, "field2");
        FieldView field3View = new FieldView(controller, "field3");
        
        ControllerGUI gui = new ControllerGUI(controller, field1View, field2View, field3View);
        gui.setVisible(false);
        
        if(field1View.isEditable()==true && field2View.isEditable()==false && field3View.isEditable()==false)
            System.out.println("Test 2 - 1/3: SUCCESS");
        else
            System.out.println("Test 2 - 1/3: FAILURE");
        
        field1View.setText("10");
        for(FocusListener listener: field1View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field1View,0));
        }
        
        if(field1View.isEditable()==true && field2View.isEditable()==true && field3View.isEditable()==true)
            System.out.println("Test 2 - 2/3: SUCCESS");
        else
            System.out.println("Test 2 - 2/3: FAILURE");
        
        field1View.setText("");
        for(FocusListener listener: field1View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field1View,0));
        }
        if(field1View.isEditable()==true && field2View.isEditable()==false && field3View.isEditable()==false)
            System.out.println("Test 2 - 3/3: SUCCESS");
        else
            System.out.println("Test 2 - 3/3: FAILURE");
    }
    
    public static void test3(){
        Model model = new Model();
        Controller controller = new Controller(model, new ModelLimiter());
        
        FieldView field1View = new FieldView(controller, "field1");
        FieldView field2View = new FieldView(controller, "field2");
        FieldView field3View = new FieldView(controller, "field3");
        
        ControllerGUI gui = new ControllerGUI(controller, field1View, field2View, field3View);
        gui.setVisible(false);
        
        field1View.setText("10");
        for(FocusListener listener: field1View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field1View,0));
        }
        field2View.setText("50");
        for(FocusListener listener: field2View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field2View,0));
        }
        field3View.setText("40");
        for(FocusListener listener: field3View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field3View,0));
        }
        
        if(model.getField1()==10 && model.getField2()==50 && model.getField3()==40)
            System.out.println("Test 3 - 1/3: SUCCESS");
        else
            System.out.println("Test 3 - 1/3: FAILURE");
        
        field1View.setText("15");
        for(FocusListener listener: field1View.getFocusListeners()){
            listener.focusLost(new FocusEvent(field1View,0));
        }
        
        if(field1View.isEditable()==true && field2View.isEditable()==true && field3View.isEditable()==true)
            System.out.println("Test 3 - 2/3: SUCCESS");
        else
            System.out.println("Test 3 - 2/3: FAILURE");
        
        if(field1View.getText().equals("15") && field2View.getText().equals("") && field3View.getText().equals(""))
            System.out.println("Test 3 - 3/3: SUCCESS");
        else
            System.out.println("Test 3 - 3/3: FAILURE");
        
    }
}
