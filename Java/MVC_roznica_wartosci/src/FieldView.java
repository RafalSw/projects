
import java.awt.Dimension;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.JFormattedTextField;

public class FieldView extends JFormattedTextField implements PropertyChangeListener{
    String name;
    Controller controller;
    public FieldView(Controller c,String n){
        name = n;
        controller = c;  
        addFocusListener(new FocusListener(){

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                switch(name){
                    case "field1":{
                        controller.setField1(getText());
                        break;
                    }
                    case "field2":{
                        controller.setField2(getText());
                        break;
                    }
                    case "field3":{
                        controller.setField3(getText());
                        break;
                    }
                }
            }
        });
                
        setPreferredSize(new Dimension(100, 20));
    }
    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        if(pce.getPropertyName().equals(name)){
            String value;
            String oldValue;
            if(pce.getNewValue() instanceof Integer)
                value = Integer.toString((Integer)pce.getNewValue());
            else
                value = (String)pce.getNewValue();
            
            if(pce.getOldValue() instanceof Integer)
                oldValue = Integer.toString((Integer)pce.getOldValue());
            else
                oldValue = (String)pce.getOldValue();
            
            if(!value.equals(oldValue)){
                if(value.equals("hide")){
                    setText("");
                    setEditable(false);
                    setFocusable(false);
                }
                else if(value.equals("")){
                    setText("");
                    setEditable(true);
                    setFocusable(true);
                }
                else{
                    setEditable(true);
                    setFocusable(true);
                    setText(value);
                }
                System.out.println("Value changed from " + pce.getOldValue() + " to " + pce.getNewValue());
            }
        }
    }
    
}
