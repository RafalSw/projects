
import java.awt.Container;
import javax.swing.BoxLayout;
import javax.swing.JFrame;

public class ControllerGUI extends JFrame{
    
    ControllerGUI(Controller controller, FieldView field1, FieldView field2, FieldView field3){
        Container container = getContentPane();
        container.setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        
        controller.addPropertyChangeListener(field1);
        controller.addPropertyChangeListener(field2);
        controller.addPropertyChangeListener(field3);
        
        field2.setEditable(false);
        field3.setEditable(false);
        
        container.add(field1);
        container.add(field2);
        container.add(field3);
        
        this.setLocationRelativeTo(null);
        setDefaultCloseOperation(3);
        pack();
        setVisible(true);
    }
    
}
