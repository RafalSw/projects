
import java.beans.PropertyChangeEvent;
import java.beans.PropertyVetoException;
import java.beans.VetoableChangeListener;

public class ModelLimiter implements VetoableChangeListener{
    
    @Override
    public void vetoableChange(PropertyChangeEvent pce) throws PropertyVetoException {
        if((Integer)pce.getNewValue()<(Integer)pce.getOldValue()-Integer.parseInt(pce.getPropertyName()))
            throw new PropertyVetoException("Wartosc spoza dopuszczalnego zakresu", pce);
    }
    
}
