/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package e.korepetycje.checker;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import static java.util.concurrent.TimeUnit.MINUTES;
import java.util.logging.Level;
import java.util.logging.Logger;
 

/**
 *
 * @author Rafal
 */
public class EKorepetycjeChecker {

    private final String USER_AGENT = "Mozilla/5.0";
    public String login;
    public String password;
    public int interval;
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    
    public static void main(String[] args){
        EKorepetycjeChecker http = new EKorepetycjeChecker();
        new Window(http);
    }
    
    public void run(){
        try {
            sendPost();
        } catch (Exception ex) {
            Logger.getLogger(EKorepetycjeChecker.class.getName()).log(Level.SEVERE, null, ex);
        }
        final Runnable loging = new Runnable() {
            public void run() { try {
                sendPost();
                } catch (Exception ex) {
                    Logger.getLogger(EKorepetycjeChecker.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        
        final ScheduledFuture<?> logingHandle = scheduler.scheduleAtFixedRate(loging, interval, interval, MINUTES);
    }
    
    private void sendPost() throws Exception {
 
        System.out.println("sendPost");
        String url = "http://www.e-korepetycje.net/zaloguj";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        String urlParameters = "login="+login+"&passwd="+password+"&post_check=login_form";
        

        // Send post request
        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(urlParameters);
        wr.flush();
        wr.close();

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'POST' request to URL : " + url);
        System.out.println("Post parameters : " + urlParameters);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
        }
        in.close();

        //print result
        //System.out.println(response.toString());

    }
}
