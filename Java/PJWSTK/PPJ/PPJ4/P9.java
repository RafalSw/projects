import java.util.Scanner;

public class P9 {
    public static void main(String[] args) {
        int liczba, liczbaCyfr=1;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący liczbę cyfr danej liczby\nPodaj liczbę:");
        liczba=scanner.nextInt();
        
        
        int i=10;
        while(liczba/i>0){
            i*=10;
            liczbaCyfr++;
        }
        
        if(liczbaCyfr==1){ //zabawa z poprawnością językową. Można sobie to darować i mieć niepoprawne końcówki
            System.out.println("Liczba "+liczba+" ma "+liczbaCyfr+" cyfrę");
        }
        else if(liczbaCyfr>=2 && liczbaCyfr<=4){
            System.out.println("Liczba "+liczba+" ma "+liczbaCyfr+" cyfry");
        }
        else{
            System.out.println("Liczba "+liczba+" ma "+liczbaCyfr+" cyfr");
        }
    }
}
