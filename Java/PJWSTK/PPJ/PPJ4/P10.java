import java.util.Scanner;

public class P10 {
    public static void main(String[] args) {
        int liczba;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący liczby mniejsze od zadanej, których suma cyfr jest parzysta\nPodaj liczbę:");
        liczba=scanner.nextInt();
        System.out.println("Wyniki: ");
        for(int i=1;i<liczba;i++){ //dla każdej mniejszej liczby od zadanej rozpoczynamy nowe badanie
            int mnoznik=1;
            int sumaCyfr=0;
            while(i/mnoznik>0){ //sprawdzamy kolejne cyfry
                sumaCyfr+=i/mnoznik; //dodajemy do sumy wartosc cyfry
                mnoznik*=10;
            }
            if(sumaCyfr%2==0){//sprawdzenie czy suma jest parzysta
                System.out.println(i);
            }
        }
    }
}
