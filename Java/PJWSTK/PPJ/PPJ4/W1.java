import java.util.Scanner;

public class W1 {

    public static void main(String[] args) {
        double a,b,c,delta,result1,result2;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie wspolczynnikow rownania kwadratowego ax^2+bx+c=0\nPodaj a:");
        a=scanner.nextDouble();
        System.out.println("Podaj b:");
        b=scanner.nextDouble();
        System.out.println("Podaj c:");
        c=scanner.nextDouble();
        
        delta=b*b-4*a*c;
        if(delta<0){
            System.out.println("Brak rozwiazań");
        }
        else{
            result1=(-b-Math.sqrt(delta))/(2*a);
            result2=(-b+Math.sqrt(delta))/(2*a);
            
            if(Double.compare(result1, result2)==0){//==0 oznacza ze sa rowne
                System.out.println("Uklad ma jedno rozwiazanie: "+result1);
            }
            else{
                System.out.println("Układ ma 2 rozwiązania: "+result1+" oraz "+result2);
            }
        }
    }
}
