import java.util.Scanner;

public class P5 {
    public static void main(String[] args) {
        int liczba, silnia=1;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program liczący silnię danej liczby\nPodaj liczbę:");
        liczba=scanner.nextInt();
        
        while(liczba>0){ //silnia liczby 5 to 5*4*3*2*1. Wymanżamy otrzymany wynik przez liczbę o 1 mniejszą, dlatego co obrót pętli zmniejszamy liczbę.
            silnia*=liczba;
            liczba--;
        }
        
        System.out.println("Silnia podanej liczby to "+silnia);
    }
}
