import java.util.Scanner;

public class P4 {
    public static void main(String[] args) {
        int liczba1, liczba2, nwd=1;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący NWD dwóch liczb\nPodaj pierwszą liczbę:");
        liczba1=scanner.nextInt();
        System.out.println("Podaj drugą liczbę:");
        liczba2=scanner.nextInt();
        
        for(int i=1;i<=Math.min(liczba1, liczba2);i++){ //Math.min znajduje mniejsza z dwoch zadanych liczb
            if(liczba1%i==0 && liczba2%i==0){ //%oznacza resztę z dzielenia. jeśli jest równa 0, to znaczy że liczba jest podzielna przez i. Jesli obie są, to jest to jeden ze wspólnych dzielnikó
                nwd=i;
            }
        }
        
        System.out.println("Największy wspólny dzielnik liczb "+liczba1+" oraz "+liczba2+" to: "+ nwd);
    }
    
}
