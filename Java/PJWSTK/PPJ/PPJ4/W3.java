
import java.util.Scanner;

public class W3 {
    public static void main(String[] args) {
        double a1,b1,a2,b2,resultX,resultY;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie punktu przecięcia prostych\nPodaj wspolczynnik a pierwszej prostej:");
        a1=scanner.nextDouble();
        System.out.println("Podaj wspolczynnik b pierwszej prostej:");
        b1=scanner.nextDouble();
        System.out.println("Podaj wspolczynnik a drugiej prostej:");
        a2=scanner.nextDouble();
        System.out.println("Podaj wspolczynnik b drugiej prostej:");
        b2=scanner.nextDouble();
        
        resultX=(b2-b1)/(a1-a2);
        resultY=(a2*b1-a1*b2)/(a2-a1);
        
        System.out.println("Punkt przecięcia się tych prostych to P=("+resultX+";"+resultY+")");
    }    
}
