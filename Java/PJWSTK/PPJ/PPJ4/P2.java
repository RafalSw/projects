import java.util.Scanner;

public class P2 {
    public static void main(String[] args) {
        int liczba;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący dzielniki danej liczby\nPodaj liczbę:");
        liczba=scanner.nextInt();
        
        System.out.println("Dzielniki liczby "+liczba+":");
        for(int i=1;i<=liczba;i++){
            if(liczba%i==0){ //%oznacza resztę z dzielenia. jeśli jest równa 0, to znaczy że liczba jest podzielna przez i, więc i jest dzielnikiem
                System.out.print(i+", ");
            }
        }
    }
}
