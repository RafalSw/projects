
import java.util.Scanner;

public class W4 {
    public static void main(String[] args) {
        int predkoscDozwolona, predkoscOsiagnieta, kara, punkty;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie wartosci kary i ilosci punktow za przekroczenie predkosci\nPodaj osiagnieta predkosc: ");
        predkoscOsiagnieta=scanner.nextInt();
        System.out.println("Podaj dozwolona predkosc: ");
        predkoscDozwolona=scanner.nextInt();
        
        if(predkoscOsiagnieta<=predkoscDozwolona){
            kara=0;
            punkty=0;
        }
        else if(predkoscOsiagnieta-predkoscDozwolona<=20){
            kara=100;
            punkty=5;
        }
        else if(predkoscOsiagnieta-predkoscDozwolona<=40){
            kara=200;
            punkty=8;
        }
        else{
            kara=500;
            punkty=12;
        }
        
        System.out.println("Za taką prędkość kara to "+kara+"zł oraz "+punkty+" punktów.");
    }
    
}
