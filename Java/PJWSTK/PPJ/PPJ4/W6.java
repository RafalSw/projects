
import java.util.Scanner;

public class W6 {
    public static void main(String[] args) {
        double a,b,c;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Sprawdzanie poprawności trójkąta\nPodaj bok a:");
        a=scanner.nextDouble();
        System.out.println("Podaj bok b:");
        b=scanner.nextDouble();
        System.out.println("Podaj bok c:");
        c=scanner.nextDouble();
        
        if(a<=0 || b<=0 || c<=0){
            System.out.println("Przynajmniej jeden bok został podany niepoprawnie!");
        }
        else{
            if(a+b<=c || b+c<=a || a+c<=b){
                System.out.println("Taki trójkąt nie istnieje.");
            }
            else
            {
                System.out.println("Taki trójkąt istnieje.");
            }
        }
    }    
}
