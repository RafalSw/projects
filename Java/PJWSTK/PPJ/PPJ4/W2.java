
import java.util.Scanner;

public class W2 {
    public static void main(String[] args) {
        double a, result=0;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie podatku\nPodaj kwotę:");
        a=scanner.nextDouble();
        
        if(a<=44490){
            result=19*a/100-586.85;
        }
        else if(a<=85528){
            result=7866.25+30/100*(a-44490);
        }
        else{
            result=20177+40/100*(a-85528);
        }
        
        if(result<0){
            result = 0; //bo nie chcemy podatku ujemnego
        }
        System.out.println("Obliczony podatek to: "+result);
    }
}
