import java.util.Scanner;

public class W5 {

    public static void main(String[] args) {
        double r;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Obliczanie pola i obwodu kola\nPodaj promien:");
        r=scanner.nextDouble();
        
        if(r<0){
            System.out.println("Ujemna długość promienia nie jest dozwolona!");
        }
        else{
            System.out.println("Pole koła to "+(Math.PI*r*r)+" a obwod to "+2*Math.PI*r);
        }
    }
    
}
