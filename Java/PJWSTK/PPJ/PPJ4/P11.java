import java.util.Scanner;

public class P11 {
    public static void main(String[] args) {
        int liczba;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący cyfry zadanej liczby w odwrotnej kolejnosci\nPodaj liczbę:");
        liczba=scanner.nextInt();
        
        int mnoznik=1;
        while(liczba/mnoznik>0){ //sprawdzamy kolejne cyfry
            System.out.print((liczba/mnoznik)%10);
            mnoznik*=10;
        }
        System.out.println();
    }
}
