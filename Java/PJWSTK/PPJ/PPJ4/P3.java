import java.util.Scanner;

public class P3 {
    public static void main(String[] args) {
        int liczba;
        boolean result=true;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program sprawdzający czy liczba jest pierwszą\nPodaj liczbę:");
        liczba=scanner.nextInt();

        for(int i=2;i<Math.sqrt(liczba);i++){ //wystarczy sprawdzać do wartości pierwiastka
            if(liczba%i==0){ //%oznacza resztę z dzielenia. jeśli jest równa 0, to znaczy że liczba jest podzielna przez i, więc i jest dzielnikiem, czyli liczba nie jest pierwsza
                result=false;
            }
        }
        
        if(result==true){
            System.out.println("Liczba "+liczba+" jest liczbą pierwszą");
        }
        else{
            System.out.println("Liczba "+liczba+" NIE jest liczbą pierwszą");
        }
    }
    
}
