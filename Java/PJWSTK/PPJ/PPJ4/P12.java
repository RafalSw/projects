import java.util.Scanner;

public class P12 {
    public static void main(String[] args) {
        int liczba;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program wypisujący liczby mniejsze od zadanej, podzielne prze 5 i niepodzielne przez 3\nPodaj liczbę:");
        liczba=scanner.nextInt();
        
        for(int i=1;i<liczba;i++){
            if(i%5==0 && i%3!=0){
                System.out.println(i);
            }
        }
    }
}
