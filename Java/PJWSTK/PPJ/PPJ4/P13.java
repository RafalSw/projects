import java.util.Scanner;

public class P13 {
    public static void main(String[] args) {
        int liczba1, liczba2;
        boolean result=true;
        Scanner scanner=new Scanner(System.in);
        System.out.println("Program badający czy liczby są względnie pierwsze\nPodaj pierwszą liczbę:");
        liczba1=scanner.nextInt();
        System.out.println("Podaj drugą liczbę:");
        liczba2=scanner.nextInt();
        
        
        //SZTUCZKA: mamy gotowy program do liczenia NWD. Modyfikujemy żeby szukał tylko większych od 1. Jeśli coś znajdzie to koniec poszukiwań.
        for(int i=2;i<=Math.min(liczba1, liczba2);i++){ //Math.min znajduje mniejsza z dwoch zadanych liczb
            if(liczba1%i==0 && liczba2%i==0){ //%oznacza resztę z dzielenia. jeśli jest równa 0, to znaczy że liczba jest podzielna przez i. Jesli obie są, to jest to jeden ze wspólnych dzielnikó
                result=false;
                break;
            }
        }
        
        if(result==true){
            System.out.println("Liczby są względnie pierwsze.");
        }
        else{
            System.out.println("Liczby NIE są względnie pierwsze.");
        }
    }
    
}
