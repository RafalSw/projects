public class PPJ7_3 {
    
    public static void kwadrat(int rozmiar, char znak){
        if(!(znak=='x' || znak=='o')){
            System.out.println("Podano zly znak poczatkowy!");
        }
        else if(rozmiar<1){
            System.out.println("Nie mozna stworzyc takiego kwadratu!");
        }
        else{
            char[] tab={'x','o'};
            int idZnaku, idStartowe;
            
            if(znak=='x'){
                idStartowe=0;
            }
            else{
                idStartowe=1;
            }
            
            for(int i=0;i<rozmiar;i++){
                idZnaku=idStartowe+i; //kazda linijka musi się zaczynać od innego znaku.
                for(int j=0;j<rozmiar;j++){
                    System.out.print(tab[idZnaku%2]); //wypisujemy albo x - gdy idZnaku jest podzielne przez 2, albo o gdy nie jest
                    idZnaku++;
                }
                System.out.println();
            }
        }
    }
    
    public static void main(String[] args) {
        kwadrat(6,'x');
    }
    
}
