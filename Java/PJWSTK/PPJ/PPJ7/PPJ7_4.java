public class PPJ7_4 {
    public static void zaszyfruj(char[] tab){
        for(int i=0;i<tab.length;i++){
            tab[i]=(char)((int)tab[i]+i); //dodajemy do znaku wartosc indeksu
        }
    }
    
    public static void odszyfruj(char[] tab){
        for(int i=0;i<tab.length;i++){
            tab[i]=(char)((int)tab[i]-i); //odejmujemy od znaku wartosc indeksu
        }
    }
    
    public static void main(String[] args) {
        String str = "Jakies testowe zdanie";
        char[] tab=str.toCharArray(); //przepisanie Stringa do tablicy
        
        System.out.println(tab);
        zaszyfruj(tab);
        System.out.println(tab);
        odszyfruj(tab);
        System.out.println(tab);
    }
}
