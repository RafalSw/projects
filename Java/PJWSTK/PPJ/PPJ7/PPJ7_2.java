public class PPJ7_2 {
    
    public static boolean isDiagonal(int[][] tab){
        //Sprawdzenie kwadratowosci tablicy
        for(int i=0;i<tab.length;i++){
            if(tab[i].length != tab.length){ //dlugosc wiersza nie jest rowna ilosci wierszy - tablica nie jest kwadratowa
                return false;
            }
        }
        
        for(int i=0;i<tab.length;i++){
            for(int j=0;j<tab[i].length;j++){
                if(i!=j && tab[i][j]!=0){ //sprawdzenie wartosci spoza przekatnej - jesli nie sa 0 to zwracamy false
                    return false;
                }
            }
        }
        return true;
    }
    
    public static void main(String[] args) {
        int[][] tab1={{3,0,0},{0,5,0},{0,0,1}}; //tablica kwadratowa diagonalna
        int[][] tab2={{3,0,1},{0,5,0},{0,0,1}}; ////tablica kwadratowa NIE diagonalna
        int[][] tab3={{3,0},{0,5},{0,0}}; // tablica NIE kwadratowa
        
        System.out.println("Czy tab1 jest diagonalna? "+ isDiagonal(tab1));
        System.out.println("Czy tab2 jest diagonalna? "+ isDiagonal(tab2));
        System.out.println("Czy tab3 jest diagonalna? "+ isDiagonal(tab3));
    }
}
