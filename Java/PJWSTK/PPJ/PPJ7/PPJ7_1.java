
import java.util.Random;

public class PPJ7_1 {

    public static boolean check(int[][] tab){
        boolean result = false;
        
        //sprawdzenie przekatne z lewej gory do prawego dolu
        for(int i=0;i<tab.length;i++){
            int counter = 0;
            for(int m=i+1;m<tab.length;m++){ //sprawdzamy czy kolejne liczby na przekatnej sa takie same jak tab[i][i] i zliczamy je
                if(tab[m][m]==tab[i][i]){
                    counter++;
                }
            }
            
            if(counter>=3){
                result=true;
                break;
            }
        }
        
        //sprawdzenie przekatne z prawej gory do lewego dolu
        for(int i=0;i<tab.length;i++){
            int counter = 0;
            for(int m=i+1;m<tab.length;m++){ //sprawdzamy czy kolejne liczby na przekatnej sa takie same jak tab[i][i] i zliczamy je
                if(tab[m][tab.length-1-m]==tab[i][tab.length-1-i]){
                    counter++;
                }
            }
            
            if(counter>=3){
                result=true;
                break;
            }
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        int[][] tab = new int[8][8];
        Random rand = new Random();
        
        for(int i=0;i<8;i++){
            for(int j=0;j<8;j++){
                tab[i][j]=rand.nextInt(11);
                System.out.print(tab[i][j]+"\t");
            }
            System.out.println();
        }
        
        System.out.println(check(tab));
    }
}
