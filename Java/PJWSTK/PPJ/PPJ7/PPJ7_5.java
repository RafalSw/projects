import java.util.Random;

public class PPJ7_5 {


    public static int nwd(int x, int y){
        int z=-1;
        while(y!=0){
            z=x%y;
            x=y;
            y=z;
        }
        return x;
    }
    
    public static void main(String[] args) {
        int[][] tab=new int[10][3];
        
        Random rand = new Random();
        
        for(int i=0;i<tab.length;i++){
            tab[i][0]=rand.nextInt(101); //losowanie 1 liczby
            tab[i][1]=rand.nextInt(101); //losowanie 2 liczby
            tab[i][2]=nwd(tab[i][0],tab[i][1]); //wywolywanie funkcji liczacej NWD dla wylosowanych liczb
        }
        
        for(int i=0;i<tab.length;i++){
            System.out.println(i+". NWD("+tab[i][0]+","+tab[i][1]+")="+tab[i][2]);
        }
    }
}
