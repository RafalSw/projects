public class PPJ5_5 {
    public static void main(String[] args) {
        int liczbaSymboli=17;
        
        for(int i=0;i<liczbaSymboli;i++){
            for(int j=0;j<liczbaSymboli;j++){
                if(i<liczbaSymboli/2){//gorna polowa klepsydry
                    if(j>=i && j<liczbaSymboli-i){
                        System.out.print("*");
                    }
                    else{
                        System.out.print(" ");
                    }
                }
                else{//dolna polowa klepsydry
                    if(j>liczbaSymboli-i-2 && j<=i){
                        System.out.print("*");
                    }
                    else{
                        System.out.print(" ");
                    }
                }
            }
            System.out.println();
        }
    }
}
