
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Bin implements BinInterface{
    List<Garbage> garbage;
    
    public Bin(){
        garbage = new ArrayList();
    }
    @Override
    public <T extends Garbage> void toBin(T sth) {
        garbage.add(sth);
    }

    @Override
    public List<Garbage> getSorted(WasteType wt) {
        List<Garbage> result = new ArrayList();
        
        //uzupelnianie listy o smieci danego rodzaju
        for(int i=0; i<garbage.size(); i++){
            if(garbage.get(i).TYPE == wt){
                result.add(garbage.get(i));
            }
        }
        //sortowanie smieci na liscie result
        sort(result);
        
        return result;
    }
    
    // Funkcja sortująca listy typu ArrayList
    void sort(List garbageList){
        if(!garbageList.isEmpty()){
            if(((Garbage)garbageList.get(0)).TYPE==WasteType.GLASS){
                
                //Lista jest sortowana dzięki algorytmowi napisanemu przez twórców JAVA, trzeba tylko powiedzieć według czego należy sortować. Używamy do tego funkcji compare.
                Collections.sort(garbageList, new Comparator<Glass>() {
                    @Override
                    public int compare(Glass t, Glass t1) {
                        if(t.volume > t1.volume)
                            return -1;
                        else if (t.volume == t1.volume)
                            return 0;
                        else
                            return 1;
                    }
                });
            }
            else if(((Garbage)garbageList.get(0)).TYPE==WasteType.RADIOACTIVE){
                Collections.sort(garbageList, new Comparator<Radioactive>() {
                    @Override
                    public int compare(Radioactive t, Radioactive t1) {
                        if(t.radiativeFlux > t1.radiativeFlux)
                            return -1;
                        else if (t.radiativeFlux == t1.radiativeFlux)
                            return 0;
                        else
                            return 1;
                    }
                });

            }
            else if(((Garbage)garbageList.get(0)).TYPE==WasteType.RECYCLABLE){
                Collections.sort(garbageList, new Comparator<Recyclable>() {
                    @Override
                    public int compare(Recyclable t, Recyclable t1) {
                        if(t.weight > t1.weight)
                            return -1;
                        else if (t.weight == t1.weight)
                            return 0;
                        else
                            return 1;
                    }
                });

            }
            else{
                System.out.println("BŁĄD! Ten typ śmieci nie jest obsługiwany");
            }
        }
    }
}
