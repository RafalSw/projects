import java.util.List;


public interface BinInterface {
	/**
	 * Umieszcza odpad w smietniku. 
	 * @param sth - referencja do obiektu-smiecia.s
	 */
	public <T extends Garbage> void toBin( T sth );
	
	/** 
	 * Zwraca posortowana malejaco liste odpadkow zadanego rodzaju.
	 * Lista jest posortowana wg. charakterystycznej dla danego
	 * rodzaju smieci wlasnosci - np. w przypadku smieci szklanych
	 * to objetosc.
	 * @param wt - typ odpadow, ktore maja zostac posortowane i 
	 * zwrocone jako wynik pracy metody.
	 * @return Posortowana malejaco lista odpadkow zadanego rodzaju.
	 */
	public List<Garbage> getSorted( WasteType wt );
}
