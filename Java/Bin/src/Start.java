import java.util.List;

public class Start {
	private static BinInterface bi = new Bin();
	
	private static boolean testAndShow( List<Garbage> lg ) {
		System.out.println( "-----------------------------------------------------" );
		Glass gl = null;
		Radioactive ra = null;
		Recyclable rc = null;
		
		if ( lg.size() != 4 ) { 
			System.out.println( "Za malo elementow, powinny byc 4" );
			return false;
		}
		
		for ( Garbage g : lg ) {
			System.out.println( g );
			
			if ( g.getWasteType() == WasteType.GLASS ) {
				if ( gl != null ) {
					if ( gl.getVolume() < ((Glass)g).getVolume() ) {
						System.out.println( "Blad w sortowaniu przedmiotow szklanych !!!");
						return false;
					}
				}
				gl = (Glass)g;
			}
			if ( g.getWasteType() == WasteType.RECYCLABLE ) {
				if ( rc != null ) {
					if ( rc.getWeight() < ((Recyclable)g).getWeight() ) {
						System.out.println( "Blad w sortowaniu przedmiotow nadajacych sie do recyklingu !!!");
						return false;
					}
				}
				rc = (Recyclable)g;
			}
			if ( g.getWasteType() == WasteType.RADIOACTIVE ) {
				if ( ra != null ) {
					if ( ra.getRadiativeFlux() < ((Radioactive)g).getRadiativeFlux() ) {
						System.out.println( "Blad w sortowaniu przedmiotow radioaktywnych !!!");
						return false;
					}
				}
				ra = (Radioactive)g;
			}
		}
		
		return true;
	}
	
	public static void main(String[] args) {
		bi.toBin( new Glass( "Szklana pipeta", 0.01 ) );
		bi.toBin( new Radioactive( "Radioaktywny kopernik", 1 ));
		bi.toBin( new Glass( "Szklana szklanka", 0.25 ) );
		bi.toBin( new Recyclable( "Rec Rower", 10 ) );
		bi.toBin( new Radioactive( "Radioaktywny uran", 100 ));
		bi.toBin( new Glass( "Szklany sloik", 1.0 ) );
		bi.toBin( new Radioactive( "Radioaktywny pluton", 10 ));
		bi.toBin( new Recyclable( "Rec Auto", 1350 ) );
		bi.toBin( new Glass( "Szklana probowka", 0.1 ) );
		bi.toBin( new Recyclable( "Rec Puszka", 0.1 ) );
		bi.toBin( new Recyclable( "Rec Wagon", 10000 ) );
		bi.toBin( new Radioactive( "Radioaktywny Polon", 250 ));
		
		boolean result = true;
		result &= testAndShow( bi.getSorted( WasteType.GLASS ) );
		result &= testAndShow( bi.getSorted( WasteType.RECYCLABLE ) );
		result &= testAndShow( bi.getSorted( WasteType.RADIOACTIVE ) );	

		    System.out.println( "  ######################################################");
		if ( result ) {
			System.out.println( "    -=-=-  Wszystkie testy poprawnie zaliczone  -=-=-" );
		} else {
			System.out.println( "             NIE ZALICZONO WSZYSTKICH TESTOW" );
		}
	    System.out.println( "  ######################################################\n");
	}

}
