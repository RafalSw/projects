
public class Recyclable extends Garbage {
	protected final double weight;
	
	public Recyclable( String description, double weight ) {
		super( WasteType.RECYCLABLE, description );
		this.weight = weight;
	}
	
	public double getWeight() {
		return weight;
	}

	public String toString() {
		return super.toString() + " moja waga: " + weight;
	}
}
