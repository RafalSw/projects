public class Radioactive extends Garbage {
	protected final double radiativeFlux;
	
	public Radioactive( String description, double radiativeFlux ) {
		super( WasteType.RADIOACTIVE, description );
		this.radiativeFlux = radiativeFlux;
	}
	
	public double getRadiativeFlux() {
		return radiativeFlux;
	}

	public String toString() {
		return super.toString() + " moja emisja: " + radiativeFlux;
	}

}
