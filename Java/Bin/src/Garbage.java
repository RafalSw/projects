abstract public class Garbage {
	protected final WasteType TYPE;
	protected final String description;
	
	/**
	 * Zwraca typ odpadu.
	 * @return - typ odpadu.
	 */
	public WasteType getWasteType() {
		return TYPE;
	}
	
	/**
	 * Ustawia typ odpadu.
	 * @param type - typ odpadu
	 */
	public Garbage( WasteType type, String description ) {
		this.TYPE = type;
		this.description = description;
	}
	
	public String toString() {
		return "Jestem smiec typu : " + TYPE.name() + ", a dokladniej jestem " + description;
	}
}
