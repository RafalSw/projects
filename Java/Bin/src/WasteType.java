/**
 * Typ wyliczeniowy okreslajacy typ odpadu.
 * 
 * @author ram
 *
 */
public enum WasteType {
	RECYCLABLE, GLASS, RADIOACTIVE,
}
