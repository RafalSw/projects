public class Glass extends Garbage {
	protected final double volume;
	
	public Glass( String description, double volume ) {
		super( WasteType.GLASS, description );
		this.volume = volume;
	}
	
	public double getVolume() {
		return volume;
	}

	public String toString() {
		return super.toString() + " moja obietosc: " + volume;
	}

}
