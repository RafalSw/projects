import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

class Element extends Component {
    Color color;
    Map map;
    int x,y;
    
    public Element(Color c, int locx, int locy, Map m){
        setVisible(true);
        setSize(1000,800);
        color=c;
        x=locx;
        y=locy;
        map=m;
    }
    
    
    public void paint(Graphics g) {
        int size=map.mc.jpanel.getWidth()/11;
        g.setColor(color);
        g.fillRect(x*size+1, y*size+1, size, size);
        g.setColor(Color.BLACK);
        g.drawRect(x*size+1, y*size+1, size, size);
    }
}

class Map{
    Element[][] elements;
    Start mc;
    
    public void generate(){
        Random rand = new Random();
        

        for(int j=0;j<11;j++){
            int chosen = rand.nextInt(11);
            for(int i=0;i<11;i++){
                if(j==0 && i==chosen){
                    elements[i][j]=new Element(Color.GREEN,i,j,this);
                }
                else if(j==10 && i==5){
                    elements[i][j]=new Element(Color.RED,i,j,this);
                }
                else if(j%2==0){
                    elements[i][j]=new Element(Color.WHITE,i,j,this);
                }
                else if(j%2==1 && i==chosen){
                    elements[i][j]=new Element(Color.WHITE,i,j,this);
                }
                else{
                    elements[i][j]=new Element(Color.BLUE,i,j,this);
                }
            }
        }
        
        putTextures();
    }

    
    public void putTextures(){
        for(int i=0;i<elements.length;i++)
            for(int j=0; j<elements[0].length;j++){
                Element e=elements[i][j];
                if(e!=null){
                    e.setVisible(true);
                    mc.screen.getPan().add(e);
                    mc.screen.getPan().validate();
                }
            }
    }

    public void show(){
        for(Element[] ex: elements){
            for(Element e:ex){
                if(e!=null){
                    e.setVisible(true);
                }
            } 
        }
    }

    
    public Map(int x, int y, Start m){
        mc=m;
        elements=new Element[x][y];
    }
}

class Start {
    Map map;
    Game screen;
    JPanel jpanel;

    
    public Start(){
        screen = new Game(this);
        jpanel=screen.getPan();
        createMap(11,11);
        map.show();
        jpanel.repaint();
    }
    
    public void createMap(int x, int y){
        map=new Map(x,y, this);
        map.generate();
    }
    
    public void newGame(){
        jpanel.removeAll();
        createMap(11,11);
    }
    
}

public class Game extends javax.swing.JFrame {
    
    Start start;
    int posX, posY, counter;
    /**
     * Creates new form Game
     */
    public Game(Start s) {
        start=s;
        initComponents();
        setLocationRelativeTo(null);
        setFocusable(true);
        requestFocusInWindow();
        setVisible(true);
        posX=5;
        posY=10;
        counter=0;
        jLabel2.setText(Integer.toString(counter));
    }
    
    public JPanel getPan(){
        return this.jPanel1;
    }
    
    public void isWon(){
        String result = "You won!";
        JOptionPane.showMessageDialog(this, result);
    }
    
    public static void main(String args[]){
        new Start();
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jButton4 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(120, 120));

        jPanel1.setMinimumSize(new java.awt.Dimension(500, 500));
        jPanel1.setPreferredSize(new java.awt.Dimension(580, 580));
        jPanel1.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                jPanel1ComponentResized(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 580, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 580, Short.MAX_VALUE)
        );

        jButton4.setText("Again");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton3.setText("Right");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton2.setText("Up");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton1.setText("Left");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Counter:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57)
                        .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton4)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1)
                        .addComponent(jButton2)
                        .addComponent(jButton3)
                        .addComponent(jButton4))
                    .addComponent(jLabel1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        boolean isWon=false;
        if(posX-1>=0){
            if(start.map.elements[posX-1][posY].color==Color.GREEN)
                isWon=true;
            start.map.elements[posX-1][posY].color=Color.RED;
            start.map.elements[posX][posY].color=Color.WHITE;
            posX--;
        }
        counter++;
        jLabel2.setText(Integer.toString(counter));
        
        if(isWon)
            isWon();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        boolean isWon=false;
        if(posX+1<=10){
            if(start.map.elements[posX+1][posY].color==Color.GREEN)
                isWon=true;
            start.map.elements[posX+1][posY].color=Color.RED;
            start.map.elements[posX][posY].color=Color.WHITE;
            posX++;
        }
        counter++;
        jLabel2.setText(Integer.toString(counter));
        
        if(isWon)
            isWon();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        boolean isWon=false;
        if(posY-1>=0 && start.map.elements[posX][posY-1].color!=Color.BLUE){
            if(start.map.elements[posX][posY-1].color==Color.GREEN)
                isWon=true;
            start.map.elements[posX][posY-1].color=Color.RED;
            start.map.elements[posX][posY].color=Color.WHITE;
            posY--;
        }
        counter++;
        jLabel2.setText(Integer.toString(counter));
        
        if(isWon)
            isWon();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        counter=0;
        jLabel2.setText(Integer.toString(counter));
        start.newGame();
        posX=5;
        posY=10;
        
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jPanel1ComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_jPanel1ComponentResized
        jPanel1.setSize(jPanel1.getWidth(),jPanel1.getWidth());        
    }//GEN-LAST:event_jPanel1ComponentResized


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
