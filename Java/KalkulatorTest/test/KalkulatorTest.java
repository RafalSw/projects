import org.junit.Test;
import static org.junit.Assert.*;


public class KalkulatorTest { 

    /**
     * Test of setAccumulator method, of class Kalkulator.
     */
    @Test
    public void testConstructor() {
        System.out.println("Kalkulator()");
        
        try{
            Kalkulator instance = new Kalkulator();
            assertNotNull("Nie udało się utworzyć obiektu.",instance);
        }
        catch(Exception e){
            fail("Nie udało się utworzyć obiektu. Błąd: "+e.getMessage());
        }
       
    }
    
    /**
     * Test of setAccumulator method, of class Kalkulator.
     */
    @Test
    public void testSetAccumulator() {
        System.out.println("setAccumulator");
        KalkulatorI.Accumulator a = null;
        Kalkulator instance = new Kalkulator();
        try{
            instance.setAccumulator(a);
        }
        catch(Exception e){
            fail("Nie udało się wykonać metody setAccumulator dla niezainicjowanego akumulatora. Błąd: "+e.getMessage());
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(0);
        try{
            
            instance.setAccumulator(acc);
        }
        catch(Exception e){
            fail("Nie udało się wykonać metody setAccumulator dla zainicjowanego akumulatora. Błąd: "+e.getMessage());
        }
    }

    /**
     * Test of getAccumulator method, of class Kalkulator.
     */
    @Test
    public void testGetAccumulator() {
        System.out.println("setAccumulator + getAccumulator");
        Kalkulator instance = new Kalkulator();
        KalkulatorI.Accumulator expResult = null;
        KalkulatorI.Accumulator result;
        try{
        result = instance.getAccumulator();
        assertEquals("Mimo nie ustawionego akumulatora, getAccumulator zwraca wartosc inna niz null",expResult, result);
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody getAccumulator. Błąd: "+e.getMessage());
        }
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(0);
        instance.setAccumulator(acc);
        expResult=acc;
        result = instance.getAccumulator();
        assertSame("Mimo ustawionego akumulatora, getAccumulator zwraca inny obiekt",expResult, result);
        if(result!=expResult){
            int res1 = expResult.get();
            int res2 = result.get();
            assertFalse("Zwrocony obiekt zawiera inne wartosci",res1==res2);
            assertTrue("Zwrocony obiekt zawiera te same wartosci - prawdopodobnie zwrocono kopię",res1==res2);
        }
    }

    /**
     * Test of get method, of class Kalkulator.
     */
    @Test
    public void testGet() {
        System.out.println("get");
        Kalkulator instance = new Kalkulator();
        int expResult = Integer.MIN_VALUE;
        int result;
        try{
            result = instance.get();
            assertEquals("Akumulator nie powinien być ustawiony, a mimo to zwracana wartość to nie Integer.MIN_VALUE",expResult, result);
        }
        catch(Exception e){
            fail("Nie udało się wykonać metody get(). Błąd: "+e.getMessage());
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        expResult = 5;
        result = instance.get();
        assertEquals("Akumulator powinien być ustawiony i zawierać 5",expResult, result);
    }

    /**
     * Test of add method, of class Kalkulator.
     */
    @Test
    public void testAdd() {
        System.out.println("add");
        int i = 0;
        int result;
        Kalkulator instance = new Kalkulator();
        
        try{
            instance.add(1);
            assertNull("Nieustawiony akumulator - spodziewamy się null",instance.getAccumulator());
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody add() dla nieustawionego akumulatora.");
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        instance.add(i);
        result = instance.get();
        assertEquals("Dodawanie 0 zmieniło liczbę!",5, result);
        
        instance.add(5);
        result = instance.get();
        assertEquals("Dodawanie 5 zmieniło liczbę na inną niż 10!",10, result);
        
        acc.set(10);
        instance.setAccumulator(acc);
        instance.add(-5);
        result = instance.get();
        assertEquals("Dodawanie liczby ujemnej nie zadziałało jak odejmowanie!",5, result);
        
    }

    /**
     * Test of div method, of class Kalkulator.
     */
    @Test
    public void testDiv() {
        System.out.println("div");
        int i = 0;
        Kalkulator instance = new Kalkulator();
        
        try{
            instance.div(1);
            assertNull("Nieustawiony akumulator - spodziewamy się null",instance.getAccumulator());
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody div() dla nieustawionego akumulatora.");
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        instance.div(1);
        assertEquals("Dzielenie przez 1 zmieniło wynik",5, instance.get());
        instance.div(-1);
        assertEquals("Dzielenie ujemną nie dało oczekiwanego wyniku",-5, instance.get());
        
        instance.div(0);
        assertEquals("Dzielenie ujemnej przez 0 nie dało Integer.MIN_VALUE",Integer.MIN_VALUE, instance.get());
        
        acc.set(5);
        instance.setAccumulator(acc);
        instance.div(0);
        assertEquals("Dzielenie dodatniej przez 0 nie dało Integer.MAX_VALUE",Integer.MAX_VALUE, instance.get());
        
        acc.set(0);
        instance.setAccumulator(acc);
        instance.div(0);
        assertEquals("Dzielenie 0 przez 0 nie dało 0", 0, instance.get());
    }

    /**
     * Test of sub method, of class Kalkulator.
     */
    @Test
    public void testSub() {
        System.out.println("sub");
        int i = 0;
        int result;
        Kalkulator instance = new Kalkulator();
        
        try{
            instance.sub(1);
            assertNull("Nieustawiony akumulator - spodziewamy się null",instance.getAccumulator());
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody sub() dla nieustawionego akumulatora.");
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        instance.sub(i);
        result = instance.get();
        assertEquals("Odejmowanie 0 zmieniło liczbę!",5, result);
        
        instance.sub(5);
        result = instance.get();
        assertEquals("Odejmowanie 5 zmieniło liczbę na inną niż 0!",0, result);
        
        acc.set(10);
        instance.setAccumulator(acc);
        instance.sub(-5);
        result = instance.get();
        assertEquals("Odejmowanie liczby ujemnej nie zadziałało jak dodawanie!",15, result);
        
    }

    /**
     * Test of mul method, of class Kalkulator.
     */
    @Test
    public void testMul() {
        System.out.println("mul");
        int i = 0;
        Kalkulator instance = new Kalkulator();
        try{
            instance.mul(1);
            assertNull("Nieustawiony akumulator - spodziewamy się null",instance.getAccumulator());
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody mul() dla nieustawionego akumulatora.");
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        instance.mul(1);
        assertEquals("Mnożenie przez 1 zmieniło wynik", 5, instance.get());
        instance.mul(0);
        assertEquals("Mnożenie przez 0 nie wyzerowało wyniku", 0, instance.get());
        
        acc.set(10);
        instance.setAccumulator(acc);
        instance.mul(-1);
        assertEquals("Mnożenie przez ujemna nie zmieniło wyniku", -10, instance.get());
        instance.mul(2);
        assertEquals("Mnożenie ujemnej nie zmieniło wyniku", -20, instance.get());

    }

    /**
     * Test of zero method, of class Kalkulator.
     */
    @Test
    public void testZero() {
        System.out.println("zero");
        Kalkulator instance = new Kalkulator();
        try{
            instance.zero();
            assertNull("Nieustawiony akumulator - spodziewamy się null",instance.getAccumulator());
        }
        catch(Exception e){
            fail("Nie udało się wywołać metody zero() dla nieustawionego akumulatora.");
        }
        
        KalkulatorI.Accumulator acc = new KalkulatorI.Accumulator(5);
        instance.setAccumulator(acc);
        instance.zero();
        assertNotNull("Ustawiony akumulator - spodziewamy się wyzerowanego obiektu",instance.getAccumulator());
        assertEquals("Zerowanie nie sprawia, że akumulator ma wartość 0",0, instance.get());
    }
    
}
