public class Kalkulator implements KalkulatorI{
    Accumulator acc;
    @Override
    public void setAccumulator(Accumulator a) {
        acc=a;
    }

    @Override
    public Accumulator getAccumulator() {
        return acc;
    }

    @Override
    public int get() {
        if(acc!=null)
            return acc.get();
        else
            return Integer.MIN_VALUE;
    }

    @Override
    public void add(int i) {
        if(acc!=null)
            acc.set(acc.get()+i);
    }

    @Override
    public void div(int i) {
        if(acc!=null){
            if(i==0){
                if(acc.get()==0)
                    acc.set(0);
                else if(acc.get()>0)
                    acc.set(Integer.MAX_VALUE);
                else
                    acc.set(Integer.MIN_VALUE);
            }
            else{
                acc.set(acc.get()/i);
            }
        }
    }

    @Override
    public void sub(int i) {
        if(acc!=null)
            acc.set(acc.get()-i);
    }

    @Override
    public void mul(int i) {
        if(acc!=null)
            acc.set(acc.get()*i);
    }

    @Override
    public void zero() {
        if(acc!=null)
            acc.set(0);
    }
    
}
