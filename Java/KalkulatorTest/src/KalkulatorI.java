public interface KalkulatorI {

/** Klasa zagniezdzona */

   public class Accumulator {
      private int acc;
      
      public void set( int acc ) {
         this.acc = acc;
      }
      
      public int get() {
         return acc;
      }
      
      public Accumulator( int ini ) {
         set( ini );
      }

      public Accumulator( ) {}
   } 
   
   /** Instaluje w kalkulatorze akumulator, ktory sluzy do wykonywania obliczen */
   void setAccumulator( Accumulator a );
   
   /** Zwraca akumulator uzywany przez Kalkulator */
   Accumulator getAccumulator();
   
   /** Zwraca zawartosc akumulator-a lub Integer.MIN_VALUE jesli go brak */
   int get();
   
   /** Dodaje argument do akumulatora, o ile ten zostal ustawiony */
   void add( int i );
   
   /** Zawartosc akumulatora dzieli przez argument. Jesli nie ma ustawionego akumulatora
    * to nic sie nie dzieje. Jesli argument rowny jest zero i akumulator jest ustawiony
    * to jego wartosc po wykonaniu div wynosi 0, jesli w akumulatorze bylo 0, 
    * Integer.MIN_VALUE, jesli w akumulatorze byla wartosc ujemna oraz
    * Integer.MAX_VALUE, jesli w akumulatorze byla wartosc dodatnia.
    */
   void div( int i );
   
   /** Odejmuje argument od akumulatora, o ile zostal ustawiony */
   void sub( int i );
   
   /** Mnozy przez argument zawartosc akumulatora, jesli jest ustawiony */ 
   void mul( int i );
   
   /** Zeruje akumulator, jesli jest ustawiony */
   void zero();
}
