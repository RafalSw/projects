#pragma once
#include <cstdint>

/*
//reprezentacja na 8 bitach
typedef uint8_t TypReprezentacji;
const TypReprezentacji maxReprezentacji = UINT8_MAX;
typedef uint16_t TypReprezentacji2;
*/

/*
//reprezentacja na 16 bitach
typedef uint16_t TypReprezentacji;
const TypReprezentacji maxReprezentacji = UINT16_MAX;
typedef uint32_t TypReprezentacji2;
*/


//reprezentacja na 32 bitach
typedef uint32_t TypReprezentacji;
const TypReprezentacji maxReprezentacji = UINT32_MAX;
typedef uint64_t TypReprezentacji2;



struct Header
{
	TypReprezentacji iloscZer;
	TypReprezentacji iloscJedynekIZer;
	char dopelnienie;
};