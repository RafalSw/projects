#pragma once
#include <vector>
#include "Header.h"

using namespace std;

//klasa abstrakcyjna po ktorej dziedzicza koder i dekoder
class Baza
{
protected:
	bool konsola = true; //okresla czy informacje maja byc wypisywane na konsoli
	bool konsolaMore = false; //okresla czy dodatkowe informacje maja byc wypisywane na konsoli
	Header naglowek;
	vector<char> odkodowane;
	vector<bool> zakodowane;
	vector<bool> odkodowaneBin;
public:
	virtual bool wczytajZPliku(string nazwa) = 0;
	virtual bool zapiszDoPliku(string nazwa) = 0;
	virtual bool wykonajOperacje() = 0;
	void setKonsola(bool x)
	{
		konsola = x;
	}
	void setKonsolaMore(bool x)
	{
		konsolaMore = x;
	}
};