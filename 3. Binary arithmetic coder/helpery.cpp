#include "Helpery.h"

bool sprawdzBit(TypReprezentacji b, char miejsce)
{
	return(b & (1 << miejsce));
}

bool warunekE1(TypReprezentacji l, TypReprezentacji u)
{
	if (!sprawdzBit(l, sizeof(TypReprezentacji) * 8 - 1) && !sprawdzBit(u, sizeof(TypReprezentacji) * 8 - 1))
		return true;
	return false;
}

bool warunekE2(TypReprezentacji l, TypReprezentacji u)
{
	if (sprawdzBit(l, sizeof(TypReprezentacji) * 8 - 1) && sprawdzBit(u, sizeof(TypReprezentacji) * 8 - 1))
		return true;
	return false;
}

bool warunekE3(TypReprezentacji l, TypReprezentacji u)
{
	if (sprawdzBit(l, sizeof(TypReprezentacji) * 8 - 2) && !sprawdzBit(u, sizeof(TypReprezentacji) * 8 - 2))
		return true;
	return false;
}