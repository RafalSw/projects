#include <iostream>
#include <string>
#include "Koder.h"
#include "Dekoder.h"
#include "Helpery.h"

using namespace std;

bool testHelperow()
{
	bool e11 = warunekE1(0x00, 0x7F); //0x00000000, 0x01111111
	bool e12 = warunekE1(0xFF, 0x80); //0x11111111, 0x10000000
	bool e21 = warunekE2(0x00, 0x7F); //0x00000000, 0x01111111
	bool e22 = warunekE2(0xFF, 0x80); //0x11111111, 0x10000000
	bool e31 = warunekE3(0x40, 0xBF); //0x01000000, 0x10111111
	bool e32 = warunekE3(0xBF, 0x40); //0x10111111, 0x01000000

	if (e11 && !e12 && !e21 && e22 && e31 && !e32)
		return true;
	else
		return false;
}

void uruchom(string plikWe, string plikZakodowany, string plikWy, bool konsola = 0, bool konsolaMore = 0)
{
	Koder koder;
	koder.setKonsola(konsola);
	koder.setKonsolaMore(konsolaMore);
	koder.wczytajZPliku(plikWe);
	koder.wykonajOperacje();
	koder.zapiszDoPliku(plikZakodowany);

	Dekoder dekoder;
	dekoder.setKonsola(konsola);
	dekoder.setKonsolaMore(konsolaMore);
	dekoder.wczytajZPliku(plikZakodowany);
	dekoder.wykonajOperacje();
	dekoder.zapiszDoPliku(plikWy);
}


int main(int argc, char **argv)
{
	if (!(argc == 4 || argc == 5 || argc == 6))
	{
		string nazwa = argv[0];
		nazwa = nazwa.substr(nazwa.find_last_of("\\")+1);
		cout << "Poprawne uzycie:" << endl;
		cout << nazwa << " <plik wejsciowy> <plik zakodowany> <plik wyjsciowy>" << endl;
		cout << nazwa << " <plik wejsciowy> <plik zakodowany> <plik wyjsciowy> <konsola (1/0)>" << endl;
		cout << nazwa << " <plik wejsciowy> <plik zakodowany> <plik wyjsciowy> <konsola (1/0)> <wiecej konsoli (1/0)>" << endl;
		system("PAUSE");
		return 0;
	}
	else
	{
		if (argc==4)
			uruchom(argv[1], argv[2], argv[3]);
		else if (argc == 5)
			uruchom(argv[1], argv[2], argv[3], &argv[4]);
		else
			uruchom(argv[1], argv[2], argv[3], &argv[4], &argv[6]);
	}

	return 0;
}