#pragma once
#include <string>
#include <vector>
#include "Baza.h"

using namespace std;

class Koder:public Baza
{
protected :
	virtual void zmianaSystemu(); //OK
public:
	virtual bool wczytajZPliku(string nazwa); //OK
	virtual bool zapiszDoPliku(string nazwa); //OK
	virtual bool wykonajOperacje();

	void test2()
	{
		odkodowane.push_back(0x23);
	}

	void temp()
	{
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(1);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(0);
		zakodowane.push_back(1);
	}
};