#include <fstream>
#include <iostream>
#include "Dekoder.h"
#include "Helpery.h"

using namespace std;

TypReprezentacji decZBin(vector<bool>& tablica, unsigned int index)
{
	TypReprezentacji wynik = 0;
	for (unsigned int i = 0; i < sizeof(TypReprezentacji) * 8; i++)
	{
		if (index + i<tablica.size())
			wynik += tablica.at(index+i);
		if (i + 1 < sizeof(TypReprezentacji) * 8)
			wynik = wynik << 1;
	}
	return wynik;
}

void Dekoder::zmianaSystemu()
{
	char znak;
	int j;
	if (odkodowaneBin.size() % 8 != 0)
	{
		cout << "Blad przy zamianie z bin na char" << endl;
	}
	for (int i = 0; i < odkodowaneBin.size(); i+=8)
	{
		znak = 0;
		j = 0;
		while (j<7)
		{
			znak += odkodowaneBin.at(i+j);
			znak = znak << 1;
			j++;
		}
		znak += odkodowaneBin.at(i + j);
		odkodowane.push_back(znak);
	}
}

bool Dekoder::wczytajZPliku(string nazwa)
{
	fstream plik(nazwa, ios::in | ios::binary);
	if (plik.good())
	{
		if (konsola)
			cout << "OTWARTO PLIK " << nazwa << endl;
		plik.seekg(0, std::ios::end);
		streamsize rozmiarPliku = plik.tellg();
		plik.seekg(0, std::ios::beg);

		if (rozmiarPliku * 8 > zakodowane.max_size())
		{
			cout << "Za duzo danych do odkodowania! Ta implementacja nie pozwala na taka ilosc!" << endl;
			system("PAUSE");
			exit(-1);
		}

		zakodowane.clear();
		zakodowane.reserve((rozmiarPliku - sizeof(naglowek))*8);

		plik.read((char*)&naglowek, sizeof(naglowek));

		if (naglowek.iloscJedynekIZer > odkodowaneBin.max_size())
		{
			cout << "Za duzo danych po odkodowaniu! Ta implementacja nie pozwala na taka ilosc!" << endl;
			system("PAUSE");
			exit(-1);
		}

		unsigned char analizowany;
		int j;
		while (plik.read((char*)&analizowany, 1))
		{
			j = 0;
			do
			{
				if ((analizowany>>(7-j) & 1) == 0)
					zakodowane.push_back(false);
				else
					zakodowane.push_back(true);
				j++;
			} while (j<8);


		}

		//usuniecie dopelnienia
		for (int i = 0; i < naglowek.dopelnienie; i++)
			zakodowane.pop_back();

		plik.close();
		return true;
	}
	else{
		if (konsola)
			cout << "B�AD W OTWIERANIU PLIKU " << nazwa << endl;
		return false;
	}
}

bool Dekoder::zapiszDoPliku(string nazwa)
{
	fstream plik(nazwa, ios::out | ios::binary);
	if (plik.good())
	{
		if (konsola)
			cout << "OTWARTO PLIK " << nazwa << endl;

		char c;
		for (unsigned  int i = 0; i < odkodowane.size(); i++)
		{
			c = odkodowane.at(i);
			plik.write(&c,1);
		}

		plik.close();
		return true;
	}
	else{
		if (konsola)
			cout << "B�AD W OTWIERANIU PLIKU " << nazwa << endl;
		return false;
	}
}

bool Dekoder::wykonajOperacje()
{
	if (konsola)
		cout << "Dekodowanie" << endl;

	TypReprezentacji l = 0;
	TypReprezentacji u = maxReprezentacji;
	TypReprezentacji t = decZBin(zakodowane, 0);
	TypReprezentacji2 liczbaZer = naglowek.iloscZer;
	TypReprezentacji2 liczbaZeriJedynek = naglowek.iloscJedynekIZer;

	unsigned int indexZakodowanych = sizeof(TypReprezentacji)*8;
	unsigned int iloscOdkodowanych = 0;
	int postep = 0;

	TypReprezentacji2 roznica1, roznica2, iloczyn;
	bool wartosc;
	int counterNadmiarowych = 0;
	while (iloscOdkodowanych < naglowek.iloscJedynekIZer)
	{

		//TU JEST COS SKOPANE
		roznica1 = t - l;
		roznica1++; //t-l+1
		iloczyn = roznica1 * liczbaZeriJedynek;
		iloczyn--; //(t-l+1)*Total - 1
		roznica2 = u - l;
		roznica2++; //u-l+1

		if (iloczyn / roznica2 >= naglowek.iloscZer)
			wartosc = 1;
		else
			wartosc = 0;

		//KONIEC SKOPANIA

		odkodowaneBin.push_back(wartosc);

		iloscOdkodowanych++;

		
		if (wartosc)
		{
			l = l + (roznica2*liczbaZer / liczbaZeriJedynek);
			//u sie nie zmienia
		}
		else
		{
			//l sie nie zmienia
			u = l + (roznica2 * liczbaZer / liczbaZeriJedynek) - 1;
		}

		bool w1 = warunekE1(l, u);
		bool w2 = warunekE2(l, u);
		bool w3 = warunekE3(l, u);
		while (w1 || w2 || w3)
		{
			if (w1 || w2)
			{
				l = l << 1;
				u = u << 1;
				u |= 1;
				t = t << 1;
				if (!(indexZakodowanych + 1 >= zakodowane.size()))
					t += zakodowane.at(indexZakodowanych++);
				else
					counterNadmiarowych++;

			}
			
			if (w3) //warunek E3
			//else //warunek E3
			{
				l = l << 1;
				u = u << 1;
				u |= 1;
				t = t << 1;
				if (!(indexZakodowanych + 1 >= zakodowane.size()))
					t += zakodowane.at(indexZakodowanych++);
				else
					counterNadmiarowych++;


				l ^= 1 << sizeof(TypReprezentacji) * 8 - 1;
				u ^= 1 << sizeof(TypReprezentacji) * 8 - 1;
				t ^= 1 << sizeof(TypReprezentacji) * 8 - 1;
			}
			w1 = warunekE1(l, u);
			w2 = warunekE2(l, u);
			w3 = warunekE3(l, u);
		}
	}

	cout << "Counter liczb nadmiarowych: " << counterNadmiarowych << endl;

	if (konsola)
		cout << "\nFinalizowanie" << endl;
	zmianaSystemu();
	if (konsola)
		cout << "Zakonczono dekodowanie" << endl;
	return true;
}
