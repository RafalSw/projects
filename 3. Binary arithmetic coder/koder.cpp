#include <fstream>
#include <iostream>
#include "Koder.h"
#include "Helpery.h"

using namespace std;

void Koder::zmianaSystemu()
{
	int j;
	bool temp[8];
	char analizowany;
	TypReprezentacji iloscZer = 0;
	TypReprezentacji iloscJedynek = 0;

	odkodowaneBin.clear();
	odkodowaneBin.reserve(odkodowane.size() * 8);

	for (unsigned int i = 0; i < odkodowane.size();i++)
	{
		j = 0;
		analizowany = odkodowane.at(i);
		do
		{
			if ((analizowany & 1) == 0)
				temp[7 - j] = false;
			else
				temp[7 - j] = true;

			analizowany >>= 1;
			j++;
		} while (j<8);

		for (int i = 0; i < 8; i++)
		{
			if (temp[i] == 0)
			{
				odkodowaneBin.push_back(0);
				iloscZer++;
			}
			else
			{
				odkodowaneBin.push_back(1);
				iloscJedynek++;
			}
		}
	}
	naglowek.iloscZer = iloscZer;
	naglowek.iloscJedynekIZer = iloscZer + iloscJedynek;
}

bool Koder::wczytajZPliku(string nazwa)
{
	fstream plik(nazwa, ios::in | ios::binary);
	if (plik.good())
	{
		if (konsola)
			cout << "OTWARTO PLIK " << nazwa << endl;

		//sprawdzenie rozmiaru pliku
		plik.seekg(0, std::ios::end);
		streamsize rozmiarPliku = plik.tellg();
		plik.seekg(0, std::ios::beg);

		if (rozmiarPliku > odkodowane.max_size() || rozmiarPliku * 8 > odkodowaneBin.max_size())
		{
			cout << "Za duzo danych do zakodowania! Ta implementacja nie pozwala na taka ilosc!" << endl;
			system("PAUSE");
			exit(-1);
		}

		odkodowane.clear();
		odkodowane.reserve(rozmiarPliku);

		char c;
		while (plik.read(&c, 1))
		{
			odkodowane.push_back(c);
		}

		plik.close();
		return true;
	}
	else{
		if (konsola)
			cout << "B�AD W OTWIERANIU PLIKU " << nazwa << endl;
		return false;
	}
}

bool Koder::zapiszDoPliku(string nazwa)
{
	fstream plik(nazwa, ios::out | ios::binary);
	if (plik.good())
	{
		if (konsola)
			cout << "OTWARTO PLIK " << nazwa << endl;

		naglowek.dopelnienie = 8 - zakodowane.size()%8;
		plik.write((char*)&naglowek, sizeof(naglowek));

		char bufor = 0;
		unsigned int i;
		for (i = 0; i < zakodowane.size(); i++)
		{
			bufor += zakodowane.at(i);
			if ((i+1) % 8 == 0)
			{
				plik.write(&bufor, 1);
				bufor = 0;
			}
			bufor = bufor << 1;
		}

		//uzupelnianie zerami		
		if ((i - 1) % 8 != 0)
		{
			while ((i - 1) % 8)
			{
				bufor += 0;
				if ((i + 1) % 8 == 0)
				{
					plik.write(&bufor, 1);
					bufor = 0;
				}
				bufor = bufor << 1;
				i++;
			}
		}
		plik.close();
		return true;
	}
	else{
		if (konsola)
			cout << "B�AD W OTWIERANIU PLIKU " << nazwa << endl;
		return false;
	}
}

bool Koder::wykonajOperacje()
{
	if (konsola)
		cout << "Kodowanie\nPrzygotowanie..." << endl;
	zmianaSystemu();
	if (konsola)
		cout << "Zakonczono przygotowanie" << endl;
	TypReprezentacji l = 0;
	TypReprezentacji u = maxReprezentacji;
	unsigned int skala = 0;
	zakodowane.clear();
	zakodowane.reserve(odkodowaneBin.size());

	if (konsola)
		cout << "Rozpoczecie kodowania" << endl;

	int postep = 0;
	TypReprezentacji2 jeden = 1;
	TypReprezentacji2 liczbaZer = naglowek.iloscZer;
	TypReprezentacji2 LiczbaZerIJedynek = naglowek.iloscJedynekIZer;
	TypReprezentacji2 roznica, roznicaZJedynka;
	TypReprezentacji obliczonaWartosc;

	for (unsigned int i = 0; i < odkodowaneBin.size(); i++)
	{
		if (konsola)
			if (100 * i / odkodowaneBin.size() > postep)
			{
				postep++;
				cout << '#';
			}

		//moze to na koniec?
		//->
		roznica = u - l;
		roznica++; //u-l+1
		obliczonaWartosc = roznica*liczbaZer / LiczbaZerIJedynek;

		bool wartosc = odkodowaneBin.at(i);
		if (wartosc)
		{
			l = l + obliczonaWartosc;
		}
		else
		{
			u = l + obliczonaWartosc - 1;
		}
		//<-

		bool w1 = warunekE1(l, u);
		bool w2 = warunekE2(l, u);
		bool w3 = warunekE3(l, u);
		while (w1 || w2 || w3)
		{
			if (w1 || w2)
			{
				if (w1)
				{
					zakodowane.push_back(0);
				}
				else
				{
					zakodowane.push_back(1);
				}
				l = l << 1;
				l |= 0;
				u = u << 1;
				u |= 1;
				while (skala > 0)
				{
					if (w1)
					{
						zakodowane.push_back(1);
					}
					else
					{
						zakodowane.push_back(0);
					}

					skala--;
				}
			}

			//if (w3) //warunek E3
			else //warunek E3
			{
				l = l << 1;
				u = u << 1;
				u |= 1;

				l ^= 1 << sizeof(TypReprezentacji) * 8 - 1;
				u ^= 1 << sizeof(TypReprezentacji) * 8 - 1;

				skala++;
			}
			w1 = warunekE1(l, u);
			w2 = warunekE2(l, u);
			w3 = warunekE3(l, u);
		}
	}

	if (skala > 0)
	{
		//przeslanie 1. znaku l
		if (l < maxReprezentacji / 2)
			zakodowane.push_back(0);
		else
			zakodowane.push_back(1);

		while (skala > 0)
		{
			zakodowane.push_back(1);
			skala--;
		}

		//przeslanie reszty l
		for (int i = sizeof(TypReprezentacji) * 8 - 2; i >= 0; i--)
			zakodowane.push_back((l >> i) & 1);
	}
	else
	{
		for (int i = sizeof(TypReprezentacji) * 8 - 1; i >= 0; i--)
			zakodowane.push_back((l >> i) & 1);
	}
	if (konsola)
		cout << "\nZakonczono kodowanie" << endl;

	return true;
}
