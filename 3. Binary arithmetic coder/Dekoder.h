#pragma once
#include <string>
#include "Baza.h"

using namespace std;

class Dekoder:public Baza
{
public:
	void zmianaSystemu(); //OK
public:
	virtual bool wczytajZPliku(string nazwa); //OK
	virtual bool zapiszDoPliku(string nazwa); //OK
	virtual bool wykonajOperacje();
	void test1()
	{
		odkodowane.push_back('a');
		odkodowane.push_back('l');
		odkodowane.push_back('a');
		odkodowane.push_back(' ');
		odkodowane.push_back('m');
		odkodowane.push_back('a');
		odkodowane.push_back(' ');
		odkodowane.push_back('a');
	}

	void test2()
	{
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(1);
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(0);
		odkodowaneBin.push_back(0);
		zmianaSystemu();
	}
};