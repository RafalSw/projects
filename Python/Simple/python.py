''' #komentarz
slowo = 'test'
wyraz = "dwa"
'''

''''
\n #nowa linia
\t #tabulacja
\' # '
'''

def nazwaFunkcji(argument): #np. dla 5: suma = 5+4+3+2+1
    suma = 0
    while argument>0:
        suma = suma + argument
        argument = argument - 1
    return suma


print(''+')')
print('slowo w \'cudzyslowie\' i backspace \t tekst')
#for(i=0;i<10;i++)
for i in range(0,10):
    print(i)

i=0
while(i<10):
    print(i)
    i = i+1


lista = [4,5,6]
print(lista)
lista.append(1)
print(lista)

#wygodna wersja:
suma = 0
for element in lista:
    suma = suma + element
    print(suma)

#alternatywa
suma = 0
for indeks in range(0, len(lista)):
    suma = suma + lista[indeks]
    print(suma)


print("5+4+3+2+1="+str(nazwaFunkcji(5)))

#print("5+4+3+2+1="+str(nazwaFunkcji('a')))

