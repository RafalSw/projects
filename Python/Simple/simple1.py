import math

def zad11():
    a = float(input("Podaj pierwsza wartosc: "))
    b = float(input("Podaj druga wartosc: "))
    print("Zad 1.1: "+str(a)+"+"+str(b)+"="+str(a+b))
    return a+b

def zad22(promien):
    print("Promien: "+str(promien))
    print("Obwod: "+str(math.pi*2*promien))
    print("Pole: "+str(math.pi*promien**2))

def zad23(bokA, bokB):
    print("Boki: "+str(bokA)+", "+str(bokB))
    print("Obwod: "+str(2*(bokA+bokB)))
    print("Pole: "+str(bokA*bokB))

def zad24(znak, powtorzenia):
    print("Znak: "+znak+" powtorzenia: "+str(powtorzenia))
    wynik=""
    while powtorzenia>0:
        wynik = wynik+znak
        powtorzenia = powtorzenia - 1
    print(wynik)

def zad25(silnia):
    kopia = silnia
    wynik = 1
    while silnia>1:
        wynik = wynik*silnia
        silnia = silnia - 1
    print(str(kopia)+"!="+str(wynik))

def zad26(zdanie, znak):
    print("Zdanie: "+zdanie)
    print("Znak: "+znak)
    while znak in zdanie:
        indeks = zdanie.find(znak)
        zdanie = zdanie[0:indeks]+zdanie[indeks+1:] #[indeks+1:] czyli od indeks+1 do konca
        print(zdanie[0:indeks])
    print("Zdanie po usunieciu znakow: "+zdanie)

#zad11()
zad22(5)
zad23(4,8)
zad24('#',10)
zad25(5)
zad26("Ala ma kota","m")


