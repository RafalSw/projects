#Listy:

nazwaListy=["Agata","Marta","Monika","Klaudia"]
print(nazwaListy)

print("nazwaListy[2]: "+nazwaListy[2]) #"Monika - indeksowanie od 0

print("nazwaListy[-1]: "+nazwaListy[-1])

nowaLista = nazwaListy[1:3]
print("nazwaListy[1:3]: "+str(nowaLista))

nazwaListy.append("Ola") #dodawanie na koncu listy
print(nazwaListy)

nazwaListy.insert(2,"Ania")
print(nazwaListy)

nazwaListy.extend(["Marysia", "Kinga"])
print(nazwaListy)
nowaLista.append(["Marysia", "Kinga","Sandra","Kasia"])
print(nowaLista)
print(nowaLista[2])
print(nowaLista[0])
print(nowaLista[2][0])
print(nowaLista[2][1:3])

print("Index \"Ola\": "+str(nazwaListy.index("Ola")))
print("Jola" in nazwaListy)
print("Monika" in nazwaListy)

print("Przed sortowaniem:\n\t"+str(nazwaListy))
nazwaListy.sort()
print("Po sortowaniu:\n\t"+str(nazwaListy))

lista2=list(nazwaListy)
nazwaListy.reverse()
print("odwrócone:\n\t"+str(nazwaListy))
print("odwrócone:\n\t"+str(lista2))

nazwaListy.remove("Ania") #pierwsza znaleziona wartosc
print(nazwaListy)

print("Ostatni element: "+nazwaListy.pop())
print(nazwaListy)

print("Element o indeksie 3: "+nazwaListy.pop(3))
print(nazwaListy)

while("Ania" in nazwaListy):
    nazwaListy.remove("Ania")

for imie in nazwaListy:
    print("Przykladowe imie to: "+imie)

nazwaListy[3] = "Zuzia"
print(nazwaListy)

s2 = ", ".join(nazwaListy)
print(s2)

print("\n\n\n\n-------------KROTKI(tuple)--------------")
tup = ("Agata","Marta","Monika","Klaudia")
print(tup)

print("tup[2]: "+tup[2]) #"Monika - indeksowanie od 0

print("tup[-1]: "+tup[-1])

#tup[3]="Zuzia" #nie ma mozliwosci przypisania
print(tup)

s=", ".join(tup)
print(s)
print(s.split(", "))

tup1 = "3.14","100"
tup2 = "test","slowo"
print(tup1)
print(tup2)
tup3 = tup1+tup2
print(tup3)

print(len(tup3))
print(cmp(tup1,tup2))

del(tup3)
print(tup3)

#lista tupli:
listaNazwisk = [("Jan","Kowalski"),("Karol","Nowak"),("Donald","Tusk"),("Janina","Nowak")]

for imie,nazwisko in listaNazwisk:
    if(imie[-1]=='a'):
        print(imie+" "+nazwisko+" to kobieta")
    else:
        print(imie+" "+nazwisko+" to mezczyzna")

