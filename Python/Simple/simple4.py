import re

def compare(text1, text2):
    word1 = re.split('; |, |. | |\n',text1[:-1])
    word2 = re.split('; |, |. | |\n',text2[:-1])
    if len(word2)==0:
        return 0
    
    counter = 0
    for word in word2:
        if word in word1:
            counter += 1
    print(float(counter)/len(word2))
    return float(counter)/len(word2)

def analyzeTemplate(inputFileName, textBase):
    inputFile = open(inputFileName, 'r')

    
    for line in inputFile:
        if len(line) < 2:
            break
        elements = line.split(',')
        textBase.append([elements[1],elements[4]])
        
    inputFile.close()

    
def analyzeFile(inputFileName, textBase):
    edge = 0.75
    result={}
    for element in textBase:
        result[element[0]]=0
    
    inputFile = open(inputFileName, 'r')
    for line in inputFile:
        elements = line.split(';')
        if len(elements)<8:
            continue
        elif elements[5]=="Dostarczony do odbiorcy":
            for company in textBase:
                if compare(elements[7], company[1])>edge:
                    result[company[0]]=result[company[0]]+1
                    break
    return result   
    

textBase=[]
templateFileName = "Fake szablon.csv"
fileName = "Fake plik z bazy.csv"
analyzeTemplate(templateFileName, textBase)
result = analyzeFile(fileName, textBase)
print(result)


