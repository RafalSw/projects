import datetime
import sys
import os.path

def compare(text1, descriptors):
    for descriptor in descriptors:
        if not  descriptor in text1:
            return False
    return True

def analyzeTemplate(inputFileName, textBase):
    inputFile = open(inputFileName, 'r')
    
    for line in inputFile:
        if len(line) < 2:
            break
        elements = line.split(',')

        #if(elements[3] == '3'):
            #continue
        
        description = elements[4]
        while ('{' in description) and ('}' in description):
            index1 = description.find('{')
            index2 = description.find('}')
            description = description[0:index1]+'#'+description[index2+1:]
        
        textBase.append([elements[1],description.split('#')])
        
    inputFile.close()

    
def analyzeFile(inputFileName, textBase):
    lineNumbers = 0
    result={}
    for element in textBase:
        result[element[0]]=0
    
    inputFile = open(inputFileName, 'r')
    for line in inputFile:
        lineNumbers+=1
        elements = line.split(';')
        if len(elements)<8:
            continue
        elif elements[5]=="Dostarczony do odbiorcy":
            for company in textBase:
                if compare(elements[7], company[1]):
                    result[company[0]]+=1
                    break
    print("Przeanalizowano "+str(lineNumbers)+" linijek")
    return result   

if len(sys.argv)>2:
    print("Szablon: "+sys.argv[1])
    if sys.argv[1][-4:].lower()!=".csv":
        print("Podaj poprawną nazwę pliku zawierającego szablon - musi to być plik *.csv")
        sys.exit(-1)
    elif os.path.isfile(sys.argv[1]) == False:
        print("Podaj poprawną nazwę pliku zawierającego szablon - plik musi istnieć!")
        sys.exit(-1)
    templateFileName = sys.argv[1]
    
    print("Dane: "+sys.argv[2])
    if sys.argv[2][-4:].lower()!=".csv":
        print("Podaj poprawną nazwę pliku zawierającego dane - musi to być plik *.csv")
        sys.exit(-1)
    elif os.path.isfile(sys.argv[2]) == False:
        print("Podaj poprawną nazwę pliku zawierającego dane - plik musi istnieć!")
        sys.exit(-1)
    fileName = sys.argv[2]

    start = datetime.datetime.now()
    textBase=[]
    analyzeTemplate(templateFileName, textBase)
    result = analyzeFile(fileName, textBase)
    stop = datetime.datetime.now()
    print("Czas: ")
    print(stop-start)
    print(result)
    
else:
    print("Poprawne użycie programu:")
    print("python "+sys.argv[0]+" <nazwa pliku z szablonem>.csv <nazwa pliku z danymi>.csv")
    



