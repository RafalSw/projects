#UWAGI:
# wyjście z labiryntu o współrzędnych (n-1, k-1) oznaczam jako E

# 1. wczytanie pliku od użytkownika
def czytanie_pliku(nazwa):
    plik = open(nazwa, 'r')
    return plik

# 2. wypisanie labiryntu na ekranie
def rysuj(plik):
    maze = []
    for line in plik: #@@@@ kolejne linijki pliku sa analizowane
        #i = i.rstrip('\n') # TRZEBA KAZAC MU WYCIAC KONKRETNY ELEMENT A NIE WSZYTSKIE OSTATNIE - CZY PLIK JEST LISTA OD POCZAKU? #@@@@ to juz nie jest potrzebne
        line = line.replace("\n","") #@@@@ usuniecie znaku nowej linii (o ile istnieje) poprzez zastapienie go niczym
        chars = [] #@@@@ dodatkowo kazda linijka zamieniana jest na ciag znakow
        for char in line:
            chars.append(char)
        maze.append(chars)
    print (maze) # rysuje labirynt w formie listy list
    return maze

# 3. czy da się przejść labirynt?
def search(x, y, maze):
    #@@@@ dla ulatwienia - wypisanie listy krokow
    #if maze[x][y] == '.':
        #print(x,y)
    if maze[x][y] == 'E': # od razu pierwszy element jest zarazem wyjściem
        #print ('found at %d,%d' % (x, y), 'True')
        return True
    elif maze[x][y] == '#':
        return False
    elif maze[x][y] == '*':
        return False
    #maze[x][y] = list(maze[x][y]) # oznaczamy, że już w tym miejscu labiryntu byliśmy #@@@@ To nie potrzebne
    maze[x][y] = '*'
    if ((x < len(maze)-1 and search(x+1, y, maze)) # sprawdzamy sąsiadujące miejsca (zgodnie ze wskazówkami zegara, zaczynając od pierwszej po prawej)
        or (y > 0 and search(x, y-1,maze))
        or (x > 0 and search(x-1, y,maze))
        or (y < len(maze[0])-1 and search(x, y+1,maze))): #@@@@ tu brakowalo [0], przez co analizowal tylko kwadrat
        return True
    return False 
    
def main():
    nazwa = input("Podaj nazwe pliku z rozszerzeniem .txt ")
    plik = czytanie_pliku(nazwa)
    #plik = [list(linia) for linia in plik.readlines()] #@@@@ nie dzielimy pliku na linie, tylko w dalszej czesci wczytujemy po linijce
    #@@@@  tu bylo troche pomieszane, bo maze zamiast byc tym co zwroci 'rysuj' byl nadpisywany plikiem
    #rysuj(plik)
    #maze = plik
    maze = rysuj(plik)
    x = search(0, 0, maze)
    if x !=True:
        print ('False')
    else:
        print ('True')
    
if __name__ == "__main__":
    main()
