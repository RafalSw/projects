import re #wykorzystuje wyrazenia regularne: https://docs.python.org/2/library/re.html
import collections #do sortowania zawartosci raportu
import csv
import sys
from datetime import datetime, time, timedelta, date
from os import listdir
from os.path import isfile, join

#definiowanie nazw plikow
dirName = "data_1"
reportFileName = "report.csv"


def reportByDate(reports, start):
    if str(start.date()) not in reports.keys():
        reports[str(start.date())] = [0,0,0] #suma_FP_all, suma_FP_ack, cunter_ASSR
    return reports[str(start.date())]

def analyzeFile(reports, inputFileName):
    print("Analiza pliku "+inputFileName)
    inputFile = open(inputFileName, 'r') #otwarcie do odczytu

    state = 0
    
    suma_FP_all = 0 #ilosc lotow
    suma_FP_act = 0 #ilosc lotow zrealizowanych
    counter_ASSR = 0
    counterOfActive_Transfer_ASSR = [0,0,-1,-1] #Active, Transfer, ASSR
    lineNumber = 0
    dateStart = None # data RECEIVED ACT

    #plik jest analizowany po linijce. wyszukiwane sa konkretne linijki z uzyciem wyrazen regularnych (regex)
    for line in inputFile:
        lineNumber = lineNumber + 1
        matchResult = None
        # poszukiwanie *******
        matchResult = re.match(r"^\s\*\*\*\*\*\*\*\*\*", line)
        if matchResult is not None: #linia zaczyna sie zgodnie z zalozeniem
            if state == 0: #czekalismy na gwiazdki - czyszczenie danych wyjsciowych dla danego rozkazu, przejscie do kolejnego stanu
                state = 1
                print(line)
            else:
                print("Plik jest niespojny - ********* w niewlasciwym momencie: "+str(lineNumber))
                counterOfActive_Transfer_ASSR = [0,0,-1,-1]
            continue #przejscie do kolejnej linijki
        if state==1:
            #poszukiwanie pierwszej linijki z data
            matchResult = re.match(r".+/([0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9])", line)
            if matchResult is not None:
                if state == 1:
                    state = 2
                    dateStart = datetime.strptime(matchResult.groups(0)[0], "%d-%m-%y %H:%M:%S")
                    print(line)
                else:
                    print("Plik jest niespojny - RECEIVED ACT/ w niewlasciwym momencie: "+str(lineNumber))
                    counterOfActive_Transfer_ASSR = [0,0,-1,-1]
                continue #przejscie do kolejnej linijki

        #poszukiwanie ACTIVE
        matchResult = re.search(r"ACTIVE", line)
        if matchResult is not None:
            counterOfActive_Transfer_ASSR[0]=1
            continue #przejscie do kolejnej linijki
           
        #poszukiwanie TRANSFER.
        matchResult = re.search(r"TRANSFER", line)
        if matchResult is not None:
           counterOfActive_Transfer_ASSR[1]=1
           continue #przejscie do kolejnej linijki

        #poszukiwanie ASSR
        matchResult = re.match(r"\bASSR ?: ?\b([0-9][0-9][0-9][0-9])", line)
        if matchResult is not None:
            print("linia ASSR to: ", line)
            if state > 1: # oczekiwalismy na ASSR i w koncu nadszedl - analizujemy
                print("linia ASSR w zmiennej to: ", matchResult.groups(0)[0]) # 14.05 druk na ekran sq po ASSR ze zmiennej
                counterOfActive_Transfer_ASSR[2] = int(matchResult.groups(0)[0]) # 14.05 zmienna przypisana do tablicy
                print("linia ASSR w tablicy to: ", counterOfActive_Transfer_ASSR[2]) # 14.05 druk na ekran wartosci z tablicy z kodem sq
            continue #przejscie do kolejnej linijki

        #poszukiwanie PSSR i okreslanie jego typu
        matchResult = re.match(r"\bPSSR ?: ?\b([0-9][0-9][0-9][0-9])", line)
        if matchResult is not None:
            print("PSSR ze zmiennej: ", matchResult.groups(0)[0]) # 14.05 druk na ekran sq po PSSR ze zmiennej
            counterOfActive_Transfer_ASSR[3] = int(matchResult.groups(0)[0])
            print("PSSR z tablicy: ", counterOfActive_Transfer_ASSR[3]) # 14.05 druk na ekran wartosci z tablicy z kodem sq
            continue #przejscie do kolejnej linijki
        
        #poszukiwanie FP TERMINATED
        matchResult = re.match(r"\bFP TERMINATED\b", line)
        if matchResult is not None:

            if dateStart is None:
                print(counterOfActive_Transfer_ASSR)
                if state > 0:
                    print("Brak daty dla telegremu konczacego sie w "+str(lineNumber)+" linijce")
                    suma_FP_all +=1
                if counterOfActive_Transfer_ASSR[0]==1 and counterOfActive_Transfer_ASSR[1]==1 and counterOfActive_Transfer_ASSR[2]!=-1:
                    suma_FP_act +=1
                    if counterOfActive_Transfer_ASSR[3] == -1 or counterOfActive_Transfer_ASSR[3]!=counterOfActive_Transfer_ASSR[2]:
                        counter_ASSR +=1
            else:
                print(counterOfActive_Transfer_ASSR)
                if state > 0:
                    reportByDate(reports, dateStart)[0]+=1
                if counterOfActive_Transfer_ASSR[0]==1 and counterOfActive_Transfer_ASSR[1]==1 and counterOfActive_Transfer_ASSR[2]!=-1:
                    reportByDate(reports, dateStart)[1]+=1
                    if counterOfActive_Transfer_ASSR[3]!=-1 or counterOfActive_Transfer_ASSR[3]!=counterOfActive_Transfer_ASSR[2]:
                        reportByDate(reports, dateStart)[2]+=1
            counterOfActive_Transfer_ASSR = [0,0,-1,-1]
            '''
            if state == 3:
                #if assr != -1 and currentType != -1: #sprawdzenie czy linia "kwalifikuje sie" do zapisu - czy ASSR zostal znaleziony i czy jest poprawny, okreslenie czasu uzycia
                print("")
            else:
                print("Plik jest niespojny - FP TERMINATED w niewlasciwym momencie: "+str(lineNumber))
            '''
            #czyszczenie zmiennych
            state = 0
            dateStart = None
            
            continue #przejscie do kolejnej linijki
    
    #zamkniecie uzywanych plikow
    inputFile.close()
    return suma_FP_all, suma_FP_act, counter_ASSR

reports = {}

onlyfiles = [ f for f in listdir(dirName) if isfile(join(dirName,f)) ]
for fileName in onlyfiles:
    analyzeFile(reports, dirName+"\\"+fileName)

#sortowanie
order = []
for key in reports.keys():
    #print reports.keys() #14.05 drukuje daty raportow
    order.append(key)
order.sort()

#zapis raportu do pliku
data=[]
data.append(["Date","suma_FP_all","suma_FP_ack","counter_ASSR"])
for key in order:
    line = []
    line.append(key)
    #print line
    for partOfReport in reports[key]:
        line.append(str(partOfReport))
    data.append(line)

f=None
if sys.version_info >= (3,0,0):
    f = open(reportFileName, 'w', newline='')
else:
    f = open(reportFileName, 'wb')

a = csv.writer(f, dialect='excel')
for line in data:
    a.writerow(line)
    
f.close()
