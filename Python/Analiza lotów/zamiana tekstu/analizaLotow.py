import re #wykorzystuje wyrazenia regularne: https://docs.python.org/2/library/re.html
import string
    
#definiowanie nazw plikow
inputFileName = "data.txt"
outputFileName = "output.txt"

inputFile = open(inputFileName, 'r') #otwarcie do odczytu
outputFile = open(outputFileName, 'w', newline='\n') #otwarcie do zapisu

IFRCounter = 1
lineNumber = 0
flag = 0
state = 0 #0-oczekiwanie na nawias otwierajacy, 1-oczekiwanie na nawias zamykajacy

outputString = ""
#plik jest analizowany po linijce. wyszukiwane sa konkretne linijki z uzyciem wyrazen regularnych (regex)
for line in inputFile:
    lineNumber+=1

    #poszukiwanie poczatku planu lotu, podzielenie go na grupy, gdzie [0] to CALLSIGN, a [1] to typ, ktory ewentualnie jest zamieniany
    matchResult = re.match("\(FPL-([a-zA-Z0-9]+)-([a-zA-Z0-9]+)", line)
    if matchResult is not None:
        if state!= 0:
            print("Blad w linii "+str(lineNumber)+"! Nieoczekiwany poczatek planu lotu")
            break;
        state=1;
        outputString+="(FPL-IFR"+str(IFRCounter).zfill(3)+"-"
        struct = matchResult.groups(0)[1]
        if struct[0]=="V":
            struct="I"+struct[1]
            flag = 1
        elif struct=="ZG":
            struct = "IG"
            flag = 1
        elif struct=="YN":
            struct="IN"
            flag = 1
        elif struct=="ZN":
            struct="IN"
            flag = 1
        elif struct=="YG":
            struct="IG"
            flag = 1
        outputString+=struct+"\n"
        
        continue

    #znaleziono koniec planu lotu, dodawanie aktualnej linijki i zapis bufora outputString do pliku
    matchResult = re.search("\)", line)
    if matchResult is not None and state==1:
        line = line.replace("VFR","")
        line = line.replace("IFR","")
        outputString+=line
        if flag==1:
            IFRCounter+=1
            outputFile.write(outputString)
            outputFile.write("\n")
        flag = 0
        outputString=""
        state=0;
        continue
        
    if state==1:
        line = line.replace("VFR","")
        line = line.replace("IFR","")
        outputString+=line
    
#zamkniecie uzywanych plikow
inputFile.close()
outputFile.close()
