import re #wykorzystuje wyrazenia regularne: https://docs.python.org/2/library/re.html
import collections #do sortowania zawartosci raportu
from datetime import datetime, time, timedelta, date

import os

#funkcja przygotowujaca liste zera
def listOfZeros(size):
    result = [0]*size
    return result

#funkcja przygotowujaca szablon raportu w oparciu o ilosc typow i rozdzielczosc czasowa. Szablon to godzina i tablica zer
def prepareReport(types, timeResolution):
    report = []
    simpleTime = datetime.combine(date.today(), time(0,0))
    while (simpleTime + timedelta(minutes = timeResolution)).day == date.today().day: #generowanie godzin az do konca dnia
        simpleTime = simpleTime + timedelta(minutes = timeResolution)
        report.append([[str(simpleTime.time())],listOfZeros(len(types))]) #kazda z godzin wypelniana zerami
    return report

#funkcja wypelniajaca raport zgodnie z zadanym typem lotu i zakresami czasowymi
def fillReport(reports, typeId, start, stop):
    report = reportByDate(reports, start, stop)
    if len(report)==1:
        report = report[0]
        findStart = -1 #indeks startu
        findStop = -1 #indeks stop
        for index in range(0, len(report)): #znalezienie poczatkowego zakresu czasu - poszukiwania od konca az do znalezienia mniejszej wartosci
            if report[len(report)-index-1][0][0]<str(start.time()):
                findStart = len(report)-index-1
                break

        #poszukiwanie konca zakresu
        for index in range(0, len(report)+1): #poszukiwanie konca - analiza od poczatku az do znalezienia wiekszej wartosci lub konca dnia
            if index == len(report):
                findStop = index
                break
            elif report[index][0][0]>str(stop.time()):
                findStop = index
                break

        #zwiekszenie ilosci wystapien o 1 w danym zakresie czasowym dla danego typu lotu
        for i in range(findStart, findStop):
            report[i][1][typeId]+=1
    else:
        findStart = -1 #indeks startu
        findStop = -1 #indeks stop
        report1=report[0]
        report2=report[1]
        for index in range(0, len(report1)): #znalezienie poczatkowego zakresu czasu - poszukiwania od konca az do znalezienia mniejszej wartosci
            if report1[len(report1)-index-1][0][0]<str(start.time()):
                findStart = len(report1)-index-1
                break

        #poszukiwanie konca zakresu
        for index in range(0, len(report2)+1): #poszukiwanie konca - analiza od poczatku az do znalezienia wiekszej wartosci lub konca dnia
            if index == len(report2):
                findStop = index
                break
            elif report2[index][0][0]>str(stop.time()):
                findStop = index
                break

        #zwiekszenie ilosci wystapien o 1 w danym zakresie czasowym dla danego typu lotu
        for i in range(findStart, len(report1)):
            report1[i][1][typeId]+=1
        for i in range(0, findStop):
            report2[i][1][typeId]+=1
    return reports

def reportByDate(reports, start, stop):
    if start.date() == stop.date():
        if str(start.date()) not in reports.keys():
            reports[str(start.date())] = prepareReport(types,timeResolution)
        return [reports[str(start.date())]]
    else:
        result = []
        if str(start.date()) not in reports.keys():
            reports[str(start.date())] = prepareReport(types,timeResolution)
        if str(stop.date()) not in reports.keys():
            reports[str(stop.date())] = prepareReport(types,timeResolution)
        result.append(reports[str(start.date())])
        result.append(reports[str(stop.date())])
        return result
    
#definiowanie nazw plikow
inputFileName = "data.txt"
outputFileName = "output.txt"
reportFileName = "report.txt"
timeResolution = 10 #rozdzielczosc czasu w raporcie wyrazona w minutach

inputFile = open(inputFileName, 'r') #otwarcie do odczytu
outputFile = open(outputFileName, 'w') #otwarcie do zapisu
reportFile = open(reportFileName, 'w') #otwarcie pliku z raportem

#stan kontroluje poprawnosc danych - czy nastepuja kolejno po sobie
state = 0
# state:
# 0 - oczekiwanie na "*****"
# 1 - oczekiwanie na RECEIVED ACT/
# 2 - oczekiwanie na ASSR
# 3 - oczekiwanie na CANCEL/EVENT/
# 4 - oczekiwanie na FP TERMINATED

types = [[1,17],[1330,1377],[1730,1737],[4130,4177],[4501,4577],[4601,4637],[6301,6377]] #obslugiwane typy ASSR

reports = {}

suma_FP_all = 0 #ilosc lotow
suma_FP_act = 0 #ilosc lotow zrealizowanych
counterOfActive_Transfer_ASSR = [0,0,0] #Active, Transfer, ASSR
lineNumber = 0
assr = -1 #numer assr
currentType = -1 #typ lotu (przedzial z types)
outputLine = "" #linia tekstu, pojedynczy rekord zapisywany do pliku wyjsciowego
dateStart = None # data RECEIVED ACT
dateStop = None # data CANCEL/EVENT

#plik jest analizowany po linijce. wyszukiwane sa konkretne linijki z uzyciem wyrazen regularnych (regex)
for line in inputFile:
    lineNumber = lineNumber + 1
    matchResult = None
    # poszukiwanie *******
    matchResult = re.match(r"^\s\*\*\*\*\*\*\*\*\*", line)
    if matchResult is not None: #linia zaczyna sie zgodnie z zalozeniem
        if state == 0: #czekalismy na gwiazdki - czyszczenie danych wyjsciowych dla danego rozkazu, przejscie do kolejnego stanu
            outputLine = ""
            state = 1
        else:
            print("Plik jest nie spojny - ********* w niewlasciwym momencie: "+str(lineNumber))
            counterOfActive_Transfer_ASSR = [0,0,0]
        continue #przejscie do kolejnej linijki

    #poszukiwanie RECEIVED ACT/
    matchResult = re.match(r"\bRECEIVED ACT/\b([0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9])", line)
    if matchResult is not None:
        if state == 1:
            state = 2
            dateStart = datetime.strptime(matchResult.groups(0)[0], "%d-%m-%y %H:%M:%S")
        else:
            print("Plik jest nie spojny - RECEIVED ACT/ w niewlasciwym momencie: "+str(lineNumber))
            counterOfActive_Transfer_ASSR = [0,0,0]
        continue #przejscie do kolejnej linijki

    #poszukiwanie ACTIVE
    matchResult = re.search(r"ACTIVE", line)
    if matchResult is not None:
        counterOfActive_Transfer_ASSR[0]=1
       
    #poszukiwanie TRANSFER.
    matchResult = re.match(r"\bTRANSFER.\b", line)
    if matchResult is not None:
       counterOfActive_Transfer_ASSR[1]=1

    #poszukiwanie ASSR i okreslanie jego typu
    matchResult = re.match(r"\bASSR: \b([0-9][0-9][0-9][0-9])", line)
    if matchResult is not None:
        if state == 2: # oczekiwalismy na ASSR i w koncu nadszedl - analizujemy
            counterOfActive_Transfer_ASSR[2] = 1
            state = 3
            assr = int(matchResult.groups(0)[0])
            i=0 #indeks listy types
            for typeValues in types: #poszukiwanie przedzialu z types - okreslenie typu lotu
                if assr >= typeValues[0] and assr <= typeValues[1]:
                    currentType = i
                    outputLine += str(assr).zfill(4)+"\t["+str(typeValues[0])+", "+str(typeValues[1])+"]\t" #dodanie do outputLine informacji o ASSR(uzupelnionej zerami do 4 cyfr) i o zakresie
                    break #znaleziono wlasciwa grupe - wyjscie z petli poszukujacej
                else:
                    i = i+1
        else:
            print("Plik jest nie spojny - ASSR w niewlasciwym momencie: "+str(lineNumber))
            counterOfActive_Transfer_ASSR = [0,0,0]
        continue #przejscie do kolejnej linijki
    
    
    #poszukiwanie CANCEL/EVENT/
    matchResult = re.match(r"\bCANCEL/EVENT/\b([0-9][0-9]-[0-1][0-9]-[0-3][0-9] [0-2][0-9]:[0-5][0-9]:[0-5][0-9])", line)
    if matchResult is not None:
        if state == 3:
            state = 4
            dateStop = datetime.strptime(matchResult.groups(0)[0], "%d-%m-%y %H:%M:%S")
        else:
            print("Plik jest nie spojny - CANCEL/EVENT/ w niewlasciwym momencie: "+str(lineNumber))
        continue #przejscie do kolejnej linijki

    #poszukiwanie FP TERMINATED
    matchResult = re.match(r"\bFP TERMINATED\b", line)
    if matchResult is not None:
        
        if state >0:
            suma_FP_all +=1
        if counterOfActive_Transfer_ASSR[0]==1 and counterOfActive_Transfer_ASSR[1]==1 and counterOfActive_Transfer_ASSR[2]==1:
            suma_FP_act +=1
            counterOfActive_Transfer_ASSR = [0,0,0]
        if state == 4:
            if assr != -1 and currentType != -1: #sprawdzenie czy linia "kwalifikuje sie" do zapisu - czy ASSR zostal znaleziony i czy jest poprawny, okreslenie czasu uzycia
                timeOfUse = dateStop-dateStart
                outputFile.write(outputLine+str(timeOfUse)+'\n')#zapis outputLine do pliku
                fillReport(reports, currentType, dateStart, dateStop)
        else:
            print("Plik jest nie spojny - FP TERMINATED w niewlasciwym momencie: "+str(lineNumber))
        #czyszczenie zmiennych
        state = 0
        outputLine=""
        assr = -1
        currentType = -1
        dateStart = None
        dateStop = None
        
        continue #przejscie do kolejnej linijki

print("Ilosc wszystkich lotow: "+str(suma_FP_all))
print("Ilosc zrealizowanych lotow: "+str(suma_FP_act))

#sortowanie
order = []
for key in reports.keys():
    order.append(key)
order.sort()

#zapis raportu do pliku
for key in order:
    header = "Raport z dnia: "+key+"\ntime\t"
    for simpleType in types:
        header+= "["+str(simpleType[0])+", "+str(simpleType[1])+"]\t"
    reportFile.write(header+"\n")

    for partOfReport in reports[key]:
        line = partOfReport[0][0]+"\t"
        for value in partOfReport[1]:
            line+=str(value)+"\t"
        line+="\n"
        reportFile.write(line)
    reportFile.write("\n")
    

#zamkniecie uzywanych plikow
inputFile.close()
outputFile.close()
reportFile.close()
