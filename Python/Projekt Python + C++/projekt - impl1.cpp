#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <Windows.h>
#include <locale> 

using namespace std;

const int ROZMIAR_POCZATKOWY = 512;
double powiekszInt, sortujInt, dodajInt, wyszukajInt;
class Zestaw
{
public:
	Zestaw(string s)
	{
		slowo = s;
		ilosc = 1;
	}
	bool operator<(Zestaw& rhs)
	{
		for(unsigned int i=0; i<slowo.length()&&i<rhs.slowo.length();i++)
		{
			if(tolower(slowo.at(i))<tolower(rhs.slowo.at(i)))
				return true;
			else if(tolower(slowo.at(i))>tolower(rhs.slowo.at(i)))
				return false;
		}
		if(slowo.length()<=rhs.slowo.length())
			return true;
		else
			return false;
	}

	string slowo;
	int ilosc;
};

class Slowa
{
public:
	Slowa()
	{
		rozmiar = ROZMIAR_POCZATKOWY;
		tablica = (Zestaw**)malloc(rozmiar*sizeof(Zestaw*));
		uzywane = 0;
	}
	~Slowa()
	{
		for(int i=0;i<uzywane;i++)
		{
			delete tablica[i];
		}
		free(tablica);
	}
	void powieksz()
	{
		time_t start = clock();
		Zestaw **temp = (Zestaw**)malloc(2*rozmiar*sizeof(Zestaw*));
		for(int i=0; i<rozmiar; i++)
		{
			temp[i]=tablica[i];
		}
		Zestaw **temp2 = tablica;
		tablica=temp;
		free(temp2);
		rozmiar*=2;
		time_t stop = clock();
		powiekszInt+=difftime(stop, start);
	}
	void dodaj(string slowo)
	{
		time_t start = clock();
		int index = wyszukaj(slowo);
		if(index != -1)
		{
			tablica[ index ]->ilosc++;
		}
		else
		{
			if( ( uzywane + 1 ) == rozmiar )
			{
				powieksz();
			}

			Zestaw * nowy = new Zestaw( slowo );
			tablica[ uzywane ] = nowy;
            uzywane++;
			
		}
		time_t stop = clock();
		dodajInt+=difftime(stop, start);
		sortowaneIlosciowe();
	}
    void sortowaneIlosciowe()
    {
		time_t start = clock();
        int i, j;
	    Zestaw *tmp;
	    for (i = 1; i<uzywane; i++)
	    {
		    j = i;
		    tmp = tablica[i];
		    while (j > 0 && tablica[j - 1]->ilosc<tmp->ilosc)
		    {
			    tablica[j] = tablica[j - 1];

			    j--;
		    }
		    tablica[j] = tmp;
	    }
		time_t stop = clock();
		sortujInt+=difftime(stop, start);
    }
	int wyszukaj(string slowo)
	{
		time_t start = clock();
		int result = -1;
		for(int i=0; i<uzywane; i++)
		{
			if( tablica[ i ]->slowo == slowo)
			{
				result = i;
				break;
			}

		}
		
		time_t stop = clock();
		wyszukajInt+=difftime(stop, start);
		return result;
	}
	void wypiszTop20(int ilosc)
	{
		sortowaneIlosciowe();
		for(int i=0;i<min(20,uzywane);i++)
		{
			cout<<i+1<<".\t"<<tablica[i]->slowo<<": "<<double(tablica[i]->ilosc)/ilosc<<endl;
		}
	}
private:
	Zestaw **tablica;
	int rozmiar;
	int uzywane;
};

void main()
{
	powiekszInt=sortujInt=dodajInt=wyszukajInt = 0;
	setlocale( LC_ALL, "polish" );
	ifstream plik;
	plik.open("potop.txt", ifstream::in);
	Slowa slowa;
	string slowo;
	int licznik = 0;
	char c = '\0';

	time_t start = clock();
	cout<<"START\nPocz�tek odczytu"<<endl;
	while(plik.good())
	{
		c = plik.get();
		
		if(IsCharAlphaA(c))
		{
			c = tolower(c);
			slowo.push_back(c);
		}
		else if(!slowo.empty())
		{
			licznik++;
			slowa.dodaj(slowo);
			slowo = "";
		}
	}
	cout<<"Koniec odczytu"<<endl;
	cout<<"Liczba s��w: "<<licznik<<endl;

	slowa.sortowaneIlosciowe();
    slowa.wypiszTop20(licznik);
	time_t stop = clock();	
	cout<<"STOP"<<endl;
	double delta = difftime(stop, start);
	std::cout<<"Czas: "<<delta<<" ms"<<endl;
	plik.close();
	cout<<"Sortowanie: "<<sortujInt<<" ms Dodaj: "<<dodajInt<<" ms Powieksz: "<<powiekszInt<<" ms Wyszukaj: "<<wyszukajInt<<" ms"<<endl;
	system("PAUSE");
}