import collections
import datetime

file = open('potop.txt', 'r')
licznik = 0
c = ''
slowo = ""
slownik = dict()
start = datetime.datetime.now()
print("START: " + str(start))
print("Początek odczytu")
while 1:
    c = file.read(1)
    if c == "":
        print("Koniec odczytu")
        break
    else:
        if c.isalpha():
            c = c.lower()
            slowo += c
        else:
            if slowo != "":
                licznik+=1
                if slowo in slownik:
                    slownik[slowo] = slownik[slowo]+1
                else:
                    slownik[slowo] = 1
                slowo = ""

wynik = collections.OrderedDict(sorted(slownik.items(), key = lambda t: t[1]))

print("Liczba słów: " + str(licznik))
for i in range(0,20):
    temp = wynik.popitem()
    print(str(i+1) + ".\t" + temp[0]+": " + str(temp[1]/licznik))

stop = datetime.datetime.now()
print("STOP: " + str(stop))
delta = stop - start
print("Czas: " + str(delta.microseconds/1000) + " ms")

