#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include <string>
#include <time.h>
#include <Windows.h>
#include <locale> 

using namespace std;

const int ROZMIAR_POCZATKOWY = 512;
double powiekszInt, sortujInt, dodajInt, wyszukajInt;
class Zestaw
{
public:
	Zestaw(string s)
	{
		slowo = s;
		ilosc = 1;
	}
	bool operator<(Zestaw& rhs)
	{
		for(unsigned int i=0; i<slowo.length()&&i<rhs.slowo.length();i++)
		{
			if(tolower(slowo.at(i))<tolower(rhs.slowo.at(i)))
				return true;
			else if(tolower(slowo.at(i))>tolower(rhs.slowo.at(i)))
				return false;
		}
		if(slowo.length()<=rhs.slowo.length())
			return true;
		else
			return false;
	}

	string slowo;
	int ilosc;
};

class Slowa
{
public:
	Slowa()
	{
		rozmiar = rozmiarWyniki = ROZMIAR_POCZATKOWY;
		tablica = (Zestaw**)malloc(rozmiar*sizeof(Zestaw*));
		tablicaWynikow = (Zestaw**)malloc(rozmiarWyniki*sizeof(Zestaw*));
		uzywane = uzywaneWyniki = 0;
	}
	~Slowa()
	{
		for(int i=0;i<uzywane;i++)
		{
			delete tablica[i];
		}
		for(int i=0;i<uzywaneWyniki;i++)
		{
			delete tablicaWynikow[i];
		}
		free(tablica);
		free(tablicaWynikow);
	}
	void powieksz()
	{
		time_t start = clock();
		Zestaw **temp = (Zestaw**)malloc(2*rozmiar*sizeof(Zestaw*));
		for(int i=0; i<rozmiar; i++)
		{
			temp[i]=tablica[i];
		}
		Zestaw **temp2 = tablica;
		tablica=temp;
		free(temp2);
		rozmiar*=2;
		time_t stop = clock();
		powiekszInt+=difftime(stop, start);
	}
	void powiekszWyniki()
	{
		time_t start = clock();
		Zestaw **temp = (Zestaw**)malloc(2*rozmiarWyniki*sizeof(Zestaw*));
		for(int i=0; i<rozmiarWyniki; i++)
		{
			temp[i]=tablicaWynikow[i];
		}
		Zestaw **temp2 = tablicaWynikow;
		tablicaWynikow=temp;
		free(temp2);
		rozmiarWyniki*=2;
		time_t stop = clock();
		powiekszInt+=difftime(stop, start);
	}
	void dodaj(string slowo)
	{
		time_t start = clock();
		if( ( uzywane + 1 ) == rozmiar )
		{
			powieksz();
		}

		Zestaw * nowy = new Zestaw( slowo );
		tablica[ uzywane ] = nowy;
        uzywane++;
		time_t stop = clock();
		dodajInt+=difftime(stop, start);
	}
	void sortowanieAlfabetyczneShella()
	{
		time_t start = clock();
		Zestaw *x;
		int i,h = 1;
		while(h<uzywane)
			h = 3*h+1;
		if(h==0)
			h=1;

		while(h>0)
		{
			for(int j = uzywane - h - 1; j >= 0; j--)
			{
				x = tablica[j];
				i=j+h;

				while((i < uzywane) && (*tablica[i] < *x))
				{
					tablica[i-h] = tablica[i];
					i+=h;
				}
				tablica[i-h]=x;
			}
			h/=3;
		}
		time_t stop = clock();
		sortujInt+=difftime(stop, start);
	}
    void sortowaneIlosciowe()
    {
		time_t start = clock();
        int i, j;
	    Zestaw *tmp;
	    for (i = 1; i<uzywaneWyniki; i++)
	    {
		    j = i;
		    tmp = tablicaWynikow[i];
		    while (j > 0 && tablicaWynikow[j - 1]->ilosc<tmp->ilosc)
		    {
			    tablicaWynikow[j] = tablicaWynikow[j - 1];

			    j--;
		    }
		    tablicaWynikow[j] = tmp;
	    }
		time_t stop = clock();
		sortujInt+=difftime(stop, start);
    }
	
	void licz()
	{
		sortowanieAlfabetyczneShella();
		string ostatnie = "";
		int counter = 1;
		for(int i=0;i<uzywane;i++)
		{
			if(i==0)
			{
				ostatnie = tablica[i]->slowo;
			}
			else if(tablica[i]->slowo == ostatnie)
			{
				counter++;
			}
			else
			{
				Zestaw *temp = new Zestaw(ostatnie);
				temp->ilosc = counter;
				if( ( uzywaneWyniki + 1 ) == rozmiarWyniki )
				{
					powieksz();
				}
				tablicaWynikow[ uzywaneWyniki ] = temp;
				uzywaneWyniki++;
				ostatnie = tablica[i]->slowo;
				counter = 1;
			}
		}
	}

	void wypiszTop20(int ilosc)
	{
		licz();
		sortowaneIlosciowe();
		for(int i=0;i<min(20,uzywaneWyniki);i++)
		{
			cout<<i+1<<".\t"<<tablicaWynikow[i]->slowo<<": "<<double(tablicaWynikow[i]->ilosc)/ilosc<<endl;
		}
	}
private:
	Zestaw **tablica;
	Zestaw **tablicaWynikow;
	int rozmiar;
	int rozmiarWyniki;
	int uzywane;
	int uzywaneWyniki;
};

void main()
{
	powiekszInt=sortujInt=dodajInt=wyszukajInt = 0;
	setlocale( LC_ALL, "polish" );
	ifstream plik;
	plik.open("potop.txt", ifstream::in);
	Slowa slowa;
	string slowo;
	int licznik = 0;
	char c = '\0';

	time_t start = clock();
	cout<<"START\nPocz�tek odczytu"<<endl;
	while(plik.good())
	{
		c = plik.get();
		
		if(IsCharAlphaA(c))
		{
			c = tolower(c);
			slowo.push_back(c);
		}
		else if(!slowo.empty())
		{
			licznik++;
			slowa.dodaj(slowo);
			slowo = "";
		}
	}
	cout<<"Koniec odczytu"<<endl;
	cout<<"Liczba s��w: "<<licznik<<endl;

	slowa.sortowaneIlosciowe();
    slowa.wypiszTop20(licznik);
	time_t stop = clock();	
	cout<<"STOP"<<endl;
	double delta = difftime(stop, start);
	std::cout<<"Czas: "<<delta<<" ms"<<endl;
	plik.close();
	cout<<"Sortowanie: "<<sortujInt<<" ms Dodaj: "<<dodajInt<<" ms Powieksz: "<<powiekszInt<<" ms Wyszukaj: "<<wyszukajInt<<" ms"<<endl;
	system("PAUSE");
}