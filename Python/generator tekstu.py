import string
import random

sentences = 127
words = 11
wordsVar = 7
letters = 6
lettersVar = 5

result=""
for i in range(0,sentences):
    wordsVar2 = random.randint(-wordsVar, wordsVar)
    numberOfWords = words+wordsVar2

    for j in range(0, numberOfWords):
        lettersVar2 = random.randint(-lettersVar, lettersVar)
        numberOfLetters = letters+lettersVar2

        for k in range(0, numberOfLetters):
            if j==0 and k==0:
                result+=random.choice(string.ascii_uppercase)
            else:
                result+=random.choice(string.ascii_lowercase)
        if j+1 != numberOfWords:
            result+=" "
    result+=". "
print(result)
