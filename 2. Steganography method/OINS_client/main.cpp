#include <iostream>
#include <vector>
#include "pcap.h"
#include "../OINS/packetdata.h"
#include "../OINS/packethacker.h"
#include "../OINS/variables.h"

using namespace std;

extern "C"
{
	void compressionTest(const char* fileName);
	void compressionTest2(const char* fileName);
}


int initPcap(pcap_t **fp, FILE **logfile, int snaplen);
int capturingTCPpackets2(Variables* pcapVariables, vector<PacketData*> &packets, pcap_t *fp, FILE *logfile);
int sendTCPPacket(pcap_t *fp, uint8_t *data, int size);
int pcapClose(pcap_t *fp, FILE *logfile);
bool netTest();


int main(int argc, char* argv[]){
	unsigned char mac_address[6] = { 0x00, 0xc3, 0x51, 0x78, 0x0d, 0x34 }; // adres mac komputera docelowego w wesji hexadecymalnej
	//unsigned char mac_address[6] = { 0xBC, 0x5F, 0xF4, 0xC7, 0xF4, 0xA5 };
	//unsigned char mac_address[6] = { 0x00, 0x1e, 0x65, 0xf4, 0x95, 0x82 };

	string fileName = "../Sofokles - Antygona(wynik).txt";
	Variables* pcapVariables = new Variables();
	PacketHacker *packetHacker = new PacketHacker();

	pcapVariables->key = packetHacker->getKey();
	pcapVariables->keySize = packetHacker->getKeySize();
	pcapVariables->N_start = sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader) + sizeof(hackedHeader); // minimalna d�ugo�� pakietu
	pcapVariables->N_stop = 2000;// maksymalna d�ugo�� pakietu
	pcapVariables->mac_address = (unsigned char*)&mac_address;
	pcapVariables->port_start = 0; // Port pocz�tkowy przechwytywania [standardowo: 1 - 65535]
	pcapVariables->port_stop = 65300; // Port ko�cowy przechwytywania [standardowo: 1 - 65535]

	//Kolejka
	vector<PacketData*> receivedPackets;

	pcap_t *fp;
	FILE *logfile;
	int captureResult = -2;
	int sendPacketSize = -1;

	if (initPcap(&fp, &logfile, pcapVariables->N_stop) != 0)
	{
		cout << "Nie udalo sie uzyskac dostepu do interfejsu, koniec dzialania programu!" << endl;
		return -1;
	}

	while (true) //petla glowna, tu bedzie inny warunek
	{
		captureResult = capturingTCPpackets2(pcapVariables, receivedPackets, fp, logfile);

		if (captureResult == -1 || captureResult == 0) //timeout lub brak wlasciwych danych
			continue;
		else if (captureResult == -2)
		{
			//blad
			break;
		}
		else if (captureResult == 2) //dotarlismy do konca pakietu
			break;
	}

	//odczytanie uzyskanych pakietow
	if (captureResult == 2)
	{
		packetHacker->unhackPacket(receivedPackets);
		packetHacker->unpack(fileName);
	}
	pcapClose(fp, logfile);

	delete pcapVariables;
	delete packetHacker;

	system("pause");
	return 0;
}

