/* zpipe.c: example of proper use of zlib's inflate() and deflate()
   Not copyrighted -- provided to the public domain
   Version 1.4  11 December 2005  Mark Adler */

/* Version history:
   1.0  30 Oct 2004  First version
   1.1   8 Nov 2004  Add void casting for unused return values
                     Use switch statement for inflate() return values
   1.2   9 Nov 2004  Add assertions to document zlib guarantees
   1.3   6 Apr 2005  Remove incorrect assertion in inf()
   1.4  11 Dec 2005  Add hack to avoid MSDOS end-of-line conversions
                     Avoid some compiler warnings for input and output buffers
 */

#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "zlib.h"


#if defined(MSDOS) || defined(OS2) || defined(WIN32) || defined(__CYGWIN__)
#  include <fcntl.h>
#  include <io.h>
#  define SET_BINARY_MODE(file) setmode(fileno(file), O_BINARY)
#else
#  define SET_BINARY_MODE(file)
#endif

#define CHUNK 16384*2

/* Compress from file source to file dest until EOF on source.
   def() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_STREAM_ERROR if an invalid compression
   level is supplied, Z_VERSION_ERROR if the version of zlib.h and the
   version of the library linked do not match, or Z_ERRNO if there is
   an error reading or writing the files. */
int def(FILE *source, int level, char** result, int *resultSize) //result to wskaznik na tablice z danymi, resultSize to rozmiar tej tablicy
{
    int ret, flush;
	long outputFileSize;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];
	FILE* dest = fopen("temp.dat", "wb+");
	if (!dest)
	{
		printf("Error occured! Cannot create temp.dat file!\n");
		*result = NULL;
		*resultSize = -1;
		return -1000;
	}

    /* allocate deflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    ret = deflateInit(&strm, level);
    if (ret != Z_OK)
        return ret;

    /* compress until end of file */
    do {
        strm.avail_in = fread(in, 1, CHUNK, source);
        if (ferror(source)) {
            (void)deflateEnd(&strm);
            return Z_ERRNO;
        }
        flush = feof(source) ? Z_FINISH : Z_NO_FLUSH;
        strm.next_in = in;

        /* run deflate() on input until output buffer not full, finish
           compression if all of source has been read in */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = deflate(&strm, flush);    /* no bad return value */
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            have = CHUNK - strm.avail_out;

            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)deflateEnd(&strm);
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);
        assert(strm.avail_in == 0);     /* all input will be used */

        /* done when last data in file processed */
    } while (flush != Z_FINISH);
    assert(ret == Z_STREAM_END);        /* stream will be complete */
	
	fseek(dest, 0L, SEEK_END);
	outputFileSize = ftell(dest);
	rewind(dest);

	*result = calloc(1, outputFileSize + 1);	
	if (fread(*result, outputFileSize, 1, dest) != 1)
	{
		printf("Error occured! Cannot read temp.dat file!\n");
		*result = NULL;
		*resultSize = -1;
	}
	else
	{
		*resultSize = outputFileSize;
	}
	
	fclose(dest);
	remove("temp.dat");

	fclose(dest);
	remove("temp.dat");

    /* clean up and return */
    (void)deflateEnd(&strm);
    return Z_OK;
}

/* Decompress from file source to file dest until stream ends or EOF.
   inf() returns Z_OK on success, Z_MEM_ERROR if memory could not be
   allocated for processing, Z_DATA_ERROR if the deflate data is
   invalid or incomplete, Z_VERSION_ERROR if the version of zlib.h and
   the version of the library linked do not match, or Z_ERRNO if there
   is an error reading or writing the files. */
int inf(char* data, FILE *dest, int size)
{
    int ret;
    unsigned have;
    z_stream strm;
    unsigned char in[CHUNK];
    unsigned char out[CHUNK];
	FILE *source = fopen("temp.dat", "wb+");
	if (fwrite(data, 1, size, source) != size)
	{
		printf("Error occured! Cannot write temp.dat file!\n");
		fclose(source);
		return -1000;
	}
	rewind(source);

    /* allocate inflate state */
    strm.zalloc = Z_NULL;
    strm.zfree = Z_NULL;
    strm.opaque = Z_NULL;
    strm.avail_in = 0;
    strm.next_in = Z_NULL;
	//ret = inflateInit2(&strm, -MAX_WBITS);
	ret = inflateInit(&strm);
    if (ret != Z_OK)
        return ret;

    /* decompress until deflate stream ends or end of file */
    do {
		strm.avail_in = fread(in, 1, CHUNK, source);
		if (ferror(source)) {
			(void)inflateEnd(&strm);
			fclose(source);
			remove("temp.dat");
			return Z_ERRNO;
		}

        if (strm.avail_in == 0)
            break;
        strm.next_in = in;

        /* run inflate() on input until output buffer not full */
        do {
            strm.avail_out = CHUNK;
            strm.next_out = out;
            ret = inflate(&strm, Z_NO_FLUSH);
            assert(ret != Z_STREAM_ERROR);  /* state not clobbered */
            switch (ret) {
            case Z_NEED_DICT:
                ret = Z_DATA_ERROR;     /* and fall through */
            case Z_DATA_ERROR:
            case Z_MEM_ERROR:
                (void)inflateEnd(&strm);
				fclose(source);
				remove("temp.dat");
                return ret;
            }
            have = CHUNK - strm.avail_out;
            if (fwrite(out, 1, have, dest) != have || ferror(dest)) {
                (void)inflateEnd(&strm);
				fclose(source);
				remove("temp.dat");
                return Z_ERRNO;
            }
        } while (strm.avail_out == 0);

        /* done when inflate() says it's done */
    } while (ret != Z_STREAM_END);

    /* clean up and return */
    (void)inflateEnd(&strm);
	fclose(source);
	remove("temp.dat");
    return ret == Z_STREAM_END ? Z_OK : Z_DATA_ERROR;
}

/* report a zlib or i/o error */
void zerr(int ret)
{
    fputs("zpipe: ", stderr);
    switch (ret) {
    case Z_ERRNO:
        if (ferror(stdin))
            fputs("error reading stdin\n", stderr);
        if (ferror(stdout))
            fputs("error writing stdout\n", stderr);
        break;
    case Z_STREAM_ERROR:
        fputs("invalid compression level\n", stderr);
        break;
    case Z_DATA_ERROR:
        fputs("invalid or incomplete deflate data\n", stderr);
        break;
    case Z_MEM_ERROR:
        fputs("out of memory\n", stderr);
        break;
    case Z_VERSION_ERROR:
        fputs("zlib version mismatch!\n", stderr);
    }
}


int compressFile(const char *fileName, char** data, int *dataSize)
{
	printf("Data compression module\n");
	FILE *file = fopen(fileName, "rb");
	*data = NULL;
	*dataSize = -1;
	if (file == NULL)
	{
		printf("Cannot open file %s for compression!\n", fileName);
		printf("End of data compression!\n");
		return -1;
	}

	int ret = def(file, Z_DEFAULT_COMPRESSION, data, dataSize);
	fclose(file);
	if (ret != Z_OK)
		zerr(ret);

	if (*dataSize < 0)
	{
		printf("File read error...\n");
		printf("End of data compression!\n");
		return -1;
	}

	printf("Data size: %d\n", *dataSize);
	printf("End of data compression!\n");

	return ret;
}

int decompressFile(const char *fileName, char* data, int *dataSize)
{
	printf("Data decompression module\n");
	FILE *file = fopen(fileName, "wb");
	if (file == NULL)
	{
		printf("Cannot open file %s for decompression!\n", fileName);
		printf("End of data decompression!\n");
		return -1;
	}

	int ret = inf(data, file, *dataSize);
	fclose(file);
	if (ret != Z_OK)
		zerr(ret);

	*dataSize = 0;
	printf("End of data decompression!\n");
	return ret;
}

void compressionTest2(const char* fileName)
{
	char* data = NULL;
	int dataSize;

	if (compressFile(fileName, &data, &dataSize))
	{
		printf("CompressionTest2 - compression failure\n");
		return;
	}
	else
	{
		if (decompressFile("wynik.txt", data, &dataSize))
		{
			printf("CompressionTest2 - decompression failure\n");
			return;
		}
	}
}

void compressionTest(const char* fileName)
{
	int ret;
	FILE *file = fopen(fileName, "rb");
	FILE *file2 = fopen("wynik.txt", "wb");

	if (file == NULL)
	{
		printf("Nie udalo sie otworzyc pliku %s\n", fileName);
	}
	char* data = NULL;
	int dataSize;

	ret = def(file, Z_DEFAULT_COMPRESSION, &data, &dataSize); //kompresja
	if (ret != Z_OK)
		zerr(ret);
	fclose(file);

	if (dataSize < 0)
	{
		printf("Blad odczytu...\n");
		return;
	}

	printf("Rozmiar danych: %d\n", dataSize);

	ret = inf(data, file2, dataSize);
	if (ret != Z_OK)
		zerr(ret);
	fclose(file2);

	if (data != NULL)
		free(data);

}

