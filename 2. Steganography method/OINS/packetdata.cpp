#include <cstdlib>
#include <cstring>
#include <WinSock2.h>
#include "packetdata.h"

PacketData::~PacketData()
{
	if (tcp != NULL)
		delete tcp;
}

PacketData* PacketData::load(uint8_t* data, uint16_t size)
{
	if (size >= sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader))
	{
		PacketData* result = new PacketData();
		result->eth = etherHeader::load(data);

		if (result->eth.llc_len == 0x0800) //contains IP
		{
			result->ip = ipHeader::load(data + sizeof(etherHeader));
			uint8_t ihl = result->ip.ihl();
			uint8_t ver = result->ip.ver_ihl & 0xF0;
			size_t s = result->ip.size();
			if (result->ip.protocol == 6) //contains TCP
			{
				result->tcp = TcpPacket::load(data + sizeof(etherHeader) + sizeof(ipHeader), size - sizeof(etherHeader) - sizeof(ipHeader));
				return result;
			}
			else
			{
				delete result;
				return NULL;
			}

		}
		else
		{
			delete result;
			return NULL;
		}
	}
	else
		return NULL;
}

uint8_t* PacketData::craft(PacketData *data, int *size)
{
	PacketData prepareData;
	memcpy(&prepareData.eth, &data->eth, sizeof(etherHeader));
	memcpy(&prepareData.ip, &data->ip, sizeof(ipHeader));
	prepareData.tcp = new TcpPacket(*data->tcp);

	prepareData.eth.llc_len = htons(prepareData.eth.llc_len);

	prepareData.ip.total_length = ntohs(prepareData.ip.total_length);
	prepareData.ip.id = ntohs(prepareData.ip.id);
	prepareData.ip.flags_fo = htons(prepareData.ip.flags_fo);
	prepareData.ip.checksum = htons(prepareData.ip.checksum);
	prepareData.ip.src_addr = htonl(prepareData.ip.src_addr);
	prepareData.ip.dst_addr = htonl(prepareData.ip.dst_addr);

	prepareData.tcp->header.src_port = htons(prepareData.tcp->header.src_port);
	prepareData.tcp->header.dst_port = htons(prepareData.tcp->header.dst_port);
	prepareData.tcp->header.seq = htonl(prepareData.tcp->header.seq);
	prepareData.tcp->header.ack = htonl(prepareData.tcp->header.ack);
	prepareData.tcp->header.window_size = htons(prepareData.tcp->header.window_size);
	prepareData.tcp->header.checksum = htons(prepareData.tcp->header.checksum);
	prepareData.tcp->header.urgent_p = htons(prepareData.tcp->header.urgent_p);

	*size = sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader) + data->tcp->optionsSize + data->tcp->dataSize;
	uint8_t* result = (uint8_t*)calloc(*size, sizeof(uint8_t));
	memcpy(result, &prepareData.eth, sizeof(etherHeader));
	memcpy(result + sizeof(etherHeader), &prepareData.ip, sizeof(ipHeader));
	memcpy(result + sizeof(etherHeader) + sizeof(ipHeader), &prepareData.tcp->header, sizeof(tcpHeader));
	memcpy(result + sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader), prepareData.tcp->options, prepareData.tcp->optionsSize);
	memcpy(result + sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader) + prepareData.tcp->optionsSize, prepareData.tcp->data, prepareData.tcp->dataSize);

	return result;
}
