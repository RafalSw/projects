#include <ctime>
#include <iostream>
#include <queue>
//#include <windows.h>
#include "pcap.h"
#include "packetdata.h"
#include "packethacker.h"
#include "variables.h"

using namespace std;

extern "C"
{
	void compressionTest(const char* fileName);
	void compressionTest2(const char* fileName);
}


int initPcap(pcap_t **fp, FILE **logfile, int snaplen);
int capturingTCPpackets(Variables* pcapVariables, queue<PacketData*> &packetQueue, pcap_t *fp, FILE *logfile);
int sendTCPPacket(pcap_t *fp, uint8_t *data, int size);
int pcapClose(pcap_t *fp, FILE *logfile);
bool netTest();

unsigned int mean(unsigned int *timestamps, int size)
{
	unsigned int result = 0;
	for (int i = 1; i < size; i++)
		result += (timestamps[i]-timestamps[i-1]);
	return (result / size);
}


int main(int argc, char* argv[]){ 
	unsigned int lastSend[5];
	int lastSendSize = 0;
	const int SENDING_MULTIP = 50;
	//Przechwytywanie ruchu sieciowego oraz dodawanie pakiet�w do kolejki
	
	//unsigned char mac_address[6] = { 0x00, 0xc3, 0x51, 0x78, 0x0d, 0x34 }; // adres mac komputera docelowego w wesji hexadecymalnej
	unsigned char mac_address[6] = { 0xBC, 0x5F, 0xF4, 0xC7, 0xF4, 0xA5 };
	//unsigned char mac_address[6] = { 0x00, 0x1e, 0x65, 0xf4, 0x95, 0x82 };

	string fileName = "../Sofokles - Antygona.txt";
	Variables* pcapVariables = new Variables();
	PacketHacker *packetHacker = new PacketHacker();
	srand(time(NULL));

	pcapVariables->key = packetHacker->getKey();
	pcapVariables->keySize = packetHacker->getKeySize();
	//pcapVariables->N_start = sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader) + 2*sizeof(hackedHeader); // minimalna d�ugo�� pakietu
	pcapVariables->N_start = 1000;
	pcapVariables->N_stop = 2000;// maksymalna d�ugo�� pakietu
	pcapVariables->mac_address = (unsigned char*)&mac_address;
	pcapVariables->port_start = 0; // Port pocz�tkowy przechwytywania [standardowo: 1 - 65535]
	pcapVariables->port_stop = 65300; // Port ko�cowy przechwytywania [standardowo: 1 - 65535]
	pcapVariables->packetNo = 0;
	pcapVariables->packetMod = 15; //srednio co piaty pakiet jest oszukany
	pcapVariables->packetVariance = 3; //wariancja oszukiwanych pakietow (+/- 3 wzgledem 5)
	pcapVariables->nextPacket = 0;

	packetHacker->pack(fileName);
	
	//Kolejka
	queue<PacketData*> packetQueue;
	queue<PacketData*> sendQueue;

	pcap_t *fp;
	FILE *logfile;
	int captureResult = -2;
	int sendPacketSize = -1;
	int i;
	unsigned int nextSendingAt = 0;

	if (initPcap(&fp, &logfile, pcapVariables->N_stop) != 0)
	{
		cout << "Nie udalo sie uzyskac dostepu do interfejsu, koniec dzialania programu!" << endl;
		return -1;
	}
	
	while (packetHacker->isFinished())
	{
		captureResult = capturingTCPpackets(pcapVariables, packetQueue, fp, logfile);
		
		if (captureResult == 1)
		{
			if (lastSendSize < (sizeof(lastSend) / sizeof(*lastSend)))
			{
				lastSend[lastSendSize] = GetTickCount();
				lastSendSize++;
			}
			else
			{
				for (i = 0; i < (sizeof(lastSend) / sizeof(*lastSend)) - 1; i++)
				{
					lastSend[i] = lastSend[i + 1];
				}
				lastSend[i] = GetTickCount();
			}
		}
		else if (captureResult == -1 || captureResult == 0) //timeout lub brak wlasciwych danych
			continue;
		else if (captureResult == -2)
		{
			//blad
			break;
		}

		//zmiana zawartosci
		if (!packetQueue.empty())
		{
			PacketData* hackedData = packetQueue.front();
			packetQueue.pop();
			packetHacker->hackPacket(hackedData, pcapVariables);
			sendQueue.push(hackedData);
		}

		//wysylanie
		if (!sendQueue.empty() && GetTickCount()>nextSendingAt)
		{
			PacketData* sendData = sendQueue.front();
			uint8_t *data = PacketData::craft(sendData, &sendPacketSize);
			if (sendTCPPacket(fp, data, sendPacketSize) == 0)
			{
				nextSendingAt = GetTickCount() + SENDING_MULTIP*mean(lastSend, lastSendSize);
				cout << "Wysylanie bonusowego o " << GetTickCount() << " nastepne o " << nextSendingAt << endl;
				sendQueue.pop();
				delete sendData;
			}
		}
	}
	//dosylanie tego co zostalo
	while (!sendQueue.empty())
	{
		captureResult = capturingTCPpackets(pcapVariables, packetQueue, fp, logfile);

		if (captureResult == 1)
		{
			if (lastSendSize < (sizeof(lastSend) / sizeof(*lastSend)))
			{
				lastSend[lastSendSize] = GetTickCount();
				lastSendSize++;
			}
			else
			{
				for (i = 0; i < (sizeof(lastSend) / sizeof(*lastSend)) - 1; i++)
				{
					lastSend[i] = lastSend[i + 1];
				}
				lastSend[i] = GetTickCount();
			}
		}
		else if (captureResult == -1 || captureResult == 0) //timeout lub brak wlasciwych danych
		{
			Sleep(10);
			continue;
		}
		else if (captureResult == -2)
		{
			//blad
			break;
		}

		if (GetTickCount()>nextSendingAt)
		{
			PacketData* sendData = sendQueue.front();
			uint8_t *data = PacketData::craft(sendData, &sendPacketSize);
			if (sendTCPPacket(fp, data, sendPacketSize) == 0)
			{
				nextSendingAt = GetTickCount() + SENDING_MULTIP*mean(lastSend, lastSendSize);
				cout << "Wysylanie bonusowego o " << GetTickCount() << " nastepne o " << nextSendingAt << endl;
				sendQueue.pop();
				delete sendData;
			}
			Sleep(10);
		}
		else
		{
			Sleep(5);//to ryzykowne zagranie!
		}
	}

	pcapClose(fp, logfile);

	delete pcapVariables;
	delete packetHacker;

	system("pause");
	return 0;
}

