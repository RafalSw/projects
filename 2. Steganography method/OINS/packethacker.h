#ifndef PACKETHACKER_H
#define PACKETHACKER_H
#include <string>
#include "PacketData.h"
#include "variables.h"

using namespace std;

struct hackedHeader
{
	uint8_t uniqueKey[8];
	uint16_t packetNumber;
	uint16_t length;
};

class PacketHacker
{
private:
	string fileNameStr;
	int currentPosition;
	char *data;
	int dataSize;
	uint16_t packetNumber;
	uint8_t *key;
	int keySize;

public:
	
	PacketHacker();
	~PacketHacker();
	int pack(string fileName);
	int unpack(string fileName);
	void hackPacket(PacketData *packet, Variables* pcapVariables);
	void unhackPacket(vector<PacketData*> packets);
	inline bool isFinished()
	{
		return dataSize != currentPosition;
	}
	inline const uint8_t* getKey()
	{
		return key;
	}
	inline int getKeySize()
	{
		return keySize;
	}
};

#endif //PACKETHACKER_H