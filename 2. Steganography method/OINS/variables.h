#ifndef VARIABLES_H
#define VARIABLES_H
#include <cstdint>
#include "pcap.h"

class Variables{

public:
	const uint8_t* key;
	uint8_t keySize;
	int res;
	char buffer[100];
	u_char *pkt_data;
	time_t seconds;
	struct tm tbreak;
	struct pcap_pkthdr *header;
	unsigned char *mac_address;
	unsigned int port_start;
	unsigned int port_stop;
	unsigned int N_start;
	unsigned int N_stop;
	int packetNo;
	int packetMod;
	int packetVariance;
	int nextPacket;
	int lastSeq;
};
#endif //VARIABLES_H
