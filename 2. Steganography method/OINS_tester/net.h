#ifndef TCPPACKET_H
#define TCPPACKET_H

#include <cstdint>

#pragma pack(push, 1)

struct etherHeader
{
	uint8_t  dst_addr[6];
	uint8_t  src_addr[6];
	uint16_t llc_len;
	static etherHeader load(uint8_t* data);
};

struct ipHeader
{
	uint8_t ver_ihl;  // 4 bits version and 4 bits internet header length
	uint8_t tos;
	uint16_t total_length;
	uint16_t id;
	uint16_t flags_fo; // 3 bits flags and 13 bits fragment-offset
	uint8_t ttl;
	uint8_t protocol;
	uint16_t checksum;
	uint32_t src_addr;
	uint32_t dst_addr;

	uint8_t ihl() const;
	size_t size() const;
	static ipHeader load(uint8_t* data);
};

struct tcpHeader
{
	uint16_t src_port;
	uint16_t dst_port;
	uint32_t seq;
	uint32_t ack;
	uint8_t  data_offset;  // 4 bits
	uint8_t  flags;
	uint16_t window_size;
	uint16_t checksum;
	uint16_t urgent_p;
};

struct pseudoTcpHeader
{
	uint32_t ip_src;
	uint32_t ip_dst;
	uint8_t zero;//always zero
	uint8_t protocol;// = 6;//for tcp
	uint16_t tcp_len;
	tcpHeader tcph;
};

class TcpPacket
{
public:
	tcpHeader header;
	//tcpOption option;
	uint8_t* options;
	uint8_t optionsSize;
	uint8_t* data;
	uint16_t dataSize;
	TcpPacket();
	TcpPacket(TcpPacket &packet);
	~TcpPacket();
	static TcpPacket* load(uint8_t* data, uint16_t len);
};

#pragma pack(pop)

#endif //TCPPACKET_H