#include <Winsock2.h>
#include "net.h"


etherHeader etherHeader::load(uint8_t* data)
{
	etherHeader header;
	memcpy(&header, data, sizeof(etherHeader));
	header.llc_len = ntohs(header.llc_len);
	return header;
}

uint8_t ipHeader::ihl() const
{
	return (ver_ihl & 0x0F);
}

size_t ipHeader::size() const
{
	return ihl() * sizeof(uint32_t);
}

ipHeader ipHeader::load(uint8_t* data)
{
	ipHeader header;
	memcpy(&header, data, sizeof(ipHeader));
	header.total_length = ntohs(header.total_length);
	header.id = ntohs(header.id);
	header.flags_fo = ntohs(header.flags_fo);
	header.checksum = ntohs(header.checksum);
	header.src_addr = ntohl(header.src_addr);
	header.dst_addr = ntohl(header.dst_addr);
	return header;
}
TcpPacket::TcpPacket()
{
	data = NULL;
	options = NULL;
	optionsSize = 0;
}

TcpPacket::TcpPacket(TcpPacket &packet)
{
	memcpy(&this->header, &packet.header, sizeof(tcpHeader));
	if (packet.dataSize > 0)
	{
		memcpy(&this->options, &packet.options, packet.optionsSize);
		dataSize = packet.dataSize;
	}
	else
	{
		dataSize = 0;
		options = NULL;
		optionsSize = 0;
	}
	if (dataSize > 0)
	{
		data = new uint8_t[dataSize];
		memcpy(data, packet.data, dataSize);
	}
	else
		data = NULL;
}

TcpPacket::~TcpPacket()
{
	if (data != NULL)
		delete data;
	if (options != NULL)
		delete options;
}

TcpPacket* TcpPacket::load(uint8_t* data, uint16_t len)
{
	TcpPacket *packet = new TcpPacket();
	memcpy(&(packet->header), data, sizeof(tcpHeader));
	packet->header.src_port = ntohs(packet->header.src_port);
	packet->header.dst_port = ntohs(packet->header.dst_port);
	packet->header.seq = ntohl(packet->header.seq);
	packet->header.ack = ntohl(packet->header.ack);
	packet->header.window_size = ntohs(packet->header.window_size);
	packet->header.checksum = ntohs(packet->header.checksum);
	packet->header.urgent_p = ntohs(packet->header.urgent_p);

	if (packet->header.data_offset / 4 > len)
	{
		memcpy(&(packet->options), data + (sizeof(tcpHeader)), packet->header.data_offset / 4 - sizeof(tcpHeader));
		packet->optionsSize = packet->header.data_offset / 4 - sizeof(tcpHeader);
	}
	
	packet->dataSize = max(0, len - packet->header.data_offset / 4);
	if (packet->dataSize > 0)
	{
		packet->data = new uint8_t[packet->dataSize];
		memcpy(packet->data, data + packet->header.data_offset / 4, packet->dataSize);
	}
	return packet;
}