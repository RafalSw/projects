#include <algorithm>
#include <iostream>
#include <vector>
#include <WinSock2.h>
#include "packethacker.h"

extern "C"
{
	int compressFile(const char *fileName, char** data, int *dataSize); //zpipe.c
	int decompressFile(const char *fileName, char* data, int *dataSize); //zpipe.c
}

using namespace std;


uint16_t TCPcheckSum(uint16_t *buffer, int size)
{
	unsigned long cksum = 0;
	while (size >1)
	{
		cksum += *buffer++;
		size -= sizeof(uint16_t);
	}
	if (size)
		cksum += *(uint8_t*)buffer;

	cksum = (cksum >> 16) + (cksum & 0xffff);
	cksum += (cksum >> 16);
	return (uint16_t)(~cksum);
}

PacketHacker::PacketHacker()
{
	uint8_t tab[] = { 0x1c, 0xd5, 0x62, 0x2f, 0x02, 0x54, 0xcb, 0x0a };
	keySize = sizeof(tab) / (sizeof(*tab));
	key = new uint8_t[keySize];
	memcpy(key, tab, keySize);
	fileNameStr = "";
	currentPosition = 0;
	packetNumber = 1;
}

PacketHacker::~PacketHacker()
{
	if (data != NULL && dataSize>0)
		delete data;
	delete key;
}

int PacketHacker::pack(string fileName)
{
	if (fileName.empty())
	{
		cout << "File name is empty!" << endl;
		return -1;
	}

	if (compressFile(fileName.c_str(), &data, &dataSize) == 0)
		return 0;
	else
		return -1;
}

int PacketHacker::unpack(string fileName)
{
	if (fileName.empty())
	{
		cout << "File name is empty!" << endl;
		return -1;
	}
	if (data == NULL || dataSize==0)
	{
		cout << "No data to unpack!" << endl;
		return -1;
	}

	if (decompressFile(fileName.c_str(), data, &dataSize) == 0)
		return 0;
	else
		return -1;
}


long checksum(unsigned short *addr, unsigned int count) {
	/* Compute Internet Checksum for "count" bytes
	*         beginning at location "addr".
	*/
	register long sum = 0;


	while (count > 1)  {
		/*  This is the inner loop */
		sum += *addr++;
		count -= 2;
	}
	/*  Add left-over byte, if any */
	if (count > 0)
		sum += *(unsigned char *)addr;

	/*  Fold 32-bit sum to 16 bits */
	while (sum >> 16)
		sum = (sum & 0xffff) + (sum >> 16);

	return ~sum;
}

void PacketHacker::hackPacket(PacketData *packet, Variables* pcapVariables)
{
	if (packet->tcp->dataSize > sizeof(hackedHeader))
	{
		int availableSize = packet->tcp->dataSize - sizeof(hackedHeader);
		int currentDataSize = min(availableSize, dataSize - currentPosition);
		hackedHeader *currentHeader = new hackedHeader();
		memcpy(currentHeader->uniqueKey, key, keySize);

		if (currentDataSize != availableSize)//doszlismy do konca wiadomosci, informujemy o tym dajac max shorta
			currentHeader->packetNumber = htons(0x7FFF);
		else
		{
			currentHeader->packetNumber = htons(packetNumber);
			packetNumber++;
		}
		
		currentHeader->length = htons(currentDataSize);
		memcpy(packet->tcp->data, currentHeader, sizeof(hackedHeader));
		delete currentHeader;

		memcpy(packet->tcp->data + sizeof(hackedHeader), data + currentPosition, currentDataSize);
		currentPosition += currentDataSize;
		packet->tcp->header.seq = pcapVariables->lastSeq + 10000;


		pseudoTcpHeader *pseudoHeader = new pseudoTcpHeader();
		pseudoHeader->ip_dst = htonl(packet->ip.dst_addr);
		pseudoHeader->ip_src = htonl(packet->ip.src_addr);
		pseudoHeader->zero = 0x00;
		pseudoHeader->protocol = 6;
		pseudoHeader->tcp_len = htons((packet->tcp->header.data_offset) / 4 + packet->tcp->dataSize);
		memcpy(&(pseudoHeader->tcph), &(packet->tcp->header), sizeof(tcpHeader));
		pseudoHeader->tcph.checksum = 0x0000;
		pseudoHeader->tcph.src_port = htons(pseudoHeader->tcph.src_port);
		pseudoHeader->tcph.dst_port = htons(pseudoHeader->tcph.dst_port);
		pseudoHeader->tcph.seq = htonl(pseudoHeader->tcph.seq);
		pseudoHeader->tcph.ack = htonl(pseudoHeader->tcph.ack);
		pseudoHeader->tcph.window_size = htons(pseudoHeader->tcph.window_size);
		pseudoHeader->tcph.urgent_p = htons(pseudoHeader->tcph.urgent_p);

		packet->tcp->header.checksum = TCPcheckSum((uint16_t*)pseudoHeader, sizeof(pseudoTcpHeader));
		delete pseudoHeader;
	}
}

bool sortPackets(PacketData *p1, PacketData *p2)
{
	return ntohs(*(u_short*)(p1->tcp->data + sizeof(hackedHeader) - 4)) < ntohs(*(u_short*)(p2->tcp->data + sizeof(hackedHeader) - 4));
}

void PacketHacker::unhackPacket(vector<PacketData*> packets)
{
	sort(packets.begin(), packets.end(), sortPackets);

	dataSize = 0;
	for (size_t i = 0; i < packets.size(); i++)
	{
		dataSize += ntohs(*(u_short*)(packets.at(i)->tcp->data + sizeof(hackedHeader) - 2));
	}
	currentPosition = 0;
	data = new char[dataSize];
	int lengthOfFragment = 0;

	for (size_t i = 0; i < packets.size(); i++)
	{
		lengthOfFragment = ntohs(*(u_short*)(packets.at(i)->tcp->data + sizeof(hackedHeader) - 2));
		if (lengthOfFragment > 0)
		{
			memcpy(data + currentPosition, packets.at(i)->tcp->data + sizeof(hackedHeader), lengthOfFragment);
			currentPosition += lengthOfFragment;
		}
	}
}