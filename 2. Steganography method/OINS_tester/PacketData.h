#ifndef PACKETDATA_H
#define PACKETDATA_H
#include "net.h"

class PacketData
{
public:
	etherHeader eth;
	ipHeader ip;
	TcpPacket *tcp;
	~PacketData();

	static PacketData *load(uint8_t* data, uint16_t size); //wczytuje dane na podstawie wskaznika na caly pakiet (wlacznie z ethernetem). size to rozmiar odebranych danych
	static uint8_t *craft(PacketData *data, int *size);
};

#endif //PACKETDATA_H