#include <iostream>
#include <vector>
#include "pcap.h"
#include "../OINS/packetdata.h"
#include "../OINS/packethacker.h"
#include "../OINS/variables.h"

using namespace std;

extern "C"
{
	void compressionTest(const char* fileName);
	void compressionTest2(const char* fileName);
}


int initPcap(pcap_t **fp, FILE **logfile, int snaplen);
int capturingTCPpackets2(Variables* pcapVariables, vector<PacketData*> &packets, pcap_t *fp);
int sendTCPPacket(pcap_t *fp, uint8_t *data, int size);
int pcapClose(pcap_t *fp, FILE *logfile);
bool netTest();


int main(int argc, char* argv[]){

	string fileName = "Sofokles - Antygona.txt";
	Variables* pcapVariables = new Variables();
	PacketHacker *packetHacker = new PacketHacker();
	int loop = 1000000;

	pcapVariables->key = packetHacker->getKey();
	pcapVariables->keySize = packetHacker->getKeySize();
	pcapVariables->N_start = sizeof(etherHeader) + sizeof(ipHeader) + sizeof(tcpHeader) + sizeof(hackedHeader); // minimalna d�ugo�� pakietu
	pcapVariables->N_stop = 2000;// maksymalna d�ugo�� pakietu
	pcapVariables->port_start = 0; // Port pocz�tkowy przechwytywania [standardowo: 1 - 65535]
	pcapVariables->port_stop = 65300; // Port ko�cowy przechwytywania [standardowo: 1 - 65535]

	//Kolejka
	vector<PacketData*> receivedPackets;

	pcap_t *fp;
	int captureResult = -2;
	int sendPacketSize = -1;

	char errbuf[PCAP_ERRBUF_SIZE];
	char source[PCAP_BUF_SIZE];

	if (argc != 2){

		printf("usage: %s filename", argv[0]);
		return -1;

	}

	/* Create the source string according to the new WinPcap syntax */
	if (pcap_createsrcstr(source,         // variable that will keep the source string
		PCAP_SRC_FILE,  // we want to open a file
		NULL,           // remote host
		NULL,           // port on the remote host
		argv[1],        // name of the file we want to open
		errbuf          // error buffer
		) != 0)
	{
		fprintf(stderr, "\nError creating a source string\n");
		return -1;
	}

	/* Open the capture file */
	if ((fp = pcap_open(source,         // name of the device
		65536,          // portion of the packet to capture
		// 65536 guarantees that the whole packet will be captured on all the link layers
		PCAP_OPENFLAG_PROMISCUOUS,     // promiscuous mode
		1000,              // read timeout
		NULL,              // authentication on the remote machine
		errbuf         // error buffer
		)) == NULL)
	{
		fprintf(stderr, "\nUnable to open the file %s.\n", source);
		return -1;
	}


	while (loop--) //petla glowna, tu bedzie inny warunek
	{
		captureResult = capturingTCPpackets2(pcapVariables, receivedPackets, fp);

		if (captureResult == -1 || captureResult == 0) //timeout lub brak wlasciwych danych
			continue;
		else if (captureResult == -2)
		{
			//blad
			break;
		}
		else if (captureResult == 2) //dotarlismy do konca pakietu
			break;
	}

	//odczytanie uzyskanych pakietow
	if (captureResult == 2)
	{
		packetHacker->unhackPacket(receivedPackets);
		packetHacker->unpack(fileName);
		cout << "Plik zawiera dodatkowa transmisje, ktora zostala zapisana jako " << fileName << endl;
	}
	else
	{
		cout << "Plik nie zawiera dodatkowej transmisji" << endl;
	}

	delete pcapVariables;
	delete packetHacker;

	system("pause");
	return 0;
}

