#define _CRT_SECURE_NO_WARNINGS
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define BUFFER_SIZE 8

int main(int argc, char **argv)
{
	const char key[] = { 0x1c, 0xd5, 0x62, 0x2f, 0x02, 0x54, 0xcb, 0x0a };
	int size = sizeof(key) / sizeof(*key);
	char* buffer[BUFFER_SIZE];
	int currentBufferSize = 0;

	if (argc != 2)
	{
		printf("Sposob uzycia: %s <nazwaPliku>\n", argv[0]);
		return -1;
	}

	FILE* file = fopen(argv[1], "rb");
	char c = fgetc(file);
	int i;
	while (c != EOF)
	{
		if (currentBufferSize < BUFFER_SIZE)
		{
			buffer[currentBufferSize] = c;
			currentBufferSize++;
		}
		else
		{
			for (i = 0; i < BUFFER_SIZE - 1; i++)
				buffer[i] = buffer[i + 1];
			buffer[BUFFER_SIZE - 1] = c;

			if (memcmp(buffer, key, BUFFER_SIZE) == 0)
			{
				printf("Plik zawiera ukryta transmisje.\n");
				system("PAUSE");
				return 0;
			}
		}
		c = fgetc(file);
	}
	printf("Plik zawiera nie zawiera transmisji.\n");
	system("PAUSE");
	return 0;
}