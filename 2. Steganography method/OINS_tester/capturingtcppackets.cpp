#include <cstdio>
#include <queue>
#include <Winsock2.h>   //need winsock for inet_ntoa and ntohs methods
#include "packetdata.h"
#include "packethacker.h"
#include "pcap.h"
#include "variables.h"

using namespace std;

int processPacket(PacketData* packet, Variables* pcapVariables, queue<PacketData*> &packetQueue);
int processPacket2(PacketData* packet, Variables* pcapVariables, vector<PacketData*> &packets);

int initPcap(pcap_t **fp, FILE **logfile, int snaplen)
{
	pcap_if_t *alldevs, *d;
	char errbuf[PCAP_ERRBUF_SIZE];
	int i, inum;

	fopen_s(logfile, "log_tcp_mac_port_3000_N_1500_1600_123.txt", "w");

	if (logfile == NULL)
	{
		printf("Unable to create file.");
	}

	/* The user didn't provide a packet source: Retrieve the local device list */
	if (pcap_findalldevs_ex(PCAP_SRC_IF_STRING, NULL, &alldevs, errbuf) == -1)
	{
		fprintf(stderr, "Error in pcap_findalldevs_ex: %s\n", errbuf);
		return -1;
	}

	/* Print the list */
	for (i = 0, d = alldevs; d; d = d->next)
	{
		printf("%d. %s\n    ", ++i, d->name);

		if (d->description)
		{
			printf(" (%s)\n", d->description);
		}
		else
		{
			printf(" (No description available)\n");
		}
	}

	if (i == 0)
	{
		fprintf(stderr, "No interfaces found! Exiting.\n");
		return -1;
	}

	printf("Enter the interface number you would like to sniff : ");
	scanf_s("%d", &inum);


	/* Jump to the selected adapter */
	for (d = alldevs, i = 0; i < inum - 1; d = d->next, i++);

	/* Open the device */
	if ((*fp = pcap_open(d->name,
		snaplen /*snaplen*/,
		PCAP_OPENFLAG_PROMISCUOUS /*flags*/,
		20 /*read timeout*/,
		NULL /* remote authentication */,
		errbuf)
		) == NULL)
	{
		fprintf(stderr, "\nError opening adapter\n");
		return -1;
	}

	return 0;
}

int pcapClose(pcap_t *fp, FILE *logfile)
{
	pcap_close(fp);
	fclose(logfile);
	return 0;
}

int capturingTCPpackets( Variables* pcapVariables, queue<PacketData*> &packetQueue, pcap_t *fp, FILE *logfile)
{
	pcapVariables->res = pcap_next_ex(fp, &pcapVariables->header, &pcapVariables->pkt_data);

	if (pcapVariables->res == 0)
	{
		// Timeout elapsed
		return -1;
	}
	else if (pcapVariables->res == -1)
	{
		fprintf(stderr, "Error reading the packets: %s\n", pcap_geterr(fp));
		return -2;
	}
	//else:

	pcapVariables->seconds = pcapVariables->header->ts.tv_sec;
	localtime_s(&pcapVariables->tbreak, &pcapVariables->seconds);
	strftime(pcapVariables->buffer, 80, "%d-%b-%Y %I:%M:%S %p", &pcapVariables->tbreak); //print pkt timestamp and pkt len
	
	if (((pcapVariables->header->len) >= pcapVariables->N_start) && ((pcapVariables->header->len) <= pcapVariables->N_stop))
	{
		fprintf(logfile, "\nNext Packet : %s.%ld (Packet Length : %ld bytes) ", pcapVariables->buffer, pcapVariables->header->ts.tv_usec, pcapVariables->header->len);
		PacketData *packetData = PacketData::load(pcapVariables->pkt_data, pcapVariables->header->caplen);
		if (packetData != NULL)
		{
			return processPacket(packetData, pcapVariables, packetQueue);
		}
	}

	return 0;
}

int capturingTCPpackets2( Variables* pcapVariables, vector<PacketData*> &packets, pcap_t *fp)
{
	pcapVariables->res = pcap_next_ex(fp, &pcapVariables->header, &pcapVariables->pkt_data);

	if (pcapVariables->res == 0)
	{
		// Timeout elapsed
		return -1;
	}
	else if (pcapVariables->res == -1)
	{
		fprintf(stderr, "Error reading the packets: %s\n", pcap_geterr(fp));
		return -2;
	}
	//else:

	if (((pcapVariables->header->len) >= pcapVariables->N_start) && ((pcapVariables->header->len) <= pcapVariables->N_stop))
	{
		PacketData *packetData = PacketData::load(pcapVariables->pkt_data, pcapVariables->header->caplen);
		if (packetData != NULL)
		{
			return processPacket2(packetData, pcapVariables, packets);
		}
	}

	return 0;
}

//zwraca 1 jesli udalo sie cos dodac do kolejki
int processPacket(PacketData* packet, Variables* pcapVariables, queue<PacketData*> &packetQueue)
{
	
	if (packet->tcp->header.src_port >= pcapVariables->port_start &&
		packet->tcp->header.src_port <= pcapVariables->port_stop &&
		pcapVariables->mac_address[0] == packet->eth.dst_addr[0] &&
		pcapVariables->mac_address[1] == packet->eth.dst_addr[1] &&
		pcapVariables->mac_address[2] == packet->eth.dst_addr[2] &&
		pcapVariables->mac_address[3] == packet->eth.dst_addr[3] &&
		pcapVariables->mac_address[4] == packet->eth.dst_addr[4] &&
		pcapVariables->mac_address[5] == packet->eth.dst_addr[5])
	{ // filtering by mac
		pcapVariables->lastSeq = packet->tcp->header.seq;
		if (pcapVariables->nextPacket == pcapVariables->packetNo++)
		{
			printf("%d -> %d !\n", packet->tcp->header.src_port, packet->tcp->header.dst_port);
			pcapVariables->nextPacket = pcapVariables->packetNo - 1 + pcapVariables->packetMod + rand() % (2 * pcapVariables->packetVariance) - pcapVariables->packetVariance;
			packetQueue.push(packet);
			return 1;
		}
		else
		{
			printf("%d -> %d\n", packet->tcp->header.src_port, packet->tcp->header.dst_port);
			return 1;
		}
	}
	else
	{
		delete packet; //unikanie wyciekow pamieci
	}
	
	return 0;
}

int processPacket2(PacketData* packet, Variables* pcapVariables, vector<PacketData*> &packets)
{
	
	if (packet->tcp->dataSize > sizeof(hackedHeader) && memcmp(packet->tcp->data, pcapVariables->key, pcapVariables->keySize)==0) //znaleziono klucz na poczatku danych
	{
		if (ntohs(*(u_short*)(packet->tcp->data + sizeof(hackedHeader) - 4)) == 0x7FFF)
		{
			packets.push_back(packet);
			return 2;
		}
		else
		{
			packets.push_back(packet);
			return 1;
		}
	}
	else
	{
		delete packet;
	}
	return 0;
}


int sendTCPPacket(pcap_t *fp, uint8_t *data, int size)
{
	if (data != NULL)
	{
		if (pcap_sendpacket(fp, data, size) != 0)
		{
			fprintf(stderr, "\nError sending the packet: \n", pcap_geterr(fp));
			free(data);
			return -1;
		}
		free(data);
		return 0;
	}
	return -1;
}
