--
-- DUMP FILE
--
-- Database is ported from MS Access
--------------------------------------------------------------------
-- Created using "MS Access to MSSQL" form http://www.bullzip.com
-- Program Version 5.3.259
--
-- OPTIONS:
--   sourcefilename=C:\Users\Rafal\Documents\Car showroom.mdb
--   sourceusername=
--   sourcepassword=
--   sourcesystemdatabase=
--   destinationserver=DOM-RAFAL\SQLEXPRESS
--   destinationauthentication=Windows
--   destinationdatabase=movedb
--   dropdatabase=0
--   createtables=1
--   unicode=1
--   autocommit=1
--   transferdefaultvalues=1
--   transferindexes=1
--   transferautonumbers=1
--   transferrecords=1
--   columnlist=1
--   tableprefix=
--   negativeboolean=0
--   ignorelargeblobs=0
--   memotype=VARCHAR(MAX)
--

IF NOT EXISTS (SELECT * FROM master.dbo.sysdatabases WHERE name = N'movedb') CREATE DATABASE [movedb]
USE [movedb]

--
-- Table structure for table 'Car'
--

IF object_id(N'Car', 'U') IS NOT NULL DROP TABLE [Car]

CREATE TABLE [Car] (
  [ID_car] INT NOT NULL IDENTITY, 
  [Price] MONEY NOT NULL DEFAULT 0, 
  [Version] NVARCHAR(255), 
  [Description] NVARCHAR(MAX), 
  [Make] NVARCHAR(255), 
  [Model] NVARCHAR(255), 
  [Warranty] INT DEFAULT 0, 
  [Year_of_manufacture] INT DEFAULT 0, 
  [Color] NVARCHAR(255), 
  [ID_manufacturer] INT NOT NULL DEFAULT 0, 
  PRIMARY KEY ([ID_car])
)

SET IDENTITY_INSERT [Car] ON
GO

--
-- Dumping data for table 'Car'
--

-- 0 records

SET IDENTITY_INSERT [Car] OFF
GO

CREATE INDEX [ID_car] ON [Car] ([ID_car])
GO

CREATE INDEX [ID_manufacturer] ON [Car] ([ID_manufacturer])
GO

--
-- Table structure for table 'Customer'
--

IF object_id(N'Customer', 'U') IS NOT NULL DROP TABLE [Customer]

CREATE TABLE [Customer] (
  [ID_customer] INT NOT NULL IDENTITY, 
  [Last_name] NVARCHAR(255) NOT NULL, 
  [First_name] NVARCHAR(255) NOT NULL, 
  [Mobile] NVARCHAR(255) NOT NULL, 
  [Email] NVARCHAR(255) NOT NULL, 
  [Address] NVARCHAR(255), 
  [City] NVARCHAR(255), 
  PRIMARY KEY ([ID_customer])
)

SET IDENTITY_INSERT [Customer] ON
GO

--
-- Dumping data for table 'Customer'
--

-- 0 records

SET IDENTITY_INSERT [Customer] OFF
GO

--
-- Table structure for table 'Discount'
--

IF object_id(N'Discount', 'U') IS NOT NULL DROP TABLE [Discount]

CREATE TABLE [Discount] (
  [ID_discount] INT NOT NULL IDENTITY, 
  [Status] BIT DEFAULT 0, 
  [Discount] INT NOT NULL DEFAULT 0, 
  [Short_name] NVARCHAR(255) NOT NULL, 
  [Description] NVARCHAR(MAX), 
  PRIMARY KEY ([ID_discount])
)

SET IDENTITY_INSERT [Discount] ON
GO

--
-- Dumping data for table 'Discount'
--

-- 0 records

SET IDENTITY_INSERT [Discount] OFF
GO

--
-- Table structure for table 'Employee'
--

IF object_id(N'Employee', 'U') IS NOT NULL DROP TABLE [Employee]

CREATE TABLE [Employee] (
  [ID_employee] INT NOT NULL IDENTITY, 
  [Last_name] NVARCHAR(255) NOT NULL, 
  [First_name] NVARCHAR(255) NOT NULL, 
  [Birthday] DATETIME2 NOT NULL, 
  [Date_of_employment] DATETIME2, 
  [Mobile] NVARCHAR(255) NOT NULL, 
  [Email] NVARCHAR(255), 
  [Address] NVARCHAR(255), 
  [City] NVARCHAR(255), 
  [ID_salary] INT NOT NULL DEFAULT 0, 
  PRIMARY KEY ([ID_employee])
)

SET IDENTITY_INSERT [Employee] ON
GO

--
-- Dumping data for table 'Employee'
--

-- 0 records

SET IDENTITY_INSERT [Employee] OFF
GO

CREATE INDEX [ID_salary] ON [Employee] ([ID_salary])
GO

--
-- Table structure for table 'Manufacturer'
--

IF object_id(N'Manufacturer', 'U') IS NOT NULL DROP TABLE [Manufacturer]

CREATE TABLE [Manufacturer] (
  [ID_manufacturer] INT NOT NULL IDENTITY, 
  [Name_of] NVARCHAR(255) NOT NULL, 
  PRIMARY KEY ([ID_manufacturer])
)

SET IDENTITY_INSERT [Manufacturer] ON
GO

--
-- Dumping data for table 'Manufacturer'
--

-- 0 records

SET IDENTITY_INSERT [Manufacturer] OFF
GO

--
-- Table structure for table 'Payment'
--

IF object_id(N'Payment', 'U') IS NOT NULL DROP TABLE [Payment]

CREATE TABLE [Payment] (
  [ID_payment] INT NOT NULL IDENTITY, 
  [Date_of_transaction] NVARCHAR(255) NOT NULL, 
  [Value_of] MONEY NOT NULL DEFAULT 0, 
  PRIMARY KEY ([ID_payment])
)

SET IDENTITY_INSERT [Payment] ON
GO

--
-- Dumping data for table 'Payment'
--

-- 0 records

SET IDENTITY_INSERT [Payment] OFF
GO

--
-- Table structure for table 'Salary'
--

IF object_id(N'Salary', 'U') IS NOT NULL DROP TABLE [Salary]

CREATE TABLE [Salary] (
  [ID_salary] INT NOT NULL IDENTITY, 
  [Position] NVARCHAR(255) NOT NULL, 
  [Value_of] MONEY NOT NULL DEFAULT 0, 
  PRIMARY KEY ([ID_salary])
)

SET IDENTITY_INSERT [Salary] ON
GO

--
-- Dumping data for table 'Salary'
--

-- 0 records

SET IDENTITY_INSERT [Salary] OFF
GO

--
-- Table structure for table 'Sale'
--

IF object_id(N'Sale', 'U') IS NOT NULL DROP TABLE [Sale]

CREATE TABLE [Sale] (
  [ID_sale] INT NOT NULL IDENTITY, 
  [Price] MONEY NOT NULL DEFAULT 0, 
  [Date_of] DATETIME2 NOT NULL, 
  [ID_car] INT NOT NULL DEFAULT 0, 
  [ID_employee] INT NOT NULL DEFAULT 0, 
  [ID_customer] INT NOT NULL DEFAULT 0, 
  [ID_discount] INT DEFAULT 0, 
  [ID_payment] INT DEFAULT 0, 
  PRIMARY KEY ([ID_sale])
)

SET IDENTITY_INSERT [Sale] ON
GO

--
-- Dumping data for table 'Sale'
--

-- 0 records

SET IDENTITY_INSERT [Sale] OFF
GO

CREATE INDEX [ID_client] ON [Sale] ([ID_customer])
GO

CREATE INDEX [ID_discount] ON [Sale] ([ID_discount])
GO

CREATE INDEX [ID_employee] ON [Sale] ([ID_employee])
GO

CREATE UNIQUE INDEX [ID_new_car] ON [Sale] ([ID_car])
GO

CREATE UNIQUE INDEX [ID_payment] ON [Sale] ([ID_payment])
GO

