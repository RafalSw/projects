
-- 'Klient'

IF object_id(N'Klient', 'U') IS NOT NULL DROP TABLE [Klient]

CREATE TABLE [Klient] (
  [ID_klienta] INT NOT NULL IDENTITY, 
  [Nazwisko] NVARCHAR(255) NOT NULL, 
  [Imie] NVARCHAR(255) NOT NULL, 
  [Data_urodzenia] DATETIME2 NOT NULL, 
  [Telefon] NVARCHAR(255) NOT NULL, 
  [Email] NVARCHAR(255) NOT NULL, 
  PRIMARY KEY ([ID_klienta])
)

SET IDENTITY_INSERT [Klient] ON
GO


INSERT INTO [Klient] ([ID_klienta], [Nazwisko], [Imie], [Data_urodzenia], [Telefon], [Email]) VALUES (1, N'Kowalski', N'Jan', '1967-04-02 00:00:00', N'123456789', N'jankowalki@gmail.com')
INSERT INTO [Klient] ([ID_klienta], [Nazwisko], [Imie], [Data_urodzenia], [Telefon], [Email]) VALUES (2, N'Wierzbicki', N'Zygmunt', '1982-03-11 00:00:00', N'234567890', N'wierzbicki@wp.pl')
-- 2 records

SET IDENTITY_INSERT [Klient] OFF
GO

-- 'Nowy_samochod'


IF object_id(N'Nowy_samochod', 'U') IS NOT NULL DROP TABLE [Nowy_samochod]

CREATE TABLE [Nowy_samochod] (
  [ID_nowego_samochodu] INT NOT NULL IDENTITY, 
  [ID_samochodu] INT NOT NULL DEFAULT 0, 
  [Cena] MONEY NOT NULL DEFAULT 0, 
  [Wersja] NVARCHAR(255), 
  [Opis] NVARCHAR(MAX), 
  PRIMARY KEY ([ID_nowego_samochodu])
)

SET IDENTITY_INSERT [Nowy_samochod] ON
GO

INSERT INTO [Nowy_samochod] ([ID_nowego_samochodu], [ID_samochodu], [Cena], [Wersja], [Opis]) VALUES (1, 1, 70000, N'S', N'Standardowa wersja wyposażenia')
INSERT INTO [Nowy_samochod] ([ID_nowego_samochodu], [ID_samochodu], [Cena], [Wersja], [Opis]) VALUES (2, 1, 95000, N'XL', N'Samochod wyposazony we wszystkie dodatkowe opcje')
INSERT INTO [Nowy_samochod] ([ID_nowego_samochodu], [ID_samochodu], [Cena], [Wersja], [Opis]) VALUES (3, 2, 60000, N'Sport', N'Wersja popularnego hatchback''a ze sportowym zacieciem')
INSERT INTO [Nowy_samochod] ([ID_nowego_samochodu], [ID_samochodu], [Cena], [Wersja], [Opis]) VALUES (4, 3, 30000, N'Economic', N'Mały samochód z niewielką ilością wyposażenia, idealny dla osób starszych')
-- 4 records

SET IDENTITY_INSERT [Nowy_samochod] OFF
GO

CREATE INDEX [ID_car] ON [Nowy_samochod] ([ID_samochodu])
GO

-- 'Pracownik'

IF object_id(N'Pracownik', 'U') IS NOT NULL DROP TABLE [Pracownik]

CREATE TABLE [Pracownik] (
  [ID_pracownika] INT NOT NULL IDENTITY, 
  [Nazwisko] NVARCHAR(255) NOT NULL, 
  [Imie] NVARCHAR(255) NOT NULL, 
  [Data_rodzenia] DATETIME2 NOT NULL, 
  [Data_zatrudnienia] DATETIME2, 
  [Telefon] NVARCHAR(255) NOT NULL, 
  [Email] NVARCHAR(255), 
  PRIMARY KEY ([ID_pracownika])
)

SET IDENTITY_INSERT [Pracownik] ON
GO

INSERT INTO [Pracownik] ([ID_pracownika], [Nazwisko], [Imie], [Data_rodzenia], [Data_zatrudnienia], [Telefon], [Email]) VALUES (1, N'Zakrzewski', N'Marek', '1969-09-24 00:00:00', '2012-05-07 00:00:00', N'987654321', N'm.zakrzewski@salon.pl')
INSERT INTO [Pracownik] ([ID_pracownika], [Nazwisko], [Imie], [Data_rodzenia], [Data_zatrudnienia], [Telefon], [Email]) VALUES (2, N'Machulski', N'Przemysław', '1974-01-01 00:00:00', '2014-10-11 00:00:00', N'876543210', N'p.machulski@salon.pl')
-- 2 records

SET IDENTITY_INSERT [Pracownik] OFF
GO

-- 'Samochod'

IF object_id(N'Samochod', 'U') IS NOT NULL DROP TABLE [Samochod]

CREATE TABLE [Samochod] (
  [ID_samochodu] INT NOT NULL IDENTITY, 
  [Marka] NVARCHAR(255) NOT NULL, 
  [Model] NVARCHAR(255) NOT NULL, 
  [Data_produkcji] DATETIME2 NOT NULL, 
  [Kolor] NVARCHAR(255) NOT NULL, 
  PRIMARY KEY ([ID_samochodu])
)

SET IDENTITY_INSERT [Samochod] ON
GO


INSERT INTO [Samochod] ([ID_samochodu], [Marka], [Model], [Data_produkcji], [Kolor]) VALUES (1, N'Ford', N'Fiesta', '2015-05-01 00:00:00', N'Czarny')
INSERT INTO [Samochod] ([ID_samochodu], [Marka], [Model], [Data_produkcji], [Kolor]) VALUES (2, N'Opel', N'Astra', '2015-04-11 00:00:00', N'Zielony')
INSERT INTO [Samochod] ([ID_samochodu], [Marka], [Model], [Data_produkcji], [Kolor]) VALUES (3, N'Fiat', N'Punto', '2015-04-10 00:00:00', N'Zielony')
-- 3 records

SET IDENTITY_INSERT [Samochod] OFF
GO

--'Sprzedaz'

IF object_id(N'Sprzedaz', 'U') IS NOT NULL DROP TABLE [Sprzedaz]

CREATE TABLE [Sprzedaz] (
  [ID_sprzedazy] INT NOT NULL IDENTITY, 
  [Gwarancja] INT NOT NULL DEFAULT 0, 
  [ID_nowego_samochodu] INT NOT NULL DEFAULT 0, 
  [ID_pracownika] INT NOT NULL DEFAULT 0, 
  [ID_klienta] INT NOT NULL DEFAULT 0, 
  [Cena] MONEY NOT NULL DEFAULT 0, 
  [Data] DATETIME2 NOT NULL, 
  PRIMARY KEY ([ID_sprzedazy])
)

SET IDENTITY_INSERT [Sprzedaz] ON
GO


INSERT INTO [Sprzedaz] ([ID_sprzedazy], [Gwarancja], [ID_nowego_samochodu], [ID_pracownika], [ID_klienta], [Cena], [Data]) VALUES (1, 36, 1, 1, 1, 70000, '2015-06-06 00:00:00')
INSERT INTO [Sprzedaz] ([ID_sprzedazy], [Gwarancja], [ID_nowego_samochodu], [ID_pracownika], [ID_klienta], [Cena], [Data]) VALUES (2, 72, 3, 2, 2, 60000, '2015-04-09 00:00:00')
INSERT INTO [Sprzedaz] ([ID_sprzedazy], [Gwarancja], [ID_nowego_samochodu], [ID_pracownika], [ID_klienta], [Cena], [Data]) VALUES (3, 24, 2, 1, 2, 95000, '2015-04-09 00:00:00')
-- 3 records

SET IDENTITY_INSERT [Sprzedaz] OFF
GO

CREATE INDEX [ID_client] ON [Sprzedaz] ([ID_klienta])
GO

CREATE INDEX [ID_employee] ON [Sprzedaz] ([ID_pracownika])
GO

CREATE INDEX [ID_new_car] ON [Sprzedaz] ([ID_nowego_samochodu])
GO

-- Wypisz pracownikow
SELECT Pracownik.Nazwisko, Pracownik.Imie FROM Pracownik;

-- Wypisz klientow
SELECT Klient.Nazwisko, Klient.Imie FROM Klient;

-- Wypisz samochody Kowalskiego 
SELECT Samochod.Marka, Samochod.Model, Nowy_samochod.Wersja FROM (Samochod INNER JOIN Nowy_samochod ON Samochod.ID_samochodu = Nowy_samochod.ID_samochodu) INNER JOIN (Klient INNER JOIN Sprzedaz ON Klient.ID_klienta = Sprzedaz.ID_klienta) ON Nowy_samochod.ID_nowego_samochodu = Sprzedaz.ID_nowego_samochodu WHERE (((Klient.Nazwisko)="Kowalski"));

-- Wypisz samochody Wierzbickiego 
SELECT Samochod.Marka, Samochod.Model, Nowy_samochod.Wersja FROM (Samochod INNER JOIN Nowy_samochod ON Samochod.ID_samochodu = Nowy_samochod.ID_samochodu) INNER JOIN (Klient INNER JOIN Sprzedaz ON Klient.ID_klienta = Sprzedaz.ID_klienta) ON Nowy_samochod.ID_nowego_samochodu = Sprzedaz.ID_nowego_samochodu WHERE (((Klient.Nazwisko)="Wierzbicki"));

-- Wypisz najlepiej sprzedajace sie marki
SELECT Samochod.Marka, Count(Samochod.Model) AS Ilosc FROM (Samochod INNER JOIN Nowy_samochod ON Samochod.ID_samochodu = Nowy_samochod.ID_samochodu) INNER JOIN Sprzedaz ON Nowy_samochod.ID_nowego_samochodu = Sprzedaz.ID_nowego_samochodu GROUP BY Samochod.Marka ORDER BY Count(Samochod.Model) DESC;
