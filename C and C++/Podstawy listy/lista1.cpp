#include <iostream>
using namespace std;

class Element
{
public:
	Element()
	{
		next = NULL;
	}
	int data; //u�yteczne dane
	Element *next; //wska�nik na kolejny element
};

class Container
{
public:
	Container()
	{
		first = NULL;
	}
	~Container()
	{
		Element *current = first;
		Element *next = first->next;

		while(current!=NULL)
		{
			delete current;
			current = next;
			if(next!=NULL)
				next = next->next;
		}
	}

	Element *first;

	void add(int i)
	{
		Element *newElement = new Element(); //Do wska�nika na element przypisujemy nowo zainicjowany obiekt (dynamiczna alokacja pami�ci) - odbywa si� przez wywo�anie konstruktora
		newElement->data = i;

		if(first == NULL)
		{
			first = newElement;
		}
		else
		{
			Element *current = first; //zaczynamy od pocz�tku poszukiwania

			while(current->next!=NULL) //do znaliezienia ostatniego
				current = current->next; //przesuwamy sie do nastepnego

			current->next = newElement;
		}
	}
};

int main()
{
	Container lista;
	/*Element strona1;
	strona1.data=1;

	Element strona2;
	strona2.data=2;

	strona1.next = &strona2;

	lista.first = &strona1;

	//pierwszy element listy:
	cout<<lista.first->data<<endl; // -> bo first jest wska�nikiem

	//drugi element listy:
	cout<<lista.first->next->data<<endl;*/

	lista.add(5);
	lista.add(9);
	lista.add(81);

	//wyciek pamieci
	int *a = new int(5);
	delete a; //poprawne rozwi�zanie problemu
	a = new int(6);

	delete a;

	system("PAUSE");

}