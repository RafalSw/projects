#include <iostream>
#include <vector>
#include <queue>

using namespace std;

struct Wezel
{
	int wartosc;
	int odleglosc;
	Wezel* rodzic;
};

bool dodajKrawedz(vector<int> *sasiedzi, int poczatek, int koniec) //funkcja sprawdzajaca czy takiej krawedzi jeszcze nie ma i dodajaca nowa krawedz
{
	for (int i = 0; i < sasiedzi[poczatek].size(); i++)
	{
		if (sasiedzi[poczatek].at(i) == koniec)
			return false; //taka krawedz juz istnieje
	}

	//nie ma potrzeby sprawdzac w druga strone - graf jest nieskierowany, a krawedzie dodawane do obu punktow jednoczesnie
	/*
	for (int i = 0; i < sasiedzi[koniec].size(); i++)
	{
		if (sasiedzi[koniec].at(i) == poczatek)
			return false;
	}*/

	//dodanie sasiadow dla wskazanych elementow
	sasiedzi[poczatek].push_back(koniec);
	sasiedzi[koniec].push_back(poczatek);
	return true;
}

bool czyZawiera(vector<Wezel*> *przeszukane, int szukana, int odleglosc) //przeszukiwanie wektora pod katem danej wartosci
{
	for (int i = 0; i < przeszukane->size(); i++)
	{
		if (przeszukane->at(i)->wartosc == szukana) //taka wartosc juz jest
			if (przeszukane->at(i)->odleglosc > odleglosc) //ale za to dalej niz obecna, dlatego warto ja dodac
				return false;
			else
				return true;
	}
	return false;
}

//metoda przeszukania wszerz
void szukaj(vector<int> *sasiedzi, int poczatek, int koniec)
{
	vector<Wezel*> *przeszukane = new vector<Wezel*>();
	queue<Wezel*> *doPrzeszukania = new queue<Wezel*>();

	//wezel poczatkowy
	Wezel* start = new Wezel();
	start->wartosc = poczatek;
	start->rodzic = NULL;
	start->odleglosc = 0;
	przeszukane->push_back(start);
	doPrzeszukania->push(start);

	Wezel* sprawdzany = NULL;
	bool result = false;

	while (!doPrzeszukania->empty()) //az kolejka bedzie pusta
	{
		sprawdzany = doPrzeszukania->front();
		doPrzeszukania->pop();
		przeszukane->push_back(sprawdzany);

		//budowanie wektora z przeszukanymi wartosciami
		for (int i = 0; i < sasiedzi[sprawdzany->wartosc].size(); i++)
		{
			//tworzenie wezla
			Wezel* nowyWezel = new Wezel();
			nowyWezel->wartosc = sasiedzi[sprawdzany->wartosc].at(i);
			nowyWezel->rodzic = sprawdzany;
			nowyWezel->odleglosc = sprawdzany->odleglosc + 1; //jeden krok dalej

			if (!czyZawiera(przeszukane, nowyWezel->wartosc, nowyWezel->odleglosc))
			{
				//wezel jeszcze nie byl przeszukany lub znaleziono krotsza droge do niego, dodanie do kolejki
				doPrzeszukania->push(nowyWezel);
			}
			else
			{
				//wezel juz zostal przeszukany
				delete nowyWezel;
			}			
		}
		przeszukane->push_back(sprawdzany);
	}

	Wezel* najkrotszy = NULL;
	for (int i = 0; i < przeszukane->size(); i++)
	{
		if (przeszukane->at(i)->wartosc == koniec)
			if (najkrotszy == NULL || najkrotszy->odleglosc > przeszukane->at(i)->odleglosc)
				najkrotszy = przeszukane->at(i);
	}

	if (najkrotszy == false)
		cout << "Nie istnieje polaczenie pomiedzy grafami" << endl;
	else
	{
		cout << "Najkrotsza droga: " << endl;
		Wezel* aktualny = najkrotszy;
		while (aktualny != NULL)
		{
			cout << aktualny->wartosc;
			if (aktualny->rodzic !=NULL)
				cout<< "<-";
			aktualny = aktualny->rodzic;
		}
		cout << endl;
	}
}

int main()
{
	int liczbaWierzcholkow;
	int liczbaKrawedzi;
	cout << "Podaj liczbe wierzcholkow:" << endl;
	cin >> liczbaWierzcholkow;
	cout << "Podaj liczbe krawedzi:" << endl;
	cin >> liczbaKrawedzi;

	vector<int> *sasiedzi = new vector<int>[liczbaWierzcholkow]; //tablica wektorow zawiera sasiadow danego wierzcholka

	//pobranie danych
	cout << "Pobieranie danych. Podawaj parami krance kolejnych krawedzi. Wartosci wierzcholkow oddziel spacja (np: 0 2)" << endl;
	for (int i = 0; i < liczbaKrawedzi; i++)
	{
		int poczatek;
		int koniec;
		cin >> poczatek >> koniec;
		if (dodajKrawedz(sasiedzi, poczatek, koniec))
			cout << "Dodano krawedz " << poczatek << "->" << koniec << endl;
		else
			cout << "Taka krawedz juz istnieje, wpis pominiety" << endl;
	}

	//wypisanie danych
	for (int i = 0; i < liczbaWierzcholkow; i++)
	{
		cout << i << ": ";
		for (int j = 0; j < sasiedzi[i].size(); j++)
		{
			cout << sasiedzi[i].at(j) << "\t";
		}
		cout << endl;
	}

	int poczatek;
	int koniec;
	cout << "Poszukiwanie najkrotszej sciezki:\npodaj poczatek: ";
	cin >> poczatek;
	cout << "podaj koniec: ";
	cin >> koniec;

	szukaj(sasiedzi, poczatek, koniec);

	delete[] sasiedzi;

	system("PAUSE");
	return 0;
}