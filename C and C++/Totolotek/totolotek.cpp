#include <iostream>
#include <cstdlib> //do srand()
#include <ctime> //do time(NULL)
#include <fstream> // do zapisu do pliku

using namespace std;

int main()
{
	int liczbaLosowan; //liczba wykonywanych losowan
	int tablica[6][100]; //tablica przechowujaca wyniki
	//1. Wyswietlenie nazwy programu, daty i autora
	cout << "Program \"Totolotek\" " << endl;

	//2. Zapytanie o ilosc losowan
	cout << "Ile losowan program ma wykonac? Maksymalna wartosc to 100!" << endl;
	cin >> liczbaLosowan;
	if (liczbaLosowan > 100)
	{
		cout << "Wprowadzona liczba losowan nie jest prawidlowa - liczba musi byc mniejsza niz 100. Program konczy dzialanie" << endl;
		return -1;
	}
	if (liczbaLosowan < 1)
	{
		cout << "Wprowadzona liczba losowan nie jest prawidlowa - liczba musi byc wieksza niz 0. Program konczy dzialanie" << endl;
		return -1;
	}

	//3. Wyznaczenie liczb i zapisanie ich w tablicy dwuwymiarowej
	srand(time(NULL)); //ustawienie generatora liczb

	for (int i = 0; i < liczbaLosowan; i++)
	{
		int powtarzajaSie; //sprawdzanie czy liczby sie nie powtarzaja (0-jest ok, 1 jesli sie powtarzaja)
		do
		{
			powtarzajaSie = 0;
			//kazde losowanie oznacza wybranie 6 liczb:
			for (int j = 0; j < 6; j++)
			{
				tablica[j][i] = rand() % 49 + 1; //%49 daje losowanie liczb od 0 do 48, a dodanie 1 przesuwa przedzial na 1 do 49
			}
			//sprawdzenie czy sie nie powtarzaja
			for (int j = 0; j < 5; j++)
			{
				for (int k = j + 1; k < 6; k++)
				{
					if (tablica[j][i] == tablica[k][i])
						powtarzajaSie = 1;
				}
			}
		} while (powtarzajaSie == 1); //jesli sie powtarzaja, to warunek spelniony i losowanie jeszcze raz
		

	}

	//sortowanie babelkowe wynikow
	for (int i = 0; i < liczbaLosowan; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			for (int k = 0; k < 5; k++)
			{
				if (tablica[k][i] > tablica[k + 1][i])//konieczna zamiana
				{
					int temp = tablica[k][i];
					tablica[k][i] = tablica[k + 1][i];
					tablica[k + 1][i] = temp;
				}
			}
		}
	}

	//4. Wypisanie wynikow losowania
	cout << "Wyniki kolejnych losowan:" << endl;
	for (int i = 0; i < liczbaLosowan; i++)
	{
		//wypisanie kazdej z 6 liczb
		for (int j = 0; j < 6; j++)
		{
			cout << tablica[j][i];
			if (j < 5)
				cout << ",\t";//po pierwszych 5 liczbach wstawiamy przecinek i tabulacje
		}
		cout << endl; //nowa linia po wypisaniu calego losowania
	}

	//5. Zapis do pliku
	fstream plik;
	plik.open("wyniki.txt", ios::out);
	//powtorzenie kodu - tak jak wypisywanie, tylko ze do pliku zamiast na konsole (plik zamiast cout)
	plik << "Wyniki kolejnych losowan:" << endl;
	for (int i = 0; i < liczbaLosowan; i++)
	{
		//wypisanie kazdej z 6 liczb
		for (int j = 0; j < 6; j++)
		{
			plik << tablica[j][i];
			if (j < 5)
				plik << ",\t";//po pierwszych 5 liczbach wstawiamy przecinek i tabulacje
		}
		plik << endl; //nowa linia po wypisaniu calego losowania
	}
	plik.close();

	return 0;
}
