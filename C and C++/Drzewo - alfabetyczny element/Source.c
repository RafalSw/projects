
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 64
#define bool char /* lepsze niz int, bo mniejsza pamiec */
#define true 1
#define false 0

typedef struct _Node
{
	char dane;
	struct _Node *rodzic;
	struct _Node *lewy;
	struct _Node *prawy;
}Node;

void dodaj(Node** korzen, char dane, char* lokalizacja)
{
	Node* temp = *korzen;
	Node* parrent = NULL;
	char last = '\0';
	unsigned int i = 0;
	unsigned int len = strlen(lokalizacja);

	if (*korzen == NULL)
	{
		*korzen = (Node*)malloc(sizeof(Node));
		(*korzen)->dane = '\0';
		(*korzen)->lewy = NULL;
		(*korzen)->prawy = NULL;
		(*korzen)->rodzic = NULL;
	}
	if (strlen(lokalizacja) == 0)
	{
		(*korzen)->dane = dane;
	}
	else
	{
		while (i < len)
		{
			if (temp == NULL)
			{
				temp = (Node*)malloc(sizeof(Node));
				temp->dane = '\0';
				temp->lewy = NULL;
				temp->prawy = NULL;

				temp->rodzic = parrent;
				if (last == 'L')
					parrent->lewy = temp;
				else if (last == 'R')
					parrent->prawy = temp;
			}
			if (lokalizacja[i] == 'L')
			{
				parrent = temp;
				temp = temp->lewy;
				last = 'L';
			}
			else if (lokalizacja[i] == 'R')
			{
				parrent = temp;
				temp = temp->prawy;
				last = 'R';
			}
			i++;
		}

		if (temp != NULL)
		{
			temp->dane = dane;
		}
		else
		{
			temp = (Node*)malloc(sizeof(Node));
			temp->dane = dane;
			temp->lewy = NULL;
			temp->prawy = NULL;
			temp->rodzic = parrent;

			if (last == 'L')
				parrent->lewy = temp;
			else if (last == 'R')
				parrent->prawy = temp;
		}
	}
}

void wczytaj(FILE* f, Node** korzen)
{
	char c, dane;
	char lokalizacja[N+1];

	do
	{
		memset(lokalizacja, '\0', N + 1);
#ifdef _MSC_VER
		c = fgetc(f);
#else
		c = getchar_unlocked();
#endif
		if (c<97 || c>122)
			continue;
		else
		{
			dane = c;

#ifdef _MSC_VER
			c = fgetc(f);
#else
			c = getchar_unlocked();
#endif
			if (c != 32)
				continue;
			else
			{
				while (strlen(lokalizacja)<N)
				{
#ifdef _MSC_VER
					c = fgetc(f);
#else
					c = getchar_unlocked();
#endif
					if (c == '\n' || c == EOF)
					{
						dodaj(korzen, dane, lokalizacja);
						break;
					}
					else
					{
						lokalizacja[strlen(lokalizacja)] = c;
					}
				}
			}
		}
	} while (c != EOF);
}

bool sortowanie(Node* node1, Node* node2)
{
	while (true)
	{
		if (node1 == NULL)
			return true;
		if (node2 == NULL)
			return false;
		if (node1->dane < node2->dane)
			return true;
		if (node1->dane == node2->dane)
		{
			node1 = node1->rodzic;
			node2 = node2->rodzic;
			continue;
		}
		return false;
	}
}


void przejscie(Node* korzen, Node** najstarszy)
{
	if (korzen->lewy == NULL && korzen->prawy == NULL)
	{
		if (!sortowanie(korzen, *najstarszy))
			*najstarszy = korzen;
	}
	else
	{
		if (korzen->lewy != NULL)
			przejscie(korzen->lewy, najstarszy);
		if (korzen->prawy != NULL)
			przejscie(korzen->prawy, najstarszy);
	}
}

void wypisz(Node* lisc)
{
	while (lisc != NULL)
	{
#ifdef _MSC_VER
		printf("%c", lisc->dane);
#else
		putchar_unlocked(lisc->dane);
#endif
		lisc = lisc->rodzic;
	}
}

void wyszukajNajstarszy(Node* korzen)
{
	Node* najstarszy = NULL;
	przejscie(korzen, &najstarszy);
	wypisz(najstarszy);
}

int main()
{
	FILE *f;
	Node *korzen = NULL;

#ifdef _MSC_VER
	f = fopen("Text.txt", "r");
	if (f == NULL)
		printf("Nie udalo sie wczytac pliku\n");
#endif
	
	wczytaj(f, &korzen);
	wyszukajNajstarszy(korzen);

#ifdef _MSC_VER
	fclose(f);
	printf("\n");
	system("PAUSE");
#endif

	return 0;
}