#ifndef _AVL_H_ //oryginalnie by�o ifdef, co nie mialo prawa dzialac
#define _AVL_H_


struct Node {
	int Value;
	int BF;
	Node* Parent;
	Node* Left;
	Node* Right;
};

class AvlTree {
public:
	AvlTree();
	bool Find(int pValue);
	void Insert(int pValue);
	void Remove(int pValue);

	void Print(Node* pRoot);
};


#endif // _AVL_H_