#include <iostream>
#include "avl.h"

using namespace std;

Node* root; //wskaznik na pierwszy element drzewa

void rotationRR(Node* n1) // Rotacja RR
{
	Node* n2 = n1->Right;
	Node* prev = n1->Parent;

	n1->Right = n2->Left;
	if (n1->Right)
		n1->Right->Parent = n1;

	n2->Left = n1;
	n2->Parent = prev;
	n1->Parent = n2;

	if (prev != NULL)
	{
		if (prev->Left == n2)
			prev->Left = n2;
		else
			prev->Right = n2;
	}
	else
		root = n2;

	if (n2->BF == -1)
	{
		n1->BF = 0;
		n2->BF = 0;
	}
	else
	{
		n1->BF = -1;
		n2->BF = 1;
	}
}

void rotationLL(Node* n1) // Rotacja LL
{
	Node* n2 = n1->Left;
	Node* prev = n1->Parent;

	n1->Left = n2->Right;
	if (n1->Left)
		n1->Left->Parent = n1;

	n2->Right = n1;
	n2->Parent = prev;
	n1->Parent = n2;

	if (prev != NULL)
	{
		if (prev->Right == n2)
			prev->Right = n2;
		else
			prev->Left = n2;
	}
	else
		root = n2;

	if (n2->BF == 1)
	{
		n1->BF = 0;
		n2->BF = 0;
	}
	else
	{
		n1->BF = 1;
		n2->BF = -1;
	}
}


void rotationRL(Node * n1) // Rotacja RL
{
	Node *prev = n1->Parent;
	Node *n2 = n1->Right;
	Node *n3 = n2->Left;


	n2->Left = n3->Right;
	if (n2->Left)
		n2->Left->Parent = n2;

	n1->Right = n3->Left;
	if (n1->Right)
		n2->Right->Parent = n1;

	n3->Left = n1;
	n3->Right = n2;
	n1->Parent = n2->Parent = n3;
	n3->Parent = prev;

	if (prev != NULL)
	{
		if (prev->Left == n1)
			prev->Left = n3;
		else
			prev->Right = n3;
	}
	else
		root = n3;

	if (n3->BF == -1)
		n1->BF = 1;
	else
		n1->BF = 0;

	if (n3->BF == 1)
		n2->BF = -1;
	else
		n2->BF = 0;

	n3->BF = 0;
}

void rotationLR(Node * n1) // Rotacja LR
{
	Node *prev = n1->Parent;
	Node *n2 = n1->Left;
	Node *n3 = n2->Right;


	n2->Right = n3->Left;
	if (n2->Right)
		n2->Right->Parent = n2;

	n1->Left = n3->Right;
	if (n1->Left)
		n2->Left->Parent = n1;

	n3->Right = n1;
	n3->Left = n2;
	n1->Parent = n2->Parent = n3;
	n3->Parent = prev;

	if (prev != NULL)
	{
		if (prev->Left == n1)
			prev->Left = n3;
		else
			prev->Right = n3;
	}
	else
		root = n3;

	if (n3->BF == 1)
		n1->BF = -1;
	else
		n1->BF = 0;

	if (n3->BF == -1)
		n2->BF = 1;
	else
		n2->BF = 0;

	n3->BF = 0;
}

void create(int pValues[], int size, AvlTree& tree)  //funkcja create MUSI pobierac rozmiar tablicy, alternatywa jest zdefiniowanie np. przez #define N 10 i wtedy uzycie createTree(root, pValues, 0, N); ale wtedy rozmiar tablicy jest z gory ustalony
{
	for (int i = 0; i < size; i++)
	{
		tree.Insert(pValues[i]);
	}
	//createTree(root, pValues, 0, size);
}

Node* removeHelper(int pValue)
{
	Node* temp = root;
	while (temp != NULL)
	{
		if (temp->Value == pValue)
			return temp;
		if (temp->Value < pValue)
			temp = temp->Right;
		else
			temp = temp->Left;
	}
	return NULL;
}

//implementacje funkcji z AvlTree
AvlTree::AvlTree()
{
	root = NULL;
}

bool AvlTree::Find(int pValue)
{
	//poszukiwanie warosci poprzez porownanie, a nastepnie przejscie do kolejnych wezlow
	Node* temp = root;
	while (temp != NULL)
	{
		if (temp->Value == pValue)
			return true;
		if (temp->Value < pValue)
			temp = temp->Right;
		else
			temp = temp->Left;
	}
	return false;
}

void AvlTree::Insert(int pValue)
{
	if (root != NULL && Find(pValue) == true)
		return; //nie mozna dwa razy dodawac takiej samej wartosci do drzewa
	Node *temp = root;
	Node *prev = NULL;
	Node *superParrent = NULL;
	bool result = false;

	//poszukiwanie wlasciwego miejsca do wstawienia wezla
	while (temp != NULL)
	{
		if (temp->Value == pValue)
		{
			return; //istnieje juz taka wartosc w drzewie
		}
		prev = temp;
		if (temp->Value < pValue)
			temp = temp->Right;
		else
			temp = temp->Left;
	}

	//jesli taka wartosc nie istnieje, to tworzymy nowy wezel
	Node* newNode = new Node();
	newNode->Value = pValue;
	newNode->Left = newNode->Right = NULL;
	newNode->BF = 0;
	newNode->Parent = prev;


	//dolaczenie nowego wezla do drzewa
	if (prev == NULL)
	{
		root = newNode;
		return; //nie ma tu nic wiecej do roboty
	}
	else if (newNode->Value < prev->Value)
		prev->Left = newNode;
	else
		prev->Right = newNode;

	if (prev->BF != 0) //istnialo jakies wezel w drugiej galezi, teraz nastapi wyrownanie
	{
		prev->BF = 0;
	}
	else
	{
		if (prev->Left == newNode)
			prev->BF = 1;
		else
			prev->BF = -1;

		//superParrent = prev->Parent;
		superParrent = prev->Parent;
		while (superParrent != NULL)
		{
			if (superParrent->BF != 0)
			{
				result = true; //znaleziono niezrownowazony wezel
				break;
			}

			//superParrent->BF jest rowny 0
			if (superParrent->Left == prev)
				superParrent->BF = 1;
			else
				superParrent->BF = -1;

			prev = superParrent;
			superParrent = superParrent->Parent;
		}

		if (result == true)//znalzwiono niezbalansowane drzewo
		{
			if (superParrent->BF == 1)
			{
				if (superParrent->Right == prev)
					superParrent->BF = 0;  // ga��zie si� r�wnowa��
				else if (prev->BF == -1)
					rotationLR(superParrent);
				else
					rotationLL(superParrent);
			}
			else
			{
				if (superParrent->Left == prev)
					superParrent->BF = 0;  // ga��zie si� r�wnowa��
				else if (prev->BF == -1) //tu by�o 1
					rotationRR(superParrent);
				else
					rotationRL(superParrent);
			}
		}
	}
}

void AvlTree::Remove(int pValue)
{
	Node* temp = root;
	Node* toBeRemoved = NULL;
	while (temp != NULL)
	{
		if (temp->Value == pValue)
			toBeRemoved = temp;

		if (temp->Value < pValue)
			temp = temp->Right;
		else
			temp = temp->Left;
	}

	if (toBeRemoved != NULL)
	{
		Node  *node1, *node2, *node3;
		bool result;

		if (toBeRemoved->Left && toBeRemoved->Right)
		{
			Node* temp1 = toBeRemoved;
			Node* temp2;

			if (temp1 != NULL)
			{
				if (temp1->Left != NULL)
				{
					temp1 = temp1->Left;
					while (temp1->Right)
						temp1 = temp1->Right;
				}
				else
					do
					{
						temp2 = temp1;
						temp1 = temp1->Parent;
					} while (temp1 && temp1->Right != temp2);
			}

			node2 = removeHelper(temp1->Value);
			Remove(temp1->Value);
			result = false;
		}
		else
		{
			result = true;
			toBeRemoved->BF = 0;
			if (toBeRemoved->Left)
			{
				node2 = toBeRemoved->Left;
				toBeRemoved->Left = NULL;
			}
			else
			{
				node2 = toBeRemoved->Right;
				toBeRemoved->Right = NULL;
			}
		}

		if (node2 != NULL)
		{
			node2->Parent = toBeRemoved->Parent;
			node2->Left = toBeRemoved->Left;

			if (node2->Left)
				node2->Left->Parent = node2;

			node2->Right = toBeRemoved->Right;
			if (node2->Right)
				node2->Right->Parent = node2;

			node2->BF = toBeRemoved->BF;
		}

		if (toBeRemoved->Parent)
		{
			if (toBeRemoved->Parent->Left == toBeRemoved)
				toBeRemoved->Parent->Left = node2;
			else
				toBeRemoved->Parent->Right = node2;
		}
		else
			root = node2;

		if (result == true)
		{
			node3 = node2;
			node2 = toBeRemoved->Parent;
			while (node2 != NULL)
			{
				if (!node2->BF)
				{
					if (node2->Left == node3)
						node2->BF = -1;
					else
						node2->BF = 1;
					break;
				}
				else
				{
					if (((node2->BF == 1) && (node2->Left == node3)) || ((node2->BF == -1) && (node2->Right == node3)))
					{
						node2->BF = 0;
						node3 = node2;
						node2 = node2->Parent;
					}
					else
					{
						if (node2->Left == node3)
							node1 = node2->Right;
						else
							node1 = node2->Left;

						if (!node1->BF)
						{
							if (node2->BF == 1)
								rotationLL(node2);
							else
								rotationRR(node2);
							break;
						}
						else if (node2->BF == node1->BF)
						{
							if (node2->BF == 1)
								rotationLL(node2);
							else
								rotationRR(node2);
							node3 = node1;
							node2 = node1->Parent;
						}
						else
						{
							if (node2->BF == 1)
								rotationLR(node2);
							else
								rotationRL(node2);
							node3 = node2->Parent;
							node2 = node3->Parent;
						}
					}
				}
			}
		}
	}
}

void AvlTree::Print(Node* pRoot)
{
	if (pRoot != NULL)
	{
		Print(pRoot->Left);
		cout << pRoot->Value << ", ";
		Print(pRoot->Right);
	}
}


//funkcja glowna 
int main()
{
	AvlTree avl;
	int tab[] = { 1, 4, 7, 8, 11, 12, 15, 16, 89, 92, 105 };
	create(tab, (sizeof(tab) / sizeof(*tab)),avl);

	int choice, number;
	do {
		cout << "1. dodaj element" << endl;
		cout << "2. usun element" << endl;
		cout << "3. wyszukaj element" << endl;
		cout << "4. wypisz drzewo AVL" << endl;
		cout << "5. zakoncz" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
		{
			cout << "Podaj liczbe: " << endl;
			cin >> number;
			avl.Insert(number);
			break;
		}
		case 2:
		{
			cout << "Liczba do usuniecia?: " << endl;
			cin >> number;
			avl.Remove(number);
			break;
		}
		case 3:
		{
			cout << "Szukana liczba?: " << endl;
			cin >> number;
			if (avl.Find(number) == true)
			{
				cout << "Liczba " << number << " wystepuje w drzewie" << endl;
			}
			else
			{
				cout << "Liczba " << number << " NIE wystepuje w drzewie" << endl;
			}
			break;
		}
		case 4:
		{
			avl.Print(root);
			cout << endl;
		}
		}
	} while (choice != 5);
	return 0;
}
