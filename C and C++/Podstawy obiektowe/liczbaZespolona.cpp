#include <iostream>
#include "liczbaZespolona.h"

using namespace std;

LiczbaZespolona::LiczbaZespolona()
{
	cout<<"podaj wartosc rzeczywista: ";
	cin>>re;
	cout<<endl;
	cout<<"podaj wartosc urojona: ";
	cin>>im;
	cout<<endl;
}

LiczbaZespolona::LiczbaZespolona(const LiczbaZespolona& rf)
{
	//konstruktor kopiuj�cy przypisuje warto�� rzeczywist� z kopiowanego elementu, a urojonej przypisuje 0
	re = rf.re;
	im = 0;
}

LiczbaZespolona::~LiczbaZespolona()
{
	cout<<"Usunieto obiekt"<<endl;
}

void LiczbaZespolona::wypisz()
{
	cout<<re<<"+"<<im<<"i"<<endl;
}