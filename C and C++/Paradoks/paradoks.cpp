#include <iostream>
#include <cstdlib>
#include <math.h>
#include <sstream>

using namespace std;

void drukujDouble(double d)
{
	string str;
	ostringstream oss;
	oss << scientific << d;
	str = oss.str();
	if (str.find_last_of("-") == str.length() - 4)
		if (str.at(str.length() - 3) == '0')
			str.replace(str.length() - 3, 1, "");
	cout << str;
}

double policzP(int n, int k)
{
	double p = 1;
	for (int i = 1; i < n; i++)
	{
		p *= 1 - (double)i / k;
	}
	return 1-p; //1-p bo nie szukamy roznych (jak we wzorze), tylko takich samych
}

int policzN(double p, int k)
{
	int i = 0;
	while (policzP(i, k) < p)
		i++;
	return i;
}

int policzK(double p, int n)
{
	int i=0;
	while (policzP(n, i+1) > p)
		i++;
	return i;
}
int main(int argc, char* argv[])
{
	if (argc < 2)
	{
		cout << "Jedna z wartosci k,n,p powinna byc ujemna, pozostale dwie - dodatnie." << endl;
		return EXIT_FAILURE;
	}

	int k=-1, n=-1;
	double p=-1;

	for (int i = 1; i < argc; i++)
	{
		if (argv[i][0] == 'n')
		{
			string text = argv[i];
			text = text.substr(2);
			stringstream str(text);
			int x;
			str >> x;
			if (!str)
			{
				cout << "Niepoprawny argument: " << argv[i] << endl;
				return EXIT_FAILURE;
			}
			else
			{
				n = x;
			}
		}
		else if (argv[i][0] == 'k')
		{
			string text = argv[i];
			text = text.substr(2);
			stringstream str(text);
			int x;
			str >> x;
			if (!str)
			{
				cout << "Niepoprawny argument: " << argv[i] << endl;
				return EXIT_FAILURE;
			}
			else
			{
				k = x;
			}
		}
		else if (argv[i][0] == 'p')
		{
			string text = argv[i];
			text = text.substr(2);
			stringstream str(text);
			double x;
			str >> x;
			if (!str)
			{
				cout << "Niepoprawny argument: " << argv[i] << endl;
				return EXIT_FAILURE;
			}
			else
			{
				p = x;
			}
		}
		else
		{
			cout << "Niepoprawny argument: " << argv[i] << endl;
			return EXIT_FAILURE;
		}
	}

	std::cout.precision(6);
	cout << scientific;

	if (k >= 0 && n >= 0 && p<0)
	{
		cout << "p(k,n)= ";
		drukujDouble(policzP(n, k));
		cout<< endl;
	}
	else if (k >= 0 && p >= 0 && n<0)
	{
		int n = policzN(p, k);
		cout << "n(k,p)= " << n << " P()=";
		drukujDouble(policzP(n, k));
		cout << endl;
	}
	else if (n >= 0 && p >= 0 && k<0)
	{
		int k = policzK(p, n);
		cout << "k(n,p)= " << k << " P()=";
		drukujDouble(policzP(n, k));
		cout << endl;
	}
	else
	{
		cout << "Jedna z wartosci k,n,p powinna byc ujemna, pozostale dwie - dodatnie." << endl;
		return EXIT_FAILURE;
	}

	return 0;
}