#include <iostream>
#include<string>
#include <sstream>
#include <cctype> //isNumber

using namespace std;


//element stosu - z racji, ze jest trzymany za ostatni element, to posiada wskaznik na previous
class StackElement
{
public:
	int data;
	StackElement* previous;

	StackElement(int i) //konstruktor przypisuje argument do danych
	{
		data = i;
		previous = NULL;
	}
};


//implementacja stosu - lista jednokierunkowa, tym razem trzymana za ostatni element
class Stack
{
	StackElement* last;

public:
	Stack()
	{
		last = NULL;
	}

	void push(int i) //dodaje nowy element na koniec
	{
		StackElement* newElement = new StackElement(i);
		newElement->previous = last; //poprzednikiem dodawanego jest obecny koniec
		last = newElement; //koniec staje sie dodawanym elementem
	}

	int top() //zwraca wartosc ze szczytu stosu, ale nie kasuje elementu
	{
		return last->data; 
	}

	int pop() //zwraca wartosc ze szczytu stosu i kasuje element
	{
		int result = last->data;
		StackElement* toBeRemoved = last; //zapamietanie elementu do skasowania
		last = last->previous; //przypisanie do ostatniego elementu poprzednika z elementu kasowanego
		delete toBeRemoved;
		return result;
	}

	bool empty() //true jesli stos jest pusty
	{
		if (last == NULL)
			return true;
		return false;
	}

	int size() //zwraca rozmiar stosu zliczajac elementy od ostatniego do pierwszego
	{
		int result = 0;
		StackElement* current = last;
		while (current != NULL)
		{
			current = current->previous;
			result++;
		}
		return result;
	}
};

int stringToNumber(string s) //zamiana stringu na wartosc
{
	int i;
	istringstream iss(s);
	iss >> i; //wlasciwa konwersja strumieniowa, zupelnie jak cin
	return i;
}

bool isNumber(char c) //true jesli znak jest z zakresuu 0-9
{
	return isdigit(c);
}

bool lowerPriority(char c1, char c2) //okreslenie priorytetow wykonywanych dzialan
{
	switch (c1)
	{
	case '-':
		return c2 == '-';
	case '+':
		return c2 == '+' || c2 == '-';
	case '*':
		return c2 != '/';
	case '/':
	case '%':
	case '^':
		return true;
	default:
		return false;
	}
}

void calculate(char c, Stack &values) //pojedyncze obliczenie
{
	if (values.size() < 2) //musza byc dwie wartosci do wykonania obliczenia
		return; //blad - nie ma dwoch wartosci do dzialania


	//pobieranie wartosci ze stosu, kasowanie ich na stosie
	int value1 = values.pop();
	int value2 = values.pop();

	//wykonanie operacji i wrzucenie wyniku na stos
	switch (c)
	{
	case '+': 
		values.push(value1 + value2);
		break;
	case '-':
		values.push(value2 - value1);
		break;
	case '*':
		values.push(value1 * value2);
		break;
	case '/':
		if (value1 == 0)
		{
			cout << "Nie wolno dzilic przez 0!" << endl;
			exit(-1);
		}
		values.push(value2 / value1);
		break;
	case '%': //reszta z dzielenia
		values.push(value2%value1);
		break;
	case '^': //potegowanie
	{
		if (value1 == 0) //jesli wykladnik == 0
			values.push(1);
		else
		{
			int result = value2; //potegowanie "na piechote"
			for (int i = 1; i < value1; i++)
				result *= value2;
			values.push(result);
		}
		break;
	}
	default:
		return;
	}
}

int main(void)
{
	string s;
	//dwa stosy - jeden dla wartosci, drugi dla operatorow
	Stack values;
	Stack operators;

	do
	{
		getline(std::cin, s); //wczytywanie linii z wyrazeniem
		if (s.length() == 0 || s[0] == 'c') //wyjscie z programu po wpisaniu 'c' lub zwyklym enterze
		{
			break;
		}
		int iter = 0;
		string number = "";

		//analiza wczytanej linii
		for (int i = 0; i < s.length(); i++)
		{
			//jelsli znak jest cyfra, dodajemy go jak string do stringu przechowujacego docelowa liczbe
			if (isNumber(s[iter]))
			{
				number += s[iter];
			}
			if (!isNumber(s[iter]) || s.length() == i + 1) //jesli juz nie jest liczba(czyli operator) lub koniec linijki
			{
				int numberz = stringToNumber(number);//konwersja wczytanej wartosci na int i wrzucenie jej na stos
				values.push(numberz);
				number = "";

				//jesli to nie koniec to mamy do czynienia z operatorem
				if (s.length() != i + 1)
				{
					char sign = s[iter]; //pobranie operatora z ciagu

					//do puki lista operatorow nie jest pusta, a przy tym wykonujemy dzialania o wyzszym priorytecie niz obecny
					while (!operators.empty() && lowerPriority(operators.top(), sign))
					{
						calculate(operators.pop(), values); //wykonaj dzialanie pobrane ze stosu operatorow na stosie wartosci
					}
					operators.push(sign);//wrzuc aktualny operator na stos jako ten o nizszym priotytecie
				}
			}
			iter++;
		}

		//doszlismy do sytuacji, gdzie trzeba podliczyc to co zostalo (same operacje rownorzedne). Do puki istneje wi�cej niz jedna wartosc, to trzeba obliczac z uwzglednieneim ostatniego operatora
		while (values.size()>1)
		{
			calculate(operators.pop(), values);
		}

		//sprawdzenie czy calosc jest prawdopodobnie poprawna
		if (values.size() != 1)
			cout << "Blad obliczania!" << endl;
		cout << "= " << values.pop() << endl; //wypisanie wyniku - ostatniej wartosc ze stosu. teraz stos jest pusty
	} while (true);

	return 0;
}