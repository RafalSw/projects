#define _CRT_SECURE_NO_WARNINGS
//Karol Ruprecht

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <fstream>

using namespace std;

const int rozmiarMax = 10000000;
struct Element{
	int wartosc;
	int dane;
};

struct ElementListy{
	Element element;
	ElementListy *nastepny;
};


Element *tabLosowe;
ElementListy *pierwszy;



void swap(int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}
void swap(Element *a, Element *b){
	Element temp = *a;
	*a = *b;
	*b = temp;
}

void przeladujListe(Element *tab, int liczbaEl)
{
	ElementListy* poprzedni = NULL;
	for (int i = 0; i < liczbaEl; i++)
		{
			if(i==0)
			{
				pierwszy = (ElementListy*)malloc(sizeof(ElementListy));
				pierwszy->element = tabLosowe[i];
				pierwszy->nastepny = NULL;
				poprzedni = pierwszy;
			}
			else
			{
				ElementListy* nowy = (ElementListy*)malloc(sizeof(ElementListy));
				nowy->element = tabLosowe[i];
				nowy->nastepny = NULL;
				poprzedni->nastepny = nowy;
				poprzedni = nowy;
			}
		}
}

void wczytywanieDanych(){
	FILE *plik;
	plik = fopen("dane.txt", "r");
	if (plik != NULL){
		printf("Otwieranie pliku do oczytu");
		ElementListy* poprzedni = NULL;
		for (int i = 0; i < rozmiarMax; i++)
		{
			fscanf(plik, "%d,%d\t", &(tabLosowe[i].wartosc),&(tabLosowe[i].dane));
		}
	}
	else
	{
		printf("Otwieranie pliku do zapisu.");
		//nie ma danych do wczytania - generujemy
		plik = fopen("dane.txt", "w");
		srand(time(NULL));
		
		ElementListy* poprzedni = NULL;
		for (int i = 0; i < rozmiarMax; i++)
		{
			tabLosowe[i].wartosc = rand() % 200 - 100;
			tabLosowe[i].dane = rand() % 200 - 100;
			
			fprintf(plik, "%d,%d\t", tabLosowe[i].wartosc,tabLosowe[i].dane);
		}
	}

	fclose(plik);
}


void odwrocTablice(Element *tab, int iloscEl){
	int x = iloscEl - 1;
	for (int i = 0; i < x; i++, x--)
	{
		Element temp = tab[i];
		tab[i] = tab[x];
		tab[x] = temp;
	}
}


//--------------------------- POCZATEK GRUPY I --------------------------------
void sortowanieWstawianie(Element *tab, int iloscEl)
{
	int i, j;
	Element tmp;
	for (i = 1; i<iloscEl; i++)
	{
		j = i;
		tmp = tab[i];
		while (tab[j - 1].wartosc>tmp.wartosc && j > 0)
		{
			tab[j] = tab[j - 1];

			j--;
		}
		tab[j] = tmp;
	}
}

void sortowanieSelekcja(int* tab, int size)
{
	int k; //indeks najmniejszego elementu
	int i; //analizowane miejsce - indeks dzielacy na czesc posortowana i nieposortowaną
	int j; //indeksy na prawo od analizowanego miejsca - potrzebne do poszukiwania najmniejszego elementu
	int temp;
	for (i = 0; i < size - 1; i++)
	{
		k = i;
		for (j = i + 1; j < size; j++){
			if (tab[j] < tab[k])
				k = j;
		}
		temp = tab[k];
		tab[k] = tab[i];
		tab[i] = temp;
	}
}
/*
void sortowanieSelekcja(Element *tab, int iloscEl)
{
	int i, j, min;
	Element tmp;
	for (i = 0; i < iloscEl; i++)
	{
		min = i;
		for (j = i + 1; j < iloscEl; j++)
		{
			if (tab[j].wartosc < tab[min].wartosc)
				min = j;
			tmp = tab[min];
			tab[min] = tab[i];
			tab[i] = tmp;
		}
	}
}*/

void sortowanieBabelkowe(Element *tab, int iloscEl){
	int n = iloscEl;
	do
	{
		for (int i = 0; i < n - 1; i++)
		{
			if (tab[i].wartosc > tab[i + 1].wartosc)
			{
				swap(&tab[i], &tab[i + 1]);
			}
		}
		n = n - 1;
	} while (n > 1);
}
//----------------------------- KONIEC GRUPY I --------------------------------
//--------------------------- POCZATEK GRUPY II -------------------------------
void quickSort(Element *tab, int lewy, int prawy)
{
	int i, j;
	int piwot, piwotdana;
	i = (lewy + prawy) / 2;
	piwot = tab[i].wartosc;
	piwotdana = tab[i].dane;
	tab[i] = tab[prawy];
	for (j = i = lewy; i < prawy; i++)
		if (tab[i].wartosc < piwot)
		{
			swap(&tab[i], &tab[j]);
			j++;
		}
	tab[prawy] = tab[j];
	tab[j].wartosc = piwot;
	tab[j].dane = piwotdana;
	if (lewy < j - 1)  quickSort(tab, lewy, j - 1);
	if (j + 1 < prawy) quickSort(tab, j + 1, prawy);
}


void sortowanieShella(Element *tab, int iloscEl)
{
	Element x;
	int i,h = 1;
	while(h<iloscEl)
		h = 3*h+1;
	if(h==0)
		h=1;

	while(h>0)
	{
		for(int j = iloscEl - h - 1; j >= 0; j--)
		{
			x = tab[j];
			i=j+h;

			while((i < iloscEl) && (tab[i].wartosc < x.wartosc))
			{
				tab[i-h] = tab[i];
				i+=h;
			}
			tab[i-h]=x;
		}
		h/=3;
	}
}


void kopcowanie(Element *tab, int l, int r)
{
	Element x;
	int j, k;
	j=l;
	k=2*j;
	x=tab[j];

	while (k<r)
	{
		if (k+1<=r)
		{
			if (tab[k].wartosc<tab[k+1].wartosc)
				k+=1;
		}
		if (x.wartosc<tab[k].wartosc)
		{
			tab[j]=tab[k];
			j=k;
			k=2*j;
		}
		else k=r+1;
	}
	tab[j]=x;
}


void zbudujKopiec(Element *tab, int iloscEl)
{
	for(int i = iloscEl - 1;i>=0;i--)
	{
		kopcowanie(tab, i, iloscEl);
	}
}


void sortowaniePrzezKopcowanie(Element *tab, int iloscEl)
{
	zbudujKopiec(tab, iloscEl);
	for(int i = iloscEl-1; i >= 0; i--)
	{
		swap(&tab[0],&tab[i]);
		kopcowanie(tab, 0, i-1);
	}
}
//----------------------------- KONIEC GRUPY II -------------------------------
void sortowanieBabelkoweListy(){
	ElementListy *aktualny = pierwszy;
	ElementListy *poprzedni = NULL;
	long int licznik = 0;
	while(aktualny!=NULL)
	{
		licznik++;
		aktualny = aktualny->nastepny;
	}
	aktualny = pierwszy;
	while(licznik-->0)
	{
		while(aktualny!=NULL)
		{
			if(aktualny->nastepny != NULL)
			{
				if(aktualny->element.wartosc > aktualny->nastepny->element.wartosc)
				{
					if(poprzedni!=NULL)
					{
						poprzedni->nastepny = aktualny->nastepny;
						aktualny->nastepny = poprzedni->nastepny->nastepny;
						poprzedni->nastepny->nastepny = aktualny;
					}
					else
					{
						
						pierwszy = aktualny->nastepny;
						aktualny->nastepny = aktualny->nastepny->nastepny;
						pierwszy->nastepny=aktualny;
					}
				}
			}
			poprzedni = aktualny;
			aktualny = aktualny->nastepny;
		}
		aktualny = pierwszy;
		poprzedni = NULL;
	}
}
void wyswietlTablice(Element tab[], int iloscEl){
	printf("Wyswietlanie tablicy:\n");
	for (int i = 0; i < iloscEl; i++)
	{
		printf("%d. Wartosc: %d, Dane:%d\n", i, tab[i].wartosc, tab[i].dane);
	}
}

int main(int argc, char* argv[])
{
	tabLosowe = (Element*)malloc((rozmiarMax+1)*sizeof(Element));
	wczytywanieDanych();
	

	Element *tab;
	double k = 6. / 19.;
	int iloscEl;
	double czas;
	clock_t time1, time2;
	FILE *plik;
	for (int iloscEl = 1000; iloscEl <= 100000; iloscEl*=3)
	{
		tab = (Element*)malloc(sizeof(Element)* iloscEl);
		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);

		plik = fopen("wyniki.txt", "a");

		
		//wypelnijTablice(tab, iloscEl);
		printf("Sortowanie przez wstawianie:\n");
		fprintf(plik,"Sortowanie przez wstawianie - %d elementow:\n", iloscEl);
		//sortowanie losowych elementow
		time1 = clock();
		sortowanieWstawianie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "1. %.3f\n", czas);
		//sortowanie posortowanych elementow
		time1 = clock();
		sortowanieWstawianie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "2. %.3f\n", czas);
		//sortowanie odwrotnie posortowanych elementow
		odwrocTablice(tab, iloscEl);
		time1 = clock();
		sortowanieWstawianie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "3. %.3f\n\n", czas);

		
		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);
		printf("Sortowanie przez selekcje:\n");
		fprintf(plik,"Sortowanie przez selekcje - %d elementow:\n", iloscEl);
		//sortowanie losowych elementow
		time1 = clock();
		sortowanieSelekcja(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "1. %.3f\n", czas);
		//sortowanie posortowanych elementow
		time1 = clock();
		sortowanieSelekcja(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "2. %.3f\n", czas);
		//sortowanie odwrotnie posortowanych elementow
		odwrocTablice(tab, iloscEl);
		time1 = clock();
		sortowanieSelekcja(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "3. %.3f\n\n", czas);
		
		
		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);
		printf("Sortowanie babelkowe:\n");
		fprintf(plik,"Sortowanie babelkowe - %d elementow:\n", iloscEl);
		//sortowanie losowych elementow
		time1 = clock();
		sortowanieBabelkowe(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "1. %.3f\n", czas);
		//sortowanie posortowanych elementow
		time1 = clock();
		sortowanieBabelkowe(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "2. %.3f\n", czas);
		//sortowanie odwrotnie posortowanych elementow
		odwrocTablice(tab, iloscEl);
		time1 = clock();
		sortowanieBabelkowe(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "3. %.3f\n\n", czas);

		free(tab);
		fclose(plik);
	}

	for (int iloscEl = 1000; iloscEl <= rozmiarMax; iloscEl*=5)
	{
		tab = (Element*)malloc(sizeof(Element)* iloscEl);
		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);

		plik = fopen("wyniki.txt", "a");
		
		if(iloscEl<1000000)
		{
			printf("QucikSort:\n");
			fprintf(plik,"Sortowanie Quicksort - %d elementow:\n", iloscEl);
			//sortowanie losowych elementow
			time1 = clock();
			quickSort(tab, 0, iloscEl - 1);
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
			fprintf(plik, "1. %.3f\n", czas);
			//sortowanie posortowanych elementow
			time1 = clock();
			quickSort(tab, 0, iloscEl - 1);
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
			fprintf(plik, "2. %.3f\n", czas);
			//sortowanie odwrotnie posortowanych elementow
			odwrocTablice(tab, iloscEl);
			time1 = clock();
			quickSort(tab, 0, iloscEl - 1);
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n\n", iloscEl, czas);
			fprintf(plik, "3. %.3f\n\n", czas);
		}
		
		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);
		printf("Sortowanie Shella:\n");
		fprintf(plik,"Sortowanie Shella - %d elementow:\n", iloscEl);
		//sortowanie losowych elementow
		time1 = clock();
		sortowanieShella(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "1. %.3f\n", czas);
		//sortowanie posortowanych elementow
		time1 = clock();
		sortowanieShella(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "2. %.3f\n", czas);
		//sortowanie odwrotnie posortowanych elementow
		odwrocTablice(tab, iloscEl);
		time1 = clock();
		sortowanieShella(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n\n", iloscEl, czas);
		fprintf(plik, "3. %.3f\n\n", czas);

		memcpy(tab,tabLosowe,sizeof(Element)* iloscEl);
		printf("Sortowanie przez kopcowanie:\n");
		fprintf(plik,"Sortowanie przez kopcowanie - %d elementow:\n", iloscEl);
		//sortowanie losowych elementow
		time1 = clock();
		sortowaniePrzezKopcowanie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "1. %.3f\n", czas);
		//sortowanie posortowanych elementow
		time1 = clock();
		sortowaniePrzezKopcowanie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
		fprintf(plik, "2. %.3f\n", czas);
		//sortowanie odwrotnie posortowanych elementow
		odwrocTablice(tab, iloscEl);
		time1 = clock();
		sortowaniePrzezKopcowanie(tab, iloscEl);
		time2 = clock();
		czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
		printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n\n", iloscEl, czas);
		fprintf(plik, "3. %.3f\n\n", czas);

		if(iloscEl<100000)
		{
			przeladujListe(tabLosowe,iloscEl);
			printf("Sortowanie babelkowe listy:\n");
			fprintf(plik,"Sortowanie babelkowe listy - %d elementow:\n", iloscEl);
			//sortowanie losowych elementow
			time1 = clock();
			sortowanieBabelkoweListy();
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d losowych elementow z czasem: %.3lf s.\n", iloscEl, czas);
			fprintf(plik, "1. %.3f\n", czas);
			//sortowanie posortowanych elementow
			przeladujListe(tab,iloscEl);
			time1 = clock();
			sortowanieBabelkoweListy();
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d posortowanych elementow z czasem: %.3lf s.\n", iloscEl, czas);
			fprintf(plik, "2. %.3f\n", czas);
			//sortowanie odwrotnie posortowanych elementow
			odwrocTablice(tab, iloscEl);
			przeladujListe(tab,iloscEl);
			time1 = clock();
			sortowanieBabelkoweListy();
			time2 = clock();
			czas = (double)(time2 - time1) / CLOCKS_PER_SEC;
			printf("Dla %d posortowanych odwrotnie elementow z czasem: %.3lf s.\n\n", iloscEl, czas);
			fprintf(plik, "3. %.3f\n\n", czas);
		}

		free(tab);
		fclose(plik);
	}
	getchar();
	return 0;
}