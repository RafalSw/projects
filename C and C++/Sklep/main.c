#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h> 
#include <string.h> //dla strlen
#include <ctype.h> //dla tolower

#define ROZMIAR_MAGAZYNU 200


typedef struct
{
	char nazwa[32];
	float cena;
	int ilosc;
	char data[12];
}Towar;


int porownanieNazw(char nazwa1[32], char nazwa2[32]) //funkcja porownujaca 2 ciagi znakow. Jesli nazwa1 jest alfabetycznie wieksza, niz nazwa2, to zwracane jest 1, w p.p. 0
{
	//Zbadanie dlugosci slow
	int dlugosc1 = strlen(nazwa1);
	int dlugosc2 = strlen(nazwa2);
	int i;
	int min;
	if (dlugosc1 < dlugosc2)
	{
		min = dlugosc1;
	}
	else
	{
		min = dlugosc2;
	}

	for (i = 0; i < min; i++) //petla od poczatku wyrazow do konca krotszego z nich
	{
		if (tolower(nazwa1[i]) > tolower(nazwa2[i]))
			return 1;
		else if (tolower(nazwa1[i] == tolower(nazwa2[i]))) //na razie znaki sa rowne, porownujemy kolejne
			continue;
		else
			return 0;
	}

	//jesli do tej pory funkcja sie nie zakonczyla, to znaczy, ze doszlismy do konca krotszego wyrazu

	if (i == dlugosc1)//doszlismy do konca nazwa1
		return 0;
	else //doszlismy do konca nazwa2
		return 1;
}

void sortowanie(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar, int kryterium) //kryterium: 0-wg. ceny, 1-wg.nazwy
{
	int i, j;
	int rozmiar = *aktualnyRozmiar;
	//sortowanie babelkowe:
	for (i = 0; i < rozmiar - 1; i++)
	{
		for (j = 0; j < rozmiar - 1; j++)
		{
			if ((kryterium == 0 && towary[j].cena > towary[j + 1].cena) || (kryterium == 1 && porownanieNazw(towary[j].nazwa, towary[j + 1].nazwa) == 1)) //zaleznie od kryterium porownujemy w inny sposob
			{
				Towar temp = towary[j];
				towary[j] = towary[j + 1];
				towary[j + 1] = temp;
			}
		}
	}
}

void dodaj(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	if (*aktualnyRozmiar == ROZMIAR_MAGAZYNU)
	{
		printf("Magazyn jest juz pelny!\n");
		return;
	}

	printf("Podaj nazwe produktu: ");
	scanf("%s", towary[*aktualnyRozmiar].nazwa);
	printf("Podaj cene: ");
	scanf("%f", &(towary[*aktualnyRozmiar].cena));
	printf("Podaj ilosc: ");
	scanf("%d", &(towary[*aktualnyRozmiar].ilosc));
	printf("Podaj date spozycia: ");
	scanf("%s", &(towary[*aktualnyRozmiar].data));

	(*aktualnyRozmiar)++;


}

void wypisz(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{

	int i;
	printf("Towary w sklepie:\nnr.\tNazwa\tCena\tIlosc\tData spozycia\n");
	for (i = 0; i < *aktualnyRozmiar; i++)
	{
		printf("%d\t%s\t%.2f\t%d\t%s\n", i, towary[i].nazwa, towary[i].cena, towary[i].ilosc, towary[i].data);
	}
}

void wartoscDanegoTowaru(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	int numerDoWyswietlenia;
	printf("Wartosc ktorego towaru mam wyswietlic? ");
	scanf("%d", &numerDoWyswietlenia);
	if (numerDoWyswietlenia < 0 || numerDoWyswietlenia >= *aktualnyRozmiar)
	{
		printf("Nie ma takiego elementu!\n");
	}
	else
	{
		printf("Wartosc towaru %s to %.2f\n", towary[numerDoWyswietlenia].nazwa, towary[numerDoWyswietlenia].ilosc*towary[numerDoWyswietlenia].cena);
	}
}
void wartoscWszystkichTowarow(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	printf("Wartosci kolejnych towatow:\n");
	int i;
	for (i = 0; i < *aktualnyRozmiar;i++)
		printf("\tWartosc %s to %.2f\n", towary[i].nazwa, towary[i].ilosc*towary[i].cena);
}

void wyszukaj(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	char szukanaNazwa[32];
	int i;
	int czyZnaleziono = 0;
	printf("Podaj nazwe wyszukiwanego towaru: ");
	scanf("%s", szukanaNazwa);

	for (i = 0; i < *aktualnyRozmiar; i++)
	{
		if (strcmp(szukanaNazwa, towary[i].nazwa) == 0)//jesli wartosci sa takie same
		{
			czyZnaleziono = 1;
			printf("Znaleziono towar %s. Cena: %.2f, ilosc sztuk: %d, wartosc: %.2f, data przydatnosci %s\n", towary[i].nazwa, towary[i].cena, towary[i].ilosc, towary[i].ilosc*towary[i].cena, towary[i].data);
		}
	}

	if (czyZnaleziono == 0)
	{
		printf("Nie ma takiego towaru!\n");
	}
}

void zapiszDane(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	int i;
	FILE *fp;
	fp = fopen("towary.txt", "w");

	if (fp == NULL)
	{
		printf("Nie udalo sie otworzyc pliku do zapisu!");
		return;
	}

	for (i = 0; i < *aktualnyRozmiar; i++) {
		fprintf(fp, "%-20d %-30s %-30f %-20d %-20s \n", i, towary[i].nazwa, towary[i].cena, towary[i].ilosc, towary[i].data);
	}

	fclose(fp);
}

void wczytajDane(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	FILE *fp;
	int i;
	char nazwa[32];
	float cena;
	int ilosc;
	char data[12];
	fp = fopen("towary.txt", "r");

	if (fp == NULL)
	{
		printf("Nie udalo sie odtworzyc pliku do odczytu!");
		return;
	}
	printf("Wczytywanie z pliku:\n");

	while (fscanf(fp, "%d %s %f %d %s\n", &i, nazwa, &cena, &ilosc, data) == 5)
	{
		if (*aktualnyRozmiar == ROZMIAR_MAGAZYNU)
		{
			printf("Magazyn jest juz pelny! Koniec wczytywania danych\n");
			break;
		}

		strcpy(towary[*aktualnyRozmiar].nazwa, nazwa); //strcpy kopiuje wartosc z nazwa do tablicy towwary
		towary[*aktualnyRozmiar].cena = cena;
		towary[*aktualnyRozmiar].ilosc = ilosc;
		strcpy(towary[*aktualnyRozmiar].data, data);
		printf("%d\t%s\t%.2f\t%d\t%s\n", *aktualnyRozmiar, towary[*aktualnyRozmiar].nazwa, towary[*aktualnyRozmiar].cena, towary[*aktualnyRozmiar].ilosc, towary[*aktualnyRozmiar].data);
		(*aktualnyRozmiar)++;
	}
	fclose(fp);
}


/* void edytuj(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{

int numerDoEdycji;
printf("Podaj numer towaru, ktory chcesz edytowac: ");
scanf("%d", &numerDoEdycji);
if (numerDoEdycji < 0 || numerDoEdycji >= *aktualnyRozmiar)
{
printf("Nie ma takiego elementu!\n");
return;
}
printf("Aktualne dane:\n\tNazwa: %s\n\tCena: %.2f\n\tIlosc: %d\n", towary[numerDoEdycji].nazwa, towary[numerDoEdycji].cena, towary[numerDoEdycji].ilosc);
printf("Podaj nowa nazwe: ");
scanf("%s", towary[numerDoEdycji].nazwa);
printf("Podaj nowa cene: ");
scanf("%f", &(towary[numerDoEdycji].cena));
printf("Podaj nowa ilosc: ");
scanf("%d", &(towary[numerDoEdycji].ilosc));
}

void usun(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
int numerDoEdycji;
int i;
printf("Podaj numer towaru, ktory chcesz usunac: ");
scanf("%d", &numerDoEdycji);
if (numerDoEdycji < 0 || numerDoEdycji >= *aktualnyRozmiar)
{
printf("Nie ma takiego elementu!\n");
return;
}

for (i = numerDoEdycji; i < *aktualnyRozmiar - 1; i++)
{
towary[i] = towary[i + 1];
}
(*aktualnyRozmiar)--;
}
*/

void menu(Towar towary[ROZMIAR_MAGAZYNU], int *aktualnyRozmiar)
{
	while (1)
	{
		char wybor;
		printf("              MENU\n");
		printf("1. Dodaj nowy produkt do sklepu\n");
		printf("2. Pokaz produkty dostepne w sklepie\n");
		printf("3. Sortowanie wg. ceny\n");
		printf("4. Sortowanie wg. nazwy\n");
		printf("5. Zapisz wprowadzone dane\n");
		printf("6. Wczytaj dane z pliku\n");
		printf("7. Pokaz wartosc danego towaru\n");
		printf("8. Pokaz wartosc wszystkich towarow\n");
		printf("9. Wyszukaj w sklepie\n");
		printf("0. Wyjdz ze sklepu\n");

		scanf(" %c", &wybor);
		switch (wybor)
		{
		case '1':
			dodaj(towary, aktualnyRozmiar);
			break;
		case '2':
			wypisz(towary, aktualnyRozmiar);
			break;
		case '3':
			sortowanie(towary, aktualnyRozmiar, 0);
			wypisz(towary, aktualnyRozmiar);
			break;
		case '4':
			sortowanie(towary, aktualnyRozmiar, 1);
			wypisz(towary, aktualnyRozmiar);
			break;
		case '5':
			zapiszDane(towary, aktualnyRozmiar);
			break;
		case '6':
			wczytajDane(towary, aktualnyRozmiar);
			break;
		case '7':
			wartoscDanegoTowaru(towary, aktualnyRozmiar);
			break;
		case '8':
			wartoscWszystkichTowarow(towary, aktualnyRozmiar);
			break;
		case '9':
			wyszukaj(towary, aktualnyRozmiar);
			break;
		case '0':
			printf("Koniec\n");
			exit(0);
		}
	}
}



int main()
{
	Towar towary[ROZMIAR_MAGAZYNU];
	int iloscTowarow = 0;

	menu(towary, &iloscTowarow);

	return 0;
}