#include<stdio.h>
#include<conio.h>
#include<iostream>

using namespace std;

const int MIN_DEGREE = 0;

struct BDNode
{
	int *data;
	BDNode **child_ptr;
	bool leaf;
	int n;
}*root = NULL, *l = NULL, *x = NULL;

void usun(int k, BDNode *root);

BDNode * init()   // init - licznik
{
	int i;
	l = new BDNode;
	l->data = new int[5];
	l->child_ptr = new BDNode *[6];
	l->leaf = true;
	l->n = 0;
	for (i = 0; i < 6; i++)
	{
		l->child_ptr[i] = NULL;
	}
	return l;
}

int split_child(BDNode *x, int i)  // podzia� "dzieci" drzewa B
{
	int j, mid;
	BDNode *l1, *l3, *y;
	l3 = init();   // 
	l3->leaf = true;
	if (i == -1)
	{
		mid = x->data[2];
		x->data[2] = 0;
		x->n--;
		l1 = init();
		l1->leaf = false;
		x->leaf = true;
		for (j = 3; j < 5; j++)
		{
			l3->data[j - 3] = x->data[j];
			l3->child_ptr[j - 3] = x->child_ptr[j];
			l3->n++;
			x->data[j] = 0;
			x->n--;
		}
		for (j = 0; j < 6; j++)
		{
			x->child_ptr[j] = NULL;
		}
		l1->data[0] = mid;
		l1->child_ptr[l1->n] = x;
		l1->child_ptr[l1->n + 1] = l3;
		l1->n++;
		root = l1;
	}
	else
	{
		y = x->child_ptr[i];
		mid = y->data[2];
		y->data[2] = 0;
		y->n--;
		for (j = 3; j < 5; j++)
		{
			l3->data[j - 3] = y->data[j];
			l3->n++;
			y->data[j] = 0;
			y->n--;
		}
		x->child_ptr[i + 1] = y;
		x->child_ptr[i + 1] = l3;
	}
	return mid;
}

void sortowanie(int *p, int n)  // sortowanie drzewa B
{
	int i, j, temp;
	for (i = 0; i < n; i++)
	{
		for (j = i; j <= n; j++)
		{
			if (p[i] > p[j])
			{
				temp = p[i];
				p[i] = p[j];
				p[j] = temp;
			}
		}
	}
}

void insert(int a)  // wstawianie
{
	int i, temp;
	x = root;
	if (x == NULL)
	{
		root = init();
		x = root;
	}
	else
	{
		if (x->leaf == true && x->n == 5)
		{
			temp = split_child(x, -1);
			x = root;
			for (i = 0; i < (x->n); i++)
			{
				if ((a > x->data[i]) && (a < x->data[i + 1]))
				{
					i++;
					break;
				}
				else if (a < x->data[0])
				{
					break;
				}
				else
				{
					continue;
				}
			}
			x = x->child_ptr[i];
		}
		else
		{
			while (x->leaf == false)
			{
				for (i = 0; i < (x->n); i++)
				{
					if ((a > x->data[i]) && (a < x->data[i + 1]))
					{
						i++;
						break;
					}
					else if (a < x->data[0])
					{
						break;
					}
					else
					{
						continue;
					}
				}
				if ((x->child_ptr[i])->n == 5)
				{
					temp = split_child(x, i);
					x->data[x->n] = temp;
					x->n++;
					continue;
				}
				else
				{
					x = x->child_ptr[i];
				}
			}
		}
	}
	x->data[x->n] = a;
	sortowanie(x->data, x->n);
	x->n++;
}
void przejscie(BDNode *p)  // przejscie po drzewie
{
	cout << endl;
	int i;
	for (i = 0; i < p->n; i++)
	{
		if (p->leaf == false)
		{
			przejscie(p->child_ptr[i]);
		}
		cout << " " << p->data[i];
	}
	if (p->leaf == false)
	{
		przejscie(p->child_ptr[i]);
	}

}

// funkcja poszukujaca klucza
int findKey(int k, BDNode *root)
{
	int temp = 0;
	while (temp < root->n && root->data[temp] < k)
	{
		++temp;
	}
	return temp;
}

//funkcja poszukujaca poprzednika
int getPre(int temp, BDNode* root)
{
	BDNode *current = root->child_ptr[temp];
	while (!current->leaf)
	{
		current = current->child_ptr[current->n];
	}

	return current->data[current->n - 1];
}

//funkcja poszukujaca nastepnika
int getSuccessor(int temp, BDNode* root)
{
	BDNode *current = root->child_ptr[temp + 1];
	while (!current->leaf)
	{
		current = current->child_ptr[0];
	}

	return current->data[0];
}

//usuwanie wartosci z liscia
void removeFromLeaf(int temp, BDNode* root)
{
	for (int i = temp + 1; i < root->n; ++i)
	{
		root->data[i - 1] = root->data[i];
	}

	root->n--;
	return;
}

//funkcja laczaca po usunieciu
void merge(int temp)
{
	BDNode *child = root->child_ptr[temp];
	BDNode *sibling = root->child_ptr[temp + 1];

	child->data[MIN_DEGREE - 1] = root->data[temp];

	for (int i = 0; i < sibling->n; ++i)
	{
		child->data[i + MIN_DEGREE] = sibling->data[i];
	}
	if (!child->leaf)
	{
		for (int i = 0; i <= sibling->n; ++i)
		{
			child->child_ptr[i + MIN_DEGREE] = sibling->child_ptr[i];
		}
	}

	for (int i = temp + 1; i < root->n; ++i)
	{
		root->data[i - 1] = root->data[i];
	}

	for (int i = temp + 2; i <= root->n; ++i)
	{
		root->child_ptr[i - 1] = root->child_ptr[i];
	}
	child->n += sibling->n + 1;
	root->n--;

	//zwalnianie pamieci
	delete(sibling);
	return;
}

// usuniecie wartosci z wezla, ktory nie jest lisciem
void removeFromNonLeaf(int temp, BDNode* root)
{
	int k = root->data[temp];

	if (root->child_ptr[temp]->n >= MIN_DEGREE)
	{
		int pred = getPre(temp, root);
		root->data[temp] = pred;
		usun(pred, root->child_ptr[temp]);
	}
	else if (root->child_ptr[temp + 1]->n >= MIN_DEGREE)
	{
		int succ = getSuccessor(temp, root);
		root->data[temp] = succ;
		usun(succ, root->child_ptr[temp + 1]);
	}
	else
	{
		merge(temp);
		usun(k, root->child_ptr[temp]);
	}
	return;
}

//funkcja pomocnicza, pozyczanie z poprzedniego drzewa
void borrowFromPrev(int temp, BDNode* root)
{

	BDNode *child = root->child_ptr[temp];
	BDNode *sibling = root->child_ptr[temp - 1];

	for (int i = child->n - 1; i >= 0; --i)
	{
		child->data[i + 1] = child->data[i];
	}

	if (!child->leaf)
	{
		for (int i = child->n; i >= 0; --i)
		{
			child->child_ptr[i + 1] = child->child_ptr[i];
		}
	}

	child->data[0] = root->data[temp - 1];

	if (!root->leaf)
	{
		child->child_ptr[0] = sibling->child_ptr[sibling->n];
	}

	root->data[temp - 1] = sibling->data[sibling->n - 1];

	child->n += 1;
	sibling->n -= 1;
	return;
}

//funkcja pomocnicza, pozyczanie z nastepnego drzewa
void borrowFromNext(int temp, BDNode* root)
{
	BDNode *child = root->child_ptr[temp];
	BDNode *sibling = root->child_ptr[temp + 1];

	child->data[(child->n)] = root->data[temp];

	if (!(child->leaf))
	{
		child->child_ptr[(child->n) + 1] = sibling->child_ptr[0];
	}

	root->data[temp] = sibling->data[0];

	for (int i = 1; i < sibling->n; ++i)
	{
		sibling->data[i - 1] = sibling->data[i];
	}

	if (!sibling->leaf)
	{
		for (int i = 1; i <= sibling->n; ++i)
		{
			sibling->child_ptr[i - 1] = sibling->child_ptr[i];
		}
	}

	child->n += 1;
	sibling->n -= 1;

	return;
}

// funckja pomocnicza, wypelnianie potomka
void fill(int temp, BDNode* root)
{
	if (temp != 0 && root->child_ptr[temp - 1]->n >= MIN_DEGREE)
	{
		borrowFromPrev(temp, root);
	}
	else if (temp != root->n && root->child_ptr[temp + 1]->n >= MIN_DEGREE)
	{
		borrowFromNext(temp, root);
	}
	else
	{
		if (temp != root->n)
			merge(temp);
		else
			merge(temp - 1);
	}
	return;
}

void usun(int k, BDNode *root)
{
	int temp = findKey(k, root); //poszukiwanie klucza

	// sprawdzenie czy element do usuniecia znajduje sie w tym wezle i proba usuniecia
	if (temp < root->n && root->data[temp] == k)
	{

		if (root->leaf)
			removeFromLeaf(temp,root);
		else
			removeFromNonLeaf(temp,root);
	}
	else
	{
		if (root->leaf)
		{
			//usuwany klucz nie wystepuje w drzewie
			return;
		}

		bool flag = ((temp == root->n) ? true : false);

		if (root->child_ptr[temp]->n < MIN_DEGREE)
		{
			fill(temp, root);
		}

		if (flag && temp > root->n)
			usun(k, root->child_ptr[temp - 1]);
		else
			usun(k, root->child_ptr[temp]);
	}
	return;
}

int main()
{
	int i, n, r;
	cout << "Jaka liczbe elementow chcesz dodac?\n";
	cin >> n;
	for (i = 0; i < n; i++)
	{
		cout << "Dodaj element\n";
		cin >> r;
		insert(r);
	}
	cout << "Przejscie przez skonstruowane drzewo\n";
	przejscie(root);

	cout << "\n\nIle liczb chcesz usunac?";
	cin >> n;
	for (i = 0; i < n; i++)
	{
		cout << "Usun element\n";
		cin >> r;
		usun(r,root);
		cout << "Przejscie przez drzewo\n";
		przejscie(root);
	}
	

	system("PASUE");
	return 0;
}