#include <iostream>
#include <cstdlib>
#include <ctime>

using namespace std;

bool merge(int* tab, int start, int mid, int stop) //wskaznik na tablice wraz z ich dlugosciami
{
	if(tab == NULL) //wskaznik na tablice jest pusty
		return false;
	if(start>stop)
		return false;
	if(mid<start)
		return false;
	if(mid>stop)
		return false;



	int* result = new int[stop-start+1];
	
	int tab1Iterator=start, tab2Iterator=mid+1, resultIterator=0;

	while(tab1Iterator<=mid && tab2Iterator<=stop) //nie doszlismy do konca zadnej z tablic
	{
		if(tab[tab1Iterator]<tab[tab2Iterator])
		{
			result[resultIterator]=tab[tab1Iterator];
			resultIterator++;
			tab1Iterator++;
		}
		else
		{
			result[resultIterator]=tab[tab2Iterator];
			resultIterator++;
			tab2Iterator++;
		}
	}
	
	
	while(tab1Iterator<=mid) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
	{
		result[resultIterator]=tab[tab1Iterator];
		resultIterator++;
		tab1Iterator++;
	}
	
	while(tab2Iterator<=stop) //tab2 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
	{
		result[resultIterator]=tab[tab2Iterator];
		resultIterator++;
		tab2Iterator++;
	}
	
	for(int i=0;i<resultIterator;i++)
	{
		tab[i+start]=result[i];
	}

	delete[] result;

	return true;
}


bool merge3(int* tab, int start, int mid1, int mid2, int stop) //merge dla funkcji mergesort3 - dzieli na 3 podzbiory
{
	if(tab == NULL) //wskaznik na tablice jest pusty
		return false;
	if(start>stop)
		return false;
	if(mid1<start)
		return false;
	if(mid2>stop)
		return false;
	if(mid1>mid2)
		return false;



	int* result = new int[stop-start+1];
	
	int tab1Iterator=start, tab2Iterator=mid1+1, tab3Iterator=mid2+1,resultIterator=0;


	while(tab1Iterator<=mid1 && tab2Iterator<=mid2 && tab3Iterator<=stop) //nie doszlismy do konca zadnej z tablic
	{
		
		if(tab[tab1Iterator]<tab[tab2Iterator] && tab[tab1Iterator]<tab[tab3Iterator]) // najmniejszy jest w 1 czesci
		{
			result[resultIterator]=tab[tab1Iterator];
			resultIterator++;
			tab1Iterator++;
		}
		else if(tab[tab2Iterator]<tab[tab1Iterator] && tab[tab2Iterator]<tab[tab3Iterator]) // najmniejszy jest w 2 czesci
		{
			result[resultIterator]=tab[tab2Iterator];
			resultIterator++;
			tab2Iterator++;
		}
		else // najmniejszy jest w 3 czesci
		{
			result[resultIterator]=tab[tab3Iterator];
			resultIterator++;
			tab3Iterator++;
		}
	}
	

	//doszlismy do konca jednej z 3 tablic. Teraz trzeba scalic pozostale 2 tak jak jest to w merge sort

	if(tab3Iterator>stop) //doszlismy do konca 3ciej czesci tablicy
	{
		while(tab1Iterator<=mid1 && tab2Iterator<=mid2) //nie doszlismy do konca zadnej z pozostałych tablic
		{
			if(tab[tab1Iterator]<tab[tab2Iterator])
			{
				result[resultIterator]=tab[tab1Iterator];
				resultIterator++;
				tab1Iterator++;
			}
			else
			{
				result[resultIterator]=tab[tab2Iterator];
				resultIterator++;
				tab2Iterator++;
			}
		}
	
	
		while(tab1Iterator<=mid1) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab1Iterator];
			resultIterator++;
			tab1Iterator++;
		}
	
		while(tab2Iterator<=mid2) //tab2 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab2Iterator];
			resultIterator++;
			tab2Iterator++;
		}
	}
	else if(tab1Iterator>mid1) //doszlismy do konca pierwszej czesci tablicy
	{
		while(tab2Iterator<=mid2 && tab3Iterator<=stop) //nie doszlismy do konca zadnej z pozostałych tablic
		{
			if(tab[tab2Iterator]<tab[tab3Iterator])
			{
				result[resultIterator]=tab[tab2Iterator];
				resultIterator++;
				tab2Iterator++;
			}
			else
			{
				result[resultIterator]=tab[tab3Iterator];
				resultIterator++;
				tab3Iterator++;
			}
		}
	
	
		while(tab2Iterator<=mid2) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab2Iterator];
			resultIterator++;
			tab2Iterator++;
		}
	
		while(tab3Iterator<=stop) //tab2 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab3Iterator];
			resultIterator++;
			tab3Iterator++;
		}
	}
	else //doszlismy do konca drugiej czesci tablicy
	{
		while(tab1Iterator<=mid1 && tab3Iterator<=stop) //nie doszlismy do konca zadnej z pozostałych tablic
		{
			if(tab[tab1Iterator]<tab[tab3Iterator])
			{
				result[resultIterator]=tab[tab1Iterator];
				resultIterator++;
				tab1Iterator++;
			}
			else
			{
				result[resultIterator]=tab[tab3Iterator];
				resultIterator++;
				tab3Iterator++;
			}
		}
	
	
		while(tab1Iterator<=mid1) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab1Iterator];
			resultIterator++;
			tab2Iterator++;
		}
	
		while(tab3Iterator<=stop) //tab2 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
		{
			result[resultIterator]=tab[tab3Iterator];
			resultIterator++;
			tab3Iterator++;
		}
	}

	
	for(int i=0;i<resultIterator;i++)
	{
		tab[i+start]=result[i];
	}

	delete[] result;

	return true;
}




void mergesort(int* tab, int start, int stop) //zwykly mergesort
{
	if(start<stop)
	{
		int mid = (start+stop)/2;
		mergesort(tab, start, mid);
		mergesort(tab, mid+1, stop);
		merge(tab, start, mid, stop);
	}
}

void mergesort3(int* tab, int start, int stop) //merge sort z podzialem na 3
{
	if(start<stop)
	{
		int mid = (start+stop)/3;
		mergesort(tab, start, mid);
		mergesort(tab, mid+1, 2*mid);
		mergesort(tab, 2*mid+1, stop);
		merge3(tab, start, mid, 2*mid, stop);
	}
}


void insertion(int *tab, int start, int stop)  //sortowanie przez wstawianie
{
	int i, j;
	int tmp;
	for (i = start+1; i<=stop; i++)
	{
		j = i;
		tmp = tab[i];
		while (tab[j - 1]>tmp && j > 0)
		{
			tab[j] = tab[j - 1];

			j--;
		}
		tab[j] = tmp;
	}
}

void mergesort20(int* tab, int start, int stop) //mergesort dla zbioru powyzej 20 elementow 
{
	if(stop<20)
	{
		cout<<"Insertion sort"<<endl;
		insertion(tab,start,stop);
	}
	else
	{
		cout<<"Mergesort"<<endl;
		mergesort(tab, start, stop);
	}
}



int main()
{
	//Zad 1. --------------------------
	cout<<"Zwykly mergesort i merge"<<endl;
	int *tab1=new int[6];
	srand(time(NULL));
	for(int i=0;i<6;i++)
	{
		tab1[i]=rand()%50;
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	mergesort(tab1, 0, 5); //od 0 bo taka jest konwencja numerowania indeksow w C++
	
	for(int i=0;i<6;i++)
	{
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	delete[] tab1;
	
	

	// Zad 2. --------------------------------------
	cout<<"\nMergesort i merge dziela na 3"<<endl;
	int *tab2=new int[12];
	for(int i=0;i<12;i++)
	{
		tab2[i]=rand()%50;
		cout<<tab2[i]<<", ";
	}
	cout<<endl;

	
	mergesort3(tab2, 0, 11);

	for(int i=0;i<12;i++)
	{
		cout<<tab2[i]<<", ";
	}
	cout<<endl;

	delete[] tab2;

	// Zad 3. --------------------------------------
	cout<<"\nMergesort i merge wykonuja sie dla zbioru wiekszego niz 20, w pp.sortowanie przez selekcje. Zbior 25 elementow:"<<endl;
	int *tab3=new int[25];
	for(int i=0;i<25;i++)
	{
		tab3[i]=rand()%50;
		cout<<tab3[i]<<", ";
	}
	cout<<endl;

	mergesort20(tab3, 0, 24);

	for(int i=0;i<25;i++)
	{
		cout<<tab3[i]<<", ";
	}
	cout<<endl;

	delete[] tab3;

	cout<<"\nZbior 19 elementow:"<<endl;

	int *tab4=new int[19];
	for(int i=0;i<19;i++)
	{
		tab4[i]=rand()%50;
		cout<<tab4[i]<<", ";
	}
	cout<<endl;

	mergesort20(tab4, 0, 18);

	for(int i=0;i<19;i++)
	{
		cout<<tab4[i]<<", ";
	}
	cout<<endl;

	delete[] tab4;



	return 0;
}