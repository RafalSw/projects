#ifndef PLANSZAZWIERZAT_H
#define PLANSZAZWIERZAT_H

#include <iostream>

#include "krolik.h"
#include "wilczyca.h"
#include "wilk.h"

typedef struct Wilk Wilk;
typedef struct Wilczyca Wilczyca;
typedef struct Krolik Krolik;

struct Pole
{
    Wilk *wilk;
    Wilczyca *wilczyca;
    Krolik *krolik;
};

//Definiowanie listy zwierzat, na podstawie ktorej dokonywane beda ruch
struct PlanszaZwierzat
{
    Pole *plansza;
    int n; //rozmiar planszy
};

//dodawanie nowego zwierzecia do planszy
void dodaj(char* zwierze, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat);

//sprawdzenie czy w tym miejscu moze pojawic sie nowe zwierze
bool czyWolne(Zwierze* zwierze, PlanszaZwierzat &planszaZwierzat);

//kasowanie konkretnego zwierzecia
void usun(char* usuwane, int xOryginalny, int yOryginalny, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat);

//kasowanie konkretnego zwierzecia z planszy, ale nie z pamieci
void usun2(char* usuwane, int xOryginalny, int yOryginalny, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat);

//kasowanie wszystkich zwierzat z listy
void usunWszystko(PlanszaZwierzat &planszaZwierzat);

//zwraca wskaznik do pierwszego w kolejnosci na liscie zwierzecia w sasiedztwie, jesli brak: NULL
char* znajdzZwierzeWSasiedztwie(Zwierze *odniesienie, TypZwierzecia szukanyTyp, PlanszaZwierzat &planszaZwierzat);

//zwraca wskaznik do pierwszego w kolejnosci na liscie zwierzecia na polu tym samym co zwierze odniesienia, jesli brak: NULL
char* znajdzZwierzeNaPolu(Zwierze *odniesienie, TypZwierzecia szukanyTyp, PlanszaZwierzat &planszaZwierzat);

//wypisuje liste zwierzat w postaci 2D
void wypisz(int numerRuchu, PlanszaZwierzat &planszaZwierzat);

//ruch wykonywany jest dla kazdego zwierzecia z listy, nastepuje podsumowanie po turze
void ruch(PlanszaZwierzat &planszaZwierzat);

void przesun(char* zwierze, TypZwierzecia typ, int xOryginalny, int yOryginalny, int xDocelowy, int yDocelowy, PlanszaZwierzat &planszaZwierzat);
#endif
