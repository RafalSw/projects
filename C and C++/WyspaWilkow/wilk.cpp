#include <cstdlib>
#include "wilk.h"

void ruchWilka(Wilk* wilk, PlanszaZwierzat &lista)
{
	//sprawdzenie kolejno czy istnieje w sasiedztwie krolik, wilczyca, w p.p. losowy ruch
	Krolik *szukanyKrolik;
	Wilczyca *szukanaWilczyca;
	int pozycjaX = wilk->zwierze.x;
	int pozycjaY = wilk->zwierze.y;

	if ((szukanyKrolik = (Krolik*)znajdzZwierzeWSasiedztwie(&wilk->zwierze, KROLIK, lista)) != NULL) //znaleziono krolika
	{
		wilk->zwierze.x = szukanyKrolik->zwierze.x;
		wilk->zwierze.y = szukanyKrolik->zwierze.y;
		if (czyWolne(&wilk->zwierze, lista) == true)
		{
			wilk->pogon = true;
			przesun((char*)wilk, WILK, pozycjaX, pozycjaY, wilk->zwierze.x, wilk->zwierze.y, lista);
			return;
		}
	}

	if ((szukanaWilczyca = (Wilczyca*)znajdzZwierzeWSasiedztwie(&wilk->zwierze, WILCZYCA, lista)) != NULL) //znaleziono wilczyce
	{
		wilk->zwierze.x = szukanaWilczyca->zwierze.x;
		wilk->zwierze.y = szukanaWilczyca->zwierze.y;
		if (czyWolne(&wilk->zwierze, lista) == true)
		{
			przesun((char*)wilk, WILK, pozycjaX, pozycjaY, wilk->zwierze.x, wilk->zwierze.y, lista);
			return;
		}
	}

	//losowy ruch

	int proby = 1000;
	do
	{
		proby--;
		wilk->zwierze.x = pozycjaX + rand() % 3 - 1;
		wilk->zwierze.y = pozycjaY + rand() % 3 - 1;
	} while (proby > 0 && ((wilk->zwierze.x == pozycjaX && wilk->zwierze.y == pozycjaY) || czyWolne(&wilk->zwierze, lista) == false)); //gwarancja ruchu

	//brak wolnych miejsc
	if (proby == 0)
	{
		wilk->zwierze.x = pozycjaX;
		wilk->zwierze.y = pozycjaY;
	}

	przesun((char*)wilk, WILK, pozycjaX, pozycjaY, wilk->zwierze.x, wilk->zwierze.y, lista);
}
