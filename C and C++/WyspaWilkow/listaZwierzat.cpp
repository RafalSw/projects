#include "listaZwierzat.h"

void dodajDoListy(char* zawartosc, TypZwierzecia typ, ListaZwierzat &lista)
{
    Element* nowy = new Element();
    nowy->zawartosc = zawartosc;
    nowy->typ = typ;
    nowy->nastepny = lista.pierwszy;
    lista.pierwszy = nowy;
}

void usunZListy(char* zawartosc, ListaZwierzat &lista)
{
    Element* obecny = lista.pierwszy;
    Element* poprzedni = NULL;

    while (obecny != NULL)
    {
        if (obecny->zawartosc == zawartosc)
        {
            break;
        }
        else
        {
            poprzedni = obecny;
            obecny = obecny->nastepny;
        }
    }

    if (obecny == NULL) //nie znaleziono
    {
        return;
    }
    else //trzeba usunac
    {
        if (poprzedni == NULL)
        {
            lista.pierwszy = obecny->nastepny;
        }
        else
        {
            poprzedni->nastepny = obecny->nastepny;
        }
        delete obecny;
    }
}

void wyczyscListe(ListaZwierzat &lista)
{
    while (lista.pierwszy != NULL)
        usunZListy(lista.pierwszy->zawartosc, lista);
}
