#include <cstdlib>
#include "wilczyca.h"
#include "krolik.h"
#include "planszaZwierzat.h"

void ruchWilczycy(Wilczyca* wilczyca, PlanszaZwierzat &lista)
{

	Krolik* szukanyKrolik = (Krolik*)znajdzZwierzeWSasiedztwie(&wilczyca->zwierze, KROLIK, lista);

	int pozycjaX = wilczyca->zwierze.x;
	int pozycjaY = wilczyca->zwierze.y;

	if (szukanyKrolik != NULL)//istnieje potencjalny cel, ustawienie sie na wlasciwym polu
	{
		wilczyca->zwierze.x = szukanyKrolik->zwierze.x;
		wilczyca->zwierze.y = szukanyKrolik->zwierze.y;
		if (czyWolne(&wilczyca->zwierze, lista) == true)
		{
			wilczyca->pogon = true;
			przesun((char*)wilczyca, WILCZYCA, pozycjaX, pozycjaY, wilczyca->zwierze.x, wilczyca->zwierze.y, lista);
			return;
		}
	}
	//nie ma krolika w sasiedztwie lub miejsce niedostepne, wykonywany losowy ruch

	int proby = 1000;
	do
	{
		proby--;
		wilczyca->zwierze.x = pozycjaX + rand() % 3 - 1;
		wilczyca->zwierze.y = pozycjaY + rand() % 3 - 1;
	} while (proby > 0 && ((wilczyca->zwierze.x == pozycjaX && wilczyca->zwierze.y == pozycjaY) || czyWolne(&wilczyca->zwierze, lista) == false)); //gwarancja ruchu

	//brak wolnych miejsc
	if (proby == 0)
	{
		wilczyca->zwierze.x = pozycjaX;
		wilczyca->zwierze.y = pozycjaY;
	}
	przesun((char*)wilczyca, WILCZYCA, pozycjaX, pozycjaY, wilczyca->zwierze.x, wilczyca->zwierze.y, lista);
}
