#include <iostream>
#include <cstdlib>
#include "krolik.h"
#include "planszaZwierzat.h"

void ruchKrolika(Krolik* krolik, PlanszaZwierzat &lista)
{
	krolik->numerRuchu++;
	if (krolik->numerRuchu % 9 == 0)//co 9 ruch jest odpoczynkiem
		return;


	//przypadkowe rozmnazanie
	int szansaRozmnozenia = rand() % 5;
	if (szansaRozmnozenia == 0) //1 z 5 czyli 20%
	{
		Krolik* nowyKrolik = new Krolik();
		nowyKrolik->zwierze.typ = KROLIK;
		nowyKrolik->numerRuchu = 0;
		nowyKrolik->nastepny = NULL;

		//krolik przy rozmnazaniu nie musi byc jedynym zwierzeciem na polu, przyjmuje wiec losowa pozycje
		int proby = 1000;
		do
		{
			nowyKrolik->zwierze.x = krolik->zwierze.x + rand() % 3 - 1;
			nowyKrolik->zwierze.y = krolik->zwierze.y + rand() % 3 - 1;
			proby--;
		} while (proby > 0 && (czyWolne(&nowyKrolik->zwierze, lista) == false || (nowyKrolik->zwierze.x == krolik->zwierze.x && nowyKrolik->zwierze.y == krolik->zwierze.y)));

		if (proby > 0)
			dodaj((char*)nowyKrolik, KROLIK, lista);
		else
			delete nowyKrolik;
	}

	int pozycjaX = krolik->zwierze.x;
	int pozycjaY = krolik->zwierze.y;
	int proby = 1000;
	do
	{
		krolik->zwierze.x = pozycjaX + rand() % 3 - 1;
		krolik->zwierze.y = pozycjaY + rand() % 3 - 1;
		proby--;
	} while (proby > 0 && czyWolne(&krolik->zwierze, lista) == false);
	przesun((char*)krolik, KROLIK, pozycjaX, pozycjaY, krolik->zwierze.x, krolik->zwierze.y, lista);
}
