#ifndef WILK_H
#define WILK_H
#include "zwierze.h"
#include "planszaZwierzat.h"

typedef struct PlanszaZwierzat PlanszaZwierzat;

struct Wilk
{
	Zwierze zwierze;
	float tluszcz;
    bool pogon;
};

void ruchWilka(Wilk* wilk, PlanszaZwierzat &lista);
#endif
