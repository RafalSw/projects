#ifndef ZWIERZE_H
#define ZWIERZE_H

enum TypZwierzecia { KROLIK, WILK, WILCZYCA };

struct Zwierze
{
    int x, y; //polozenie zwierzecia
    TypZwierzecia typ;
};

#endif // ZWIERZE_H

