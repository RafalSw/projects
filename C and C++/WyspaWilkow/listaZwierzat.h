#ifndef LISTAZWIERZAT_H
#define LISTAZWIERZAT_H

#include "planszaZwierzat.h"

struct Element
{
	char* zawartosc;
	TypZwierzecia typ;
	Element* nastepny;
};


struct ListaZwierzat
{
	Element* pierwszy;
};

void dodajDoListy(char* zawartosc, TypZwierzecia typ, ListaZwierzat &lista);

void usunZListy(char* zawartosc, ListaZwierzat &lista);

void wyczyscListe(ListaZwierzat &lista);

#endif
