#ifndef KROLIK_H
#define KROLIK_H
#include "zwierze.h"

typedef struct PlanszaZwierzat PlanszaZwierzat;

struct Krolik
{
	Zwierze zwierze;
	int numerRuchu;
	Krolik* nastepny;
};

void ruchKrolika(Krolik* krolik, PlanszaZwierzat &lista);
#endif
