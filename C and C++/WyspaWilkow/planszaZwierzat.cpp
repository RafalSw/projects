#include <iostream>
#include <cmath>
#include <cstdlib>
//#include "planszaZwierzat.h"
#include "listaZwierzat.h"

using namespace std;

//dodawanie nowego zwierzecia do listy
void dodaj(char* nowe, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat)
{
	switch (typ)
	{
	case KROLIK:
	{
		Krolik *dodawanyKrolik = (Krolik*)nowe;
		if (dodawanyKrolik != planszaZwierzat.plansza[dodawanyKrolik->zwierze.x*planszaZwierzat.n + dodawanyKrolik->zwierze.y].krolik)
		{
			dodawanyKrolik->nastepny = planszaZwierzat.plansza[dodawanyKrolik->zwierze.x*planszaZwierzat.n + dodawanyKrolik->zwierze.y].krolik;
			planszaZwierzat.plansza[dodawanyKrolik->zwierze.x*planszaZwierzat.n + dodawanyKrolik->zwierze.y].krolik = dodawanyKrolik;
		}
		break;
	}
	case WILK:
	{
		Wilk *dodawanyWilk = (Wilk*)nowe;
		Wilk *wilkZPola = planszaZwierzat.plansza[dodawanyWilk->zwierze.x*planszaZwierzat.n + dodawanyWilk->zwierze.y].wilk;
		if (wilkZPola != NULL)
			cout << "Blad dodawania wilka!" << endl;
		else
			planszaZwierzat.plansza[dodawanyWilk->zwierze.x*planszaZwierzat.n + dodawanyWilk->zwierze.y].wilk = dodawanyWilk;
		break;
	}
	case WILCZYCA:
	{
		Wilczyca *dodawanaWilczyca = (Wilczyca*)nowe;
		Wilczyca *wilkZPola = planszaZwierzat.plansza[dodawanaWilczyca->zwierze.x*planszaZwierzat.n + dodawanaWilczyca->zwierze.y].wilczyca;
		if (wilkZPola != NULL)
			cout << "Blad dodawania wilczycy!" << endl;
		else
			planszaZwierzat.plansza[dodawanaWilczyca->zwierze.x*planszaZwierzat.n + dodawanaWilczyca->zwierze.y].wilczyca = dodawanaWilczyca;
		break;
	}
	}
}

//sprawdzenie czy w tym miejscu moze pojawic sie nowe zwierze
bool czyWolne(Zwierze* zwierze, PlanszaZwierzat &planszaZwierzat)
{
	if (zwierze->x < 0 || zwierze->x >= planszaZwierzat.n || zwierze->y < 0 || zwierze->y >= planszaZwierzat.n)
		return false;
	else
	{
		if (zwierze->typ == KROLIK)
			return true;


		bool result = true;

		if (zwierze->typ == WILK && planszaZwierzat.plansza[zwierze->x*planszaZwierzat.n + zwierze->y].wilk != NULL)
			result = false;
		else if (zwierze->typ == WILCZYCA && planszaZwierzat.plansza[zwierze->x*planszaZwierzat.n + zwierze->y].wilczyca != NULL)
			result = false;

		bool zywoplot = true; //wlaczanie lub wylaczanie zywoplotu
		if (zywoplot && zwierze->x < (planszaZwierzat.n / 2) && zwierze->y < (planszaZwierzat.n / 2))
		{
			result = false;
		}

		return result;
	}
}

//kasowanie konkretnego zwierzecia
void usun(char* usuwane, int xOryginalny, int yOryginalny, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat)
{
	if (usuwane == NULL)
	{
		cout << "Blad podczas usuwania zwierzecia" << endl;
		return;
	}

	switch (typ)
	{
	case KROLIK:
	{
		Krolik* usuwany = (Krolik*)usuwane;
		Krolik* poprzedni = NULL;
		Krolik* znaleziony = planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik;

		while (znaleziony != NULL && znaleziony != usuwany)
		{
			poprzedni = znaleziony;
			znaleziony = znaleziony->nastepny;
		}

		if (znaleziony == NULL)
		{
			cout << "Blad usuwania krolika!" << endl;
			return;
		}

		if (poprzedni == NULL)
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik = planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik->nastepny;
		}
		else
		{
			poprzedni->nastepny = znaleziony->nastepny;
		}
		delete znaleziony;
		break;
	}
	case WILK:
	{
		Wilk* usuwany = (Wilk*)usuwane;
		if (planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilk == NULL)
			cout << "Blad usuwania wilka" << endl;
		else
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilk = NULL;
			delete usuwany;
		}
		break;
	}
	case WILCZYCA:
	{
		Wilczyca* usuwany = (Wilczyca*)usuwane;
		if (planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilczyca == NULL)
			cout << "Blad usuwania wilczycy" << endl;
		else
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilczyca = NULL;
			delete usuwany;
		}
		break;
	}
	}
}

//kasowanie konkretnego zwierzecia z planszy, ale nie z pamieci
void usun2(char* usuwane, int xOryginalny, int yOryginalny, TypZwierzecia typ, PlanszaZwierzat &planszaZwierzat)
{
	if (usuwane == NULL)
	{
		cout << "Blad podczas usuwania zwierzecia" << endl;
		return;
	}

	switch (typ)
	{
	case KROLIK:
	{
		Krolik* usuwany = (Krolik*)usuwane;
		Krolik* poprzedni = NULL;
		Krolik* znaleziony = planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik;

		while (znaleziony != NULL && znaleziony != usuwany)
		{
			poprzedni = znaleziony;
			znaleziony = znaleziony->nastepny;
		}

		if (znaleziony == NULL)
		{
			cout << "Blad usuwania krolika!" << endl;
			return;
		}

		if (poprzedni == NULL)
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik = planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].krolik->nastepny;
		}
		else
		{
			poprzedni->nastepny = znaleziony->nastepny;
		}
		break;
	}
	case WILK:
	{
		Wilk* usuwany = (Wilk*)usuwane;
		if (planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilk == NULL)
			cout << "Blad usuwania wilka" << endl;
		else
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilk = NULL;
		}
		break;
	}
	case WILCZYCA:
	{
		Wilczyca* usuwany = (Wilczyca*)usuwane;
		if (planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilczyca == NULL)
			cout << "Blad usuwania wilczycy" << endl;
		else
		{
			planszaZwierzat.plansza[xOryginalny*planszaZwierzat.n + yOryginalny].wilczyca = NULL;
		}
		break;
	}
	}
}

//kasowanie wszystkich zwierzat z listy
void usunWszystko(PlanszaZwierzat &planszaZwierzat)
{
	for (int i = 0; i < planszaZwierzat.n; i++)
	{
		for (int j = 0; j < planszaZwierzat.n; j++)
		{
			if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk != NULL)
			{
				delete planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk;
				planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk = NULL;
			}

			if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca != NULL)
			{
				delete planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca;
				planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca = NULL;
			}

			Krolik* k = planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik;
			Krolik* doUsuniecia;
			while (k != NULL)
			{
				doUsuniecia = k;
				k = k->nastepny;
				delete doUsuniecia;
			}
			planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik = NULL;
		}
	}
}

char* znajdzZwierzeWSasiedztwie(Zwierze *odniesienie, TypZwierzecia szukanyTyp, PlanszaZwierzat &planszaZwierzat)
{
	char* wyniki[9];
	int iloscWynikow = 0;
	int iStart, iStop, jStart, jStop;

	if (odniesienie->x == 0)
		iStart = 0;
	else
		iStart = odniesienie->x - 1;

	if (odniesienie->x == planszaZwierzat.n - 1)
		iStop = planszaZwierzat.n - 1;
	else
		iStop = odniesienie->x + 1;

	if (odniesienie->y == 0)
		jStart = 0;
	else
		jStart = odniesienie->y - 1;

	if (odniesienie->y == planszaZwierzat.n - 1)
		jStop = planszaZwierzat.n - 1;
	else
		jStop = odniesienie->y + 1;

	for (int i = iStart; i <= iStop; i++)
	{
		for (int j = jStart; j <= jStop; j++)
		{
			switch (szukanyTyp)
			{
			case KROLIK:
			{
				if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik != NULL)
					wyniki[iloscWynikow++] = (char*)planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik;
				else
					wyniki[iloscWynikow++] = NULL;
				break;
			}
			case WILK:
			{
				if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk != NULL)
					wyniki[iloscWynikow++] = (char*)planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk;
				else
					wyniki[iloscWynikow++] = NULL;
				break;
			}
			case WILCZYCA:
			{
				if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca != NULL)
					wyniki[iloscWynikow++] = (char*)planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca;
				else
					wyniki[iloscWynikow++] = NULL;
				break;
			}
			default:
			{
				wyniki[iloscWynikow++] = NULL;
				break;
			}
			}
		}
	}


	//dobrze zwracac losowe zwierze z okolicy. Konieczne losowanie
	bool wynikIstnieje = false;
	for (int i = 0; i < iloscWynikow; i++)
		if (wyniki[i] != NULL)
		{
			wynikIstnieje = true;
			break;
		}
    if(wynikIstnieje == true)
    {
        char* wynik = NULL;
        while (wynik == NULL)
        {
			wynik = wyniki[rand() % iloscWynikow]; //losowanie wyniku od 0 do 9
        }

        return wynik;
    }
    else
        return NULL;
}

char* znajdzZwierzeNaPolu(Zwierze* odniesienie, TypZwierzecia szukanyTyp, PlanszaZwierzat &planszaZwierzat)
{
	switch (szukanyTyp)
	{
	case KROLIK:
	{
		return (char*)planszaZwierzat.plansza[odniesienie->x*planszaZwierzat.n + odniesienie->y].krolik;
		break;
	}
	case WILK:
	{
		return (char*)planszaZwierzat.plansza[odniesienie->x*planszaZwierzat.n + odniesienie->y].wilk;
		break;
	}
	case WILCZYCA:
	{
		return (char*)planszaZwierzat.plansza[odniesienie->x*planszaZwierzat.n + odniesienie->y].wilczyca;
		break;
	}
	}
	return NULL; //nie ma wlasciwego zwierzecia
}

void wypisz(int numerRuchu, PlanszaZwierzat &planszaZwierzat)
{
	int lKrolikow = 0, lWilkow = 0, lWilczyc = 0;

	//deklaracja tablicy do wypisania i wyczyszczenie jej
	char* tablica = new char[planszaZwierzat.n*planszaZwierzat.n];
	for (int i = 0; i < planszaZwierzat.n; i++)
	{
		for (int j = 0; j < planszaZwierzat.n; j++)
		{
			tablica[i*planszaZwierzat.n + j] = ' ';
		}
	}

	//umieszczenie liter odpowiadajacych zwierzetom na tablicy
	for (int i = 0; i < planszaZwierzat.n; i++)
	{
		for (int j = 0; j < planszaZwierzat.n; j++)
		{
			if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk != NULL)
			{
				tablica[i*planszaZwierzat.n + j] = 'W';
				//cout << "W " << planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilk << " " << i << " " << j << endl;
				lWilkow++;
			}
			if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca != NULL)
			{
				tablica[i*planszaZwierzat.n + j] = 'w';
				//cout << "w " << planszaZwierzat.plansza[i*planszaZwierzat.n + j].wilczyca << " " << i << " " << j << endl;
				lWilczyc++;
			}
			if (planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik != NULL)
			{
				tablica[i*planszaZwierzat.n + j] = 'K';
				Krolik* obecny = planszaZwierzat.plansza[i*planszaZwierzat.n + j].krolik;
				while (obecny != NULL)
				{
					//cout << "K " << obecny << " " << i << " " << j << endl;
					lKrolikow++;
					obecny = obecny->nastepny;
				}
			}
		}
	}

	if (numerRuchu > 0)
		cout << "\n" << numerRuchu << endl;

	//wypisanie tablicy
	for (int i = -1; i <= planszaZwierzat.n; i++)
	{
		cout << "#";
		for (int j = 0; j < planszaZwierzat.n; j++)
		{
			if (i == -1 || i == planszaZwierzat.n)
				cout << "#";
			else
				cout << tablica[i*planszaZwierzat.n + j];
		}
		cout << "#" << endl;
	}
	cout << "Liczba krolikow: " << lKrolikow << " wilkow: " << lWilkow << " wilczyc: " << lWilczyc << endl;
	delete tablica;
}

void ruch(PlanszaZwierzat &planszaZwierzat)
{
	//tworzenie listy zwierzat, na podstawie ktorej odbywac sie bedzie ruch
	ListaZwierzat listaZwierzat;
	listaZwierzat.pierwszy = NULL;

	for (int i = 0; i < planszaZwierzat.n; i++)
	{
		for (int j = 0; j < planszaZwierzat.n; j++)
		{
			Pole p = planszaZwierzat.plansza[i*planszaZwierzat.n + j];
			if (p.wilk != NULL)
			{
				dodajDoListy((char*)p.wilk, WILK, listaZwierzat);
			}
			if (p.wilczyca != NULL)
			{
				dodajDoListy((char*)p.wilczyca, WILCZYCA, listaZwierzat);
			}
			if (p.krolik != NULL)
			{
				Krolik* aktualny = p.krolik;
				while (aktualny != NULL)
				{
					dodajDoListy((char*)aktualny, KROLIK, listaZwierzat);
					aktualny = aktualny->nastepny;
				}
			}
		}
	}


	//dla kazdego zwierzecia na liscie wykonywana jest metoda ruchu i wykonuja sie zwiazane z nia konsekwencje
	Element* obecny = listaZwierzat.pierwszy;

	while (obecny != NULL)
	{
		if (obecny->typ == WILCZYCA)
		{
			Wilczyca* wilczyca = (Wilczyca*)obecny->zawartosc;
			ruchWilczycy(wilczyca, planszaZwierzat);

			Krolik *ofiara = (Krolik*)znajdzZwierzeNaPolu(&(wilczyca->zwierze), KROLIK, planszaZwierzat);
			if (ofiara != NULL)
			{
				usun((char*)ofiara, ofiara->zwierze.x, ofiara->zwierze.y, KROLIK, planszaZwierzat);
				usunZListy((char*)ofiara, listaZwierzat);
				wilczyca->tluszcz += 1;
			}
			else if (wilczyca->pogon == true)
			{
				wilczyca->tluszcz -= (float)0.1;
			}

			wilczyca->pogon = false;

			if (wilczyca->tluszcz <= 0)
			{
				usun((char*)wilczyca, wilczyca->zwierze.x, wilczyca->zwierze.y, WILCZYCA, planszaZwierzat);
				obecny = obecny->nastepny;
				usunZListy((char*)wilczyca, listaZwierzat);
			}
			else
				obecny = obecny->nastepny;
		}
		else if (obecny->typ == WILK)
		{
			Wilk* wilk = (Wilk*)obecny->zawartosc;
			ruchWilka(wilk, planszaZwierzat);

			Krolik *ofiara = (Krolik*)znajdzZwierzeNaPolu(&(wilk->zwierze), KROLIK, planszaZwierzat);

			if (ofiara != NULL)
			{
				usun((char*)ofiara, ofiara->zwierze.x, ofiara->zwierze.y, KROLIK, planszaZwierzat);
				usunZListy((char*)ofiara, listaZwierzat);
				wilk->tluszcz += 1;
			}
			else
			{
				Wilczyca *partnerka = (Wilczyca*)znajdzZwierzeNaPolu(&(wilk->zwierze), WILCZYCA, planszaZwierzat);
				if (partnerka != NULL)
				{
					int typPotomka = rand() % 2;
					if (typPotomka == 0) //wilk
					{
						Wilk* nowyWilk = new Wilk();
						nowyWilk->tluszcz = 1;
						nowyWilk->pogon = false;
						nowyWilk->zwierze.typ = WILK;
						int proby = 1000;
						do
						{
							proby--;
							nowyWilk->zwierze.x = wilk->zwierze.x + rand() % 3 - 1;
							nowyWilk->zwierze.y = wilk->zwierze.y + rand() % 3 - 1;
						} while (proby > 0 && czyWolne(&nowyWilk->zwierze, planszaZwierzat) == false);

						if (proby != 0)
							dodaj((char*)nowyWilk, WILK, planszaZwierzat);
						else //nie ma gdzie wstawic
							delete nowyWilk;
					}
					else //wilczyca
					{
						Wilczyca* nowaWilczyca = new Wilczyca();
						nowaWilczyca->tluszcz = 1;
						nowaWilczyca->pogon = false;
						nowaWilczyca->zwierze.typ = WILCZYCA;
						int proby = 1000;
						do
						{
							proby--;
							nowaWilczyca->zwierze.x = wilk->zwierze.x + rand() % 3 - 1;
							nowaWilczyca->zwierze.y = wilk->zwierze.y + rand() % 3 - 1;
						} while (proby > 0 && czyWolne(&nowaWilczyca->zwierze, planszaZwierzat) == false);

						if (proby != 0)
							dodaj((char*)nowaWilczyca, WILCZYCA, planszaZwierzat);
						else //nie ma gdzie wstawic
							delete nowaWilczyca;
					}
				}
				else if (wilk->pogon == true) //Jesli wilk reprodukuje, to nie traci tluszczu
				{
					wilk->tluszcz -= (float)0.1;
				}
			}

			wilk->pogon = false;

			if (wilk->tluszcz <= 0)
			{
				usun((char*)wilk, wilk->zwierze.x, wilk->zwierze.y, WILCZYCA, planszaZwierzat);
				obecny = obecny->nastepny;
				usunZListy((char*)wilk, listaZwierzat);
			}
			else
				obecny = obecny->nastepny;
		}
		else //krolik
		{
			Krolik* krolik = (Krolik*)obecny->zawartosc;
			ruchKrolika(krolik, planszaZwierzat);
			obecny = obecny->nastepny;
		}
	}

	wyczyscListe(listaZwierzat);
}

void przesun(char* zwierze, TypZwierzecia typ, int xOryginalny, int yOryginalny, int xDocelowy, int yDocelowy, PlanszaZwierzat &planszaZwierzat)
{
	usun2(zwierze, xOryginalny, yOryginalny, typ, planszaZwierzat);
	dodaj(zwierze, typ, planszaZwierzat);
}

