#include <iostream>
#include <string>
#include <time.h> //do rand
#include <windows.h> //do Sleep
#include <fstream> //do obslugi plikow
#include <sstream> //potrzebny do istringstream

#include "krolik.h"
#include "wilk.h"
#include "wilczyca.h"

using namespace std;

Pole* utworzPlansze(int n)
{
	Pole* wynik = new Pole[n*n];
	for (int i = 0; i < n; i++)
	{
		for (int j = 0; j < n; j++)
		{
			wynik[i*n + j].krolik = NULL;
			wynik[i*n + j].wilk = NULL;
			wynik[i*n + j].wilczyca = NULL;
		}
	}
	return wynik;
}

PlanszaZwierzat losujZwierzeta()
{
	int n, ilosc;

	cout << "Podaj rozmiar planszy: ";
	cin >> n;

	cout << "Podaj ilosc zwierzat: ";
	cin >> ilosc;

	PlanszaZwierzat planszaZwierzat;
	planszaZwierzat.n = n;
	planszaZwierzat.plansza = utworzPlansze(n);
	for (int i = 0; i < ilosc; i++)
	{
		int typ = rand() % 3; //losowanie liczb {0,1,2}
		int proby = 1000;
		if (typ == 0)
		{
			Krolik* nowyKrolik = new Krolik();
			nowyKrolik->zwierze.typ = KROLIK;
			nowyKrolik->numerRuchu = 0;
			do
			{
				proby--;
				nowyKrolik->zwierze.x = rand() % n;
				nowyKrolik->zwierze.y = rand() % n;
			} while (proby>0 && czyWolne(&nowyKrolik->zwierze, planszaZwierzat) == false);

			if (proby > 0)
				dodaj((char*)nowyKrolik, KROLIK, planszaZwierzat);
			else
				delete nowyKrolik;
		}
		else if (typ == 1)
		{
			Wilk* nowyWilk = new Wilk();
			nowyWilk->pogon = false;
			nowyWilk->tluszcz = 1;
			nowyWilk->zwierze.typ = WILK;
			do
			{
				proby--;
				nowyWilk->zwierze.x = rand() % n;
				nowyWilk->zwierze.y = rand() % n;
			} while (proby > 0 && czyWolne(&nowyWilk->zwierze, planszaZwierzat) == false);

			if (proby > 0)
				dodaj((char*)nowyWilk, WILK, planszaZwierzat);
			else
				delete nowyWilk;
		}
		else
		{
			Wilczyca* nowaWilczyca = new Wilczyca();
			nowaWilczyca->pogon = false;
			nowaWilczyca->tluszcz = 1;
			nowaWilczyca->zwierze.typ = WILCZYCA;
			do
			{
				proby--;
				nowaWilczyca->zwierze.x = rand() % n;
				nowaWilczyca->zwierze.y = rand() % n;
			} while (proby > 0 && czyWolne(&nowaWilczyca->zwierze, planszaZwierzat) == false);

			if (proby > 0)
				dodaj((char*)nowaWilczyca, WILCZYCA, planszaZwierzat);
			else
				delete nowaWilczyca;
		}
	}
	return planszaZwierzat;
}

void zapis(PlanszaZwierzat planszaZwierzat) //przy zapisie i odczycie odwracana jest kolejnosc zwierzat na liscie
{
	ofstream plikWy;
	string nazwaPliku = "data.txt";
	plikWy.open(nazwaPliku.c_str(), ios::out);
	if (!plikWy.good())
	{
		cout << "Nie udalo sie otworzyc pliku " << nazwaPliku << " do zapisu. Zapis przerwany!" << endl;
		return;
	}
	else
	{
		plikWy << planszaZwierzat.n << endl;

		for (int i = 0; i < planszaZwierzat.n; i++)
		{
			for (int j = 0; j < planszaZwierzat.n; j++)
			{
				Pole p = planszaZwierzat.plansza[i*planszaZwierzat.n + j];
				if (p.wilk != NULL)
				{
					plikWy << "W " << p.wilk->zwierze.x << " " << p.wilk->zwierze.y << " " << p.wilk->tluszcz << endl;
				}
				if (p.wilczyca != NULL)
				{
					plikWy << "w " << p.wilczyca->zwierze.x << " " << p.wilczyca->zwierze.y << " " << p.wilczyca->tluszcz << endl;
				}
				if (p.krolik != NULL)
				{
					Krolik* aktualny = p.krolik;
					while (aktualny != NULL)
					{
						plikWy << "K " << aktualny->zwierze.x << " " << aktualny->zwierze.y << " " << aktualny->numerRuchu << endl;
						aktualny = aktualny->nastepny;
					}
				}
			}
		}
	}
	cout << "Zapisano do " << nazwaPliku << endl;
	plikWy.close();
}


PlanszaZwierzat odczyt()
{
	ifstream plikWe;
	string nazwaPliku = "data.txt";
	PlanszaZwierzat planszaZwierzat;
	planszaZwierzat.n = -1;

	plikWe.open(nazwaPliku.c_str(), ios::in);
	if (!plikWe.good())
	{
		cout << "Nie udalo sie otworzyc pliku " << nazwaPliku << " do odczytu. Odczyt przerwany!" << endl;
		return planszaZwierzat;
	}
	else
	{
		plikWe >> planszaZwierzat.n;
		planszaZwierzat.plansza = utworzPlansze(planszaZwierzat.n);
		string linia;
		while (!plikWe.eof())
		{
			getline(plikWe, linia);
			if (linia != "")
			{
				istringstream iss(linia); //strumieniowanie napisu
				char typ;
				int x, y;
				float t;
				iss >> typ >> x >> y >> t;

				switch (typ)
				{
				case 'K':
				{
					Krolik* nowyKrolik = new Krolik();
					nowyKrolik->zwierze.typ = KROLIK;
					nowyKrolik->numerRuchu = 0;
					nowyKrolik->zwierze.x = x;
					nowyKrolik->zwierze.y = y;
					nowyKrolik->numerRuchu = (int)t;
					dodaj((char*)nowyKrolik, KROLIK, planszaZwierzat);
					break;
				}
				case 'W':
				{
					Wilk* nowyWilk = new Wilk();
					nowyWilk->zwierze.typ = WILK;
					nowyWilk->pogon = false;
					nowyWilk->tluszcz = t;
					nowyWilk->zwierze.x = x;
					nowyWilk->zwierze.y = y;
					dodaj((char*)nowyWilk, WILK, planszaZwierzat);
					break;
				}
				case 'w':
				{
					Wilczyca* nowyWilczyca = new Wilczyca();
					nowyWilczyca->zwierze.typ = WILCZYCA;
					nowyWilczyca->pogon = false;
					nowyWilczyca->tluszcz = t;
					nowyWilczyca->zwierze.x = x;
					nowyWilczyca->zwierze.y = y;
					dodaj((char*)nowyWilczyca, WILCZYCA, planszaZwierzat);
					break;
				}
				}
			}
		}
	}

	cout << "Odczyt z pliku " << nazwaPliku << " zakonczony sukcesem!" << endl;
	plikWe.close();
	return planszaZwierzat;
}

//symulacja grupy zwierzat, opoznienie w ms
void symulacja(PlanszaZwierzat &planszaZwierzat, int opoznienie, int iloscRuchow)
{
	int numerRuchu = 0;
	wypisz(numerRuchu++, planszaZwierzat);
	while (iloscRuchow > 0)
	{
		ruch(planszaZwierzat);
		wypisz(numerRuchu++, planszaZwierzat);
		//dla Windowsa:
		Sleep(opoznienie);

		iloscRuchow--;
	}
}

PlanszaZwierzat utworzListe()
{
	int n, ilosc;

	cout << "Podaj rozmiar planszy: ";
	cin >> n;

	cout << "Podaj ilosc zwierzat: ";
	cin >> ilosc;

	PlanszaZwierzat planszaZwierzat;
	planszaZwierzat.n = n;
	planszaZwierzat.plansza = utworzPlansze(n);

	int licznikUtworzonych = 0;
	while (licznikUtworzonych < ilosc)
	{
		char typ;
		cout << "Podaj typ zwierzecia:\n\tK - krolik\n\tW - wilk\n\tw - wilczyca\n> ";
		cin >> typ;

		if (typ != 'K' && typ != 'W' && typ != 'w')
		{
			cout << "Nie ma takiego wyboru." << endl;
			continue;
		}

		switch (typ)
		{
		case 'K':
		{
			Krolik* nowyKrolik = new Krolik();
			nowyKrolik->numerRuchu = 0;
			nowyKrolik->zwierze.typ = KROLIK;
			do
			{
				int x, y;
				cout << "Podaj wspolrzedna x: ";
				cin >> x;
				cout << "Podaj wspolrzedna y: ";
				cin >> y;
				nowyKrolik->zwierze.x = x;
				nowyKrolik->zwierze.y = y;
			} while (czyWolne(&nowyKrolik->zwierze, planszaZwierzat) == false);
			dodaj((char*)nowyKrolik, KROLIK, planszaZwierzat);
			licznikUtworzonych--;
			break;
		}
		case 'W':
		{
			Wilk* nowyWilk = new Wilk();
			nowyWilk->pogon = false;
			nowyWilk->zwierze.typ = WILK;
			do
			{
				int x, y;
				cout << "Podaj wspolrzedna x: ";
				cin >> x;
				cout << "Podaj wspolrzedna y: ";
				cin >> y;
				nowyWilk->tluszcz = 1;
				nowyWilk->zwierze.x = x;
				nowyWilk->zwierze.y = y;
			} while (czyWolne(&nowyWilk->zwierze, planszaZwierzat) == false);
			dodaj((char*)nowyWilk, WILK, planszaZwierzat);
			licznikUtworzonych--;
			break;
		}
		case 'w':
		{
			Wilczyca* nowyWilczyca = new Wilczyca();
			nowyWilczyca->pogon = false;
			nowyWilczyca->zwierze.typ = WILCZYCA;
			do
			{
				int x, y;
				cout << "Podaj wspolrzedna x: ";
				cin >> x;
				cout << "Podaj wspolrzedna y: ";
				cin >> y;
				nowyWilczyca->tluszcz = 1;
				nowyWilczyca->zwierze.x = x;
				nowyWilczyca->zwierze.y = y;
			} while (czyWolne(&nowyWilczyca->zwierze, planszaZwierzat) == false);
			dodaj((char*)nowyWilczyca, WILCZYCA, planszaZwierzat);
			licznikUtworzonych--;
			break;
		}
		}
	}
	return planszaZwierzat;
}

int main()
{
	srand(time(NULL));
	PlanszaZwierzat planszaZwierzat;
	char wybor;
	cout << "==== Symulator wyspy wilkow ====" << endl;
	cout << "1. Generuj mape losowo" << endl;
	cout << "2. Generuj mape recznie" << endl;
	cout << "3. Wczytaj mape" << endl;
	cout << "0. Wyjdz" << endl;
	cin >> wybor;
	switch (wybor)
	{
	case '1':
		planszaZwierzat = losujZwierzeta();
		break;
	case '2':
		planszaZwierzat = utworzListe();
		break;
	case '3':
		planszaZwierzat = odczyt();
		break;
	case '0':
		return 0;
	}

	int liczbaKrokow;
	cout << "Podaj liczbe krokow symulacji" << endl;
	cin >> liczbaKrokow;

	symulacja(planszaZwierzat, 1000, liczbaKrokow);

	char wyborZapisu;
	cout << "Czy zapisac stan do pliku? [T/N]" << endl;
	cin >> wyborZapisu;

	if (wyborZapisu == 'T' || wyborZapisu == 't')
		zapis(planszaZwierzat);

	usunWszystko(planszaZwierzat); //unikniecie wyciekow pamieci
	system("PAUSE");
	return 0;
}
