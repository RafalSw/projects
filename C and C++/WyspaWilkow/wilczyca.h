#ifndef WILCZYCA_H
#define WILCZYCA_H
#include "zwierze.h"

struct Wilczyca
{
    Zwierze zwierze;
	float tluszcz;
    bool pogon;
};

void ruchWilczycy(Wilczyca* wilczyca, struct PlanszaZwierzat &lista);

#endif
