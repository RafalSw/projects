#include <iostream>
#include <stack> // niezbedne do utworzenia kolejek reprezentujacych wie�e

using namespace std;
const int N = 7; //ilosc element�w

int main()
{
	stack<int> a, b, c; //kolejne wie�e
	int moves = 0; //liczba ruch�w

	for (int i = N; i > 0; i--)
	{
		a.push(i); //wypelnianie pierwszej wiezy liczbami od N do 1
	}

	//petla dzialajaca do momentu, gdy wszystkie elementy znajda sie na trzeciej wiezy
	while (c.size() != N)
	{
		cout << ++moves << ". "; //zwiekszenie licznika krokow i wypisanie aktualnego
		/*
		Dzialanie programu polega na sprawdzaniu kolejno od lewej, czy wieza jest pista, a jesli nie, to nastepuje proba przeniesienia elementu.
		Jesli gorny element jest nieparzysty, przesuwany jest w lewo (A->C, C->B, B->A). Je�li jest parzysty, przesuwany jest w prawo.
		Co drugi ruch wykonywany jest tak, �eby nie rusza� najmniejszego elementu (r�wnego 1). Ca�o�� sprawia, �e algorytm dzia�a zgodnie z za�o�eniami.
		Dla nieparzystego N kod osi�ga minimaln� ilo�� ruch�w zgodnie z teori�: 2^N-1.
		*/
		

		if (a.size() > 0 && !(moves % 2 == 0 && a.top() == 1)) //sprawdzenie czy pierwsza wie�a nie jest pusta oraz czy je�li jest to parzysty ruch, to g�rnym elementem nie jest 1
		{
			if (a.top() % 2 == 1 && (c.size() == 0 || a.top()<c.top())) //element jest nieparzysty, wi�c o ile mo�na, to przesuwamy go w lewo
			{
				c.push(a.top()); //wrzucenie na wie�e c elementu z g�ry wie�y a
				a.pop(); //usuni�cie g�rnego elementu z wie�y a
				cout << "A -> C " << c.top() << endl;
				continue;
			}
			else if (a.top() % 2 == 0 && (b.size() == 0 || a.top() < b.top())) //element jest nieparzysty, wi�c o ile mo�na, to przesuwamy go w prawo
			{
				b.push(a.top());
				a.pop();
				cout << "A -> B " << b.top() << endl;
				continue;
			}
		}
		//analogicznie dla kolejnych wie�
		if (b.size() > 0 && !(moves % 2 == 0 && b.top() == 1))
		{
			if (b.top() % 2 == 1 && (a.size() == 0 || b.top()<a.top()))
			{
				a.push(b.top());
				b.pop();
				cout << "B -> A " << a.top() << endl;
				continue;
			}
			else if (b.top() % 2 == 0 && (c.size() == 0 || b.top() < c.top()))
			{
				c.push(b.top());
				b.pop();
				cout << "B -> C " << c.top() << endl;
				continue;
			}
		}
		if (c.size() > 0 && !(moves % 2 == 0 && c.top() == 1))
		{
			if (c.top() % 2 == 1 && (b.size() == 0 || c.top()<b.top()))
			{
				b.push(c.top());
				c.pop();
				cout << "C -> B " << b.top() << endl;
				continue;
			}
			else if (c.top() % 2 == 0 && (a.size() == 0 || c.top() < a.top()))
			{
				a.push(c.top());
				c.pop();
				cout << "C -> A " << a.top() << endl;
				continue;
			}
		}

	}
	cout << "Minimalna liczba krokow: " << moves << endl;
	system("PAUSE");
}