#include <stdio.h> //STanDard Input Output - printf, scanf
#include <iostream> // cout, cin
#include <string>

using namespace std;

int main(int args, char** argv)
{
	int i=6;
	cout<<"liczba "<<i<<" znajduje sie pod "<<&i<<endl;

	int *wskaznik = &i;
	cout<<"wskaznik: "<<wskaznik<<" przechowuje wartosc "<< *wskaznik <<endl;

	cout<<wskaznik + 1<<endl;
	cout<<*(wskaznik + 1)<<endl;

	double d = 3.14;
	float f = 0.14;

	char c = 't'; //8 bit

	bool b = false;
	//Petla for
	/*
	for(int liczba = 0; liczba<10; liczba++)
	{
		cout<<liczba<<endl;
	}

	int liczba = 0;
	while(liczba<10)
	{
		cout<<liczba<<endl;
		liczba++;
	}

	liczba = 0;

	do
	{
		cout<<liczba<<endl;
		liczba++;
	}while(liczba<10);
	*/

	const int rozmiar = 10;
	int tablica[rozmiar];

	for(int j=0;j<rozmiar;j++)
	{
		tablica[j] = 3*j;
		cout<<tablica[j]<<endl;
	}

	cout<<"tablica: "<<tablica<<endl;
	cout<<"tablica[0]: "<<*(tablica+6)<<endl;

	/*
	char znak;
	cin>>znak;
	cout<<"Wprowadziles znak: "<<znak<<endl;
	*/


	/*
	char imie[6];
	cin>>imie;
	imie[5] = '\0';
	cout<<imie<<endl;
	*/

	string nazwisko;
	cin>>nazwisko;
	cout<<nazwisko<<endl;

	cin.ignore(); // tu jako argument byl INT_MAX, a linijke wyzej cin.clear()
	string nazwisko2;
	//dla calej linjki - znak nowej linii \n
	getline(cin,nazwisko2);
	cout<<nazwisko2<<endl;

	cout << "Hello world!" << endl;
	system("PAUSE");
	return 0;
}
