//Haszowanie metoda adresowania otwartego (wersja liniowa)
//Adresowanie podwojne zblizone do liniowego, ale uzywamy hashTable[(index+counter*index)%sizeMax] zamiast hashTable[(index+counter)%sizeMax]
#pragma once //naglowek includowany jest tylko raz

#include <string>
#include "hashMethod.h"
#include "openAddressHashElement.h"

using namespace std;
class OpenAddressDoubleHash: public HashMethod
{
private:
	OpenAddressHashElement** hashTable; //tablica wskaznikow na elementy
	int sizeMax; // rozmiar tablicy
	int mode; //typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	int currentSize;
public:
	OpenAddressDoubleHash(int size, int mode); //size - rozmiar tablicy, mode - typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	~OpenAddressDoubleHash();
	void add(string key, string& value);
	string find(string key);
	void remove(string key);
};