#include <string>
#include "chainingHash.h"
#include "hashFunction.h"

using namespace std;

ChainingHashElement::ChainingHashElement(string k, string& v)
{
	value = v;
	key = k;
	next = NULL;
}

ChainingHashElement* ChainingHashElement::getNext()
{
	return next;
}

void ChainingHashElement::setNext(ChainingHashElement* n)
{
	next = n;
}

string ChainingHashElement::getKey()
{
	return key;
}

string& ChainingHashElement::getValue()
{
	return value;
}

void ChainingHashElement::setValue(string& v)
{
	value = v;
}

ChainingHash::ChainingHash(int size, int m)
{
	hashTable = new ChainingHashElement*[size];
	sizeMax = size;
	mode = m;

	//czyszcznie tablicy
	for (int i = 0; i < size; i++)
		hashTable[i] = NULL;
}

ChainingHash::~ChainingHash()
{
	delete[] hashTable;
}

void ChainingHash::add(string key, string& value)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return;

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	ChainingHashElement* target = hashTable[index]; //wskaznik na element docelowy
	ChainingHashElement* prev = NULL; //element poprzedzajacy target

	//przej�cie na koniec �a�cucha
	while (target != NULL)
	{
		if (target->getKey() == key && &target->getValue() == &value) //poszukiwanie duplikatow
			break;

		prev = target;
		target = target->getNext();
	}
	if (target == NULL)
	{
		target = new ChainingHashElement(key, value);
		if (prev != NULL)
			prev->setNext(target);
		else
			hashTable[index] = target;
	}
	else
		target->setValue(value);
}

string ChainingHash::find(string key)
{
	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	ChainingHashElement* target = hashTable[index]; //wskaznik na element docelowy

	while (target != NULL)
	{
		if (target->getKey() == key) //poszukiwanie zgodnego klucza
			return target->getValue();

		target = target->getNext();
	}

	//nic nie znaleziono
	return "";
}

void ChainingHash::remove(string key)
{
	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	ChainingHashElement* target = hashTable[index]; //wskaznik na element docelowy
	ChainingHashElement* prev = NULL;

	bool found = false;
	while (target != NULL)
	{
		if (target->getKey() == key) //poszukiwanie zgodnego klucza
		{
			found = true; //znaleziono
			break;
		}

		prev = target;
		target = target->getNext();
	}

	if (found)//znaleziono klucz do usuniecia
	{
		if (prev != NULL)
			prev->setNext(target->getNext()); //laczenie poprzedniego i nastepnego
		else
			hashTable[index] = target->getNext(); //pierwszym elementem na liscie jest teraz nastepny wzgledem znalezionego

		delete target;
	}
}
