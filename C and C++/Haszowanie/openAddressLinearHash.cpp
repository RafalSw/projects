#include <iostream>
#include <string>
#include "hashFunction.h"
#include "openAddressLinearHash.h"

using namespace std;

OpenAddressLinearHash::OpenAddressLinearHash(int size, int m)
{
	hashTable = new OpenAddressHashElement*[size];
	sizeMax = size;
	currentSize = 0;
	mode = m;

	//czyszcznie tablicy
	for (int i = 0; i < size; i++)
		hashTable[i] = NULL;
}

OpenAddressLinearHash::~OpenAddressLinearHash()
{
	delete[] hashTable;
}

void OpenAddressLinearHash::add(string key, string& value)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return;

	if (currentSize == sizeMax)
	{
		cout << "Tablica jest przepelniona, nie mozna dodac kolejnego elementu" << endl;
		return;
	}

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index+counter)%sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan wolnego miejsca
		if (target == NULL)
		{
			hashTable[(index + counter) % sizeMax] = new OpenAddressHashElement(key, value);
			currentSize++;
			return;
		}
		else if (target->getKey() == "\0")
		{
			target->setKey(key);
			target->setValue(value);
			currentSize++;
			return;
		}
		counter++;
	}
	cout << "Nie udalo sie doda� elementu o kluczu <<" << key << ">>." << endl;
}

string OpenAddressLinearHash::find(string key)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return NULL;

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index + counter) % sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan
		if (target == NULL) //nie znaleziono elementu
		{
			return NULL;
		}
		else if (target->getKey() == "\0") //pomijamy elementy wczesniej usuniete
		{
		}
		else //znaleziono kandydata, weryfikacja
		{
			if (target->getKey() == key)
			{
				return target->getValue();
			}
		}
		//kandydat okazal sie niewlasciwy
		counter++;
	}
	return NULL;
}

void OpenAddressLinearHash::remove(string key)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return;

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index + counter) % sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan
		if (target == NULL) //nie znaleziono elementu
		{
			return;
		}
		else if (target->getKey() == "\0") //pomijamy elementy wczesniej usuniete
		{
		}
		else //znaleziono kandydata, weryfikacja
		{
			if (target->getKey() == key)
			{
				//usuwanie
				target->setKey("\0");
				currentSize--;
			}
		}
		//kandydat okazal sie niewlasciwy
		counter++;
	}
}
