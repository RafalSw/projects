//Program przedstawiajacy dzialanie wybranych metod haszuj�cych opartych o wybrane funkcje haszujace.
//Kluczem wyszukiwania jest slowo, a wynikiem kopia obiektu string, w kt�rym to s�owo si� znajduje.

//Program dzia�a w oparciu o refernencje, wi�c stringi �r�d�owe nie s� powielane. Nie mo�na by�o zastosowa� wska�nik�w, poniewa� nie s� one w przypadku string�w sta�e.

#include <ctime>
#include <fstream>
#include <iostream>
#include <vector>
#include "chainingHash.h"
#include "openAddressLinearHash.h"
#include "openAddressDoubleHash.h"
#include "openAddressQuadraticHash.h"
#include "openAddressLinearHashExtended.h"

using namespace std;

vector<string> readFile(string fileName) //wczytywanie pliku do wektora zdan;
{
	vector<string> result;
	string sentence = "";
	ifstream t(fileName, ios_base::in);
	if (!t.good())
	{
		cout << "Nie mozna otworzyc pliku!" << endl;
		return result;
	}

	char c;
	while (t.get(c))
	{
		sentence += c;
		if (c == '.' || c == '!' || c == '?')//znaleziono koniec zdania
		{
			if (sentence.length() > 3) //3 to minimalna dlugosc zdania
			{
				result.push_back(sentence);
				sentence = "";
			}
		}

	}
	//dodanie ostatniego zdania
	if (sentence.length() > 3) //3 to minimalna dlugosc zdania
	{
		result.push_back(sentence);
		sentence = "";
	}
	t.close();
	return result;
}

vector<string> getWordsFromSentence(string sentence)
{
	vector<string> result;
	string word = "";
	for (unsigned int i = 0; i < sentence.length(); i++)
	{
		if (isalpha(sentence.at(i))) //slowo sklada sie tylko z liter
			word += tolower(sentence.at(i));
		else //koniec slowa
		{
			result.push_back(word);
			word = "";
		}
	}

	if (word.length()>0) //ewentualna pozostalosc, jesli nie bylo kropki na koncu
		result.push_back(word);

	return result;
}

vector<string> randomWords(vector<string> sentences, int numberOfWords)
{
	vector<string> result;
	int i = 0;
	while (i < numberOfWords)
	{
		int s = rand() % sentences.size(); //losowanie zdania
		vector<string> wordsInSentence = getWordsFromSentence(sentences.at(s));

		int w = rand() % wordsInSentence.size(); //losowanie slowa
		string word = wordsInSentence.at(w);

		//sprawdzenie czy takiego slowa juz nie ma w wynikach
		bool found = false;
		for (unsigned int j = 0; j < result.size(); j++)
			if (word == result.at(j))
				found = true; //slowo juz istnieje

		if (found == false && word.length()>0) //znaleziono slowo, kt�rego nie ma w wynikach
		{
			result.push_back(word);
			i++;
		}
	}
	return result;
}

//sentences - wektor zawierajacy zdania
//analyzedWords - wektor zawierajacy wybrane wyrazy
//hashTableSize - rozmiar tablicy haszujacej
//method - metoda haszowania: 0-lancuchowa, 1-otwarta liniowa, 2- otwarta kwadratowa, 3-otwarta podwojna, 4-otwarta liniowa zmodyfikowana
//hashFunction - funkcja haszujaca: 0-modularna, 1-Bernsteina, 2-modularna uproszczona
//numberOfTests - liczba powtorzen, wieksza wartosc zwieksza dokladnosc testu ale wydluza czas
void testing(vector<string> sentences, vector<string> analyzedWords, int hashTableSize, int method, int hashFunction, int numberOfTests)
{
	double adding = 0;
	double finding = 0;
	double removing = 0;
	string methodName;
	string hashFunctionName;
	
	switch (method)
	{
	case 0:
		methodName = "\"Lancuchowa\"";
		break;
	case 1:
		methodName = "\"Adresowania otwartego - liniowa\"";
		break;
	case 2:
		methodName = "\"Adresowania otwartego - kwadratowa\"";
		break;
	case 3:
		methodName = "\"Adresowania otwartego - podwojna\"";
		break;
	case 4:
		methodName = "\"Adresowania otwartego - liniowa zmodyfikowana\"";
		break;
	}

	switch (hashFunction)
	{
	case 0:
		hashFunctionName = "\"Modularna\"";
		break;
	case 1:
		hashFunctionName = "\"Bernsteina \"";
		break;
	case 2:
		hashFunctionName = "\"Modularna uproszczona (do 3 znakow)\"";
		break;
	}

	clock_t summaryStart, summaryStop;
	summaryStart = clock();
	for (int test = 0; test < numberOfTests; test++)
	{
		//wybor metody
		HashMethod* hashMethod=NULL;
		switch (method)
		{
		case 0:
			hashMethod = new ChainingHash(hashTableSize, hashFunction);
			break;
		case 1:
			hashMethod = new OpenAddressLinearHash(hashTableSize, hashFunction);
			break;
		case 2:
			hashMethod = new OpenAddressQuadraticHash(hashTableSize, hashFunction);
			break;
		case 3:
			hashMethod = new OpenAddressDoubleHash(hashTableSize, hashFunction);
			break;
		case 4:
			hashMethod = new OpenAddressLinearHashExtended(hashTableSize, hashFunction);
			break;
		}

		//dodawanie slow
		clock_t addingStart, addingStop;
		addingStart = clock();
		for (unsigned int i = 0; i < sentences.size(); i++) //dla wszystkich zdan
		{
			vector<string>words = getWordsFromSentence(sentences.at(i));//dzielimy zdanie na wyrazy
			for (unsigned int j = 0; j < words.size(); j++)
			{
				hashMethod->add(words.at(j), sentences.at(i));
			}
		}
		addingStop = clock();
		adding += (addingStop - addingStart) / (double)(CLOCKS_PER_SEC / 1000);

		//wyszukiwanie slow
		clock_t findingStart, findingStop;
		findingStart = clock();
		for (unsigned int i = 0; i < analyzedWords.size(); i++) //dla wszystkich slow
		{
			string foundValue = hashMethod->find(analyzedWords.at(i));
			//cout << foundValue << endl;
		}
		findingStop = clock();
		finding += (findingStop - findingStart) / (double)(CLOCKS_PER_SEC / 1000);

		//usuwanie slow
		clock_t removingStart, removingStop;
		removingStart = clock();
		for (unsigned int i = 0; i < analyzedWords.size(); i++) //dla wszystkich slow
		{
			hashMethod->find(analyzedWords.at(i));
		}
		removingStop = clock();
		removing += (removingStop - removingStart) / (double)(CLOCKS_PER_SEC / 1000);

		delete hashMethod;
	}

	summaryStop = clock();
	cout << "Metoda "<<methodName <<" funkcja haszujaca: "<<hashFunctionName<< endl;
	cout << "\tW sumie:\t" << (summaryStop - summaryStart) / (double)(CLOCKS_PER_SEC / 1000) / numberOfTests << "\tms." << endl;
	cout << "\tDodawanie:\t" << adding / numberOfTests << "\tms." << endl;
	cout << "\tSzukanie:\t" << finding / numberOfTests << "\tms." << endl;
	cout << "\tUsuwanie:\t" << removing / numberOfTests << "\tms." << endl;
	cout << endl;
}

int main()
{
	int numberOfTests = 10; //ilosc powtorzen dla danej metody
	int numberOfAnalyzedWords = 150; //ilosc slow jakie sa wyszukiwane i usuwane (dla naturalnego max ok. 169)

	cout << "Inicjowanie slownika" << endl;
	vector<string> naturalSentences = readFile("jezyk naturalny.txt");
	vector<string> generatedSentences = readFile("sztuczny.txt");

	vector<string> naturalSentencesRandomWords = randomWords(naturalSentences, numberOfAnalyzedWords);
	vector<string> generatedSentencesRandomWords = randomWords(generatedSentences, numberOfAnalyzedWords);

	cout << "Koniec inicjalizacji slownika.\nRozpoczecie testow." << endl;

	cout << "Zdania jezyka naturalnego" << endl;

	//lancuchowa
	cout << "Test 1/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 500, 0, 0, numberOfTests);

	cout << "Test 2/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 500, 0, 1, numberOfTests);

	cout << "Test 3/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 500, 0, 2, numberOfTests);

	//otwarta liniowa
	cout << "Test 4/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 1, 0, numberOfTests);

	cout << "Test 5/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 1, 1, numberOfTests);

	cout << "Test 6/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 1, 2, numberOfTests);

	//otwarta kwadratowa
	cout << "Test 7/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 2, 0, numberOfTests);

	cout << "Test 8/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 2, 1, numberOfTests);

	cout << "Test 9/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 2, 2, numberOfTests);

	
	//otwarta liniowa podwojna
	cout << "Test 10/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 3, 0, numberOfTests);

	cout << "Test 11/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 3, 1, numberOfTests);

	cout << "Test 12/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 5000, 3, 2, numberOfTests);
	
	//otwarta liniowa zmodyfikowana
	cout << "Test 13/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 100, 4, 0, numberOfTests);

	cout << "Test 14/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 100, 4, 1, numberOfTests);

	cout << "Test 15/30" << endl;
	testing(naturalSentences, naturalSentencesRandomWords, 100, 4, 2, numberOfTests);




	cout << "Zdania szutczne" << endl;

	//lancuchowa
	cout << "Test 16/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 500, 0, 0, numberOfTests);

	cout << "Test 17/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 500, 0, 1, numberOfTests);

	cout << "Test 18/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 500, 0, 2, numberOfTests);

	//otwarta liniowa
	cout << "Test 19/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 1, 0, numberOfTests);

	cout << "Test 20/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 1, 1, numberOfTests);

	cout << "Test 21/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 1, 2, numberOfTests);

	//otwarta kwadratowa
	cout << "Test 22/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 2, 0, numberOfTests);

	cout << "Test 23/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 2, 1, numberOfTests);

	cout << "Test 24/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 2, 2, numberOfTests);

	//otwarta podwojna
	cout << "Test 25/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 3, 0, numberOfTests);

	cout << "Test 26/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 3, 1, numberOfTests);

	cout << "Test 27/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 5000, 3, 2, numberOfTests);

	//otwarta liniowa zmodyfikowana
	cout << "Test 28/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 100, 4, 0, numberOfTests);

	cout << "Test 29/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 100, 4, 1, numberOfTests);

	cout << "Test 30/30" << endl;
	testing(generatedSentences, generatedSentencesRandomWords, 100, 4, 2, numberOfTests);

	return 0;
}