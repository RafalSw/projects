//Haszowanie metoda adresowania otwartego (wersja liniowa) ze zmian� rozmiaru tablicy

#pragma once //naglowek includowany jest tylko raz

#include <string>
#include "hashMethod.h"
#include "openAddressHashElement.h"

using namespace std;
class OpenAddressLinearHashExtended:public HashMethod
{
private:
	OpenAddressHashElement** hashTable; //tablica wskaznikow na elementy
	int sizeMax; // rozmiar tablicy
	int mode; //typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	int currentSize;
	void transform(int newSize);
public:
	OpenAddressLinearHashExtended(int size, int mode); //size - rozmiar tablicy, mode - typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	~OpenAddressLinearHashExtended();
	void add(string key, string& value);
	string find(string key);
	void remove(string key);
};