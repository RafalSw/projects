#pragma once //naglowek includowany jest tylko raz

#include<string>
using namespace std;

class HashFunction
{
private:
	static const int simplifiedSize = 3; //ilosc znakow branych pod uwage przy funkcji uproszczonej modularnej, jesli nie zostalo podane jako argument
public:
	static int modularHash(const char* s, int limit); //s - slowo do haszowania, limit - rozmiar tablicy haszujacej
	static int bernsteinHash(const char* s, int limit); //s - slowo do haszowania, limit - rozmiar tablicy haszujacej
	static int simplifiedModularHash(const char* s, int limit, int size); //s - slowo do haszowania, limit - rozmiar tablicy haszujacej, size - ilosc znakow uzytych do haszowania
	static int hash(const char* s, int limit, int mode, int size = simplifiedSize); //s - slowo do haszowania, limit - rozmiar tablicy haszujacej, mode - wybor funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna), size - ilosc znakow uzytych do haszowania przy funkcji uproszczonej modularnej
};
