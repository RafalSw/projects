//Haszowanie metoda lancuchowa

#pragma once //naglowek includowany jest tylko raz

#include <string>
#include "hashMethod.h"

using namespace std;

class ChainingHashElement
{
private:
	string value;
	string key;
	ChainingHashElement* next;
public:
	ChainingHashElement(string k, string& v);
	ChainingHashElement* getNext();
	void setNext(ChainingHashElement* n);
	string getKey();
	string& getValue();
	void setValue(string& v);
};

class ChainingHash: public HashMethod
{
private:
	ChainingHashElement** hashTable; //tablica wskaznikow na elementy
	int sizeMax; // rozmiar tablicy
	int mode; //typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
public:
	ChainingHash(int size, int mode); //size - rozmiar tablicy, mode - typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	~ChainingHash();
	void add(string key, string& value);
	string find(string key);
	void remove(string key);
};
