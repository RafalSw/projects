#include <iostream>
#include <string>
#include "hashFunction.h"
#include "openAddressLinearHashExtended.h"

using namespace std;

OpenAddressLinearHashExtended::OpenAddressLinearHashExtended(int size, int m)
{
	hashTable = new OpenAddressHashElement*[size];
	sizeMax = size;
	currentSize = 0;
	mode = m;

	//czyszcznie tablicy
	for (int i = 0; i < size; i++)
		hashTable[i] = NULL;
}

OpenAddressLinearHashExtended::~OpenAddressLinearHashExtended()
{
	delete[] hashTable;
}

void OpenAddressLinearHashExtended::add(string key, string& value)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return;

	if (currentSize == sizeMax)
	{
		cout << "Tablica jest przepelniona, nie mozna dodac kolejnego elementu" << endl;
		return;
	}

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index+counter)%sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan wolnego miejsca
		if (target == NULL)
		{
			hashTable[(index + counter) % sizeMax] = new OpenAddressHashElement(key, value);
			currentSize++;

			if (((double)currentSize) / sizeMax > 0.5) //sprawdzenie warunku na powiekszenie tablicy
				transform(sizeMax * 2); //powiekszenie tablicy

			return;
		}
		else if (target->getKey() == "\0")
		{
			target->setKey(key);
			target->setValue(value);
			currentSize++;

			if (((double)currentSize) / sizeMax > 0.5) //sprawdzenie warunku na powiekszenie tablicy
				transform(sizeMax * 2); //powiekszenie tablicy

			return;
		}
		counter++;
	}
	cout << "Nie udalo sie doda� elementu o kluczu <<" << key << ">>." << endl;
}

string OpenAddressLinearHashExtended::find(string key)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return NULL;

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index + counter) % sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan
		if (target == NULL) //nie znaleziono elementu
		{
			return NULL;
		}
		else if (target->getKey() == "\0") //pomijamy elementy wczesniej usuniete
		{
		}
		else //znaleziono kandydata, weryfikacja
		{
			if (target->getKey() == key)
			{
				return target->getValue();
			}
		}
		//kandydat okazal sie niewlasciwy
		counter++;
	}
	return NULL;
}

void OpenAddressLinearHashExtended::remove(string key)
{
	if (key == "\0" || key == "") //weryfikacja poprawnosci klucza
		return;

	int index = HashFunction::hash(key.c_str(), sizeMax, mode);
	OpenAddressHashElement* target; //wskaznik na element docelowy
	int counter = 0;

	while (counter < sizeMax)
	{
		target = hashTable[(index + counter) % sizeMax]; //(index+counter)%sizeMax zapewnia cyklicznosc poszukiwan
		if (target == NULL) //nie znaleziono elementu
		{
			return;
		}
		else if (target->getKey() == "\0") //pomijamy elementy wczesniej usuniete
		{
		}
		else //znaleziono kandydata, weryfikacja
		{
			if (target->getKey() == key)
			{
				//usuwanie
				target->setKey("\0");
				currentSize--;
				if ((double)currentSize / sizeMax < 0.125) //sprawdzenie warunku na zmniejszenie tablicy
					transform(sizeMax / 2); //zmniejszenie tablicy
			}
		}
		//kandydat okazal sie niewlasciwy
		counter++;
	}
}

void OpenAddressLinearHashExtended::transform(int newSize)
{
	OpenAddressHashElement** old = hashTable;
	int oldSize = sizeMax;

	hashTable = new OpenAddressHashElement*[newSize];
	sizeMax = newSize;
	currentSize = 0;


	//czyszcznie tablicy
	for (int i = 0; i < newSize; i++)
		hashTable[i] = NULL;


	for (int i = 0; i < oldSize; i++)
	{
		if (old[i] != NULL)
		{
			string key = old[i]->getKey();
			string& value = old[i]->getValue();

			if (key != "\0")
			{
				add(key, value);
			}

		}
	}
	delete[] old;
}
