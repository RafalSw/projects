#include <iostream>
#include "hashFunction.h"

using namespace std;

int HashFunction::modularHash(const char* s, int limit)
{
	int sum = 0;
	for (int i = 0; s[i]!='\0'; i++)
		sum += (int)s[i];
	return sum % limit;
}

int HashFunction::bernsteinHash(const char* s, int limit) //http://www.cse.yorku.ca/~oz/hash.html
{
	unsigned long hash = 5381;

	for (int i = 0; s[i]!='\0'; i++)
		hash = ((hash << 5) + hash) + (int)s[i]; /* hash * 33 + c */

	return hash % limit;
}

int HashFunction::simplifiedModularHash(const char* s, int limit, int size = simplifiedSize)
{
	int sum = 0;
	for (int i = 0; s[i] != '\0' && i<size; i++)
		sum += (int)s[i];

	return sum % limit;
}

int HashFunction::hash(const char* s, int limit, int mode, int size)
{
	if (mode == 0)
	{
		return modularHash(s, limit);
	}
	if (mode == 1)
	{
		return bernsteinHash(s, limit);
	}
	if (mode == 2)
		return simplifiedModularHash(s, limit, size);
	
	cout << "Wybor funkcji haszujacej (" << mode << ") nie jest dozwolony!" << endl;
	return -1;
}
