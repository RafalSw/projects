//Haszowanie metoda adresowania otwartego (wersja liniowa)
//Adresowanie kwadratowe zblizone do liniowego, ale uzywamy hashTable[(index+counter*counter)%sizeMax] zamiast hashTable[(index+counter)%sizeMax]
#pragma once //naglowek includowany jest tylko raz

#include <string>
#include "hashMethod.h"
#include "openAddressHashElement.h"

using namespace std;
class OpenAddressQuadraticHash: public HashMethod
{
private:
	OpenAddressHashElement** hashTable; //tablica wskaznikow na elementy
	int sizeMax; // rozmiar tablicy
	int mode; //typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	int currentSize;
public:
	OpenAddressQuadraticHash(int size, int mode); //size - rozmiar tablicy, mode - typ funkcji haszujacej (0-modularna, 1-Bernsteina, 2 - uproszczona modularna)
	~OpenAddressQuadraticHash();
	void add(string key, string& value);
	string find(string key);
	void remove(string key);
};