#include <string>
#include "openAddressHashElement.h"
using namespace std;

OpenAddressHashElement::OpenAddressHashElement(string k, string& v)
{
	value = v;
	key = k;
}

string OpenAddressHashElement::getKey()
{
	return key;
}

string& OpenAddressHashElement::getValue()
{
	return value;
}

void OpenAddressHashElement::setKey(string k)
{
	key = k;
}

void OpenAddressHashElement::setValue(string& v)
{
	value = v;
}