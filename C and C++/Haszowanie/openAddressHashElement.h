#pragma once //naglowek includowany jest tylko raz

#include <string>

using namespace std;

class OpenAddressHashElement
{
private:
	string value;
	string key;
public:
	OpenAddressHashElement(string k, string& v);
	string getKey();
	string& getValue();
	void setKey(string k);
	void setValue(string& v);
};