#pragma once

#include <string>
using namespace std;

//klasa abstrakcyjna do dziedziczenia, dla ulatwienia testowania
class HashMethod
{
public:
	virtual void add(string key, string& value)=0;
	virtual string find(string key)=0;
	virtual void remove(string key)=0;
};