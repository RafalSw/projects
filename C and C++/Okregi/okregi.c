#define _CRT_SECURE_NO_WARNINGS /* tylko w VisualStudio - w innych srodowiskach do usuniecia! */
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 10
#define M_PI 3.14159265358979323846

typedef struct /*zdefiniowanie struktury, typedef pozwala uzywac "Okrag" zamiast "struct Okrag" */
{
	int x;
	int y;
	int r;
}Okrag;


void stworzOkregi(Okrag* okregi)
{
	int i;
	srand(time(NULL)); /* niezbedne do losowania */
	for(i=0; i<N; i++) /* dla kazdego okregu nastepuje losowanie jego wartosci */
	{
		okregi[i].r=rand()%10+1; /* losuje liczby od 0 do 9 i przesuwa o jedna w prawo - dostajemy od 1 do 10 - promien nie powinien byc <=0 */
		okregi[i].x=rand()%21-10; /* losuje liczby od 0 do 20 i przesuwa je o 10 w lewo */
		okregi[i].y=rand()%21-10; /* losuje liczby od 0 do 20 i przesuwa je o 10 w lewo */
	}
}

void sprawdzPrzecinanie(Okrag* okregi)
{
	/* aby okregi sie przecinaly odleglosc srodkow musi byc mniejsza niz suma promieni
		jednoczesnie odleglosc musi byc wieksza niz wartosc bezwzgledna roznicy promieni */
	int i, j, odlegloscSrodkow, sumaPromieni, roznicaPromieni;
	FILE* zapis=fopen("przecinajaceSieOkregi.txt","w");
	if(zapis==NULL)
	{
		printf("Nie udalo sie uzyskac dostepu do pliku! Dzialanie funkcji sprawdzPrzecinanie przerwane.");
		return;
	}
	else
	{
		for(i=0; i<N-1; i++)
		{
			for(j=i+1; j<N; j++) /* sprawdzenie na wszystkich mozliwych parach - sprawdzamy czy i-ty przecina sie z kolejnymi */
			{
				sumaPromieni=okregi[i].r+okregi[j].r;
				roznicaPromieni=abs(okregi[i].r-okregi[j].r);
				odlegloscSrodkow=sqrt((double)(okregi[i].x-okregi[j].x)*(okregi[i].x-okregi[j].x)+(okregi[i].y-okregi[j].y)*(okregi[i].y-okregi[j].y));

				if(sumaPromieni>=odlegloscSrodkow && roznicaPromieni<=odlegloscSrodkow)
				{
					/* okregi i-ty i j-ty sie przecinaja! */
					printf("Okregi (%d, %d, %d) i (%d, %d, %d) przecianja sie.\n",okregi[i].x,okregi[i].y, okregi[i].r, okregi[j].x,okregi[j].y, okregi[j].r);
					fprintf(zapis,"Okregi (%d, %d, %d) i (%d, %d, %d) przecianja sie.\n",okregi[i].x,okregi[i].y, okregi[i].r, okregi[j].x,okregi[j].y, okregi[j].r);
				}

			}
		}
		fclose(zapis);
	}
}

void sortowanieOkregow(Okrag* okregi)
{
	int i, j;
	double pole1, pole2;

	/* sortowanie babelkowe */
	for(i=0; i<N-1; i++)
	{
		for(j=0; j<N-1; j++)
		{
			/* porownanie pola j-tego i kolejnego okregu */
			pole1=M_PI*okregi[j].r*okregi[j].r;
			pole2=M_PI*okregi[j+1].r*okregi[j+1].r;
			if(pole1<pole2)
			{
				/* zamiana miejscami */
				Okrag temp = okregi[j];
				okregi[j]=okregi[j+1];
				okregi[j+1]=temp;
			}
		}
	}

	/* wypisanie wszystkich pol */
	for(i=0; i<N; i++)
	{
		pole1=M_PI*okregi[i].r*okregi[i].r;
		printf("okrag (%d, %d, %d) ma pole: %f\n", okregi[i].x,okregi[i].y, okregi[i].r, pole1);
	}

}

int main()
{
	int i;
	Okrag okregi[N]; /* globalna tablica przechowująca N okręgów */

	printf("-------------Test funkcji stworzOkregi()--------------\n");
	stworzOkregi(okregi);
	for(i=0; i<N; i++) /*wypisanie utworzonych okregow*/
	{
		printf("%d. (%d, %d, %d)\n",i,okregi[i].x,okregi[i].y, okregi[i].r);
	}
	printf("---------Koniec testu funkcji stworzOkregi()-----------\n");


	printf("\n-------------Test funkcji sprawdzPrzecinanie()----------\n");
	sprawdzPrzecinanie(okregi);
	printf("---------Koniec testu funkcji sprawdzPrzecinanie()------\n");


	printf("\n-------------Test funkcji sortowanieOkregow()-----------\n");
	sortowanieOkregow(okregi);
	printf("---------Koniec testu funkcji sortowanieOkregow()-------\n");

	return 0;
}
