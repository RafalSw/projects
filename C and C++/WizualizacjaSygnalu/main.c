#define _USE_MATH_DEFINES
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

typedef struct
{
	double A0;
	double A;
	double fs;
	double fp;
	double t;
	double fi;
}Parametry;

typedef struct
{
	double *sygnal;
	double *czas;
	int ilosc;
}Tablica;

void generuj(Parametry *p,Tablica *t);
void zapisz (Tablica *t);
void eksport (Tablica *t);
void pobierz(Parametry *p);
void wyswietl(Tablica *t);
void filtruj_sygnal(Tablica *t);

int main(void)
{
	char c='1'; //dowolna warto��
	Parametry p={0,1,1,10,1,0};
	Tablica t={0,0,0};
	
	while(c!='0')
	{
		printf("1. Wprowadz parametry\n2. Wygeneruj sygnal\n3. Filtruj sygnal\n4. Wyswietl wartosci\n5. Zapisz wartosci\n6. Utworz wykres\n0. Wyjscie\n\n");
		scanf(" %c",&c); //spacja przed % nie jest przypadkowa - wymusza zignorowanie znaku nowej linii, kt�ry czasem si� zaplacze w buforze wej�ciowym (w koncu potwierdzamy enterem)

		switch(c)
		{
		case '1':
			printf("Pobieranie parametrow.\n");
			pobierz(&p);
			break;

		case '2':
			printf("Generowanie sygnalu.\n");
			generuj(&p,&t);
			break;

		case '3':
			printf("Filtrowanie sygnalu.\n");
			filtruj_sygnal(&t);
			break;

		case '4':
			wyswietl(&t);
			break;

		case '5':
			zapisz(&t);
			break;

		case '6':
			eksport(&t);
			break;
		
		case '0':
			if(t.czas)
				free(t.czas);
			if(t.sygnal)
				free(t.sygnal);
			return 0;
		}
		printf("\n");
	}
}

void generuj(Parametry *p, Tablica *t)
{
	int i;
	int ilosc=p->fp*p->t;
	
	//jesli juz cos bylo to usuwamy unikajac wyciekow pamieci
	if(t->czas)
		free(t->czas);
	if(t->sygnal)
		free(t->sygnal);

	t->sygnal=(double*)malloc(sizeof(double)*ilosc);
	t->czas=  (double*)malloc(sizeof(double)*ilosc);
	t->ilosc=ilosc;

	srand(time(NULL));

	for(i=0;i<ilosc;++i)
	{
		t->sygnal[i]=p->A0+p->A*sin(
			2.0*M_PI*(p->fs/p->fp)*i+p->fi)+(((((double)rand()*2)-1)/INT_MAX)*0.1*p->A);
		t->czas[i]=(double)i/p->fp;
	}
}

void zapisz (Tablica *t)
{
	char nazwa[10];
	char nazwaZRozszerzeniem[14];
	int i;
	FILE*plik;

	printf("Zapis danych do pliku\nNazwa pliku: ");
	scanf("%s", &nazwa);
	strcpy(nazwaZRozszerzeniem, nazwa);
	strcat(nazwaZRozszerzeniem,".txt");
	plik= fopen(nazwaZRozszerzeniem, "w");

	for(i=0;i<t->ilosc;i++)
	{
		fprintf(plik,"wartosc probki to %lf, czas: %lf\n", t->sygnal[i], t->czas[i]);
	}

	fclose(plik);
}

void eksport(Tablica *t)
{
	char nazwa[10];
	char nazwaZRozszerzeniem[14];
	int i;
	FILE*plik;

	printf("nazwa pliku to:");
	scanf("%s.html", &nazwa);
	strcpy(nazwaZRozszerzeniem, nazwa);
	strcat(nazwaZRozszerzeniem,".html");
	plik= fopen(nazwaZRozszerzeniem, "w");

	fprintf(plik,"<html>\n<head>\n\t<script type=\"text/javascript\" src=\"https://www.google.com/jsapi\"></script>\n\t<script type=\"text/javascript\">\n\t\tgoogle.load(\"visualization\", \"1\", {packages:[\"corechart\"]});\n");
	fprintf(plik,"\t\tgoogle.setOnLoadCallback(drawChart);\n\t\tfunction drawChart() {\n\t\tvar data = google.visualization.arrayToDataTable([\n\t\t\t['Czas', 'Wartosc'],\n");
	for(i=0;i<t->ilosc;i++)
	{
		fprintf(plik,"\t\t\t['%.02lf',%lf],\n", t->czas[i],t->sygnal[i]);
	}
	fprintf(plik,"\t\t]);\n\tvar options = {\n\t\ttitle: 'Wartosc funkcji w czasie',\n\t\tcurveType: 'function',\n\t\tlegend: { position: 'bottom' }\n\t};\n\tvar chart = new google.visualization.LineChart(document.getElementById('chart_div'));\n\tchart.draw(data, options);\n\t}\n\t</script>\n</head>\n");
	fprintf(plik,"<body>\n\t<div id=\"chart_div\" style=\"width: 900px; height: 500px;\"></div>\n</body>\n</html>");

	fclose(plik);
}

void pobierz(Parametry *p)
{
	printf("Obecna amplituda poczatkowa sygnalu to %lf\nNowa: ", p->A0);
	scanf(" %lf", &p->A0);
	printf("Obecna amplituda sygnalu to %lf\nNowa: ", p->A);
	scanf(" %lf", &p->A);
	printf("Obecna czestotliwosc sygnalu to %lf\nNowa: ", p->fs);
	scanf(" %lf", &p->fs);
	printf("Obecna czestotliwosc probek to %lf\nNowa: ", p->fp);
	scanf(" %lf", &p->fp);
	printf("Obecny czas trwania sygnalu to %lf\nNowy: ", p->t);
	scanf(" %lf", &p->t);
	printf("Obecne przesuniecie fazowe sygnalu to %lf\nNowe: ", p->fi);
	scanf(" %lf", &p->fi);
}

void wyswietl(Tablica *t)
{
	int i;
	for(i=0;i<t->ilosc;i++)
	{
		printf("wartosc probki to %lf, czas: %lf\n", t->sygnal[i], t->czas[i]);
	}
}

void filtruj_sygnal(Tablica *t)
{
	int i;

	t->sygnal[0]=(t->sygnal[0]+t->sygnal[1])/2;
	t->sygnal[1]=(t->sygnal[0]+t->sygnal[1]+t->sygnal[2])/3;

	for(i=2;i<t->ilosc-1;++i)
	{
		t->sygnal[i]=(t->sygnal[i-2]+t->sygnal[i-1]+t->sygnal[i]+t->sygnal[i+1]+t->sygnal[i+2])/5;

	}

	t->sygnal[t->ilosc-1]=(t->sygnal[i-1]+t->sygnal[i]+t->sygnal[i-2])/3;
	t->sygnal[t->ilosc]=(t->sygnal[i-1]+t->sygnal[i])/2;
}
