#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <cmath>

const double M_PI = 3.141592653589793238462643383279502884L; /* pi */

using namespace std;
class Station;
vector<Station> stations;

class Station
{
public:
	Station()
	{
		for(int i = 0; i < 7; i++)
		{
			days[i]=0;
		}
	}

	string name;
	double power;
	double frequency;
	bool days[7];
	double longitude;
	double latitude;
};

vector<string> splitString(string s)
{
	vector<string> result;
	int start = 0;
	int stop;
	while(s.find(',',start) != s.npos)
	{
		stop = s.find(',',start);
		result.push_back(s.substr(start,stop-start));
		start = stop+1;
	}
	return result;
}


bool readLongitude(const string longStr, double & longitude)
{
	if(longStr.size() == 0)
		return false;
	try
	{
		if(longStr == "0")
		{
			longitude = 0;
		}
		else if(longStr.at(longStr.size()-1) == 'E' )
		{
			longitude = stoi(longStr.substr(0, longStr.size()-1));
			if( longitude < 0 || longitude > 18000 )
				return false;
		}
		else if(longStr.at(longStr.size()-1) == 'W' )
		{
			longitude = -1 * stoi(longStr.substr(0, longStr.size()-1));
			if( longitude > 0 || longitude < -18000 )
				return false;
		}
		else
		{
			return false;
		}
	}
	catch( ... )
	{
		return false;
	}
	longitude = ((int)longitude%100)/60 + ((int)longitude/100);
	return true;
}


bool readLatitude(const string latiStr, double & latitude)
{
	if(latiStr.size() == 0)
		return false;

	try{
		if(latiStr == "0")
		{
			latitude = 0;
		}
		else if(latiStr.at(latiStr.size()-1) == 'N' )
		{
			latitude = stoi(latiStr.substr(0, latiStr.size()-1));
			if( latitude<0 || latitude > 9000 )
				return false;
		}
		else if(latiStr.at(latiStr.size()-1) == 'S' )
		{
			latitude = -1 * stoi(latiStr.substr(0, latiStr.size()-1));
			if( latitude>0 || latitude < -9000 )
				return false;
		}
		else
		{
			return false;
		}
	}
	catch( ... )
	{
		return false;
	}
	latitude = ((int)latitude%100)/60 + ((int)latitude/100);
	latitude/=100;
	return true;
}


void readFile()
{
	cout<<"Read file - start"<<endl;
	int errors = 0;
	ifstream fileIn;
	string buffer;
	fileIn.open("iocham.txt", ifstream::in);
	if(!fileIn.good())
	{
		cout<<"Read file - error"<<endl;
		return;
	}
	while(fileIn.good())
	{
		getline(fileIn, buffer);
		vector<string> data = splitString(buffer);
		if(data.size()<27)
		{
			errors++;
			continue;
		}
		else
		{
			Station temp;
			string::size_type sz;
			temp.frequency = stod(data.at(0),&sz);
			try
			{
				temp.power = stod(data.at(8),&sz);
			}
			catch ( ... )
			{
				errors++;
				continue;
			}
			temp.name = data.at(1);

			string d = "1234567";
			for(int i=0;i<7;i++)
			{
				if(data.at(3).at(i) == d.at(i))
				{
					temp.days[i]=true;
				}
			}



			double longitude = -99999;
			if(!readLongitude(data.at(25), longitude))
			{
				errors++;
				continue;
			}


			double latitude = -99999;
			if(!readLatitude(data.at(26), latitude))
			{
				errors++;
				continue;
			}
			temp.longitude = longitude;
			temp.latitude = latitude;
			stations.push_back(temp);
		}
	}
	cout<<"Reading file - ready. Errors: "<<errors<<endl;
}


double cost(Station s, double freq, int day, double longitude, double latitude)
{
	if(abs(s.frequency-freq)>0.001)//rozne czestotliwosci
		return 0;

	if(!s.days[day-1])//w tym dniu nie nadaje
		return 0;

	// na podstawie: http://pl.wikibooks.org/wiki/Astronomiczne_podstawy_geografii/Odleg%C5%82o%C5%9Bci
	double distance = sqrt(((cos(M_PI*s.longitude/180)*(latitude-s.latitude))*(cos(M_PI*s.longitude/180)*(latitude-s.latitude))) + ((longitude-s.longitude)*(longitude-s.longitude))) * M_PI * 12756.274/360;
		
	
	if(distance == 0)
		return DBL_MAX;
	else
		return 10000*s.power/distance/distance/distance; //moc maleje do sześcianu odległości.
}


string getStationName(double freq, int day, double longitude, double latitude)
{
	string result = "";
	if(stations.size() != 0)
	{
		double *tab = new double[stations.size()];

		for(unsigned int i = 0; i<stations.size(); i++)
		{
			tab[i] = cost(stations.at(i), freq, day, longitude, latitude);
		}


		double max = tab[0];
		int maxIdx = 0;

		for(unsigned int i = 1; i<stations.size(); i++)
		{
			if( max < tab[i] )
			{
				max = tab[i];
				maxIdx = i;
			}
		}
		result = stations.at(maxIdx).name;
		if(max == 0)
		{
			result = "No stations";
		}

		delete[] tab;
	}
	else
	{
		result = "No stations";
	}

	return result;

}

void searchByDay(int d)
{
	string previousName = "";
	for(unsigned int i = 0; i < stations.size(); i++)
	{
		if(stations.at(i).days[d] == true)
		{
			if(stations.at(i).name!=previousName )
			{
				cout<<stations.at(i).name<<endl;
			}
			previousName = stations.at(i).name;
		}

	}
}

void searchByFreq(double d)
{
	string previousName = "";
	for(unsigned int i = 0; i < stations.size(); i++)
	{
		if(stations.at(i).frequency == d)
		{
			if(stations.at(i).name!=previousName )
			{
				cout<<stations.at(i).name<<endl;
			}
			previousName = stations.at(i).name;
		}
	}
}

void searchByPosition(double x, double y)
{
	string previousName = "";
	for(unsigned int i = 0; i < stations.size(); i++)
	{
		if(stations.at(i).latitude == x && stations.at(i).longitude == y)
		{
			if(stations.at(i).name!=previousName )
			{
				cout<<stations.at(i).name<<endl;
			}
			previousName = stations.at(i).name;
		}
	}
}


int search()
{
	string freqStr, dayStr, longStr, latiStr;
	char c;
	cout<<"Search by:\n1.Frequency\n2.Day\n3.Position\n4.Multisearch"<<endl;
	cin>>c;

	string::size_type sz;
	switch(c)
	{
		case '1':
		{
			cout<<"Frequency (e.g. 1234.50): ";
			cin>>freqStr;
			double frequency = stod(freqStr,&sz);
			if ( frequency <=0 )
			{
				return -1;
			}
			cout<<"Result: "<<endl;
			searchByFreq(frequency);
			cout<<"\n\n\n"<<endl;
			break;
		}
		case '2':
		{
			cout<<"Day (e.g. 4): ";
			cin>>dayStr;
			int day = stoi(dayStr);
			if( day < 1 || day > 7 )
			{
				return -1;
			}
			cout<<"Result: "<<endl;
			searchByDay(day);
			cout<<"\n\n\n"<<endl;
			break;
		}
		case '3':
		{
			cout<<"Longitude (e.g. 11950E): ";
			cin>>longStr;
			cout<<"Latitude (e.g. 1528N): ";
			cin>>latiStr;
			double longitude = -99999;
			if(!readLongitude(longStr, longitude))
			{
				return -1;
			}


			double latitude = -99999;
			if(!readLatitude(latiStr, latitude))
			{
				return -1;
			}
			cout<<"Result: "<<endl;
			searchByPosition(latitude, longitude);
			cout<<"\n\n\n"<<endl;
			break;
		}
		case '4':
		{
			cout<<"Frequency (e.g. 1234.50): ";
			cin>>freqStr;
			cout<<"Day (e.g. 4): ";
			cin>>dayStr;
			cout<<"Longitude (e.g. 11950E): ";
			cin>>longStr;
			cout<<"Latitude (e.g. 1528N): ";
			cin>>latiStr;

			cout<<"\nSearching..."<<endl;
		
			
			double frequency = stod(freqStr,&sz);
			if ( frequency <=0 )
			{
				return -1;
			}
	
			int day = stoi(dayStr);
			if( day < 1 || day > 7 )
			{
				return -1;
			}

			double longitude = -99999;
			if(!readLongitude(longStr, longitude))
			{
				return -1;
			}


			double latitude = -99999;
			if(!readLatitude(latiStr, latitude))
			{
				return -1;
			}

		
			string result = getStationName(frequency,day,longitude, latitude);
			cout<<"Result: "<<result<<"\n\n\n"<<endl;
			break;
		}
		default:
		{
			break;
		}
	}
	return 0;
}


int main()
{
	readFile();

	while(true)
	{
		if(search() == -1)
		{
			cout<<"Wrong data"<<endl;
		}
		char c;
		cout<<"Another one? (Y/N): ";
		cin>>c;
		switch(c)
		{
			case 'Y':
			{
				break;
			}
			case 'N':
			default:
			{
				cout<<"Wrong key -> exit."<<endl;
				return 0 ;
			}
		}

	}
	system("PAUSE");
}
