#include <iostream>
#include "priorityQueue.h"

using namespace std;

void PriorityQueue::Heapify(int l, int r)
{
	int j, k, x;
	j = l;
	k = 2 * j;
	x = tab[j];

	while (k<=r)
	{
		if (k + 1 <= r)
		{
			if (tab[k] > tab[k + 1])
				k += 1;
		}
		if (x > tab[k])
		{
			tab[j] = tab[k];
			j = k;
			k = 2 * j;
		}
		else
			break;
	}
	tab[j] = x;
}

PriorityQueue::PriorityQueue()
{
	int size = INT_MIN;
	cout << "Podaj maksymalny rozmiar kolejki" << endl;
	while (size < 1)
	{
		cin >> size;
		if (size < 1)
		{
			cout << "Rozmiar nie jest poprawny. Podaj nowy rozmiar!" << endl;
		}
	}
	sizeMax = size+1; //+1 bo kolejka przechowywana od elementu 1 a nie 0
	tab = new int[sizeMax]; 
	n = 0;
}

int PriorityQueue::FindMin()
{
	if (n < 1)
	{
		cout << "Kolejka jest pusta" << endl;
		return INT_MIN;
	}
			
	return tab[1];
}

void PriorityQueue::Enqueue(int i)
{
	if (sizeMax > n + 1) //sprawdzenie czy nowy element jeszcze sie zmiesci
	{
		n++;
		tab[n] = i;
		Heapify(1,n);
		int k=0;
	}
	else
		cout << "Kolejka jest przepelniona!" << endl;
}

void PriorityQueue::Dequeue()
{
	if (n < 1)
		cout << "Kolejka jest pusta" << endl;
	else
	{
		tab[1] = tab[n];
		n--;
		Heapify(1, n);
	}
}


