#include <iostream>
#include "priorityQueue.h"

using namespace std;

int main()
{
	PriorityQueue queue;

	char c = '\0';
	while (c != 'q')
	{
		cout << "1. Dodaj element" << endl;
		cout << "2. Usun najmniejszy element" << endl;
		cout << "3. Wypisz najmniejszy element" << endl;
		cout << "q. Wyjscie" << endl;
		cin >> c;
		switch (c)
		{
		case '1':
		{
			int a;
			cout << "Podaj wartosc elementu: ";
			cin >> a;
			queue.Enqueue(a);
			break;
		}
		case '2':
		{
			queue.Dequeue();
			break;
		}
		case '3':
		{
			if (queue.FindMin() != INT_MIN)
				cout << "Najmniejszy element to: " << queue.FindMin() << endl;
			break;
		}
		case 'q':
			return 0;
		default:
		{
			cout << "Nie ma takiego wyboru." << endl;
		}
		}
	}
}