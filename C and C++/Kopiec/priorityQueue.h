#ifndef PRIORITY_QUEUE_H
#define PRIORITY_QUEUE_H

class PriorityQueue
{
private:
	int *tab;
	int n; //obecny rozmiar
	int sizeMax; //maksymalny rozmiar

	void Heapify(int l, int r); //kopcowanie tablicy od l do r

public:
	PriorityQueue();
	int FindMin();
	void Enqueue(int i);
	void Dequeue();
};
#endif