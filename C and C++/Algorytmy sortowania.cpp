//mergesort

int* mergesort(int* tab1, int tab1Length, int* tab2, int tab2Length) //wskaznik na tablice wraz z ich dlugosciami
{
	if(tab1 == NULL || tab2 == NULL) //wskazniki na ktoras z tablic sa puste
		return NULL;

	int* result = new int[tab1Length+tab2Length];

	int tab1Iterator=0, tab2Iterator=0, resultIterator=0;

	while(tab1Iterator<tab1Length && tab2Iterator<tab2Length) //nie doszlismy do konca zadnej z tablic
	{
		if(tab1[tab1Iterator]<tab2[tab2Iterator])
		{
			result[resultIterator]=tab1[tab1Iterator];
			resultIterator++;
			tab1Iterator++;
		}
		else
		{
			result[resultIterator]=tab2[tab2Iterator];
			resultIterator++;
			tab2Iterator++;
		}
	}

	while(tab1Iterator<tab1Length) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
	{
		result[resultIterator]=tab1[tab1Iterator];
		resultIterator++;
		tab1Iterator++;
	}

	while(tab2Iterator<tab2Length) //tab1 okazala sie dluzsza, przepisujemy jej koncowke do tablicy z wynikami
	{
		result[resultIterator]=tab2[tab2Iterator];
		resultIterator++;
		tab2Iterator++;
	}

	return result;
}