#include <iostream>
#include <ctime>

using namespace std;

void swap(int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}

int partition(int*tab, int start, int stop)
{
	int i = start-1;
	int x = tab[stop];
	for (int j  = start; j < stop; j++)
		if (tab[j] <= x)
		{
			i++;
			swap(&tab[i], &tab[j]);

		}
	swap(tab[i+1],tab[stop]);
	return i+1;
}

void quickSort(int *tab, int start, int stop)
{
	if(start<stop)
	{
		int i=partition(tab,start,stop);
		quickSort(tab,start,i-1);
		quickSort(tab,i+1,stop);
	}
}


int partitionRandomized(int*tab, int start, int stop)
{
	int i = rand()%(stop-start) + start;
	swap(&tab[i], &tab[start]);
	return partition(tab,start,stop);
}

void quickSortRandomized(int *tab, int start, int stop)
{
	if(start<stop)
	{
		int i=partitionRandomized(tab,start,stop);
		quickSortRandomized(tab,start,i-1);
		quickSortRandomized(tab,i+1,stop);
	}
}

int hoarePartition(int *tab, int start, int stop)
{
	int i=start-1;
	int j=stop;
	int x=tab[start];
    while (true) {
        do
		{
			j--;
		}while (tab[j] > x);
        do
		{
			i++;
		}while (tab[i] < x);
        if(i < j) 
            swap(&tab[i],&tab[j]);
        else 
            return j+1;
    }
}

void quickSortHoare(int *tab, int start, int stop)
{
	int i;
    if(!(stop-start<2))
	{
		i=hoarePartition(tab,start,stop);
		quickSortHoare(tab,start,i);
		quickSortHoare(tab,i,stop);
	}
}

int main()
{
	//czesc 1 - algorytm quicksort
	cout<<"QuickSort"<<endl;
	srand(time(NULL));
	int size1=6;
	int *tab1=new int[size1];
	for(int i=0;i<size1;i++)
	{
		tab1[i]=rand()%50;
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	quickSort(tab1, 0, size1-1); //od 0 bo taka jest konwencja numerowania indeksow w C++
	
	for(int i=0;i<size1;i++)
	{
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	delete[] tab1;


	//czesc 2 - algorytm randomized-quicksort
	cout<<"\nRandomizedQuickSort"<<endl;
	int size2=6;
	int *tab2=new int[size2];
	for(int i=0;i<size2;i++)
	{
		tab2[i]=rand()%50;
		cout<<tab2[i]<<", ";
	}
	cout<<endl;
	quickSortRandomized(tab2, 0, size2-1); //od 0 bo taka jest konwencja numerowania indeksow w C++
	
	for(int i=0;i<size2;i++)
	{
		cout<<tab2[i]<<", ";
	}
	cout<<endl;
	delete[] tab2;

	//czesc 3 - porownanie powyzszych algorytmow dla danych uporzadkowanych roznaco i malejaca
	cout<<"\nPorownanie QuickSort i RandomizedQuickSort"<<endl;
	int size3=30000;
	int *sortedDonor = new int[size3];
	int randomInt;
	int watcher;
	sortedDonor[0]=0;
	int realSize;
	for(realSize=1;realSize<size3;realSize++)
	{
		watcher=1000;
		while(watcher--) //tak dlugo szukamy wiekszej az znajdziemy, ale nie wiecej niz 1000 razy
		{
			randomInt=rand();
			if(randomInt>sortedDonor[realSize-1])
			{
				sortedDonor[realSize]=randomInt;
				break; //wychodzimy z while
			}
		}
	}
	
	int *sorted = new int[realSize];
	int *reversedSorted = new int[realSize];

	for(int i=0;i<realSize;i++)
	{
		sorted[i]=sortedDonor[i];
		reversedSorted[i]=sortedDonor[realSize-1-i];
	}

	//pomiary quicksort:
	clock_t tStart = clock();
	quickSort(sorted, 0, realSize-1);
	cout<<"Quicksort na danych posortowanych rosnaco: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;
	tStart = clock();
	quickSort(reversedSorted, 0, realSize-1);
	cout<<"Quicksort na danych posortowanych malejaco: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	//przywracanie danych z sortedDonor - badamy te same dane
	for(int i=0;i<realSize;i++)
	{
		sorted[i]=sortedDonor[i];
		reversedSorted[i]=sortedDonor[realSize-1-i];
	}

	//pomiary randomized-quicksort:
	tStart = clock();
	quickSortRandomized(sorted, 0, realSize-1);
	cout<<"RandomizedQuicksort na danych posortowanych rosnaco: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;
	tStart = clock();
	quickSortRandomized(reversedSorted, 0, realSize-1);
	cout<<"RandomizedQuicksort na danych posortowanych malejaco: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	delete sorted;
	delete reversedSorted;
	delete sortedDonor;


	//czesc 4 - algorytm hoare-quicksort
	cout<<"\nQuickSortHoare"<<endl;
	int size4=6;
	int *tab4=new int[size4];
	for(int i=0;i<size4;i++)
	{
		tab4[i]=rand()%50;
		cout<<tab4[i]<<", ";
	}
	cout<<endl;
	quickSortHoare(tab4, 0, size4); //od 0 bo taka jest konwencja numerowania indeksow w C++
	
	for(int i=0;i<size4;i++)
	{
		cout<<tab4[i]<<", ";
	}
	cout<<endl;
	delete[] tab4;


	//czesc5  - test 3 algorytmow na nieposortowanym zbiorze
	cout<<"\nTest 3 algorytmow"<<endl;

	int size5=30000;
	int *donor = new int[size5];
	int randomInt2;
	donor[0]=0;
	int realSize2;
	for(realSize2=1;realSize2<size5;realSize2++)
	{
		donor[realSize2]=rand();
	}
	

	int *quickSortTest = new int[realSize2];
	int *quickSortRandomizedTest = new int[realSize2];
	int *quickSortHoareTest = new int[realSize2];

	for(int i=0;i<realSize2;i++)
	{
		quickSortTest[i]=quickSortRandomizedTest[i]=quickSortHoareTest[i]=donor[i];
	}

	//pomiary quicksort:
	tStart = clock();
	quickSort(quickSortTest, 0, realSize2-1);
	cout<<"Quicksort na danych losowych: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	//pomiary randomized-quicksort:
	tStart = clock();
	quickSortRandomized(quickSortRandomizedTest, 0, realSize2-1);
	cout<<"RandomizedQuicksort na danych losowych: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	//pomiary quicksortHoare:
	tStart = clock();
	quickSortHoare(quickSortHoareTest, 0, realSize2-1);
	cout<<"QuicksortHoare na danych losowych: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	delete donor;
	delete quickSortTest;
	delete quickSortRandomizedTest;
	delete quickSortHoareTest;

	return 0;
}