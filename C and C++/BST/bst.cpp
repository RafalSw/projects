#include <iostream>

using namespace std;

//deklaracja struktuy Node
struct Node
{
	int Value;
	Node* Left;
	Node* Right;
};

//definicje funkcji
int FindMedian(int pTab[], int pFrom, int pTo, int &pPos) //pPos to referencja, bo jej wartosc jest uzywana w CreateTree, mimo braku inicjacji
{
	pPos = (pTo - pFrom) / 2 + pFrom; //mediana to srodkowa wartosc z zakresu, wiec trzeba przesunac o poczatek zakresu
	return pTab[pPos];
}

void CreateTree(Node* pRoot, int pTab[], int pFrom, int pTo)
{
	int pPos;
	pRoot->Value = FindMedian(pTab, pFrom, pTo, pPos);
	if (pFrom > pPos - 1)
	{
		pRoot->Left = NULL;
	}
	else
	{
		pRoot->Left = new Node();
		CreateTree(pRoot->Left, pTab, pFrom, pPos - 1);
	}

	if (pPos + 1 > pTo) {
		pRoot->Right = NULL;
	}
	else
	{
		pRoot->Right = new Node();
		CreateTree(pRoot->Right, pTab, pPos + 1, pTo);
	}
}

//definicja minNode, niezbedne do usuwania wezlow z dwoma potomkami
Node* minNode(Node *node)
{
	Node *temp = node;

	while (temp->Left!=NULL)
	{
		temp = temp->Left;
	}
	return temp;
}


//deklaracja struktury BST
struct BST
{
	Node* root;
	void Create(int pValues[], int size)  //funkcja Create MUSI pobierac rozmiar tablicy, alternatywa jest zdefiniowanie np. przez #define N 10 i wtedy uzycie CreateTree(root, pValues, 0, N); ale wtedy rozmiar tablicy jest z gory ustalony
	{
		CreateTree(root, pValues, 0, size);
	}

	void Insert(int pValue)
	{
		Node *temp = root;
		Node *prev = NULL;

		//poszukiwanie wlasciwego miejsca do wstawienia wezla
		while (temp != NULL)
		{
			if (temp->Value == pValue)
			{
				return; //istnieje juz taka wartosc w drzewie
			}
			prev = temp;
			if (temp->Value < pValue)
				temp = temp->Right;
			else
				temp = temp->Left;
		}

		//jesli taka wartosc nie istnieje, to tworzymy nowy wezel
		Node* newNode = new Node();
		newNode->Value = pValue;
		newNode->Left = newNode->Right = NULL;


		//dolaczenie nowego wezla do drzewa
		if (prev == NULL)
			root = newNode;
		else if (newNode->Value < prev->Value)
			prev->Left = newNode;
		else
			prev->Right = newNode;
	}

	bool Find(int pValue)
	{
		//poszukiwanie warosci poprzez porownanie, a nastepnie przejscie do kolejnych wezlow
		Node* temp = root;
		while (temp != NULL)
		{
			if (temp->Value == pValue)
				return true;
			if (temp->Value < pValue)
				temp = temp->Right;
			else
				temp = temp->Left;
		}
		return false;
	}

	bool Remove(int pValue)
	{
		if (Find(pValue) == false)
			return false; //nie ma czego usuwac

		Node* temp = root;
		Node *prev = NULL; //rodzic elementu usuwanego

		//poszukiwanie usuwanego elementu
		while (temp != NULL)
		{
			if (temp->Value == pValue)
			{
				break;//temp wskazuje na element, ktory chcemy usunac
			}
			prev = temp;
			if (temp->Value < pValue)
				temp = temp->Right;
			else
				temp = temp->Left;
		}

		if (temp->Left != NULL && temp->Right != NULL)//opcja 1: usuwany element ma oba poddrzewa
		{
			Node* node = minNode(temp->Right);
			if (node->Right != NULL) //zamieniany wezel ma potomka, ktorego trzeba przewiazac
			{
				Node* temp2 = temp->Right , *prev2=temp;
				while (temp2 != node)
				{
					prev2 = temp2;
					temp2 = temp2->Left;
				}
				if (temp->Right == node)
				{
					temp->Right = node->Right;
				}
				else
				{
					prev2->Left = node->Right;
				}
			}
			//skoro potomek jest juz przewiazany, to przepisujemy wartosc i kasujemy zbedny juz wezel
			temp->Value = node->Value;
			delete node;
			return false;
		}
		else if (temp->Left == NULL && temp->Right == NULL)//opcja 2: usuwamy lisc
		{
			if (prev != NULL) //jesli istnieje rodzic, to modyfikujemy jego potomka
			{
				if (prev->Left == temp)
					prev->Left = NULL;
				else if (prev->Right == temp)
					prev->Right = NULL;
			}
			delete temp;
			return true;
		}
		else//opcja 3: istnieje tylko jedno poddrzewo
		{
			Node* subtree = NULL;
			if (temp->Left == NULL && temp->Right != NULL)
				subtree = temp->Right;
			else if (temp->Left != NULL && temp->Right == NULL)
				subtree = temp->Left;

			if (subtree != NULL) //jesli istnieje poddrzewo
			{
				if (prev->Left == temp) //przepinamy poddrzewo do lewego potomka obecnego rodzica
					prev->Left = subtree;
				else if (prev->Right == temp)//przepinamy poddrzewo do prawego potomka obecnego rodzica
					prev->Right = subtree;
			}
			delete temp;
			return true;
		}
	}

};


int main()
{
	BST bst;
	bst.root = new Node();
	int tab[] = { 1, 4, 7, 8, 11, 12, 15, 16, 89, 92, 105 };
	bst.Create(tab, (sizeof(tab) / sizeof(*tab))-1);

	int testValue1 = 8;
	cout << "Czy w drzewie jest wartosc " << testValue1 << "? " << bst.Find(testValue1) << endl;
	int testValue2 = 9;
	cout << "Czy w drzewie jest wartosc " << testValue2 << "? " << bst.Find(testValue2) << endl;

	bst.Insert(testValue1);
	cout << "Dodanie wartosci " << testValue2 << endl;
	bst.Insert(testValue2);
	cout << "Czy w drzewie jest wartosc " << testValue2 << "? " << bst.Find(testValue2) << endl;

	cout << "Usuniecie wartosci " << testValue2 << endl;
	bst.Remove(testValue2);
	cout << "Czy w drzewie jest wartosc " << testValue2 << "? " << bst.Find(testValue2) << endl;

	int testValue3=12;
	cout << "Usuniecie wartosci " << testValue3 << endl;
	bst.Remove(testValue3);

	int testValue4 = 7;
	cout << "Usuniecie wartosci " << testValue4 << endl;
	bst.Remove(testValue4);

	return 0;
}