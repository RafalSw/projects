#include <iostream>
#include <string>
#include <fstream>
#include <vector>

using namespace std;

const string level0 = "\n\n\n\n\n\n\n==============";
const string level1 = " |\n |\n |\n |\n |\n |\n |\n==============";
const string level2 = " |----------\n |\n |\n |\n |\n |\n |\n==============";
const string level3 = " |----------\n |        |\n |\n |\n |\n |\n |\n==============";
const string level4 = " |----------\n |        |\n |        0\n |\n |\n |\n |\n==============";
const string level5 = " |----------\n |        |\n |        0\n |        |\n |\n |\n |\n==============";
const string level6 = " |----------\n |        |\n |        0\n |        |\n |       /\n |\n |\n==============";
const string level7 = " |----------\n |        |\n |        0\n |        |\n |       / \\\n |\n |\n==============";
const string level8 = " |----------\n |        |\n |        0\n |        |\n |       /|\\\n |\n |\n==============";
const string level9 = " |----------\n |        |\n |        0\n |        |\n |       /|\\\n |       /\n |\n==============";
const string level10 = " |----------\n |        |\n |        0\n |        |\n |       /|\\\n |       / \\\n |\n==============";

/* funkcja wczytujaca slowa z pliku do wektora */
bool loadWords(const string fileName, vector<string> &words)
{
	ifstream input;
	input.open(fileName, ios::in); //otwieramy plik do odczytu

	if(input.is_open()) //jesli udalo sie otworzyc
	{
		string line;
		while (getline(input,line)) //wczytujemy plik linijka po linijce, az do konca pliku
		{
			words.push_back(line); //dodajemy wczytane linijki (slowa) do wektora slow
		}
		input.close(); //zamykamy plik
		return true;
	}
	else //nie udalo sie otworzyc pliku
	{
		cout<<"Nie udalo sie otworzyc pliku "<<fileName<<endl;
		return false;
	}
}

/* funkcja rysujaca wisielca */
void draw(int i)
{
	cout<<endl;
	switch(i)
	{
	case 0:
		cout<<level0<<endl;
		break;
	case 1:
		cout<<level1<<endl;
		break;
	case 2:
		cout<<level2<<endl;
		break;
	case 3:
		cout<<level3<<endl;
		break;
	case 4:
		cout<<level4<<endl;
		break;
	case 5:
		cout<<level5<<endl;
		break;
	case 6:
		cout<<level6<<endl;
		break;
	case 7:
		cout<<level7<<endl;
		break;
	case 8:
		cout<<level8<<endl;
		break;
	case 9:
		cout<<level9<<endl;
		break;
	case 10:
		cout<<level10<<endl;
		break;
	default:
		break;
	}
	cout<<endl;
}

/* rozgrywka dla pojedynczego s�owa z zadan� ilo�ci� podej�� */
void simplePlay(string word, int numberOfTrials)
{
	// przygotowywanie maski - chcemy mie� _____ zamiast "test"
	string mask = "";
	for(int i=0; i<word.size(); i++)
		mask+="_";

	//gra rozgrywa si� a� do wykorzystania wszystkich szans lub odgadniecia slowa
	while(numberOfTrials>0)
	{
		char c; //wybrana przez uzytkownika litera
		draw(10-numberOfTrials);
		cout<<mask<<" Liczba pozostalych strzalow: "<<numberOfTrials<<endl;
		cout<<"Podaj litere: ";
		cin>>c;

		c = tolower(c); //zmiana litery na mala, niezaleznie od wprowadzenia

		bool missed = true; //zakladamy, ze strzal byl chybiony
		for(int i=0; i<word.size(); i++) //sprawdzamy kazda litere ze slowa
		{
			if(tolower(word.at(i))==c) //jesli wybrana litera pojawia sie w slowie (tolower znieczula na wielkie/male litery)
			{
				missed = false; //to znaczy, ze strzal nie byl chybiony
				mask.at(i) = word.at(i); //a trafiona litera pokazuje sie w masce
			}
		}
		if(missed == true) //jesli nie trafilismy, wypisujemy powiadomienie i zmniejszamy liczbe prob
		{
			cout<<"Nie ma takiej litery w odkrywanym slowie!"<<endl;
			numberOfTrials--;
		}
		else //jesli trafilismy, to tez wypisujemy powiadomiwnie
		{
			cout<<"Brawo! Litera \""<<c<<"\" to dobry wybor!"<<endl;
		}

		if(mask == word) //jesli slowo zostalo calkowicie odkryte (maska jest taka sama jak slowo), to wychodzimy z petli rozgrywki
		{
			break;
		}
	}

	if(numberOfTrials > 0) //nastapilo wyjscie z petli, a liczba pozostalych szans jest wieksza od 0 - wygrana
	{
		cout<<"Brawo! Wygrales!"<<endl;
	}
	else //przegrana
	{
		draw(10);
		cout<<"Niestety, przegrales!"<<endl;
	}

}

string wordLotery(vector<string> &words)
{
	
	int i = rand()%words.size();
	return words.at(i); // zwrocenie i-tego slowa z wektora
}

int main()
{
	const int numberOfTrials = 10; //liczba pr�b
	const string fileName = "data.txt"; //nazwa pliku
	vector<string> words; // tu beda przechowywane slowa wczytane z pliku

	if(loadWords(fileName, words) == false) //wywolanie funkcji wczytujacej, wektor przekazany jako referencja
	{
		//nie udalo sie wczytac pliku. wyjscie z gry
		return -1;
	}


	char c;
	do
	{
		simplePlay(wordLotery(words), numberOfTrials);
		cout<<"Czy chcesz zagrac ponownie? [T] by zagrac ponownie ";
		cin>>c;
	}
	while(c=='T');
	
	return 0;
}