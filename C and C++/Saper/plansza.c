#include <time.h>
#include <stdlib.h>

typedef struct
{
	int bomba;
	int stan; /* 0- zakryta, 1 - oznaczona 2 - odkryta*/
	int wokol;
}Pole;

void wypelnijPlansze(Pole** tab, int a, int b, int liczbaMin) /*funkcja wypelnia losowo plansze bombami*/
{
	int i, j;
	/* inicjowanie planszy pustymi polami bez min*/
	for(i=0;i<a;i++)
	{
		for(j=0;j<b;j++)
		{
			Pole p;
			p.bomba=0;
			p.stan=0;
			p.wokol=0;
			tab[i][j]=p;
		}
	}

	/* inicjowanie wybranej liczby min */
	srand(time(NULL));
	while(liczbaMin>0)
	{
		/* losowanie p�l */
		int x,y;
		int k,l;
		x=rand()%a;
		y=rand()%b;

		if(tab[x][y].bomba==0)
		{
			/* udalo sie znalezc wolne miejsce - wstawianie bomby. Do wstawienia zostala jedna mniej */
			tab[x][y].bomba=1;
			liczbaMin--;

			
			/* zwiekszanie licznika sasiednich bomb w sasiednich polach */
			
			for(k=x-1; k<=x+1; k++)
			{
				for(l=y-1; l<=y+1; l++)
				{
					/*sprawdzenie czy nie wyjdzie poza tablice */
					if(k>=0 && l>=0 &&k<a && l<b)
					{
						tab[k][l].wokol++;
					}
				}
			}
		}
	}
}

void wypelnijMaske(char** tab, int a, int b)
{
	int i, j;

	for(i=0;i<a;i++)
	{
		for(j=0;j<b;j++)
		{
			tab[i][j]='X';
		}
	}
}

void odkryjPlansze(Pole** plansza, char** maskaPlanszy, int a, int b)
{
	int i, j;
	for(i=0; i<a; i++)
	{
		for(j=0; j<b; j++)
		{
			if(plansza[i][j].bomba==1)
			{
				maskaPlanszy[i][j]='*';
			}
			else
			{
				if(plansza[i][j].wokol == 0)
				{
					maskaPlanszy[i][j]=' ';
				}
				else
				{
					maskaPlanszy[i][j]=(char)((int)'0'+plansza[i][j].wokol); /* zamiana int na char dla pojedynczej cyfry */
				}
			}
		}
	}
}

void odkrywaniePustki(Pole** plansza, char** maskaPlanszy, int a, int b, int x, int y)
{
	int k, l;
	/* dla kazdego z sasiednich elementow */
	for(k=x-1; k<=x+1; k++)
	{
		for(l=y-1; l<=y+1; l++)
		{
			/*sprawdzenie czy nie wyjdzie poza tablice */
			if(k>=0 && l>=0 && k<a && l<b && maskaPlanszy[k][l]=='X')
			{
						
				if(plansza[k][l].wokol == 0)
				{
					maskaPlanszy[k][l]=' ';
					odkrywaniePustki(plansza, maskaPlanszy, a, b, k, l);
				}
				else
				{
					maskaPlanszy[k][l]=(char)((int)'0'+plansza[k][l].wokol); /* zamiana int na char dla pojedynczej cyfry */
				}
			}
		}
	}
}

int sprawdzPlansze(Pole** plansza, char** maskaPlanszy, int a, int b) /* zwraca 1 jesli plansza juz wygrana, -1 w p.p. */
{
	int i,j;
	for(i=0;i<a;i++)
	{
		for(j=0;j<b;j++)
		{
			if(plansza[i][j].bomba==0 && (maskaPlanszy[i][j]=='X' || maskaPlanszy[i][j]=='F')) /* sprawdzenie czy wszystkie pola, gdzie nie ma bomby sa odkryte */
				return -1;
		}
	}
	return 1;
}
