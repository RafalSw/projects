#define _CRT_SECURE_NO_WARNINGS /* wymagane w VisualStudio */
#include <stdio.h>
#include <string.h>

char* nazwaPliku()
{
	return "wyniki.txt";
}

void dodajWynik(char* imie, int czas)
{
	int wyniki[6];
	char imiona[6][70];
	char wczytaneImie[70];
	int iloscWczytanych=0;
	char linia[80];
	int i,j;

	FILE* zapis;
	FILE* odczyt = fopen(nazwaPliku(),"r");
	if(odczyt==NULL)
	{
		printf("Brak pliku do wczytania wynikow - tworze nowy.");
	}
	else
	{
		while((fgets(linia, 80, odczyt) != NULL)&& strlen(linia)>1) /*pobieranie linijki z pliku i sprawdzamy czy nie jest pusty*/
		{
			sscanf(linia,"%s\t%d",wczytaneImie,&wyniki[iloscWczytanych]); /*przeksztalcanie pobranej linijki na imie i czas*/
			strcpy(imiona[iloscWczytanych],wczytaneImie);
			iloscWczytanych++;

			if(iloscWczytanych==5) /* wczytujemy do 5 wynikow */
				break;
		}
		fclose(odczyt);
	}
	

	/* dodawanie wyniku do tablic */
	strcpy(imiona[iloscWczytanych],imie);
	wyniki[iloscWczytanych]=czas;
	iloscWczytanych++;

	/* sortowanie babelkowe */
	for(i=0;i<iloscWczytanych-1;i++)
	{
		for(j=0;j<iloscWczytanych-1;j++)
		{
			if(wyniki[j]>wyniki[j+1]) /*trzeba zamienic miejscami - najmniejsze na poczatku*/
			{
				char tempImie[80];
				int temp;
				strcpy(tempImie,imiona[j]);
				temp=wyniki[j];

				strcpy(imiona[j],imiona[j+1]);
				wyniki[j]=wyniki[j+1];

				strcpy(imiona[j+1],tempImie);
				wyniki[j+1]=temp;
			}
		}
	}

	zapis = fopen(nazwaPliku(),"w");
	if(zapis==NULL)
	{
		printf("Brak pliku do wczytania wynikow - tworze nowy.");
	}
	else
	{
		for(i=0;i<iloscWczytanych;i++) /*zapisujemy 5 najlepszych wynikow */
		{
			fprintf(zapis,"%s\t%d\n", imiona[i], wyniki[i]);
		}
		fclose(zapis);
	}
	
}

void wyswietlWyniki()
{
	int wyniki[6];
	char imiona[6][70];
	int iloscWczytanych=0;
	char linia[80];
	char wczytaneImie[70];
	int i;

	FILE* odczyt = fopen(nazwaPliku(),"r");
	if(odczyt==NULL)
	{
		printf("Brak pliku do wczytania wynikow.");
		return;
	}
	else
	{
		while((fgets(linia, 80, odczyt) != NULL)&& strlen(linia)>1) /*pobieranie linijki z pliku i sprawdzamy czy nie jest pusty*/
		{
			sscanf(linia,"%s\t%d",wczytaneImie,&wyniki[iloscWczytanych]); /*przeksztalcanie pobranej linijki na imie i czas*/
			strcpy(imiona[iloscWczytanych],wczytaneImie);
			iloscWczytanych++;

			if(iloscWczytanych==5) /* wczytujemy do 5 wynikow */
				break;
		}
		fclose(odczyt);
	}
	

	printf("Lista wynikow:\n");
	for(i=0;i<iloscWczytanych;i++)
	{
		printf("%d. %s\t%d\n", i+1, imiona[i], wyniki[i]);
	}
}
