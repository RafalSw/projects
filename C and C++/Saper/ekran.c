#include <stdio.h>
#include <stdlib.h>

void czysc()
{
#ifdef linux
	system ("clear");
#else
    system("cls");
#endif
}

void drukuj(char** tab, int a, int b)
{
	int i, j;
	for(i=-2;i<a;i++)
	{
		if(i>=0)
		{
			printf("%2i|",i);
		}
		else if(i==-2)
		{
			printf("  |");
		}
		else if(i==-1)
		{
			printf("__|");
		}

		for(j=0;j<b;j++)
		{
			if(i==-2)
			{
				printf(" %-2i|", j);
			}
			else if(i==-1)
			{
				printf("___|");
			}
			else
			{
				printf(" %c |", tab[i][j]);
			}
		}
		printf("\n");
	}
}
