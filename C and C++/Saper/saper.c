#define _CRT_SECURE_NO_WARNINGS /* dla wygody program pisany w Visual Studio i sprawdzany na linuxie */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#if defined(_WIN32) || defined(_WIN64)
	#include <windows.h>
#else
	#include <unistd.h>
#endif

#define MAX 50
#define MIN 5

typedef struct
{
	int bomba;
	int stan; /* 0- zakryta, 1 - oznaczona 2 - odkryta*/
	int wokol;
}Pole;

/* funkcje w ekran.c */
void czysc();
void drukuj(char** tab, int a, int b);

/* funkcje w plansza.c */
void wypelnijMaske(char** tab, int a, int b);
void odkryjPlansze(Pole** plansza, char** maskaPlanszy, int a, int b);
void wypelnijPlansze(Pole** tab, int a, int b, int liczbaMin);
void odkrywaniePustki(Pole** plansza, char** maskaPlanszy, int a, int b, int x, int y);
int sprawdzPlansze(Pole** plansza, char** maskaPlanszy, int a, int b);

/* funkcje w wyniki.c */
void dodajWynik(char* imie, int czas);
void wyswietlWyniki();

int rozgrywka(Pole** plansza, char** maskaPlanszy, int a, int b) //funkcja zwraca 1 jestli sukces, 0 jesli porazka 
{
	int x,y;
	char c;
	
	while(sprawdzPlansze(plansza, maskaPlanszy, a, b)==-1)
	{
		czysc();
		drukuj(maskaPlanszy, a, b);
		printf("Podaj wspolrzedne pola i decyzje ( x y F lub x y S )\nF-flaga, S-sprawdznie, Q-wyjscie\n> ");
		scanf(" %d %d %c",&y,&x,&c);
		if(x<0 || x>=a || y<0 || y>=b)
		{
			printf("Wspolrzednie nie sa poprawne!");
		}
		else
		{
			if(c=='F' && maskaPlanszy[x][y]=='X')
			{
				maskaPlanszy[x][y]='F';
			}
			else if(c=='F' && maskaPlanszy[x][y]=='F')
			{
				maskaPlanszy[x][y]='X';
			}
			else if(c=='S')
			{
				if(plansza[x][y].bomba==0)
				{
					if(plansza[x][y].wokol==0)
					{
						odkrywaniePustki(plansza, maskaPlanszy, a, b, x, y);
					}
					else
					{
						maskaPlanszy[x][y]=(char)((int)'0'+plansza[x][y].wokol);  /* zamiana int na char dla pojedynczej cyfry */
					}
				}
				else
				{
					odkryjPlansze(plansza, maskaPlanszy, a, b);
					czysc();
					drukuj(maskaPlanszy, a, b);
					return 0;
				}
			}
			else if(c=='Q')
			{
				return 0;
			}
		}
	}
	odkryjPlansze(plansza, maskaPlanszy, a, b);
	czysc();
	drukuj(maskaPlanszy, a, b);
	return 1;
}

int main ()
{
	int x,y;
	int i;
	int a, b, miny;
	char wybor='N';
	Pole **plansza; //wskaznik na tablice dwuwymiarowa
	int maxminy, minminy;
	char **maskaPlanszy = NULL;
	char imie[70];
	time_t start,stop;
	int czas;

	printf ("GRA SAPER\nPODAJ LICZBE WIERSZY: ");
	
	/* petla nie potrzebna - w razie niepoprawnej wartosci i tak przyjmujemy MAX lub MIN
	/*do
	{*/
	scanf (" %d", &a); /* spacja przed %d zabezpiecza przed pozostalosciami w buforze wejsciowym aplikacji*/
	if (a>MAX)
	{
		printf ("ZA DUZO WIERSZY. PRZYJMUJE %d\n", MAX);
		a=MAX;
	}
	else if(a<MIN)
	{
		printf ("ZA MALO WIERSZY. PRZYJMUJE %d\n", MIN);
		a=MIN;
	}
	/*}
	while (a<MIN || a>MAX);*/

	printf ("PODAJ LICZBE KOLUMN: ");
	/*do
	{*/
	scanf (" %d", &b);
	if (b>MAX)
	{
		printf ("ZA DUZO KOLUMN. PRZYJMUJE %d\n", MAX);
		b=MAX;
	}
	else if (b<MIN)
	{
		printf ("ZA MALO KOLUMN. PRZYJMUJE %d\n", MIN);
		b=MIN;
	}
	/*}
	while (b<MIN>MAX);(*/

	maxminy = (0.9*(a*b));
	minminy = (0.1*(a*b));

	printf ("PODAJ LICZBE MIN: ");
	/*do
	{*/
		scanf ("%d", &miny);
		if (miny>maxminy)
		{
			printf ("ZA DUZO MIN. PRZYJMUJE %d\n", maxminy);
#if defined(_WIN32) || defined(_WIN64)
			Sleep(3000);
#elif defined(__UNIX__)
			usleep(3000000);
#endif
			miny=maxminy;
		}
		else if (miny<minminy)
		{
			printf ("ZA MALO MIN. PRZYJMUJE %d\n", minminy);
#if defined(_WIN32) || defined(_WIN64)
			Sleep(3000);
#elif defined(__UNIX__)
			usleep(3000000);
#endif
			miny=minminy;
		}
	/*}
	while (miny<MIN>maxminy);*/

	/*inicjowanie planszy i maskiPlanszy*/
	plansza = (Pole**)malloc(a * sizeof(Pole*));
	for (i = 0; i < a; i++) {
	  plansza[i] = (Pole*)malloc(b * sizeof(Pole));
	}

	maskaPlanszy = (char**)malloc(a * sizeof(char*));
	for (i = 0; i < a; i++) {
	  maskaPlanszy[i] = (char*)malloc(b * sizeof(char));
	}

	wypelnijPlansze(plansza, a, b, miny);
	wypelnijMaske(maskaPlanszy, a, b);


	/* rozgrywka */

	start = time(NULL);
	if(rozgrywka(plansza, maskaPlanszy, a, b) == 1)
	{
		stop = time(NULL);
		czas = stop-start;
		printf("\nGratulacje! :)\nTwoj czas: %d\nPodaj imie: ", czas);
		scanf(" %s",imie);

		printf("\n");
		dodajWynik(imie, czas);
		wyswietlWyniki();
	}
	else
	{
		printf("Game over! :(\n");
	}



	/*usuwanie planszy i maskiPlanszy*/
	for (i = 0; i < a; i++) {
	  free(plansza[i]);
	  free(maskaPlanszy[i]);
	}
	free(plansza);
	free(maskaPlanszy);
	
	return 0;
}
