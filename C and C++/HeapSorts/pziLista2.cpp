#include <iostream>
#include <ctime>

using namespace std;

void swap(int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}

void heapify(int *tab, int l, int r)
{
	int x;
	int j, k;
	j=l;
	k=2*j;
	x=tab[j];

	while (k<r)
	{
		if (k+1<=r)
		{
			if (tab[k]<tab[k+1])
				k+=1;
		}
		if (x<tab[k])
		{
			tab[j]=tab[k];
			j=k;
			k=2*j;
		}
		else k=r+1;
	}
	tab[j]=x;
}

void heapify3(int *tab, int l, int r)
{
	int x;
	int j, k;
	j=l;
	k=3*j;
	x=tab[j];

	while (k<r)
	{
		int add=0;
		if(k+2<=r && tab[k]<tab[k+2] && tab[k+1]<tab[k+2])
		{
			k+=2;
			add=2;
		}
		else if (k+1<=r && tab[k]<tab[k+1])
		{
			k+=1;
			add=1;
		}

		if (x<tab[k])
		{
			tab[j]=tab[k];
			j=k;
			k=3*j;
		}
		else k=r+add;
	}
	tab[j]=x;
}

void heapifyPrim(int *tab, int l, int r, int heapSize)
{
	int x;
	int j, k;
	j=l;
	k=max(2,heapSize)*j;
	x=tab[j];

	while (k<r)
	{
		int add=0;
		for(int i=heapSize; i>0;i--)
		{
			if(k+i<=r)
			{
				bool shouldAdd=true;
				for(int z = 0; z<i;z++)
				{
					if(!(tab[k+z]<tab[k+i]))
					{
						shouldAdd=false;
					}
				}
				if(shouldAdd)
				{
					k+=i;
					add=i;
					break;
				}
			}
		}

		if (x<tab[k])
		{
			tab[j]=tab[k];
			j=k;
			k=max(2,heapSize)*j;
		}
		else
			k=r+add;
	}
	tab[j]=x;
}

void buildHeap(int *tab, int size)
{
	for(int i = size - 1;i>=0;i--)
	{
		heapify(tab, i, size);
	}
}

void heapSort(int *tab, int size)
{
	buildHeap(tab, size);
	for(int i = size-1; i >= 0; i--)
	{
		swap(&tab[0],&tab[i]);
		heapify(tab, 0, i-1);
	}
}


void buildHeap3(int *tab, int size)
{
	for(int i = size - 1;i>=0;i--)
	{
		heapify3(tab, i, size);
	}
}

void heapSort3(int *tab, int size)
{
	buildHeap3(tab, size);
	for(int i = size-1; i >= 0; i--)
	{
		swap(&tab[0],&tab[i]);
		heapify3(tab, 0, i-1);
	}
}

void buildHeapPrim(int *tab, int size, int heapSize)
{
	for(int i = size - 1;i>=0;i--)
	{
		heapifyPrim(tab, i, size, heapSize);
	}
}

void heapSortPrim(int *tab, int size, int heapSize)
{
	buildHeapPrim(tab, size, heapSize);
	for(int i = size-1; i >= 0; i--)
	{
		swap(&tab[0],&tab[i]);
		heapifyPrim(tab, 0, i-1, heapSize);
	}
}

int main()
{
	//czesc 1 - algorytm heapsort
	cout<<"HeapSort"<<endl;
	srand(time(NULL));
	int size1=10;
	int *tab1=new int[size1];
	for(int i=0;i<size1;i++)
	{
		tab1[i]=rand()%50;
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	heapSort(tab1, size1);
	
	for(int i=0;i<size1;i++)
	{
		cout<<tab1[i]<<", ";
	}
	cout<<endl;
	delete[] tab1;

	//czesc 2 - algorytm heapsort3
	cout<<"\nHeapSort3"<<endl;
	int size2=10;
	int *tab2=new int[size2];
	for(int i=0;i<size2;i++)
	{
		tab2[i]=rand()%50;
		cout<<tab2[i]<<", ";
	}
	cout<<endl;
	heapSort3(tab2, size2);
	for(int i=0;i<size2;i++)
	{
		cout<<tab2[i]<<", ";
	}
	cout<<endl;
	delete[] tab2;


	//czesc3  - test 2 algorytmow na nieposortowanym zbiorze
	cout<<"\nTest 2 algorytmow - HeapSort i HeapSort3"<<endl;

	int size5=60000;
	int *donor = new int[size5];
	int randomInt;
	donor[0]=0;
	int realSize;
	for(realSize=1;realSize<size5;realSize++)
	{
		donor[realSize]=rand();
	}
	

	int *heapsortTab = new int[realSize];
	int *heapsort3Tab = new int[realSize];

	for(int i=0;i<realSize;i++)
	{
		heapsortTab[i]=heapsort3Tab[i]=donor[i];
	}

	//pomiary heapsort:
	clock_t tStart = clock();
	heapSort(heapsortTab, realSize);
	cout<<"HeapSort na danych losowych: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	//pomiary heapsort3:
	tStart = clock();
	heapSort3(heapsort3Tab, realSize);
	cout<<"HeapSort3 na danych losowych: "<<(double)(clock() - tStart)/CLOCKS_PER_SEC<<"s"<<endl;

	delete donor;
	delete heapsortTab;
	delete heapsort3Tab;


	//czesc 4 - algorytm heapsortPrim
	cout<<"\nHeapSortPrim"<<endl;
	int size3=10;
	int *tab3=new int[size3];
	for(int i=0;i<size3;i++)
	{
		tab3[i]=rand()%50;
		cout<<tab3[i]<<", ";
	}
	cout<<endl;
	heapSortPrim(tab3, size3,1);
	
	for(int i=0;i<size3;i++)
	{
		cout<<tab3[i]<<", ";
	}
	cout<<endl;
	delete[] tab3;

	//czesc 5 - algorytm buildheap1
	cout<<"\nHeapSortPrim"<<endl;
	int size4=10;
	int *tab4=new int[size4];
	for(int i=0;i<size4;i++)
	{
		tab4[i]=rand()%50;
		cout<<tab4[i]<<", ";
	}
	cout<<endl;
	buildHeap1(tab4, size4);
	
	for(int i=0;i<size4;i++)
	{
		cout<<tab4[i]<<", ";
	}
	cout<<endl;
	delete[] tab4;

	return 0;
}