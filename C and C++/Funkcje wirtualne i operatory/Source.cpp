#include <iostream>

using namespace std;

class A
{
public:
	virtual void funkcja()
	{
		cout << "A" << endl;
	}
};

class B: public A
{
public:
	void funkcja()
	{
		cout << "B" << endl;
	}
};

class Wartosc
{
public:
	int i;
	Wartosc operator+(int x)
	{
		Wartosc wynik;
		wynik.i = i + x;
		return wynik;
	}
};

Wartosc operator-(int x, const Wartosc& w)
{
	Wartosc wynik;
	wynik.i = x - w.i;
	return wynik;
}

int main()
{
	A* a = new A();
	B* b = new B();

	A* aBis = b;

	a->funkcja();
	b->funkcja();
	aBis->funkcja();


	Wartosc x;
	x.i = 3;
	Wartosc x2 = x + 3;

	Wartosc x3 = 2 - x;

	
	system("PAUSE");
}